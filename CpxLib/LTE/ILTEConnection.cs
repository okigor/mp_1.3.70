﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.LTE
{
    public interface ILTEConnection
    { 
        LTEStatus GetStatus();
    }
}
