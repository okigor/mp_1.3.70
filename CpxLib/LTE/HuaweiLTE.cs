﻿using CpxLib.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace CpxLib.LTE
{
    public class HuaweiLTE : ILTEConnection
    {
        private string _sessionId;
        private string _token;
        private string _address;

        public HuaweiLTE()
        {
            var config = AppSettings.LoadConfiguration();
            _address = config.LTEAddress?.Replace(" ", "");
        }

        public bool LoadToken()
        {
            try
            {
                var result = getLTERequest($@"http://{_address}/api/webserver/SesTokInfo");
                var xmlResult = XDocument.Parse(result);
                _sessionId = xmlResult.Descendants("SesInfo").Select(x => x.Value.Replace("SessionID=", "")).FirstOrDefault();
                _token = xmlResult.Descendants("TokInfo").Select(x => x.Value).FirstOrDefault();

                if (!string.IsNullOrEmpty(_sessionId) && !string.IsNullOrEmpty(_token))
                {
                    return true;
                }

                return false;
            }
            catch (WebException)
            {

                return false;
            }
        }



        private string getLTERequest(string requestUriString, string token = "", string sessionId = "")
        {

            var requestUri = new Uri(requestUriString);
            var request = (HttpWebRequest)HttpWebRequest.Create(requestUri);

            if (!string.IsNullOrEmpty(sessionId))
            {
                if (request.CookieContainer == null)
                {
                    request.CookieContainer = new CookieContainer();
                }
                request.CookieContainer.Add(new Cookie("SessionID", sessionId, "/", requestUri.Host));


            }

            if (!string.IsNullOrEmpty(token))
            {
                request.Headers.Add("__RequestVerificationToken", token);
            }
            request.Method = "GET";
            request.Timeout = 1000;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                var dataStream = response.GetResponseStream();
                var reader = new StreamReader(dataStream);
                var result = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                return result;
            }
        }

        private LTEConnectionStatusCodeEnum TranslateStatusCode(int status)
        {
            if (status == 2 || status == 3 || status == 5 || status == 8 || status == 20 || status == 21 || status == 23 || status == 27 || status == 28 || status == 29 || status == 30 || status == 31 || status == 32 || status == 33)
                return LTEConnectionStatusCodeEnum.ConnectionFailedInvalidProfile;
            else if (status == 7 || status == 11 || status == 14 || status == 37)
                return LTEConnectionStatusCodeEnum.NotAvailable;
            else if (status == 12 || status == 13)
                return LTEConnectionStatusCodeEnum.ConnectionFailedRoamingNotAllowed;
            else if (status == 201)
                return LTEConnectionStatusCodeEnum.ConnectionFailedBandwidthExceeded;
            else if (status == 900)
                return LTEConnectionStatusCodeEnum.Connecting;
            else if (status == 901)
                return LTEConnectionStatusCodeEnum.Connected;
            else if (status == 902)
                return LTEConnectionStatusCodeEnum.Disconnected;
            else if (status == 903)
                return LTEConnectionStatusCodeEnum.Disconnecting;
            else if (status == 904)
                return LTEConnectionStatusCodeEnum.ConnectionFailedOrDisabled;

            return LTEConnectionStatusCodeEnum.NotAvailable;
        }

        private LTEConnectionStatusEnum TranslateStatus(LTEConnectionStatusCodeEnum status)
        {
            switch (status)
            {
                case LTEConnectionStatusCodeEnum.Unkonwn:
                    return LTEConnectionStatusEnum.Unkonwn;
                case LTEConnectionStatusCodeEnum.NotAvailable:
                    return LTEConnectionStatusEnum.NotAvailable;
                case LTEConnectionStatusCodeEnum.ConnectionFailedInvalidProfile:
                case LTEConnectionStatusCodeEnum.NetworkAccessNotAllowed:
                case LTEConnectionStatusCodeEnum.ConnectionFailedRoamingNotAllowed:
                case LTEConnectionStatusCodeEnum.ConnectionFailedBandwidthExceeded:
                case LTEConnectionStatusCodeEnum.ConnectionFailedOrDisabled:
                    return LTEConnectionStatusEnum.ConnectionFailed;
                case LTEConnectionStatusCodeEnum.Connecting:
                    return LTEConnectionStatusEnum.Connecting;
                case LTEConnectionStatusCodeEnum.Connected:
                    return LTEConnectionStatusEnum.Connected;
                case LTEConnectionStatusCodeEnum.Disconnected:
                    return LTEConnectionStatusEnum.Disconnected;
                case LTEConnectionStatusCodeEnum.Disconnecting:
                    return LTEConnectionStatusEnum.Disconnecting;
                default:
                    return LTEConnectionStatusEnum.Unkonwn;
            }

        }


        public LTEStatus GetStatus()
        {

            if (string.IsNullOrEmpty(_address)) return new LTEStatus { ConnectionStatus = LTEConnectionStatusEnum.UndefinedAddress };

            if (string.IsNullOrEmpty(_token))
            {
                if (!LoadToken())
                {
                    return new LTEStatus { ConnectionStatus = LTEConnectionStatusEnum.NotAvailable };
                }
            }

            try
            {
                var result = getLTERequest($@"http://{_address}/api/monitoring/status", _token, _sessionId);

                var doc = XDocument.Parse(result);

                var connectionStatusCode = doc.Descendants("ConnectionStatus").Select(x => Convert.ToInt32(x.Value)).FirstOrDefault();

                var returnValue = new LTEStatus
                {

                    ConnectionStatusCode = TranslateStatusCode(connectionStatusCode),
                    SignalStrength = GetValue(doc,  "SignalStrength", -1),
                    SignalIcon = GetValue(doc,  "SignalIcon", -1),
                    CurrentNetworkType = GetValue(doc,  "CurrentNetworkType", 0),
                    RoamingStatus = GetValue(doc,  "RoamingStatus", 0),
                    SimStatus = GetValue(doc,  "SimStatus", 0),
                    WifiStatus = GetValue(doc,  "WifiStatus", 0),
                    WanIPAddress = GetValue(doc,  "WanIPAddress", ""),
                    PrimaryDns = GetValue(doc,  "PrimaryDns", ""),
                    SecondaryDns = GetValue(doc,  "SecondaryDns", "")
                };


                returnValue.ConnectionStatus = TranslateStatus(returnValue.ConnectionStatusCode);
                return returnValue;
            }
            catch (WebException)
            {
                _token = "";
                _sessionId = "";
                return new LTEStatus { ConnectionStatus = LTEConnectionStatusEnum.NotAvailable };
            }
            catch (Exception e)
            {
                return new LTEStatus { ConnectionStatus = LTEConnectionStatusEnum.Unkonwn };
            }



        }

        private T GetValue<T>(XDocument doc, string tagName, T defaultValue = default)
        {
            var element = doc.Descendants(tagName).FirstOrDefault();

            if (element == null) return defaultValue;

            if (string.IsNullOrEmpty(element.Value)) return defaultValue;

            if (typeof(T) == typeof(int)) return (T)(object)Convert.ToInt32(element.Value);
            if (typeof(T) == typeof(string)) return (T)(object) element.Value ;

            return defaultValue;
        }
    }
}
