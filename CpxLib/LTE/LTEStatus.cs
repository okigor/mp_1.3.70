﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.LTE
{
    public enum LTEConnectionStatusEnum
    {
        Unkonwn,
        UndefinedAddress,
        NotAvailable,
        ConnectionFailed ,      
        Connecting,
        Connected,
        Disconnected,
        Disconnecting 
    }
    public enum LTEConnectionStatusCodeEnum
    {
        Unkonwn,
        NotAvailable,
        ConnectionFailedInvalidProfile,
        NetworkAccessNotAllowed,
        ConnectionFailedRoamingNotAllowed,
        ConnectionFailedBandwidthExceeded,
        Connecting,
        Connected,
        Disconnected,
        Disconnecting,
        ConnectionFailedOrDisabled
    } 
    
    public class LTEStatus
    {
        
        public LTEConnectionStatusEnum ConnectionStatus {get; set;}
        public LTEConnectionStatusCodeEnum ConnectionStatusCode {get; set;}
        public int SignalStrength{get; set;}
        public int SignalIcon{get; set;}
        public int CurrentNetworkType{get; set;}
        public int RoamingStatus{get; set;}
        public String WanIPAddress{get; set;}
        public string PrimaryDns{get; set;}
        public string SecondaryDns {get; set;}
        public int WifiStatus { get; set; }
        public int SimStatus { get; set; }
   


 
    }
}
