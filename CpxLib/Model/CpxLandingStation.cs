﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib
{
    public class CpxLandingStation
    {
        private TcpClient _client;
        private bool _opening;
        private DateTime _lastReconnectTime;
        private int _retries = int.MaxValue;
        private bool _waiting;
        public bool stop = false;
        public bool stopped = false;
        private const int LANDING_STATION_COMMANDS_DELAY = 500;

        private const string CMD_OPEN = "u";
        private const string CMD_CLOSE = "d";
        private const string CMD_MIDDLE_CLOSE = "m";
        //private const string CMD_GET_STATUS = "s";
        private const string CMD_ERROR = "e";
        private const string CMD_CHARGE = "c";
        private const string CMD_IS_CHARGING = "C";
        private const string STATUS_OPEN = "u";
        private const string STATUS_CLOSED = "d";
        private const string STATUS_CHARGING = "c";
        private const string STATUS_STOPPED = "s";
        private const string STATUS_IS_CHARGING = "C";
        private const string STATUS_MIDDLE_CLOSED = "m";
        

        
        public bool IsOpen
        {
            get
            {
                try
                {
                    // if TcpClient not created return "Closed"
                    if (_client == null) return false;

                    // if TcpClients Client (may be redundant check) not created return "Closed"
                    if (_client.Client == null) return false;

                    // reconnect if not connected
                    if (AutoReconnect && _client.Client.Connected == false && !_opening)
                        DoAutoReconnect();

                    return _client.Client.Connected;
                }
                catch
                {
                    return false;
                }
            }
        }
        void DoAutoReconnect()
        {
            // Auto reconnect disabled get out
            if (!AutoReconnect) return;

            try
            {
                // try to reconnect only every 5 seconds.
                if (DateTime.Now <= _lastReconnectTime) return;

                // dispose connection
                try
                {
                    _client.Dispose();
                }
                catch { }

                _client = new TcpClient();

                var task = _client.ConnectAsync(Host, int.Parse(Port));

                _lastReconnectTime = DateTime.Now.AddSeconds(5);

            }
            catch { }
        }
        public void Disconnect()
        {
            if (_client != null)
            {
                _client.Close();
                _client = null;
            }
            //_client.GetStream().Close();

        }
        void VerifyConnected()
        {
            if (!IsOpen)
            {
                try
                {
                    _client.Dispose();
                }
                catch { }

                // this should only happen if we have established a connection in the first place
                if (_client != null && _retries > 0)
                {
                    _client = new TcpClient();
                    _client.Connect(Host, int.Parse(Port));
                    _retries--;
                }

                throw new Exception("The socket/serial proxy is closed");
            }
        }


        public bool Write(string value)
        {
            VerifyConnected();
            byte[] data = new System.Text.ASCIIEncoding().GetBytes(value);
            VerifyConnected();
            try
            {
                _client.Client.Send(data, data.Length, SocketFlags.None);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public string ReadString()
        {
            byte[] buffer = new byte[4000];

            if (!IsOpen) { return ""; }
            _client.Client.Receive(buffer);
            var count = 0;

            for (int i = 0; i < buffer.Length; i++)
            {
                if (buffer[i] == 0) break;

                count++;
            }

            if (count == 0) return "";

            return Encoding.ASCII.GetString(buffer, 0, count);
        }
        public async Task<bool> ChargeDroneAsync(int timeout)
        {
            return await Task.Factory.StartNew(() =>
            {
                return ChargeDrone(timeout);
            });
        }
        public async Task<bool> StopLandingStationAsync(int timeout)
        {
            return await Task.Factory.StartNew(() =>
            {
                return stopLandingStation(timeout);
            });
        }
        public bool stopLandingStation(int timeout)
        {
            var _waitingSetHere = false;
            if (_waiting) return false;
            _waiting = true;
            _waitingSetHere = true;
            var lastRetryTime = DateTime.Now.AddMilliseconds(timeout);
            try
            {
                Open();

                Write(STATUS_STOPPED);
                Task.Delay(LANDING_STATION_COMMANDS_DELAY).Wait();
                var result = ReadString();
                if (result != STATUS_STOPPED)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (_waitingSetHere)
                    _waiting = false;
            }
        }
        public bool ChargeDrone(int timeout)
        {
            var _waitingSetHere = false;
            if (_waiting) return false;
            _waiting = true;
            _waitingSetHere = true;
            var lastRetryTime = DateTime.Now.AddMilliseconds(timeout);
            try
            {
                Open();

                Write(CMD_CHARGE);
                Task.Delay(LANDING_STATION_COMMANDS_DELAY).Wait();

                var result = ReadString();

                if (result != STATUS_CHARGING)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (_waitingSetHere)
                    _waiting = false;
            }
        }

        public bool IsInCharging()
        {
            String resultIsCharging;
            var _waitingSetHere = false;
            if (_waiting) return false;
            _waiting = true;
            _waitingSetHere = true;
            try
            {
                Open();
                Write(CMD_IS_CHARGING);
                Task.Delay(LANDING_STATION_COMMANDS_DELAY).Wait();
                resultIsCharging = ReadString();
                if (resultIsCharging != STATUS_IS_CHARGING)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (_waitingSetHere)
                    _waiting = false;
                Close();
            }
        }

        public async Task<bool> CloseLandingStationAsync(int timeout)
        {
            return await Task.Factory.StartNew(() =>
            {
                return LandingStationPos(timeout, CMD_CLOSE);
            });
        }

        public bool LandingStationPos(int timeout, String position)
        {
            //position param can be d or m
            var _waitingSetHere = false;
            if (_waiting) return false;
            _waiting = true;
            _waitingSetHere = true;
            var lastRetryTime = DateTime.Now.AddMilliseconds(timeout);

            try
            {
                Open();
                Write(position);
                Task.Delay(500).Wait();
                var result = "";
                _client.Client.ReceiveTimeout = 100;
                DateTime startTime = DateTime.UtcNow;
                TimeSpan breakDuration = TimeSpan.FromSeconds(15);
                while (result == "" && DateTime.UtcNow - startTime < breakDuration)
                {
                    if (stop)
                    {
                        _client.Client.ReceiveTimeout = -1;
                        Write(STATUS_STOPPED);
                        Task.Delay(500).Wait();
                        result = ReadString();
                        stop = false;
                    }
                    try
                    {
                        if (result == "")
                        {
                            result = ReadString();
                        }

                    }
                    catch (Exception e) { }
                }
                /////change due to the data on station
                if (result != position && result != STATUS_STOPPED)
                {
                    return false;
                }
                else if (result == STATUS_STOPPED)
                {
                    stopped = true;
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (_waitingSetHere)
                    _waiting = false;
            }
        }

        public void Open()
        {
            try
            {
                if (_opening) return;

                _opening = true;

                if (_client != null && _client.Client != null && _client.Client.Connected)
                {

                    return;
                }

                _client = new TcpClient
                {
                    NoDelay = true
                };
                _client.Client.NoDelay = true;

                try
                {
                    _client.Connect(Host, int.Parse(Port));
                }
                catch
                {
                    VerifyConnected();

                }
            }
            catch
            {
                // disable if the first connect fails
                AutoReconnect = false;
                throw;
            }
            finally
            {
                _opening = false;
            }
        }

        public void Close()
        {
            try
            {
                if (_client.Client != null && _client.Client.Connected)
                {
                    _client.Client.Dispose();
                    _client.Dispose();
                }
            }
            catch { }

            try
            {
                _client.Dispose();
            }
            catch { }

            _client = new TcpClient();
        }

        public async Task<bool> OpenLandingStationAsync(int timeout)
        {
            return await Task.Factory.StartNew(() =>
            {

                return OpenLandingStation(timeout);

            });
        }

        public bool OpenLandingStation(int timeout)
        {
            var _waitingSetHere = false;
            if (_waiting) return false;
            _waiting = true;
            _waitingSetHere = true;
            var lastRetryTime = DateTime.Now.AddMilliseconds(timeout);

            try
            {
                Open();
                Write(CMD_OPEN);
               

                Task.Delay(500).Wait();

                var result = "";
                _client.Client.ReceiveTimeout = 100;
                DateTime startTime = DateTime.UtcNow;
                TimeSpan breakDuration = TimeSpan.FromSeconds(30);
                while (result == "" && DateTime.UtcNow - startTime < breakDuration)
                {
                    if (stop)
                    {
                        _client.Client.ReceiveTimeout = -1;
                        Write(STATUS_STOPPED);
                        Task.Delay(500).Wait();
                        result = ReadString();

                        stop = false;
                    }
                    try
                    {
                        if (result == "")
                        {
                            result = ReadString();
                        }

                    }
                    catch (Exception e)
                    {
                    }
                }
                //while (result != STATUS_OPEN && lastRetryTime >= DateTime.Now)
                //{
                //    Write(CMD_GET_STATUS);
                //    Task.Delay(LANDING_STATION_COMMANDS_DELAY);
                //    result = ReadString();
                //}
                if (result != STATUS_OPEN && result != STATUS_STOPPED)
                {
                    return false;
                }
                else if (result == STATUS_STOPPED)
                {
                    stopped = true;
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {

                if (_waitingSetHere)
                    _waiting = false;
                //Disconnect();
                // Close();
            }
        }
        public bool AutoReconnect { get; set; }

        public string Host { get; set; }
        public string Port { get; set; }
    }
}
