﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Model
{
    public interface IModelObject
    {
        string Id { get; set; }
        string FileType { get;  }

        void Deserialize(string source);
        string Serialize();
 
    }
}
