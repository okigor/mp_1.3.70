﻿

using MissionPlanner.Mavlink;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CpxLib.Model
{
    public class FlightRoute : IModelObject
    {
        #region Properties

        public string Id { get; set; }
        public string Name { get; set; }
        public List<MavCommand> Commands { get; set; }
        public MavCommand HomeLocation { get; set; }

        public DateTime LastWriteTime { get; private set; }
        public MAVLink.MAV_FRAME AltMode { get; internal set; }
 

        #endregion Properties
        public string FileType => "wps";

        public void Deserialize(string source)
        {
            var doc = XDocument.Parse(source);

            var generalInfo = doc.Descendants("General").FirstOrDefault();

          
            Id = generalInfo.Attribute(XName.Get("id", "")).Value;
            Name = generalInfo.Attribute(XName.Get("name", "")).Value;

            HomeLocation = new MavCommand();
            var homeLocation = doc.Descendants("Home").FirstOrDefault();

            if (homeLocation != null)
            {
                HomeLocation.Lat = double.Parse(homeLocation.Attribute(XName.Get("lat", "")).Value);
                HomeLocation.Lng = double.Parse(homeLocation.Attribute(XName.Get("lng", "")).Value);
                HomeLocation.Alt = double.Parse(homeLocation.Attribute(XName.Get("alt", "")).Value);

                AltMode = (MAVLink.MAV_FRAME)Enum.Parse(typeof(MAVLink.MAV_FRAME), homeLocation.GetAttributeValue("altmode", "3"));
            }
            var commands = doc.Descendants("Command");
            Commands = new List<MavCommand>();

            foreach (var item in commands)
            {
                var command = new MavCommand();
                command.Command = ushort.Parse(item.Attribute(XName.Get("cmd", "")).Value);
                command.Param1 = double.Parse(item.Attribute(XName.Get("p1", "")).Value);
                command.Param2 = double.Parse(item.Attribute(XName.Get("p2", "")).Value);
                command.Param3 = double.Parse(item.Attribute(XName.Get("p3", "")).Value);
                command.Param4 = double.Parse(item.Attribute(XName.Get("p4", "")).Value);
                command.Lat = double.Parse(item.Attribute(XName.Get("lat", "")).Value);
                command.Lng = double.Parse(item.Attribute(XName.Get("lng", "")).Value);
                command.Alt = double.Parse(item.Attribute(XName.Get("alt", "")).Value);

                Commands.Add(command);
            }
        }

        public string Serialize()
        {
            if (string.IsNullOrEmpty(Id))
            {
                Id = Guid.NewGuid().ToString();
            }
            var document = new XDocument(new XElement("FlyPath",
                       new XElement("General", new XAttribute("id", Id),
                                               new XAttribute("name", Name)),
                       new XElement("Home", new XAttribute("lat", HomeLocation.Lat),
                                            new XAttribute("lng", HomeLocation.Lng),
                                            new XAttribute("alt", HomeLocation.Alt),
                                            new XAttribute("altmode", (int)AltMode)),
                       new XElement("Data",
                         Commands.Where(x => x != null)
                              .Select(x => new XElement("Command",
                                            new XAttribute("cmd", x.Command),
                                            new XAttribute("p1", x.Param1),
                                            new XAttribute("p2", x.Param2),
                                            new XAttribute("p3", x.Param3),
                                            new XAttribute("p4", x.Param4),
                                            new XAttribute("lat", x.Lat),
                                            new XAttribute("lng", x.Lng),
                                            new XAttribute("alt", x.Alt)
                              ))
                       )));
            return document.ToString();
        }
    }
}
