﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Model
{
    public class Trigger
    {
        public TriggerTypeEnum TriggerType { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime FinishTime { get; set; }
        public int RepeatInterval { get; set; }
        public bool Enabled { get; set; }
        public string ActionId { get; set; }
        public string VehicleId { get; set; }
        public int[] Days { get; set; }

        public Trigger()
        {
            Days = new int[7];
            StartTime = DateTime.Now.Date;
            FinishTime = DateTime.Now.Date;
            TriggerType = TriggerTypeEnum.OneTime;
            Enabled = true;
            ActionId = "";
            VehicleId = "";
        }

        public string DaysToString()
        {
            var result = "";
            foreach (var day in Days)
            {
                result += (string.IsNullOrEmpty(result) ? "" : ",") + day.ToString();
            }

            return result;
        }
        public string Details
        {
            get
            {
                if (TriggerType == TriggerTypeEnum.OneTime)
                {
                    return $"At {StartTime.ToString("HH:mm")}";
                }
                else
                {
                    return $"From {StartTime.ToString("HH:mm")} to {FinishTime.ToString("HH:mm")} repeat every {RepeatInterval} minutes";
                }
            }
        }
        public string Status
        {
            get
            {
                return Enabled ? "Enabled" : "Disabled";
            }
        }
    }
}
