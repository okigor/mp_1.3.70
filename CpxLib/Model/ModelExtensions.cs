﻿using CpxLib.Core;
using CpxLib.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CpxLib.Model
{
    public static class ModelExtensions
    {

        public static IEnumerable<IModelObject> GetAllDataFiles(Func<IModelObject> createInstance)
        {
            var dummy = createInstance();

            foreach (var file in Directory.GetFiles(AppSettings.CopterPixDataFolder, $"*.{dummy.FileType}", SearchOption.TopDirectoryOnly))
            {
                var instance = createInstance();

                var str = File.ReadAllText(file);

                instance.Deserialize(str);

                yield return instance;
            }

        }
        public static string GetAttributeValue(this XElement element, string attributeName, string defaultValue = "")
        {
            var attrib = element.Attribute(XName.Get(attributeName, ""));

            if (attrib == null) return defaultValue;

            if (string.IsNullOrEmpty(attrib.Value)) return defaultValue;

            return attrib.Value;
        }
        public static IModelObject GetFile(string id, Func<IModelObject> createInstance)
        {
            foreach (var instance in GetAllDataFiles(createInstance))
            {
                if (instance.Id == id) return instance;
            }

            return null;
        }


        public static void Save(this Vehicle vehicle)
        {

            try
            {
                var document = vehicle.Serialize();

                var filePath = AppSettings.CopterPixDataFolder + System.IO.Path.DirectorySeparatorChar + vehicle.Id + "." + vehicle.FileType;

                System.IO.File.WriteAllText(filePath, document);
                CustomMessageBox.Show(Resources.SuccessfulOperation);

            }
            catch (Exception e)
            {
                CustomMessageBox.Show(Resources.NotSuccessfulOperation);

            }

        }
        public static void Delete(this Vehicle vehicle)
        {
            try
            {
                var filePath = AppSettings.CopterPixDataFolder + System.IO.Path.DirectorySeparatorChar + vehicle.Id + "." + vehicle.FileType;
                System.IO.File.Delete(filePath);
                CustomMessageBox.Show(Resources.SuccessfulOperation);
            }
            catch (Exception e)
            {
                CustomMessageBox.Show(Resources.NotSuccessfulOperation);

            }
        }
        public static void Delete(this FlightRoute flightRoute)
        {
            try
            {
                var filePath = AppSettings.CopterPixDataFolder + System.IO.Path.DirectorySeparatorChar + flightRoute.Id + "." + flightRoute.FileType;
                System.IO.File.Delete(filePath);
                CustomMessageBox.Show(Resources.SuccessfulOperation);
            }
            catch (Exception e)
            {
                CustomMessageBox.Show(Resources.NotSuccessfulOperation);

            }
        }
        public static void Save(this FlightRoute flightRoute)
        {
            try
            {
                var document = flightRoute.Serialize();

                var filePath = AppSettings.CopterPixDataFolder + System.IO.Path.DirectorySeparatorChar + flightRoute.Id + "." + flightRoute.FileType;
                System.IO.File.WriteAllText(filePath, document);
                CustomMessageBox.Show(Resources.SuccessfulOperation);

            }

            catch (Exception e)
            {
                CustomMessageBox.Show(Resources.NotSuccessfulOperation);
            }

        }

        public static void Save(this Schedule schedule)
        {
            try
            {
                var document = schedule.Serialize();

                var filePath = AppSettings.CopterPixDataFolder + System.IO.Path.DirectorySeparatorChar + schedule.Id + "." + schedule.FileType;

                System.IO.File.WriteAllText(filePath, document);
                CustomMessageBox.Show(Resources.SuccessfulOperation);
            }
            catch (Exception e)
            {
                CustomMessageBox.Show(Resources.NotSuccessfulOperation);
            }
        }

        public static IEnumerable<Vehicle> GetVehicleFiles()
        {
            return GetAllDataFiles(() => new Vehicle()).Cast<Vehicle>();
        }
        public static IEnumerable<Schedule> GetScheduleFiles()
        {
            return GetAllDataFiles(() => new Schedule()).Cast<Schedule>();
        }

        public static IEnumerable<FlightRoute> GetFlightRoutes()
        {
            return GetAllDataFiles(() => new FlightRoute()).Cast<FlightRoute>();
        }

        public static Vehicle GetVehicleFile(string id)
        {
            foreach (var instance in GetAllDataFiles(() => new Vehicle()))
            {
                if (instance.Id == id) return (Vehicle)instance;
            }

            return null;
        }
    }
}
