﻿
namespace CpxLib.Model
{
    public enum TriggerTypeEnum
    {
        NotSet = 0,
        OneTime = 1,
        Repeating = 2
    }
}
