﻿using DirectShowLib.BDA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Model
{
    public class CpxMobilicom
    {
        public TcpClient tcpClient { get; set; }
        public NetworkStream networkStream { get; set; }
        private string GroundIP;
        private int GroundPort;
        private string AirIP;
        private int AirPort;
        public CpxMobilicom(string groundip, int groundport, string airip, int airport)
        {
            GroundIP = groundip;
            GroundPort = groundport;
            AirIP = airip;
            AirPort = airport;
        }
        public int getSignalStrength()
        {
            try
            {
                string[] splitted = SendMessage("|I|51|I|181", true);
                if (splitted[splitted.Length - 1].Contains("|E|") || Int32.Parse(splitted[splitted.Length - 1]) == 0)
                {
                    splitted = SendMessage("|I|51|I|8", true);

                    return Math.Max(Int32.Parse(splitted[splitted.Length - 2]) / 2, Int32.Parse(splitted[splitted.Length - 1]) / 2);
                }
                else //if(!splitted[splitted.Length - 1].Contains("|E|") || Int32.Parse(splitted[splitted.Length - 1]) == 0)
                {


                    splitted = SendMessage("|I|51|I|182", true);
                    return Int32.Parse(splitted[splitted.Length - 5]);
                }
                //else
                //{
                //    return -1;
                //}
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                try
                {
                    string[] splitted = SendMessage("|I|10005|I|19", true);
                    string x = "";
                    for (int i = 0; i < splitted.Length; i++)
                    {
                        x += splitted[i] + ", ";
                    }

                    bool ping = false;
                    Ping pinger = null;

                    try
                    {
                        pinger = new Ping();
                        PingReply reply = pinger.Send(AirIP);
                        ping = reply.Status == IPStatus.Success;
                        if (!ping)
                        {
                            reply = pinger.Send(AirIP);
                            ping = reply.Status == IPStatus.Success;
                        }
                    }
                    catch (PingException)
                    {
                        // Discard PingExceptions and return false;
                    }
                    finally
                    {
                        if (pinger != null)
                        {
                            pinger.Dispose();
                        }
                    }

                    //MessageBox.Show(ping+"");
                    //MessageBox.Show((Int32.Parse(splitted[7]) / Int32.Parse(splitted[2])) / 2 + "");
                    if ((Int32.Parse(splitted[7]) / Int32.Parse(splitted[2])) / 2 <= 4 && (Int32.Parse(splitted[8]) / Int32.Parse(splitted[2])) / 2 <= 4 && ping)
                    {
                        MessageBox.Show(-Math.Max((Int32.Parse(splitted[14]) / Int32.Parse(splitted[2])) / 2, (Int32.Parse(splitted[15]) / Int32.Parse(splitted[2])) / 2) + "");
                        return -Math.Max((Int32.Parse(splitted[14]) / Int32.Parse(splitted[2])) / 2, (Int32.Parse(splitted[15]) / Int32.Parse(splitted[2])) / 2);
                    }
                    else if (Int32.Parse(splitted[5]) <= 4 && Int32.Parse(splitted[6]) <= 4 && !ping)
                    {
                        return 0;
                    }
                    else
                    {
                        return Math.Max(Int32.Parse(splitted[7]) / 2, Int32.Parse(splitted[8]) / 2);
                    }

                }
                catch (Exception ex)
                {
                    return -1;
                }

            }
        }
        public string getCommunicationType()
        {
            string[] splitted = SendMessage("|I|51|I|182", true);
            if (splitted[splitted.Length - 1].Contains("LTE"))
            {
                return "LTE";
            }
            return "SDR";
        }
        public bool ChangeCommunicationType(string commType)
        {

            string[] splitted = SendMessage("|I|51|I|181|I|" + commType + "|I|0", false);
            //string s = "";
            //for (int i = 0; i < splitted.Length; i++)
            //{
            //    s += splitted[i] + ", ";
            //}
            //MessageBox.Show(s);
            if (splitted == null)
            {
                splitted = SendMessage("|I|51|I|181|I|" + commType + "|I|0", true);
                //for (int i = 0; i < splitted.Length; i++)
                //{
                //    s += splitted[i] + ", ";
                //}
                //MessageBox.Show(s);
                if (splitted[splitted.Length - 1] == "1")
                    return true;
                else
                    return false;
            }
            else
                return false;

        }
        private string[] SendMessage(string sendCamera, bool isGround)
        {
            //MessageBox.Show(sendCamera);
            if (isGround)
                tcpClient = new TcpClient(GroundIP, GroundPort);
            else
                tcpClient = new TcpClient(AirIP, AirPort);
            networkStream = tcpClient.GetStream();
            Thread.Sleep(1000);
            Byte[] camera = System.Text.Encoding.ASCII.GetBytes(sendCamera);
            networkStream.Write(camera, 0, camera.Length);
            Byte[] answer = new byte[1024];
            if (isGround)
            {
                networkStream.Read(answer, 0, answer.Length);
                string returndata = Encoding.UTF8.GetString(answer);
                //MessageBox.Show(returndata);
                return returndata.Split(new string[] { "|I|" }, StringSplitOptions.RemoveEmptyEntries);
            }
            return null;
        }
    }
}
