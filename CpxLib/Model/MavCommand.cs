﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Model
{
    public class MavCommand
    {
        public ushort Command { get; set; }

        public double Param1 { get; set; }
        public double Param2 { get; set; }
        public double Param3 { get; set; }
        public double Param4 { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public double Alt { get; set; }
    }
}
