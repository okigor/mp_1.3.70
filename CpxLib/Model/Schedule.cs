﻿ 
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CpxLib.Model
{
    public class Schedule : IModelObject
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<Trigger> Triggers { get; set; }
        public DateTime LastWriteTime { get; private set; }

        public string FileType => "schd";

        public Schedule()
        {
            Triggers = new List<Trigger>();

            Id = "";
            Name = "";
        }
     
 

        public void Deserialize(string source)
        {
            var doc = XDocument.Parse(source);

            var generalInfo = doc.Descendants("General").FirstOrDefault();

             
            Id = generalInfo.GetAttributeValue("id", "");
            Name = generalInfo.GetAttributeValue("name", "");


            var triggers = doc.Descendants("Trigger");
            Triggers = new List<Trigger>();

            foreach (var item in triggers)
            {
                var trigger = new Trigger();
                trigger.TriggerType = (TriggerTypeEnum)int.Parse(item.GetAttributeValue("type", "0"));
                trigger.StartTime = DateTime.Parse(item.GetAttributeValue("start", DateTime.Now.ToString()));
                trigger.FinishTime = DateTime.Parse(item.GetAttributeValue("finish", DateTime.Now.ToString()));
                trigger.RepeatInterval = int.Parse(item.GetAttributeValue("interval", "0"));
                trigger.Enabled = bool.Parse(item.GetAttributeValue("enabled", "true"));
                trigger.ActionId = item.GetAttributeValue("action");
                trigger.VehicleId = item.GetAttributeValue("vehicle");

                var daysAttrib = item.Attribute(XName.Get("days", ""));

                if (daysAttrib == null || string.IsNullOrEmpty(daysAttrib.Value))
                {
                    for (int i = 0; i < 7; i++)
                    {
                        trigger.Days[i] = 1;
                    }
                }
                else
                {
                    var days = daysAttrib.Value.Split(new[] { ',' });

                    for (int i = 0; i < days.Length; i++)
                    {
                        trigger.Days[i] = int.Parse(days[i]);
                    }
                }
                Triggers.Add(trigger);
            }

        }

        public string Serialize()
        {

            if (string.IsNullOrEmpty(Id))
            {
                Id = Guid.NewGuid().ToString();
            }
            var document = new XDocument(new XElement("Schedule",
                       new XElement("General", new XAttribute("id", Id),
                                               new XAttribute("name", Name)),
                       new XElement("Data",
                         Triggers.Where(x => x != null)
                              .Select(x => new XElement("Trigger",
                                            new XAttribute("type", (int)x.TriggerType),
                                            new XAttribute("start", x.StartTime),
                                            new XAttribute("finish", x.FinishTime),
                                            new XAttribute("interval", x.RepeatInterval),
                                            new XAttribute("enabled", x.Enabled),
                                            new XAttribute("action", x.ActionId),
                                            new XAttribute("vehicle", x.VehicleId),
                                            new XAttribute("days", x.DaysToString())
                              ))
                       )));
            return document.ToString();
        }
    }
}
