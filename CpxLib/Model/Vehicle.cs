﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CpxLib.Model
{
    public class Vehicle : IModelObject
    {

        #region Properties

        public string Id { get; set; }
        public string Name { get; set; }
        public string MavId { get; set; }
        public DateTime LastWriteTime { get; private set; }
        public string MavIP { get; internal set; }
        public string Protocol { get; internal set; }
        public string MavPort { get; internal set; }
        public string NightCameraIP { get; internal set; }
        public string NightCameraPort { get; internal set; }
        public string DayCameraIP { get; internal set; }
        public string DayCameraPort { get; internal set; }
        public string DayCameraPipline { get; internal set; }
        public string NightCameraPipline { get; internal set; }
        public string LandingStationIp { get; internal set; }
        public string LandingStationPort { get; internal set; }
        public string CommunicationType { get; internal set; }
        public string WirelessName { get; internal set; }
        public string MaximumBattery { get; internal set; }
        public string MinimumBattery { get; internal set; }
        public string TimeInAir { get; internal set; }
        public string CameraType { get; set; }
        public string NextVisionIP { get; internal set; }
        public string MobilicomIP { get; internal set; }
        public string MobilicomAirIP { get; internal set; }
        public string CameraDeviation { get; internal set; }
        public string FileType { get => "vhc"; }

        #endregion Properties

        #region Methods
        public void Deserialize(string source)
        {
            var doc = XDocument.Parse(source);

            var generalInfo = doc.Descendants("General").FirstOrDefault();

            //     LastWriteTime = new FileInfo(path).LastWriteTime;
            Id = generalInfo.GetAttributeValue("id");
            Name = generalInfo.GetAttributeValue("name");
            MavId = generalInfo.GetAttributeValue("mavid");
            MavPort = generalInfo.GetAttributeValue("mav_port");
            DayCameraPort = generalInfo.GetAttributeValue("day_camera_port");
            NightCameraPort = generalInfo.GetAttributeValue("night_camera_port");
            MavIP = generalInfo.GetAttributeValue("mav_ip");
            DayCameraIP = generalInfo.GetAttributeValue("day_camera_ip");
            NightCameraIP = generalInfo.GetAttributeValue("night_camera_ip");
            DayCameraPipline = generalInfo.GetAttributeValue("day_camera_pipeline");
            NightCameraPipline = generalInfo.GetAttributeValue("night_camera_pipeline");
            LandingStationIp = generalInfo.GetAttributeValue("landing_station_ip");
            LandingStationPort = generalInfo.GetAttributeValue("landing_station_port");
            Protocol = generalInfo.GetAttributeValue("protocol", "TCP");
            CommunicationType = generalInfo.GetAttributeValue("communication_type", "");
            WirelessName = generalInfo.GetAttributeValue("wireless_name", "");
            MaximumBattery = generalInfo.GetAttributeValue("maximum_battery", "");
            MinimumBattery = generalInfo.GetAttributeValue("minimum_battery", "");
            TimeInAir = generalInfo.GetAttributeValue("time_in_air", "");
            CameraType = generalInfo.GetAttributeValue("camera_type", "");
            NextVisionIP = generalInfo.GetAttributeValue("nextvision_IP", "");
            MobilicomIP = generalInfo.GetAttributeValue("mobilicom_IP", "");
            MobilicomAirIP = generalInfo.GetAttributeValue("mobilicom_air_IP", "");
            CameraDeviation = generalInfo.GetAttributeValue("camera_deviation","");
        }

        public string Serialize()
        {
            if (string.IsNullOrEmpty(Id))
            {
                Id = Guid.NewGuid().ToString();
            }
            var document = new XDocument(new XElement("Vehicle",
                       new XElement("General", new XAttribute("id", Id),
                                               new XAttribute("name", Name),
                                               new XAttribute("mavid", MavId),
                                               new XAttribute("mav_port", MavPort),
                                               new XAttribute("day_camera_port", DayCameraPort),
                                               new XAttribute("night_camera_port", NightCameraPort),
                                               new XAttribute("mav_ip", MavIP),
                                               new XAttribute("day_camera_ip", DayCameraIP),
                                               new XAttribute("night_camera_ip", NightCameraIP),
                                               new XAttribute("day_camera_pipeline", DayCameraPipline),
                                               new XAttribute("night_camera_pipeline", NightCameraPipline),
                                               new XAttribute("landing_station_ip", LandingStationIp),
                                               new XAttribute("landing_station_port", LandingStationPort),
                                               new XAttribute("protocol", Protocol),
                                               new XAttribute("communication_type", CommunicationType),
                                               new XAttribute("wireless_name", WirelessName),
                                               new XAttribute("maximum_battery", MaximumBattery),
                                               new XAttribute("minimum_battery", MinimumBattery),
                                               new XAttribute("time_in_air", TimeInAir),
                                               new XAttribute("camera_type", CameraType),
                                               new XAttribute("nextvision_IP", NextVisionIP),
                                               new XAttribute("mobilicom_IP", MobilicomIP),
                                               new XAttribute("mobilicom_air_IP", MobilicomAirIP),
                                               new XAttribute("camera_deviation",CameraDeviation)
                                               )));
            return document.ToString();
        }


        #endregion

        public Vehicle()
        {
            Id = "";
            Name = "";
            MavId = "";
            MavIP = "";
            MavPort = "";
            NightCameraIP = "";
            NightCameraPort = "";
            DayCameraIP = "";
            DayCameraPort = "";
            DayCameraPipline = "";
            NightCameraPipline = "";
            LandingStationIp = "";
            LandingStationPort = "";
            Protocol = "TCP";
            CommunicationType = "";
            WirelessName = "";
            MaximumBattery = "";
            MinimumBattery = "";
            TimeInAir = "";
            CameraType = "";
            NextVisionIP = "";
            MobilicomIP = "";
            MobilicomAirIP = "";
            CameraDeviation = "";
            ;
        }


    }
}
