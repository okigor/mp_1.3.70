﻿
using CpxLib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CpxLib.Scheduling
{
    public class ScheduleRunner
    {
        private Schedule _schedule;
        private Job[] _jobs;

        public event EventHandler<JobStatusChangeEventArgs> JobStatusChange;


        public ScheduleRunner(Schedule schedule)
        {
            _schedule = schedule;
            _jobs = _schedule
                        .Triggers
                        .Where(x => x.Enabled)
                        .GroupBy(x => x.VehicleId)
                        .Select(x => new Job(x.ToArray(), x.Key))
                        .ToArray();
        }

        public void StopAllJobs()
        {
            foreach (var job in _jobs)
            {
                job.StopJob();
            }
            _jobs = null;
        }


        public void Start()
        {
            foreach (var job in _jobs)
            {
                
                job.JobStatusChange += Job_JobStatusChange;
                job.Start();
            }
        }

        private void Job_JobStatusChange(object sender, JobStatusChangeEventArgs e)
        {
            JobStatusChange.Invoke(this, e);
        }

        public DateTime GetNextRun()
        {
            return _jobs.Min(x => x.NextRun);
        }
    }
}
