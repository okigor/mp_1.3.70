﻿
using CpxLib.Controls;
using CpxLib.Core;
using CpxLib.Model;
using MissionPlanner;
using MissionPlanner.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Scheduling
{
    public class Job
    {
        private string status;
        private readonly Trigger[] _triggers;
        private List<FlightRoute> _flightRouteFiles;
        private readonly Vehicle _vehicleFile;
        private readonly string _vehicleId;
        private VehicleConnection _vehicleConnection;
        private DateTime _nextRun;
        private string _nextActionId;
        private CancellationTokenSource _cancelSource = new CancellationTokenSource();
        public event EventHandler<JobStatusChangeEventArgs> JobStatusChange;
        public CpxLandingStation _landingStation;
        //private readonly VehicleConnection _vehicleConnection;
        private bool isFirst;
        private const string CMD_CLOSE = "d";
        public Job(Trigger[] triggers, string vehicleId)
        {

            isFirst = true;
            _triggers = triggers;
            _flightRouteFiles = ModelExtensions.GetFlightRoutes().ToList();
            _landingStation = null;
            _vehicleId = vehicleId;
            _vehicleFile = ModelExtensions.GetVehicleFile(_vehicleId);

            if (_vehicleFile == null)
            {
                
                var config = AppSettings.LoadConfiguration();
                MessageBox.Show("IP: " + config.LandingStationIP + ", PORT: " + config.LandingStationPort);
                if (config.LandingStationIP.Replace(" ", "") != "..." && config.LandingStationPort.Replace(" ", "") != "...")
                {
                    _landingStation = new CpxLandingStation
                    {
                        Host = config.LandingStationIP?.Replace(" ", ""),
                        Port = config.LandingStationPort
                    };
                }
            }
            else
            {
                _vehicleConnection = Core.CpxHost.Instance.UI.GetVehicleConnection();
                //_landingStation=_vehicleConnection.
                //_vehicleConnection = new VehicleConnection(MainV2.comPort, vehicleId);
            }
            _nextRun = DateTime.MaxValue;
            _nextActionId = "";
        }

        private DateTime TruncSeconds(DateTime date)
        {
            return date.Date.AddMinutes(Math.Floor(date.TimeOfDay.TotalMinutes));
        }

        public void StopJob()
        {
            _cancelSource.Cancel(true);
            //_vehicleConnection._cancelSource.Cancel(true);


        }
        public async void Start()
        {
            //await Task.Factory.StartNew(async () =>
            await Task.Run(async () =>
            {
                try
                {
                    while (true)
                    {
                        if (_cancelSource.Token.IsCancellationRequested)
                        {
                            //MessageBox.Show("start canceled");
                            break;
                        }

                        CalculateNextRun();
                        //MessageBox.Show(_nextRun.ToString());
                        var now = TruncSeconds(DateTime.Now);

                        if (_nextRun != DateTime.MaxValue)
                        {

                            OnJobStatusChange(JobStatusEnum.Waiting);
                            status = "waiting";
                        }
                        else
                        {
                            OnJobStatusChange(JobStatusEnum.Stopped);
                            status = "stopped";
                        }
                        if (_nextRun > now && _nextRun != DateTime.MaxValue)
                        {
                            //MessageBox.Show(_nextRun.ToString());
                            var waitMinutes = (int)(_nextRun - DateTime.Now).TotalMilliseconds;


                            if (waitMinutes >= 0)
                            {
                                OnJobStatusChange(JobStatusEnum.Waiting);
                                status = "waiting";
                                waitMinutes= (int)(_nextRun - DateTime.Now).TotalMilliseconds;
                                Task.Delay(waitMinutes).Wait(_cancelSource.Token);

                                OnJobStatusChange(JobStatusEnum.InMission);
                                //status = "inmission";
                                //////Igor  
                                //await FlyAsync();
                                var file = _flightRouteFiles.First(x => x.Id == _nextActionId);
                                try
                                {
                                    await _vehicleConnection.DoFlyByMapAsync(file, _cancelSource.Token);
                                }
                                catch (Exception e)
                                {
                                    //MessageBox.Show(e.Message+"WHYYYY");
                                    break;
                                }
                                //MessageBox.Show("START!");
                                //Thread.Sleep(20000);
                                // replace execution
                                Task.Delay(6000).Wait();

                                OnJobStatusChange(JobStatusEnum.MissionCompleted);
                                status = "missioncomplete";
                            }
                        }
                    }
                    //MessageBox.Show("START!");
                    //Thread.Sleep(20000);

                }
                catch (OperationCanceledException)
                {
                    MessageBox.Show("CANCELED!");

                }

            }, _cancelSource.Token);
        }

        private async Task FlyAsync()
        {
            var file = _flightRouteFiles.First(x => x.Id == _nextActionId);
            /////Igor   CtrlMain.Instance.SaveWPs(MainV2.comPort, file);

            await FlyByMapAsync(MainV2.comPort, file);

            Task.Delay(10000).Wait();

            while (MainV2.comPort.MAV.cs.armed)
            {
                Task.Delay(10000).Wait();
            }
        }

        public async Task FlyByMapAsync(MAVLinkInterface ComPort, FlightRoute _currentFile)
        {
            await Task.Factory.StartNew(() =>
            {
                if (_cancelSource.Token.IsCancellationRequested)
                {
                    MessageBox.Show("FlyByMapAsync canceled");
                    return;
                }
                try
                {
                    UploadWaypoints(ComPort, _currentFile);
                    FlyByMap(ComPort, _currentFile);
                }
                catch (OperationCanceledException)
                {
                    MessageBox.Show("CANCELED!");

                }

            }, _cancelSource.Token);
        }
        public void UploadWaypoints(MAVLinkInterface ComPort, FlightRoute waypointFile)
        {
            if (!ComPort.BaseStream.IsOpen)
            {
                //CHANGEEEEEE
                MessageBox.Show("Please connect to the vehicle first!");
                //OnError(new Exception("Please connect to the vehicle first!"));
                return;
            }

            ComPort.giveComport = true;



            var home = new MissionPlanner.Utilities.Locationwp();
            try
            {
                home.id = (ushort)MAVLink.MAV_CMD.WAYPOINT;
                home.lat = waypointFile.HomeLocation.Lat;
                home.lng = waypointFile.HomeLocation.Lng;
                home.alt = ((float)waypointFile.HomeLocation.Alt / CurrentState.multiplieralt); // use saved home
            }
            catch
            {
                //CHANGEEEEEE
                MessageBox.Show("Please connect to the vehicle first!");
                //OnError(new Exception("Please connect to the vehicle first!"));
                return;
            }


            try
            {

                bool use_int = (ComPort.MAV.cs.capabilities & (uint)MAVLink.MAV_PROTOCOL_CAPABILITY.MISSION_INT) > 0;

                // set wp total
                ushort totalwpcountforupload = (ushort)(waypointFile.Commands.Count + 1);

                if (ComPort.MAV.apname == MAVLink.MAV_AUTOPILOT.PX4)
                {
                    totalwpcountforupload--;
                }

                ComPort.setWPTotal(totalwpcountforupload); // + home

                // upload from wp0
                var a = 0;

                if (ComPort.MAV.apname != MAVLink.MAV_AUTOPILOT.PX4)
                {
                    try
                    {
                        var homeans = ComPort.setWP(home, (ushort)a, MAVLink.MAV_FRAME.GLOBAL, 0, 1, use_int);
                        if (homeans != MAVLink.MAV_MISSION_RESULT.MAV_MISSION_ACCEPTED)
                        {
                            if (homeans != MAVLink.MAV_MISSION_RESULT.MAV_MISSION_INVALID_SEQUENCE)
                            {
                                //  CustomMessageBox.Show(Strings.ErrorRejectedByMAV, Strings.ERROR);
                                return;
                            }
                        }
                        a++;
                    }
                    catch (TimeoutException)
                    {
                        use_int = false;
                        // added here to prevent timeout errors
                        ComPort.setWPTotal(totalwpcountforupload);
                        var homeans = ComPort.setWP(home, (ushort)a, MAVLink.MAV_FRAME.GLOBAL, 0, 1, use_int);
                        if (homeans != MAVLink.MAV_MISSION_RESULT.MAV_MISSION_ACCEPTED)
                        {
                            if (homeans != MAVLink.MAV_MISSION_RESULT.MAV_MISSION_INVALID_SEQUENCE)
                            {
                                //   CustomMessageBox.Show(Strings.ErrorRejectedByMAV, Strings.ERROR);
                                return;
                            }
                        }
                        a++;
                    }
                }
                else
                {
                    use_int = false;
                }

                // define the default frame.
                MAVLink.MAV_FRAME frame = MAVLink.MAV_FRAME.GLOBAL_RELATIVE_ALT;

                // get the command list from the datagrid
                //     var commandlist = GetCommandList();

                // process commandlist to the mav
                for (a = 1; a <= waypointFile.Commands.Count; a++)
                {
                    //   var temp = waypointFile.Commands[a - 1];
                    var wp = new Locationwp();

                    wp.id = waypointFile.Commands[a - 1].Command;
                    wp.p1 = (float)waypointFile.Commands[a - 1].Param1;
                    wp.alt = (float)waypointFile.Commands[a - 1].Alt;
                    wp.lat = (float)waypointFile.Commands[a - 1].Lat;
                    wp.lng = (float)waypointFile.Commands[a - 1].Lng;
                    wp.p2 = (float)waypointFile.Commands[a - 1].Param2;
                    wp.p3 = (float)waypointFile.Commands[a - 1].Param3;
                    wp.p4 = (float)waypointFile.Commands[a - 1].Param4;
                    //((ProgressReporterDialogue)sender).UpdateProgressAndStatus(a * 100 / Commands.Rows.Count,
                    //    "Setting WP " + a);

                    // make sure we are using the correct frame for these commands
                    if (wp.id < (ushort)MAVLink.MAV_CMD.LAST || wp.id == (ushort)MAVLink.MAV_CMD.DO_SET_HOME)
                    {
                        frame = waypointFile.AltMode;
                    }

                    // handle current wp upload number
                    int uploadwpno = a;
                    if (ComPort.MAV.apname == MAVLink.MAV_AUTOPILOT.PX4)
                        uploadwpno--;

                    // try send the wp
                    MAVLink.MAV_MISSION_RESULT ans = ComPort.setWP(wp, (ushort)(uploadwpno), frame, 0, 1, use_int);

                    // we timed out while uploading wps/ command wasnt replaced/ command wasnt added
                    if (ans == MAVLink.MAV_MISSION_RESULT.MAV_MISSION_ERROR)
                    {
                        // resend for partial upload
                        ComPort.setWPPartialUpdate((ushort)(uploadwpno), totalwpcountforupload);
                        // reupload this point.
                        ans = ComPort.setWP(wp, (ushort)(uploadwpno), frame, 0, 1, use_int);
                    }

                    if (ans == MAVLink.MAV_MISSION_RESULT.MAV_MISSION_NO_SPACE)
                    {
                        //     sender.doWorkArgs.ErrorMessage = "Upload failed, please reduce the number of wp's";
                        return;
                    }
                    if (ans == MAVLink.MAV_MISSION_RESULT.MAV_MISSION_INVALID)
                    {
                        //      sender.doWorkArgs.ErrorMessage =
                        //          "Upload failed, mission was rejected byt the Mav,\n item had a bad option wp# " + a + " " +
                        //          ans;
                        return;
                    }
                    if (ans == MAVLink.MAV_MISSION_RESULT.MAV_MISSION_INVALID_SEQUENCE)
                    {
                        // invalid sequence can only occur if we failed to see a response from the apm when we sent the request.
                        // or there is io lag and we send 2 mission_items and get 2 responces, one valid, one a ack of the second send

                        // the ans is received via mission_ack, so we dont know for certain what our current request is for. as we may have lost the mission_request

                        // get requested wp no - 1;
                        a = ComPort.getRequestedWPNo() - 1;

                        continue;
                    }
                    if (ans != MAVLink.MAV_MISSION_RESULT.MAV_MISSION_ACCEPTED)
                    {
                        //sender.doWorkArgs.ErrorMessage = "Upload wps failed " + Enum.Parse(typeof(MAVLink.MAV_CMD), temp.id.ToString()) +
                        //                 " " + Enum.Parse(typeof(MAVLink.MAV_MISSION_RESULT), ans.ToString());
                        return;
                    }
                }

                ComPort.setWPACK();

            }
            catch (Exception ex)
            {
                // log.Error(ex);
                MainV2.comPort.giveComport = false;
                throw;
            }

            MainV2.comPort.giveComport = false;
        }
        private void FlyByMap(MAVLinkInterface ComPort, FlightRoute waypointFile)
        {
            int batteryPercentage = (int)((ComPort.MAV.cs.battery_voltage - Double.Parse(_vehicleFile.MinimumBattery)) / (Double.Parse(_vehicleFile.MaximumBattery) - Double.Parse(_vehicleFile.MinimumBattery)) * 100);
            if (batteryPercentage < 80)
            {
                Core.CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_LAW_BATTERY);
            }
            while (batteryPercentage < 80) { }
            var result = false;

            if (_landingStation == null)
            {
                if (_vehicleFile.LandingStationIp.Replace(" ", "") != "..." && _vehicleFile.LandingStationPort.Replace(" ", "") != "...")
                {
                    ConnectLandingStation(_vehicleFile.LandingStationIp.Replace(" ", ""), _vehicleFile.LandingStationPort.Replace(" ", ""));
                    try
                    {
                        Core.CpxHost.Instance.UI.setLandingStation(_landingStation);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message+"WHATTTTTT");
                    }
                }
            }
            if (_landingStation != null)
            {
                //result = _landingStation.OpenLandingStation(10000);
                //_landingStation.Disconnect();
                result = _vehicleConnection.OpenLandingStation();
                //_vehicleConnection
                if (!result)
                {
                    return;
                }
            }
            // set mode GUIDED in order to enable doArm
            Task.Delay(3 * 1000).Wait();
            while (ComPort.MAV.cs.gpshdop >= 1.4 || ComPort.MAV.cs.satcount < 9) { Thread.Sleep(5); }
            ComPort.setMode("GUIDED");
            ComPort.doARM(true);
            //CHECK HDOP AND GPS
            // get altitude
            var alt = (float)waypointFile.HomeLocation.Alt;
            alt = 15;
            // takeoff the drone

            ComPort.doCommand(MAVLink.MAV_CMD.TAKEOFF, 0, 0, 0, 0, 0, 0, alt, true);
            // calculate wait delay for takeoff (supposing the speed is 2 meter/second)
            var waitForAltitude = (int)Math.Ceiling(1000 * (decimal)alt);

            // wait until the altitude is reached
            Task.Delay(waitForAltitude).Wait();

            if (_landingStation != null)
            {
                if (ComPort.MAV.cs.armed && ComPort.MAV.cs.alt > 10)
                {
                    //_landingStation.CloseLandingStation(1000);
                    //_landingStation.Disconnect();
                    _vehicleConnection.CloseLandingStation(false);
                }
                else
                {
                    ComPort.doARM(false);
                }

                if (!ComPort.MAV.cs.armed)
                {
                    return;
                }
            }
            //Landing station logic
            CpxHost.Instance.ComPort.setParam((byte)MainV2.comPort.sysidcurrent, (byte)MainV2.comPort.compidcurrent, "MNT_RC_IN_TILT", 6);
            CpxHost.Instance.ComPort.setParam((byte)MainV2.comPort.sysidcurrent, (byte)MainV2.comPort.compidcurrent, "MNT_STAB_TILT", 1);

            ComPort.setMode("AUTO");

            int param1 = 0;
            int param3 = 1;
            ComPort.doCommand(MAVLink.MAV_CMD.MISSION_START, param1, 0, param3, 0, 0, 0, 0);

            Task.Delay(1000).Wait();
            bool landing = false;
            // monitor the flight until the vehicle is disarmed
            while (ComPort.MAV.cs.armed)
            {
                Task.Delay(1000).Wait();
                // if vehicle target is first waypoint (reached the altitude) 

                if (ComPort.MAV.cs.wpno == waypointFile.Commands.Count && !landing)
                {
                    if (_landingStation != null)
                    {
                        CpxHost.Instance.ComPort.setParam((byte)MainV2.comPort.sysidcurrent, (byte)MainV2.comPort.compidcurrent, "MNT_RC_IN_TILT", 8);
                        CpxHost.Instance.ComPort.setParam((byte)MainV2.comPort.sysidcurrent, (byte)MainV2.comPort.compidcurrent, "MNT_STAB_TILT", 0);
                        //result = _landingStation.OpenLandingStation(10000);
                        result = _vehicleConnection.OpenLandingStation();
                        //_landingStation.Disconnect();
                        if (!result)
                        {
                            return;
                        }
                        landing = true;

                    }
                    ComPort.setMode("RTL");
                }


            }
            ComPort.setMode("LOITER");
            if (_landingStation != null)
            {
                _landingStation.LandingStationPos(1000,CMD_CLOSE);
                _landingStation.ChargeDrone(20000);
                _landingStation.Disconnect();
            }
        }

        private void ConnectLandingStation(string ip, string port)
        {
            if (!string.IsNullOrEmpty(ip))
            {
                if (!string.IsNullOrEmpty(port))
                {
                    //MessageBox.Show(ip + " " + port);
                    _landingStation = new CpxLandingStation
                    {
                        Host = ip.Replace(" ", ""),
                        Port = port
                    };
                    try
                    {//74070394
                        _landingStation.Open();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("EXCEPTION");
                    }
                }
            }
            else
            {
                var config = AppSettings.LoadConfiguration();
                _landingStation = new CpxLandingStation
                {
                    Host = config.LandingStationIP?.Replace(" ", ""),
                    Port = config.LandingStationPort
                };
                try
                {
                    _landingStation.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("EXCEPTION");
                }
            }
        }


        public DateTime NextRun
        {
            get
            {
                return _nextRun;
            }
        }


        private void OnJobStatusChange(JobStatusEnum status)
        {
            if (JobStatusChange != null)
            {
                var args = new JobStatusChangeEventArgs(status, _vehicleId, _vehicleConnection.Vehicle.Name, _nextRun);
                JobStatusChange.Invoke(this, args);
            }
        }


        private void CalculateNextRun()
        {
            var now = DateTime.Now.TimeOfDay;
            var day = (int)DateTime.Now.DayOfWeek;

            foreach (var trigger in _triggers.Where(x => x.Enabled && _flightRouteFiles.Any(y => y.Id == x.ActionId)))
            {
                var nextRun = CalculateNextRun(trigger);

                if (nextRun < _nextRun || _nextRun < DateTime.Now)
                {
                    _nextRun = nextRun;
                    _nextActionId = trigger.ActionId;
                }
            }
        }
        public DateTime CalculateOneTimeNextRun(Trigger trigger)
        {
            var now = DateTime.Now.TimeOfDay;
            var day = (int)DateTime.Now.DayOfWeek;

            if (trigger.Days[day] == 1)
            {
                if (trigger.StartTime.TimeOfDay > now)
                {
                    return DateTime.Now.Date.AddTicks(trigger.StartTime.TimeOfDay.Ticks);
                }
            }
            return CalculateNextRun(trigger, day + 1);
        }

        public DateTime CalculateRepeatingTimeNextRun(Trigger trigger)
        {
            var now = DateTime.Now.TimeOfDay;
            var day = (int)DateTime.Now.DayOfWeek;

            now = new TimeSpan(now.Hours, now.Minutes, 0);
            //MessageBox.Show(now.ToString());
            if (trigger.Days[day] == 1)
            {
                if (trigger.StartTime.TimeOfDay > now)
                {
                    return DateTime.Now.Date.AddTicks(trigger.StartTime.TimeOfDay.Ticks);
                }
                var nextRun = new TimeSpan(trigger.StartTime.TimeOfDay.Hours, trigger.StartTime.TimeOfDay.Minutes, 0);

                while (nextRun <= now)
                {
                    nextRun = now + new TimeSpan(0, trigger.RepeatInterval, 0);
                }

                if (nextRun <= trigger.FinishTime.TimeOfDay)
                {
                    if (isFirst == false)
                    {
                        return DateTime.Now.Date.AddTicks(nextRun.Ticks);
                    }
                    else
                    {
                        isFirst = false;
                        nextRun = now + new TimeSpan(0, 1, 0);

                        return DateTime.Now.Date.AddTicks(nextRun.Ticks);
                    }

                }
            }
            return CalculateNextRun(trigger, day + 1);
        }
        public DateTime CalculateNextRun(Trigger trigger, int startDay)
        {
            var offset = startDay;

            for (int i = 0; i < trigger.Days.Length; i++)
            {

                if (offset >= trigger.Days.Length)
                {
                    offset = 0;
                }

                if (trigger.Days[offset] == 1)
                {
                    return DateTime.Now.AddDays((i + 1)).Date.AddTicks(trigger.StartTime.TimeOfDay.Ticks);
                }
                offset++;
            }

            return DateTime.MaxValue;
        }
        public DateTime CalculateNextRun(Trigger trigger)
        {

            if (trigger.TriggerType == TriggerTypeEnum.OneTime)
            {
                return CalculateOneTimeNextRun(trigger);
            }
            else
            {
                return CalculateRepeatingTimeNextRun(trigger);
            }
        }
    }
}
