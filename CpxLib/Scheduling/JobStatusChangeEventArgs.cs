﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Scheduling
{ 
    public enum JobStatusEnum
    {
        Initialized,
        InMission,
        Waiting,
        Paused,
        MissionCompleted,
        Stopped
    }

    public class JobStatusChangeEventArgs: EventArgs
    {

        public JobStatusChangeEventArgs(JobStatusEnum status, string mavId, string drone)
        {
            Status = status;
            MavId = mavId;
            Drone = drone;
        }

        public JobStatusChangeEventArgs(JobStatusEnum status, string mavId, string drone, DateTime nextRunTime) : this (status, mavId, drone)
        {
            NextRunTime = nextRunTime;
        }

        public JobStatusEnum Status { get; private set; }
        public string MavId { get; private set; }
        public DateTime NextRunTime { get; private set; }
        public string Drone { get; private set; }
    }
}
