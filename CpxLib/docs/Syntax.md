[![CopterPix](./img/logo.png)](https://www.copterpix.pro)      
 
Markdown syntax 
---


Contents
---
  + [Styles and Table](#styles-and-table)  
  + [Headings](#headings)
  + [Quoting text](#quoting-text)
 
  
Styles and Table
---

| Style         | Syntax        | Example      | Output         |
| ------------- | ------------- |------------- | -------------  |
| Bold                   | ** ** or __ __   |   \*\*This is bold text** |  **This is bold text**   |
| Italic                 |  * * or _ _  |  \*This text is italicized*  |  *This text is italicized*   |
| Strikethrough          |  ~~ ~~  |   \~\~This was mistaken text~~ | ~~This was mistaken text~~   |
| Bold and nested italic | ** ** and _ _ | \*\*This text is \_extremely_ important**   | **This text is _extremely_ important**    |
| All bold and italic    |  *** ***  |  \*\*\*All this text is important***  |  ***All this text is important***  |
 


Headings
---
# The largest heading
## The second largest heading
###### The smallest heading  
      
# Quoting text
 

In the words of Abraham Lincoln:
> Pardon my French

# Quoting code

```
function test() {
  console.log("notice the blank line before this function?");
}
```
####or inline quoting
Use `git status` to list all new or modified files that haven't yet been committed.

# Tasks
- [x] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request
 
require "Tasks"

# Lists   
1. First list item
   - First nested list item
     - Second nested list item 
        1. sdfsdfsfdsfdsf
        2. asfasdads
 
 
- first item
- second item 
 
 # Link
[Description](relative path e.g. docs/syntax.md) 

[Syntax for md](docs/syntax.md) 

[Introduction](#contents)
