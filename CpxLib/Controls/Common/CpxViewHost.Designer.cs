﻿namespace CpxLib.Controls.Common
{
    partial class CpxViewHost
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlWorkspace = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // pnlWorkspace
            // 
            this.pnlWorkspace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWorkspace.Location = new System.Drawing.Point(0, 0);
            this.pnlWorkspace.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnlWorkspace.Name = "pnlWorkspace";
            this.pnlWorkspace.Size = new System.Drawing.Size(622, 498);
            this.pnlWorkspace.TabIndex = 12;
            this.pnlWorkspace.Tag = "JOJO";
            // 
            // CpxViewHost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlWorkspace);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "CpxViewHost";
            this.Size = new System.Drawing.Size(622, 498);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlWorkspace;
    }
}
