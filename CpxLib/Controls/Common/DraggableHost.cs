﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel.Design;

namespace CpxLib.Controls.Common
{
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public partial class DraggableHost : UserControl
    {


        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        private bool _allowResize;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public DraggableHost()
        {
            InitializeComponent();
        }


        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

         

        public string Title
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }
        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            OnMouseDown(e);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            OnMouseDown(e);
        }

        private void resizer_MouseDown(object sender, MouseEventArgs e)
        {
            _allowResize = true;
        }

        private void resizer_MouseUp(object sender, MouseEventArgs e)
        {
            _allowResize = false;
        }

        private void resizer_MouseMove(object sender, MouseEventArgs e)
        {
            if (_allowResize)
            {
                this.Height = resizer.Top + e.Y;
                this.Width = resizer.Left + e.X;
            }
        }
    }
}
