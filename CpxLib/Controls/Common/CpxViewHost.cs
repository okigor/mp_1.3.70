﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MissionPlanner.GCSViews;
using CpxLib.Core;

namespace CpxLib.Controls.Common
{
    public partial class CpxViewHost : MyUserControl
    {
        public CpxViewHost()
        {
            InitializeComponent();
        }

        public Control HostedControl
        {
            get
            {
                return pnlWorkspace.Controls.Cast<Control>().FirstOrDefault();

            }
            set
            {
                pnlWorkspace.Controls.Clear();

                pnlWorkspace.Controls.Add(value);

                value.Dock = DockStyle.Fill;

                value.Visible = true;
            }


        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            UIUtils.ShowFlightData();
        }
    }
}
