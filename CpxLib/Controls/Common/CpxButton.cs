﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Controls.Common
{
    public class CpxButton : Button
    {
        public CpxButton()
        {
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, false);
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, false);
            SetStyle(ControlStyles.SupportsTransparentBackColor, false);
            SetStyle(ControlStyles.ContainerControl, false);
        }

        [Browsable(true)]
        public string ButtonAction { get; set; }
        [Browsable(true)]
        [DefaultValue(ButtonShapeEnum.Default)]
        [Localizable(true)]
        public ButtonShapeEnum ButtonShape { get; set; }
        protected override void OnPaint(PaintEventArgs pevent)
        {

            if (ButtonShape == ButtonShapeEnum.Ellipse)
            {
                var grPath = new GraphicsPath();
                grPath.AddEllipse(0, 0, ClientSize.Width, ClientSize.Height);
                Region = new Region(grPath);
            }
            base.OnPaint(pevent);
        }

        public enum ButtonShapeEnum
        {
            Default,
            Ellipse
        }
    }
}
