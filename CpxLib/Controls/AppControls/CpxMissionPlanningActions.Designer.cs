﻿
namespace CpxLib.Controls.AppControls
{
    partial class CpxMissionPlanningActions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CpxMissionPlanningActions));
            this.btnClearMission = new CpxLib.Controls.Common.CpxButton();
            this.btnGeoFence = new CpxLib.Controls.Common.CpxButton();
            this.btnSurveyGrid = new CpxLib.Controls.Common.CpxButton();
            this.btnClearPolygon = new CpxLib.Controls.Common.CpxButton();
            this.btnClearGeoFence = new CpxLib.Controls.Common.CpxButton();
            this.SuspendLayout();
            // 
            // btnClearMission
            // 
            this.btnClearMission.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnClearMission.ButtonAction = null;
            resources.ApplyResources(this.btnClearMission, "btnClearMission");
            this.btnClearMission.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnClearMission.ForeColor = System.Drawing.Color.White;
            this.btnClearMission.Name = "btnClearMission";
            this.btnClearMission.UseVisualStyleBackColor = false;
            // 
            // btnGeoFence
            // 
            this.btnGeoFence.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnGeoFence.ButtonAction = null;
            this.btnGeoFence.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            resources.ApplyResources(this.btnGeoFence, "btnGeoFence");
            this.btnGeoFence.ForeColor = System.Drawing.Color.White;
            this.btnGeoFence.Name = "btnGeoFence";
            this.btnGeoFence.UseVisualStyleBackColor = false;
            // 
            // btnSurveyGrid
            // 
            this.btnSurveyGrid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnSurveyGrid.ButtonAction = null;
            this.btnSurveyGrid.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnSurveyGrid.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.btnSurveyGrid, "btnSurveyGrid");
            this.btnSurveyGrid.Name = "btnSurveyGrid";
            this.btnSurveyGrid.UseVisualStyleBackColor = false;
            // 
            // btnClearPolygon
            // 
            this.btnClearPolygon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnClearPolygon.ButtonAction = null;
            resources.ApplyResources(this.btnClearPolygon, "btnClearPolygon");
            this.btnClearPolygon.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnClearPolygon.ForeColor = System.Drawing.Color.White;
            this.btnClearPolygon.Name = "btnClearPolygon";
            this.btnClearPolygon.UseVisualStyleBackColor = false;
            // 
            // btnClearGeoFence
            // 
            this.btnClearGeoFence.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnClearGeoFence.ButtonAction = null;
            this.btnClearGeoFence.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnClearGeoFence.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.btnClearGeoFence, "btnClearGeoFence");
            this.btnClearGeoFence.Name = "btnClearGeoFence";
            this.btnClearGeoFence.UseVisualStyleBackColor = false;
            // 
            // CpxMissionPlanningActions
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSurveyGrid);
            this.Controls.Add(this.btnClearGeoFence);
            this.Controls.Add(this.btnGeoFence);
            this.Controls.Add(this.btnClearPolygon);
            this.Controls.Add(this.btnClearMission);
            this.Name = "CpxMissionPlanningActions";
            this.ResumeLayout(false);

        }

        #endregion

        public Common.CpxButton btnClearMission;
        public Common.CpxButton btnGeoFence;
        public Common.CpxButton btnSurveyGrid;
        public Common.CpxButton btnClearPolygon;
        public Common.CpxButton btnClearGeoFence;
    }
}
