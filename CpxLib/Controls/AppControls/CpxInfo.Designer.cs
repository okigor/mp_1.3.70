﻿using CpxLib.Controls.Common;

namespace CpxLib.Controls.AppControls
{
    partial class CpxInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CpxInfo));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picBatteryLevel = new System.Windows.Forms.PictureBox();
            this.lblBatteryLevel = new System.Windows.Forms.Label();
            this.line7 = new CpxLib.Controls.Line();
            this.telemetryItem7 = new CpxLib.Controls.AppControls.TelemetryDataItem();
            this.line6 = new CpxLib.Controls.Line();
            this.telemetryItem6 = new CpxLib.Controls.AppControls.TelemetryDataItem();
            this.line5 = new CpxLib.Controls.Line();
            this.telemetryItem5 = new CpxLib.Controls.AppControls.TelemetryDataItem();
            this.line4 = new CpxLib.Controls.Line();
            this.telemetryItem4 = new CpxLib.Controls.AppControls.TelemetryDataItem();
            this.line3 = new CpxLib.Controls.Line();
            this.telemetryItem3 = new CpxLib.Controls.AppControls.TelemetryDataItem();
            this.line2 = new CpxLib.Controls.Line();
            this.telemetryItem2 = new CpxLib.Controls.AppControls.TelemetryDataItem();
            this.line1 = new CpxLib.Controls.Line();
            this.telemetryItem1 = new CpxLib.Controls.AppControls.TelemetryDataItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBatteryLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::CpxLib.Properties.Resources.connection_on;
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // picBatteryLevel
            // 
            this.picBatteryLevel.BackgroundImage = global::CpxLib.Properties.Resources.battery_1;
            resources.ApplyResources(this.picBatteryLevel, "picBatteryLevel");
            this.picBatteryLevel.Name = "picBatteryLevel";
            this.picBatteryLevel.TabStop = false;
            // 
            // lblBatteryLevel
            // 
            resources.ApplyResources(this.lblBatteryLevel, "lblBatteryLevel");
            this.lblBatteryLevel.ForeColor = System.Drawing.Color.White;
            this.lblBatteryLevel.Name = "lblBatteryLevel";
            // 
            // line7
            // 
            this.line7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(2)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.line7, "line7");
            this.line7.Name = "line7";
            // 
            // telemetryItem7
            // 
            resources.ApplyResources(this.telemetryItem7, "telemetryItem7");
            this.telemetryItem7.ForeColor = System.Drawing.Color.White;
            this.telemetryItem7.Key = null;
            this.telemetryItem7.Name = "telemetryItem7";
            this.telemetryItem7.Tag = "custom";
            // 
            // line6
            // 
            this.line6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(2)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.line6, "line6");
            this.line6.Name = "line6";
            // 
            // telemetryItem6
            // 
            resources.ApplyResources(this.telemetryItem6, "telemetryItem6");
            this.telemetryItem6.ForeColor = System.Drawing.Color.White;
            this.telemetryItem6.Key = null;
            this.telemetryItem6.Name = "telemetryItem6";
            this.telemetryItem6.Tag = "custom";
            // 
            // line5
            // 
            this.line5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(2)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.line5, "line5");
            this.line5.Name = "line5";
            // 
            // telemetryItem5
            // 
            resources.ApplyResources(this.telemetryItem5, "telemetryItem5");
            this.telemetryItem5.ForeColor = System.Drawing.Color.White;
            this.telemetryItem5.Key = null;
            this.telemetryItem5.Name = "telemetryItem5";
            this.telemetryItem5.Tag = "custom";
            // 
            // line4
            // 
            this.line4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(2)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.line4, "line4");
            this.line4.Name = "line4";
            // 
            // telemetryItem4
            // 
            resources.ApplyResources(this.telemetryItem4, "telemetryItem4");
            this.telemetryItem4.ForeColor = System.Drawing.Color.White;
            this.telemetryItem4.Key = null;
            this.telemetryItem4.Name = "telemetryItem4";
            this.telemetryItem4.Tag = "custom";
            // 
            // line3
            // 
            this.line3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(2)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.line3, "line3");
            this.line3.Name = "line3";
            // 
            // telemetryItem3
            // 
            resources.ApplyResources(this.telemetryItem3, "telemetryItem3");
            this.telemetryItem3.ForeColor = System.Drawing.Color.White;
            this.telemetryItem3.Key = null;
            this.telemetryItem3.Name = "telemetryItem3";
            this.telemetryItem3.Tag = "custom";
            // 
            // line2
            // 
            this.line2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(2)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.line2, "line2");
            this.line2.Name = "line2";
            // 
            // telemetryItem2
            // 
            resources.ApplyResources(this.telemetryItem2, "telemetryItem2");
            this.telemetryItem2.ForeColor = System.Drawing.Color.White;
            this.telemetryItem2.Key = null;
            this.telemetryItem2.Name = "telemetryItem2";
            this.telemetryItem2.Tag = "custom";
            this.telemetryItem2.Load += new System.EventHandler(this.telemetryItem2_Load);
            // 
            // line1
            // 
            this.line1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(2)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.line1, "line1");
            this.line1.Name = "line1";
            // 
            // telemetryItem1
            // 
            resources.ApplyResources(this.telemetryItem1, "telemetryItem1");
            this.telemetryItem1.ForeColor = System.Drawing.Color.White;
            this.telemetryItem1.Key = null;
            this.telemetryItem1.Name = "telemetryItem1";
            this.telemetryItem1.Tag = "custom";
            // 
            // CpxInfo
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.Controls.Add(this.lblBatteryLevel);
            this.Controls.Add(this.picBatteryLevel);
            this.Controls.Add(this.line7);
            this.Controls.Add(this.telemetryItem7);
            this.Controls.Add(this.line6);
            this.Controls.Add(this.telemetryItem6);
            this.Controls.Add(this.line5);
            this.Controls.Add(this.telemetryItem5);
            this.Controls.Add(this.line4);
            this.Controls.Add(this.telemetryItem4);
            this.Controls.Add(this.line3);
            this.Controls.Add(this.telemetryItem3);
            this.Controls.Add(this.line2);
            this.Controls.Add(this.telemetryItem2);
            this.Controls.Add(this.line1);
            this.Controls.Add(this.telemetryItem1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "CpxInfo";
            this.Load += new System.EventHandler(this.CpxInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBatteryLevel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private  TelemetryDataItem telemetryItem1;
        private  TelemetryDataItem telemetryItem2;
        private  TelemetryDataItem telemetryItem3;
        private  TelemetryDataItem telemetryItem4;
        private  TelemetryDataItem telemetryItem5;
        private  TelemetryDataItem telemetryItem6;
        private  TelemetryDataItem telemetryItem7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picBatteryLevel;
        private System.Windows.Forms.Label lblBatteryLevel;
        private Line line1;
        private Line line2;
        private Line line3;
        private Line line4;
        private Line line5;
        private Line line6;
        private Line line7;
    }
}
