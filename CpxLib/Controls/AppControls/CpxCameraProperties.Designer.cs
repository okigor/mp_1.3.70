﻿using System;

namespace CpxLib.Controls.AppControls
{
    partial class CpxCameraProperties
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CpxCameraProperties));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.zoomBar = new CpxLib.Controls.Common.ColorSlider();
            this.panel4 = new System.Windows.Forms.Panel();
            this.BrightnessBar = new CpxLib.Controls.Common.ColorSlider();
            this.panel5 = new System.Windows.Forms.Panel();
            this.contrastBar = new CpxLib.Controls.Common.ColorSlider();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCapture = new CpxLib.Controls.Common.CpxButton();
            this.btnRecord = new CpxLib.Controls.Common.CpxButton();
            this.Polarity = new CpxLib.Controls.Common.CpxButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel5);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.zoomBar);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // zoomBar
            // 
            this.zoomBar.BackColor = System.Drawing.Color.Transparent;
            this.zoomBar.BarInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.BarPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.BarPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.BorderRoundRectSize = new System.Drawing.Size(8, 8);
            resources.ApplyResources(this.zoomBar, "zoomBar");
            this.zoomBar.ElapsedInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.ElapsedPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.ElapsedPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.ForeColor = System.Drawing.Color.White;
            this.zoomBar.LargeChange = ((uint)(5u));
            this.zoomBar.Name = "zoomBar";
            this.zoomBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.zoomBar.ScaleDivisions = 10;
            this.zoomBar.ScaleSubDivisions = 5;
            this.zoomBar.ShowDivisionsText = true;
            this.zoomBar.ShowSmallScale = false;
            this.zoomBar.SmallChange = ((uint)(1u));
            this.zoomBar.ThumbImage = global::CpxLib.Properties.Resources.zoom_button;
            this.zoomBar.ThumbInnerColor = System.Drawing.Color.Transparent;
            this.zoomBar.ThumbOuterColor = System.Drawing.Color.Transparent;
            this.zoomBar.ThumbPenColor = System.Drawing.Color.Transparent;
            this.zoomBar.ThumbRoundRectSize = new System.Drawing.Size(16, 16);
            this.zoomBar.ThumbSize = new System.Drawing.Size(16, 16);
            this.zoomBar.TickAdd = 0F;
            this.zoomBar.TickColor = System.Drawing.Color.White;
            this.zoomBar.TickDivide = 0F;
            this.zoomBar.Value = 0;
            this.zoomBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.zoomBar_Scroll);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.BrightnessBar);
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            // 
            // BrightnessBar
            // 
            this.BrightnessBar.BackColor = System.Drawing.Color.Transparent;
            this.BrightnessBar.BarInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.BrightnessBar.BarPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.BrightnessBar.BarPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.BrightnessBar.BorderRoundRectSize = new System.Drawing.Size(8, 8);
            resources.ApplyResources(this.BrightnessBar, "BrightnessBar");
            this.BrightnessBar.ElapsedInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.BrightnessBar.ElapsedPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.BrightnessBar.ElapsedPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.BrightnessBar.ForeColor = System.Drawing.Color.White;
            this.BrightnessBar.LargeChange = ((uint)(5u));
            this.BrightnessBar.Name = "BrightnessBar";
            this.BrightnessBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.BrightnessBar.ScaleDivisions = 10;
            this.BrightnessBar.ScaleSubDivisions = 5;
            this.BrightnessBar.ShowDivisionsText = true;
            this.BrightnessBar.ShowSmallScale = false;
            this.BrightnessBar.SmallChange = ((uint)(1u));
            this.BrightnessBar.ThumbImage = global::CpxLib.Properties.Resources.zoom_button;
            this.BrightnessBar.ThumbInnerColor = System.Drawing.Color.Transparent;
            this.BrightnessBar.ThumbOuterColor = System.Drawing.Color.Transparent;
            this.BrightnessBar.ThumbPenColor = System.Drawing.Color.Transparent;
            this.BrightnessBar.ThumbRoundRectSize = new System.Drawing.Size(16, 16);
            this.BrightnessBar.ThumbSize = new System.Drawing.Size(16, 16);
            this.BrightnessBar.TickAdd = 0F;
            this.BrightnessBar.TickColor = System.Drawing.Color.White;
            this.BrightnessBar.TickDivide = 0F;
            this.BrightnessBar.Value = 50;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.contrastBar);
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // contrastBar
            // 
            this.contrastBar.BackColor = System.Drawing.Color.Transparent;
            this.contrastBar.BarInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.contrastBar.BarPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.contrastBar.BarPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.contrastBar.BorderRoundRectSize = new System.Drawing.Size(8, 8);
            resources.ApplyResources(this.contrastBar, "contrastBar");
            this.contrastBar.ElapsedInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.contrastBar.ElapsedPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.contrastBar.ElapsedPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.contrastBar.ForeColor = System.Drawing.Color.White;
            this.contrastBar.LargeChange = ((uint)(5u));
            this.contrastBar.Name = "contrastBar";
            this.contrastBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.contrastBar.ScaleDivisions = 10;
            this.contrastBar.ScaleSubDivisions = 5;
            this.contrastBar.ShowDivisionsText = true;
            this.contrastBar.ShowSmallScale = false;
            this.contrastBar.SmallChange = ((uint)(1u));
            this.contrastBar.ThumbImage = global::CpxLib.Properties.Resources.zoom_button;
            this.contrastBar.ThumbInnerColor = System.Drawing.Color.Transparent;
            this.contrastBar.ThumbOuterColor = System.Drawing.Color.Transparent;
            this.contrastBar.ThumbPenColor = System.Drawing.Color.Transparent;
            this.contrastBar.ThumbRoundRectSize = new System.Drawing.Size(16, 16);
            this.contrastBar.ThumbSize = new System.Drawing.Size(16, 16);
            this.contrastBar.TickAdd = 0F;
            this.contrastBar.TickColor = System.Drawing.Color.White;
            this.contrastBar.TickDivide = 0F;
            this.contrastBar.Value = 50;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCapture);
            this.panel2.Controls.Add(this.btnRecord);
            this.panel2.Controls.Add(this.Polarity);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnCapture
            // 
            this.btnCapture.ButtonAction = null;
            this.btnCapture.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.btnCapture, "btnCapture");
            this.btnCapture.Image = global::CpxLib.Properties.Resources.capture;
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.UseVisualStyleBackColor = true;
            // 
            // btnRecord
            // 
            this.btnRecord.ButtonAction = "stop";
            this.btnRecord.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.btnRecord, "btnRecord");
            this.btnRecord.Image = global::CpxLib.Properties.Resources.rec;
            this.btnRecord.Name = "btnRecord";
            this.btnRecord.UseVisualStyleBackColor = true;
            this.btnRecord.Click += new System.EventHandler(this.btnRecord_Click);
            // 
            // Polarity
            // 
            this.Polarity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.Polarity.ButtonAction = "15";
            this.Polarity.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.Polarity, "Polarity");
            this.Polarity.ForeColor = System.Drawing.Color.White;
            this.Polarity.Image = global::CpxLib.Properties.Resources.whiteHot1;
            this.Polarity.Name = "Polarity";
            this.Polarity.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Name = "label3";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Name = "label1";
            // 
            // CpxCameraProperties
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "CpxCameraProperties";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public Common.ColorSlider zoomBar;
        private System.Windows.Forms.Panel panel5;
        public Common.ColorSlider contrastBar;
        private System.Windows.Forms.Panel panel4;
        public Common.ColorSlider BrightnessBar;
        private System.Windows.Forms.Panel panel3;
        public Common.CpxButton Polarity;
        public Common.CpxButton btnCapture;
        public Common.CpxButton btnRecord;
    }
}
