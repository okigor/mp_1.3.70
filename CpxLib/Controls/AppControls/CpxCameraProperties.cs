﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Controls.AppControls
{
    public partial class CpxCameraProperties : UserControl
    {
        public bool takeParams { get; set; }
        public string cameraType { get; set; }
        public int lastZoom { get; set; }
        public int lastBrightness { get; set; }
        public string lastPolarity { get; set; }
        public int lastContrast { get; set; }
        public string lastDay { get; set; }
        public string lastNight { get; set; }

        public bool _ignoreEvents = false;
        public CpxCameraProperties()
        {
            InitializeComponent();
            takeParams = false;
            cameraType = "day";
            lastZoom = 0;
            lastBrightness = 0;
            lastContrast = 0;
            lastPolarity = "0";
            lastDay = "";
            lastNight = "";

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnRecord_Click(object sender, EventArgs e)
        {

        }

        private void zoomBar_Scroll(object sender, ScrollEventArgs e)
        {

        }
    }
}
