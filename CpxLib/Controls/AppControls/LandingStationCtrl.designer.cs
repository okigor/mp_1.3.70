﻿namespace CpxLib.Controls
{
    partial class LandingStationCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LandingStationCtrl));
            this.LSLable = new CpxLib.Controls.Common.CpxLabel();
            this.btnContinue = new CpxLib.Controls.Common.CpxButton();
            this.btnCharge = new CpxLib.Controls.Common.CpxButton();
            this.btnStop = new CpxLib.Controls.Common.CpxButton();
            this.close = new CpxLib.Controls.Common.CpxButton();
            this.open = new CpxLib.Controls.Common.CpxButton();
            this.SuspendLayout();
            // 
            // LSLable
            // 
            resources.ApplyResources(this.LSLable, "LSLable");
            this.LSLable.ForeColor = System.Drawing.Color.White;
            this.LSLable.Name = "LSLable";
            this.LSLable.Click += new System.EventHandler(this.LSLable_Click);
            // 
            // btnContinue
            // 
            resources.ApplyResources(this.btnContinue, "btnContinue");
            this.btnContinue.ButtonAction = null;
            this.btnContinue.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnContinue.ForeColor = System.Drawing.Color.White;
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.UseVisualStyleBackColor = true;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // btnCharge
            // 
            resources.ApplyResources(this.btnCharge, "btnCharge");
            this.btnCharge.ButtonAction = null;
            this.btnCharge.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnCharge.ForeColor = System.Drawing.Color.White;
            this.btnCharge.Name = "btnCharge";
            this.btnCharge.UseVisualStyleBackColor = true;
            this.btnCharge.Click += new System.EventHandler(this.btnCharge_Click);
            // 
            // btnStop
            // 
            resources.ApplyResources(this.btnStop, "btnStop");
            this.btnStop.ButtonAction = null;
            this.btnStop.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnStop.ForeColor = System.Drawing.Color.White;
            this.btnStop.Name = "btnStop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // close
            // 
            resources.ApplyResources(this.close, "close");
            this.close.ButtonAction = null;
            this.close.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.close.ForeColor = System.Drawing.Color.White;
            this.close.Name = "close";
            this.close.Click += new System.EventHandler(this.close_Click_1);
            // 
            // open
            // 
            resources.ApplyResources(this.open, "open");
            this.open.ButtonAction = null;
            this.open.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.open.ForeColor = System.Drawing.Color.White;
            this.open.Name = "open";
            this.open.Click += new System.EventHandler(this.open_Click_1);
            // 
            // LandingStationCtrl
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.Controls.Add(this.LSLable);
            this.Controls.Add(this.btnContinue);
            this.Controls.Add(this.btnCharge);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.close);
            this.Controls.Add(this.open);
            this.Name = "LandingStationCtrl";
            this.ResumeLayout(false);

        }

        #endregion

        private Common.CpxButton open;
        private Common.CpxButton close;
        private Common.CpxButton btnStop;
        private Common.CpxButton btnCharge;
        private Common.CpxButton btnContinue;
        private Common.CpxLabel LSLable;
    }
}
