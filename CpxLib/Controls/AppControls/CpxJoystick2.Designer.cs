﻿using CpxLib.Controls.Common;

namespace CpxLib.Controls.AppControls
{
    partial class CpxJoystick2 
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CpxButtonLeft = new CpxLib.Controls.Common.CpxButton();
            this.cpxButtonUp = new CpxLib.Controls.Common.CpxButton();
            this.cpxButtonDown = new CpxLib.Controls.Common.CpxButton();
            this.cpxButtonRight = new CpxLib.Controls.Common.CpxButton();
            this.SuspendLayout();
            // 
            // CpxButtonLeft
            // 
            this.CpxButtonLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(47)))), ((int)(((byte)(56)))));
            this.CpxButtonLeft.ButtonAction = null;
            this.CpxButtonLeft.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.CpxButtonLeft.FlatAppearance.BorderSize = 0;
            this.CpxButtonLeft.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.CpxButtonLeft.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.CpxButtonLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CpxButtonLeft.ForeColor = System.Drawing.Color.DimGray;
            this.CpxButtonLeft.Image = global::CpxLib.Properties.Resources.leftButton;
            this.CpxButtonLeft.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CpxButtonLeft.Location = new System.Drawing.Point(58, 37);
            this.CpxButtonLeft.Margin = new System.Windows.Forms.Padding(0);
            this.CpxButtonLeft.Name = "CpxButtonLeft";
            this.CpxButtonLeft.Size = new System.Drawing.Size(90, 62);
            this.CpxButtonLeft.TabIndex = 29;
            this.CpxButtonLeft.UseVisualStyleBackColor = false;
            this.CpxButtonLeft.Click += new System.EventHandler(this.CpxButtonLeft_Click);
            // 
            // cpxButtonUp
            // 
            this.cpxButtonUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(47)))), ((int)(((byte)(56)))));
            this.cpxButtonUp.ButtonAction = null;
            this.cpxButtonUp.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.cpxButtonUp.FlatAppearance.BorderSize = 0;
            this.cpxButtonUp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.cpxButtonUp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.cpxButtonUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButtonUp.ForeColor = System.Drawing.Color.DimGray;
            this.cpxButtonUp.Image = global::CpxLib.Properties.Resources.upButton;
            this.cpxButtonUp.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cpxButtonUp.Location = new System.Drawing.Point(172, 37);
            this.cpxButtonUp.Margin = new System.Windows.Forms.Padding(0);
            this.cpxButtonUp.Name = "cpxButtonUp";
            this.cpxButtonUp.Size = new System.Drawing.Size(90, 62);
            this.cpxButtonUp.TabIndex = 28;
            this.cpxButtonUp.UseVisualStyleBackColor = false;
            this.cpxButtonUp.Click += new System.EventHandler(this.cpxButtonUp_Click);
            // 
            // cpxButtonDown
            // 
            this.cpxButtonDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(47)))), ((int)(((byte)(56)))));
            this.cpxButtonDown.ButtonAction = null;
            this.cpxButtonDown.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.cpxButtonDown.FlatAppearance.BorderSize = 0;
            this.cpxButtonDown.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.cpxButtonDown.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.cpxButtonDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButtonDown.ForeColor = System.Drawing.Color.DimGray;
            this.cpxButtonDown.Image = global::CpxLib.Properties.Resources.downButton;
            this.cpxButtonDown.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cpxButtonDown.Location = new System.Drawing.Point(284, 37);
            this.cpxButtonDown.Margin = new System.Windows.Forms.Padding(0);
            this.cpxButtonDown.Name = "cpxButtonDown";
            this.cpxButtonDown.Size = new System.Drawing.Size(90, 62);
            this.cpxButtonDown.TabIndex = 27;
            this.cpxButtonDown.UseVisualStyleBackColor = false;
            this.cpxButtonDown.Click += new System.EventHandler(this.cpxButtonDown_Click);
            // 
            // cpxButtonRight
            // 
            this.cpxButtonRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(47)))), ((int)(((byte)(56)))));
            this.cpxButtonRight.ButtonAction = null;
            this.cpxButtonRight.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.cpxButtonRight.FlatAppearance.BorderSize = 0;
            this.cpxButtonRight.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.cpxButtonRight.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.cpxButtonRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButtonRight.ForeColor = System.Drawing.Color.DimGray;
            this.cpxButtonRight.Image = global::CpxLib.Properties.Resources.rightButton1;
            this.cpxButtonRight.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cpxButtonRight.Location = new System.Drawing.Point(400, 37);
            this.cpxButtonRight.Margin = new System.Windows.Forms.Padding(0);
            this.cpxButtonRight.Name = "cpxButtonRight";
            this.cpxButtonRight.Size = new System.Drawing.Size(90, 62);
            this.cpxButtonRight.TabIndex = 30;
            this.cpxButtonRight.UseVisualStyleBackColor = false;
            this.cpxButtonRight.Click += new System.EventHandler(this.CpxButtonRight_Click);
            // 
            // CpxJoystick2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(47)))), ((int)(((byte)(56)))));
            this.BackColor2 = System.Drawing.Color.Empty;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(169)))), ((int)(((byte)(1)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BorderWidth = 6;
            this.Controls.Add(this.cpxButtonRight);
            this.Controls.Add(this.CpxButtonLeft);
            this.Controls.Add(this.cpxButtonUp);
            this.Controls.Add(this.cpxButtonDown);
            this.Curvature = 20;
            this.CurveMode = ((CpxLib.Controls.Common.CornerCurveMode)((CpxLib.Controls.Common.CornerCurveMode.TopLeft | CpxLib.Controls.Common.CornerCurveMode.TopRight)));
            this.Margin = new System.Windows.Forms.Padding(12, 12, 12, 12);
            this.Name = "CpxJoystick2";
            this.Size = new System.Drawing.Size(520, 136);
            this.Load += new System.EventHandler(this.CpxJoystick2_Load);
            this.ResumeLayout(false);
        }
        #endregion
        private Common.CpxButton cpxButtonUp;
        private Common.CpxButton CpxButtonLeft;
        private Common.CpxButton cpxButtonDown;
        private CpxButton cpxButtonRight;
    }
}
