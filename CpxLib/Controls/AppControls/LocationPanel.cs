﻿using CpxLib.Controls.Common;
using CpxLib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Controls.AppControls
{
    public class LocationPanel: ShapedUserControl
    {
        public CpxLabel lblValue;

        public LocationPanel()
        {
            InitializeComponent();
            lblValue.Font = CpxFonts.GetFont(CpxFontsEnum.Rubik_Medium, 10.0F);
            
        }

        private void InitializeComponent()
        {
            this.lblValue = new CpxLib.Controls.Common.CpxLabel();
            this.SuspendLayout();
            // 
            // lblValue
            // 
            this.lblValue.BackColor = System.Drawing.Color.Transparent;
            this.lblValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblValue.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValue.ForeColor = System.Drawing.Color.White;
            this.lblValue.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblValue.Location = new System.Drawing.Point(0, 0);
            this.lblValue.Name = "lblValue";
            this.lblValue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblValue.Size = new System.Drawing.Size(250, 30);
            this.lblValue.TabIndex = 11;
            this.lblValue.Text = "VALUE";
            this.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LocationPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.Controls.Add(this.lblValue);
            this.Curvature = 20;
            this.Name = "LocationPanel";
            this.Size = new System.Drawing.Size(250, 30);
            this.ResumeLayout(false);

        }
    }
}
