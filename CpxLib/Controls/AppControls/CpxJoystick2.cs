﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CpxLib.Core;
using CpxLib.Controls.Common;
using MissionPlanner;
using CpxLib.Properties;
using System.Timers;
using System.Threading;
using Timer = System.Timers.Timer;


namespace CpxLib.Controls.AppControls
{
    public partial class CpxJoystick2 : ShapedUserControl
    {
        private static Timer loopTimer1;
        private static Timer loopTimer2;
        private static Timer loopTimer3;
        private static Timer loopTimer4;
        private bool isMouseDownU = false;
        private bool isMouseDownD = false;
        public CpxJoystick2()
        {
            InitializeComponent();

        }
        public  void setVelocity(float vx_, float vy_, float vz_, char u_or_d)
        {
            if ((u_or_d == 'd' && isMouseDownD) || (u_or_d == 'u' && isMouseDownU))
            {
                MAVLink.mavlink_set_position_target_local_ned_t go = new MAVLink.mavlink_set_position_target_local_ned_t
                {
                    time_boot_ms = 0,
                    target_system = 0,
                    target_component = 0,
                    coordinate_frame = 8,
                    type_mask = 0b0000111111000111,
                    x = 0,
                    y = 0,
                    z = 0,
                    vx = vx_,
                    vy = vy_,
                    vz = vz_,
                    afx = 0,
                    afy = 0,
                    afz = 0,
                    yaw = 0,
                    yaw_rate = 0
                };
                CpxHost.Instance.Plugin.Host.comPort.sendPacket(go, CpxHost.Instance.Plugin.Host.comPort.MAV.sysid, CpxHost.Instance.Plugin.Host.comPort.MAV.compid);
            }
        }
        private void CpxJoystick2_Load(object sender, EventArgs e)
        {
            loopClick(cpxButtonDown, 'd', 500, loopTimer1);
            loopClick(cpxButtonUp, 'u', 500, loopTimer2);
            loopClick(cpxButtonRight, 'r', 1000, loopTimer3);
            loopClick(CpxButtonLeft, 'l', 1000, loopTimer4);
        }
        private void loopClick(CpxButton dir, char c, int interval, Timer loopTimer)
        {
            loopTimer = new Timer();
            loopTimer.Interval = interval;
            loopTimer.Enabled = false;
            loopTimer.Elapsed += (sender1, e1) =>
            {
                if (c == 'u')
                    toUp(1);
                if (c == 'd')
                    toDown(1);
                if (c == 'l')
                    toLeft();
                if (c == 'r')
                    toRight();
            };
            loopTimer.AutoReset = true;
            //form button
            dir.MouseDown += (sender1, e1) =>
            {
                loopTimer.Enabled = true;
                if (c == 'u')
                    isMouseDownU = true;
                if (c == 'd')
                    isMouseDownD = true;
            };
            dir.MouseUp += (sender1, e1) =>
            {
                loopTimer.Enabled = false;
                if (c == 'u')
                    isMouseDownU = false; ;
                if (c == 'd')
                    isMouseDownD = false; 
            };
        }
        private void CpxButtonRight_Click(object sender, EventArgs e)
        {
            toRight();
        }

        private void cpxButtonDown_Click(object sender, EventArgs e)
        {
            toDown(2);
        }

        private void cpxButtonUp_Click(object sender, EventArgs e)
        {
            toUp(2);
        }

        private void CpxButtonLeft_Click(object sender, EventArgs e)
        {
            toLeft();
        }
        private void toUp(int vx)
        {
            if(MainV2.comPort.MAV.cs.mode != "Guided")
                UIUtils.SetGuidedMode();
            if (!MainV2.comPort.BaseStream.IsOpen)
            {
                CustomMessageBox.Show(Resources.MessagePleaseConnect, Resources.ErrorMessage);
                return;
            }
            try
            {
                setVelocity(vx, 0, 0, 'u');
            }
            catch
            {
            }
        }
        private void toLeft()
        {
            if (MainV2.comPort.MAV.cs.mode != "Guided")
                UIUtils.SetGuidedMode();
            try
            {
                if (!MainV2.comPort.BaseStream.IsOpen)
                {
                    CustomMessageBox.Show(Resources.MessagePleaseConnect, Resources.ErrorMessage);
                    return;
                }
                CpxHost.Instance.Plugin.Host.comPort.doCommand(MAVLink.MAV_CMD.CONDITION_YAW, 350, 0, 1, 1, 0, 0, 0);
            }
            catch (Exception ex)
            {
            }
        }
        private void toRight()
        {
            if (MainV2.comPort.MAV.cs.mode != "Guided")
                UIUtils.SetGuidedMode();
            try
            {
                if (!MainV2.comPort.BaseStream.IsOpen)
                {
                    CustomMessageBox.Show(Resources.MessagePleaseConnect, Resources.ErrorMessage);
                    return;
                }
                CpxHost.Instance.Plugin.Host.comPort.doCommand(MAVLink.MAV_CMD.CONDITION_YAW, 10, 0, 1, 1, 0, 0, 0);
            }
            catch (Exception ex)
            {
            }
        }

        private void toDown(int vx)
        {
            if (MainV2.comPort.MAV.cs.mode != "Guided")
                UIUtils.SetGuidedMode();
            if (!MainV2.comPort.BaseStream.IsOpen)
            {
                CustomMessageBox.Show(Resources.MessagePleaseConnect, Resources.ErrorMessage);
                return;
            }
            try
            {
                setVelocity(-vx, 0, 0, 'd');
            }
            catch
            {
            }
        }
        
    }
}
