﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WebCamService;
using CpxLib.Controls.Common;
using CpxLib.Core;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;
using MissionPlanner;
using MissionPlanner.Utilities;
using CpxLib.Properties;

namespace CpxLib.Controls.AppControls
{
    public partial class CpxVideoWin : DraggableHost
    {
        public event EventHandler SwapVideoAndMap;
        private string _name;
        private CancellationTokenSource _videoCancelationToken;

        public CpxVideoWin()
        {
            InitializeComponent();

        }
        public CpxVideoWin(string name)
        {
            InitializeComponent();
            _name = name;
            //CpxHost.Instance.NewImage += Instance_NewImage;
        }

        public string CameraName { get { return _name; } }
        public void Stop()
        {
            //CpxHost.Instance.NewImage -= Instance_NewImage;
            Thread.Sleep(500);
            if (_videoCancelationToken != null)
            {
                if (!_videoCancelationToken.IsCancellationRequested)
                {
                    _videoCancelationToken.Cancel();
                    _videoCancelationToken = null;
                }

            }

            cpxVideo.Enabled = false;
            //cpxVideo.Stop();
        }
        public void Start(string gstreamerCommand)
        {
            //var config = CpxLib.Core.AppSettings.LoadConfiguration();
            //var dataDirectory = config.GStreamerDir;
            //var gstdir = Path.Combine(dataDirectory, @"1.0\x86_64\bin\gst-launch-1.0.exe");


            //cpxVideo.Pipeline = gstreamerCommand;
            // cpxVideo.GStreamerInstallDir = gstdir;
            //cpxVideo.Run();
            //  CpxGStreamer.StartB(gstreamerCommand, _name);
            _videoCancelationToken = CpxHost.Instance.StartGStreamer(gstreamerCommand, _name);

            if (_videoCancelationToken != null)
            {
                cpxVideo.Enabled = true;
                _videoCancelationToken.Token.Register(() =>
                {
                    _videoCancelationToken = null;
                });
            }


        }



        //private void Instance_NewImage(object sender, NewImageEventArgs e)
        //{
        //    if (e.Name == _name)
        //    {
        //        cpxVideo.bgimage = e.Image;
        //    }


        //}

        private void btnSwapVideoAndMap_Click(object sender, EventArgs e)
        {
            SwapVideoAndMap(this, EventArgs.Empty);
        }
        private void CpxVideoWin_Resize(object sender, EventArgs e)
        {
            var height = this.Height - panel1.Height - resizer.Height;

            if (height > this.Width)
            {
                cpxVideo.Width = this.Width;
                cpxVideo.Height = height;
            }
            else
            {
                cpxVideo.Width = this.Width;
                cpxVideo.Height = height;
            }
        }

    }
}
