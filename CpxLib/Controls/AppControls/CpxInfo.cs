﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CpxLib.Properties;
using CpxLib.Core;
using System.Globalization;
using System.Threading;
using CpxLib.LTE;
using MissionPlanner;
using DirectShowLib;
using MissionPlanner.Utilities;

namespace CpxLib.Controls.AppControls
{
    public partial class CpxInfo : UserControl
    {
        public string language = Properties.Settings.Default.Language;
        private readonly TelemetryDataItem[] telemetryItems = new TelemetryDataItem[7];
        private const string Telemetry_DistToHome = "DistToHome";
        private const string Telemetry_Altitude = "Altitude";
        private const string Telemetry_AltitudeSeaLevel = "AltitudeSeaLevel";
        private const string Telemetry_GPSStrength = "GPSStrength";
        private const string Telemetry_LTE = "LTE";
        private const string Telemetry_Latitude = "Latitude";
        private const string Telemetry_Longitude = "Longitude";
        private const string Telemetry_Battery = "Battery";
        private const string Telemetry_Mode = "Mode";
        private const string Telemetry_FlightTime = "FlightTime";
        private const string Telemetry_Speed = "Speed";

        private bool energy;
        private bool connection;
        private bool wifiLow;
        private bool wifiVeryLow;

        public static bool isInCharging = false;

        //private bool energy;

        public CpxInfo()
        {

            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            InitializeComponent();
            energy = false;
            connection = false;
            wifiLow = false;
            wifiVeryLow = false;
            // set items array according to their position form left to right
            telemetryItems[0] = telemetryItem1;
            telemetryItems[1] = telemetryItem2;
            telemetryItems[2] = telemetryItem3;
            telemetryItems[3] = telemetryItem4;
            telemetryItems[4] = telemetryItem5;
            telemetryItems[5] = telemetryItem6;
            telemetryItems[6] = telemetryItem7;
            lblBatteryLevel.Text = "";

            SetupTelemetry();
        }

        private void SettingsSaving(object sender, CancelEventArgs e)
        {
            ChangeLanguage(((CpxLib.Properties.Settings)sender).Language);
        }

        private void SetupTelemetry()
        {

            this.SuspendLayout();
            SetupItem(telemetryItems[0], Telemetry_Mode, Resources.TelemetryMode, "", Resources.flight_mode);
            SetupItem(telemetryItems[1], Telemetry_DistToHome, Resources.TelemetryDistToHome, "0", Resources.distance_to_home);
            SetupItem(telemetryItems[2], Telemetry_Altitude, Resources.TelemetryAltitude, "0", Resources.height);
            SetupItem(telemetryItems[3], Telemetry_AltitudeSeaLevel, Resources.TelemetryAltitudeSeaLevel, "0", Resources.height_above_sea);
            SetupItem(telemetryItems[4], Telemetry_GPSStrength, Resources.TelemetryGPSStrength, "0", Resources.satellite);
            SetupItem(telemetryItems[5], Telemetry_LTE, Resources.TelemetryCommunication, "0", Resources.communication_off);
            SetupItem(telemetryItems[6], Telemetry_Speed, Resources.TelemetrySpeed, "0", Resources.speed);
            lblBatteryLevel.Font = CpxFonts.GetFont(CpxFontsEnum.Rubik_Medium, 18.0F);

            this.ResumeLayout();

        }

        private void ChangeLanguage(string lang)
        {
            language = lang;
            foreach (Control c in this.Controls)
            {
                ComponentResourceManager resources = new ComponentResourceManager(typeof(CpxInfo));
                resources.ApplyResources(c, c.Name, new CultureInfo(lang));
            }


            SetupTelemetry();
        }

        private void UpdateItem(string key, Action<TelemetryDataItem> updateAction)
        {
            var item = telemetryItems.FirstOrDefault(x => x.Key == key);
            if (item != null)
            {
                updateAction(item);
            }
        }
        internal void SetupItem(TelemetryDataItem telemetryItem, string key, string caption, string value, Bitmap image)
        {
            telemetryItem.Caption = caption;
            telemetryItem.Value = value;
            telemetryItem.Image = image;
            telemetryItem.Key = key;
            telemetryItem.Visible = true;

            telemetryItem.ValueFont = CpxFonts.GetFont(CpxFontsEnum.Rubik_Medium, 13.0F);
            telemetryItem.CaptionFont = CpxFonts.GetFont(CpxFontsEnum.Rubik_Medium, 10.0F);

        }
        public void ShowTelemetry(LandingStationStatusEnum status)
        {

            //   telemetryItem8.Visible = true;
            //   telemetryItem8.Value = status.ToString();
        }


        public void ShowTelemetry(DisplayTelemetryData telemetry)
        {
            UpdateItem(Telemetry_Mode, (item) => { item.Value = telemetry.mode; });
            UpdateItem(Telemetry_Altitude, (item) => { item.Value = Math.Round(telemetry.alt, 1).ToString() + " m"; });

            UpdateItem(Telemetry_AltitudeSeaLevel, (item) => { item.Value = Math.Round(telemetry.altasl, 1).ToString() + " m"; });
            UpdateItem(Telemetry_DistToHome, (item) => { item.Value = Math.Round(telemetry.distToMAV, 1).ToString() + " m"; });
            UpdateItem(Telemetry_GPSStrength, (item) => { item.Value = telemetry.satcount.ToString(); });
            UpdateItem(Telemetry_Speed, (item) => { item.Value = Math.Round(telemetry.groundspeed, 1).ToString() + " m/s"; });

            UpdateItem(Telemetry_GPSStrength, (item) =>
            {

                var _fix = Math.Max(telemetry.gpsstatus, telemetry.gpsstatus2);
                if (_fix >= 3)
                    item.Image = Resources.satellite;
                else
                    item.Image = Resources.satellite_low;
                //if (_fix == 0)
                //{
                //    //item.Value = (Resources.GPS0);
                //    item.Image = Resources.satellite_low;
                //}
                //else if (_fix == 1)
                //{
                //    //item.Value = (Resources.GPS1);
                //    item.Image = Resources.satellite_low;
                //}
                //else if (_fix == 2)
                //{
                //    //item.Value = (Resources.GPS2);
                //    item.Image = Resources.satellite;
                //}
                //else if (_fix == 3)
                //{
                //    //item.Value = (Resources.GPS3);
                //}
                //else if (_fix == 4)
                //{
                //    //item.Value = (Resources.GPS4);
                //    item.Image = Resources.satellite;
                //}
                //else if (_fix == 5)
                //{
                //    //item.Value = (Resources.GPS5);
                //    item.Image = Resources.satellite;
                //}
                //else if (_fix == 6)
                //{
                //    //item.Value = (telemetry.satcount.ToString());
                //    item.Image = Resources.satellite;
                //}
                //else
                //{
                //    //item.Value = _fix.ToString();
                //    item.Image = Resources.satellite;
                //}

            });
            //update telemetry battery
            if (telemetry.battery_remaining < 0)

                lblBatteryLevel.Text = "0%, " + Math.Round(telemetry.battery_voltage, 1) + "V";
            else
                lblBatteryLevel.Text = telemetry.battery_remaining.ToString() + "%, " + Math.Round(telemetry.battery_voltage, 1) + "V";
            //lblBatteryLevel.RightToLeft = RightToLeft.No;
            //if (telemetry.battery_remaining > 90)
            //{
            //    energy = false;
            //    picBatteryLevel.BackgroundImage = Resources.battery_1;
            //}

            if (isInCharging)
            {
                energy = false;
                picBatteryLevel.BackgroundImage = Resources.battery_7;
            }
            else if (telemetry.battery_remaining > 80)
            {
                energy = false;
                picBatteryLevel.BackgroundImage = Resources.battery_1;
            }
            else if (telemetry.battery_remaining > 50)
            {
                energy = false;
                picBatteryLevel.BackgroundImage = Resources.battery_2;
            }
            else if (telemetry.battery_remaining > 30)
            {
                energy = false;
                picBatteryLevel.BackgroundImage = Resources.battery_3;
            }
            else if (telemetry.battery_remaining > 20)
            {
                picBatteryLevel.BackgroundImage = Resources.battery_4;
                if (energy == false)
                {
                    energy = true;
                    CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_LAW_BATTERY);
                    //CustomMessageBox.Show(CpxConstants.Messages[CpxConstants.MSG_LAW_BATTERY].Message);
                    //MessageBox.Show(CpxConstants.Messages[CpxConstants.MSG_LAW_BATTERY].Message);

                }
            }
            else if (telemetry.battery_remaining < 0)
            {
                energy = false;
                picBatteryLevel.BackgroundImage = Resources.battery_1;
            }
            else
            {
                picBatteryLevel.BackgroundImage = Resources.battery_5;
                if (energy == false)
                {
                    energy = true;
                    CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_LAW_BATTERY);
                    //CustomMessageBox.Show("שים לב, מצב סוללה מתחת ל-30%");
                }
            }
        }

        internal void UpdateMobilicomInfo(int strength)
        {
            if (strength < 0)
            {
                if (Math.Abs(strength) < 7)
                {
                    UpdateMobilicomState("*" + Resources.MobilicomPoor);
                }
                else if (Math.Abs(strength) >= 7 && Math.Abs(strength) <= 10)
                {
                    UpdateMobilicomState("*" + Resources.MobilicomFair);
                }
                else
                {
                    UpdateMobilicomState("*" + Resources.MobilicomGood);
                }
            }
            else if (strength == 0)
            {
                UpdateMobilicomState(Resources.LTE_Disconneted);
            }
            else if (strength < 7)
            {
                UpdateMobilicomState(Resources.MobilicomPoor);
            }
            else if (strength >= 7 && strength <= 10)
            {
                UpdateMobilicomState(Resources.MobilicomFair);
            }
            else
            {
                UpdateMobilicomState(Resources.MobilicomGood);
            }
        }

        private void UpdateMobilicomState(string text)
        {
            try
            {
                Action<string> Delegate_ShowMobilicomTelemetry = ShowMobilicomTelemetry;
                Invoke(Delegate_ShowMobilicomTelemetry, text);
            }
            catch
            {


            }
        }

        private void ShowMobilicomTelemetry(string text)
        {
            UpdateItem(Telemetry_LTE, (item) =>
            {
                item.Value = text;

                if (text == Resources.MobilicomPoor || text == "*" + Resources.MobilicomPoor)
                {
                    item.Image = Resources.communication_on;
                    item.ForeColor = Color.FromArgb(255, 92, 97);
                    connection = false;
                }
                else if (text == Resources.MobilicomFair || text == "*" + Resources.MobilicomFair)
                {
                    item.Image = Resources.communication_on;
                    item.ForeColor = Color.FromArgb(229, 169, 0);
                    connection = false;
                }
                else if (text == Resources.MobilicomGood || text == "*" + Resources.MobilicomGood)
                {
                    item.Image = Resources.communication_on;
                    item.ForeColor = Color.White;
                    connection = false;
                }
                else
                {
                    item.Image = Resources.communication_off;
                    item.ForeColor = Color.Red;
                    if (connection == false)
                    {
                        connection = true;
                        //CustomMessageBox.Show(text);
                    }

                    //MessageBox.Show(text);
                }

            });
        }

        public void UpdateInfo(DisplayTelemetryData telemetry)
        {
            Action<DisplayTelemetryData> Delegate_ShowTelemetry = ShowTelemetry;
            Invoke(Delegate_ShowTelemetry, telemetry);
        }
        public void UpdateInfo(LandingStationStatusEnum status)
        {
            Action<LandingStationStatusEnum> Delegate_ShowTelemetry = ShowTelemetry;
            Invoke(Delegate_ShowTelemetry, status);
        }
        internal void ShowScheduleStatus(string status)
        {
            Action<string> Delegate_ShowSchedule = UpdateScheduleInfo;
            Invoke(Delegate_ShowSchedule, status);
        }

        public void UpdateScheduleInfo(string status)
        {

        }


        internal void UpdateWifiState(WifiDef state)
        {
            if (state.Strength >= 80)
            {
                UpdateWifiTelemetry(Resources.wifi_green, $"{state.Strength}%");
            }
            else if (state.Strength >= 60)
            {
                UpdateWifiTelemetry(Resources.wifi_lightgreen, $"{state.Strength}%");
            }
            else if (state.Strength >= 40)
            {
                UpdateWifiTelemetry(Resources.wifi_yellow, $"{state.Strength}%");
            }
            else if (state.Strength >= 25)
            {
                UpdateWifiTelemetry(Resources.wifi_red, $"{state.Strength}%");
            }
            else
            {
                UpdateWifiTelemetry(Resources.wifi_gray, $"{state.Strength}%");
            }




        }
        internal void UpdateLTEInfo(LTEStatus status)
        {
            switch (status.ConnectionStatus)
            {
                case LTEConnectionStatusEnum.Unkonwn:
                case LTEConnectionStatusEnum.NotAvailable:
                    UpdateLTETelemetry(false, Resources.LTE_NoDongle);
                    break;
                case LTEConnectionStatusEnum.UndefinedAddress:
                    UpdateLTETelemetry(false, Resources.LTE_Undefined);
                    break;
                case LTEConnectionStatusEnum.ConnectionFailed:
                    UpdateLTETelemetry(false, Resources.LTE_ConnectionFailed);
                    break;
                case LTEConnectionStatusEnum.Connecting:
                    UpdateLTETelemetry(false, Resources.LTE_Connecting);
                    break;
                case LTEConnectionStatusEnum.Disconnected:
                    UpdateLTETelemetry(false, Resources.LTE_Disconneted);
                    break;
                case LTEConnectionStatusEnum.Disconnecting:
                    UpdateLTETelemetry(true, Resources.LTE_Disconnecting);
                    break;
                case LTEConnectionStatusEnum.Connected:
                    UpdateLTETelemetry(true, Resources.LTE_Ok);
                    break;
                default:
                    break;
            }

        }
        public void ShowLTETelemetry(bool connected, string text)
        {
            UpdateItem(Telemetry_LTE, (item) =>
            {
                item.Value = text;

                if (connected)
                {
                    item.Image = Resources.communication_on;
                    item.ForeColor = Color.White;
                    connection = false;
                }
                else
                {
                    item.Image = Resources.communication_off;
                    item.ForeColor = Color.Red;
                    if (connection == false)
                    {
                        connection = true;
                        //CustomMessageBox.Show(text);
                    }

                    //MessageBox.Show(text);
                }

            });

        }
        public void UpdateLTETelemetry(bool connected, string text)
        {
            try
            {
                Action<bool, string> Delegate_ShowLTETelemetry = ShowLTETelemetry;
                Invoke(Delegate_ShowLTETelemetry, connected, text);
            }
            catch
            {


            }

        }
        public void ShowWifiTelemetry(Image img, string text)
        {
            UpdateItem(Telemetry_LTE, (item) =>
            {
                item.Value = text;
                item.Image = img;
                if (Int32.Parse(text.Remove(text.Length - 1)) < 40)
                {
                    item.ForeColor = Color.Red;
                    if (Int32.Parse(text.Remove(text.Length - 1)) < 10)
                    {
                        if (wifiVeryLow == false)
                        {
                            wifiVeryLow = true;
                            CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_WIFI_UNAVAILBLE);
                            //CustomMessageBox.Show(CpxConstants.Messages[CpxConstants.MSG_WIFI_UNAVAILBLE].Message);
                        }
                    }
                    else
                    {
                        if (wifiLow == false)
                        {
                            wifiLow = true;
                            CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_COMMUNICATION_PROBLEMS);
                            //CustomMessageBox.Show(CpxConstants.Messages[CpxConstants.MSG_COMMUNICATION_PROBLEMS].Message);
                        }
                    }

                }

                else
                {
                    item.ForeColor = Color.White;
                    wifiLow = false;
                    wifiVeryLow = false;
                }

            });

        }

        public void UpdateWifiTelemetry(Image img, string text)
        {

            try
            {
                Action<Image, string> Delegate_ShowWifiTelemetry = ShowWifiTelemetry;
                Invoke(Delegate_ShowWifiTelemetry, img, text);
            }
            catch
            {


            }

        }

        private void CpxInfo_Load(object sender, EventArgs e)
        {

        }

        internal void EnableControls(CurrentState state)
        {

        }

        private void telemetryItem2_Load(object sender, EventArgs e)
        {

        }

        public void flyTo(double lat, double lng)
        {
            Locationwp gotohere = new Locationwp();
            gotohere.id = (ushort)MAVLink.MAV_CMD.WAYPOINT;
            gotohere.alt = MainV2.comPort.MAV.GuidedMode.z; // back to m
            gotohere.lat = lat;
            gotohere.lng = lng;

            try
            {
                MainV2.comPort.setGuidedModeWP(gotohere);
            }
            catch (Exception ex)
            {
                MainV2.comPort.giveComport = false;
                CpxHost.Instance.UI.ShowMessage(1, Resources.ErrorMessage);
                //CustomMessageBox.Show(Resources.CommandFailed + ex.Message, Resources.ErrorMessage);
            }

        }
    }
}
