﻿namespace CpxLib.Controls.AppControls
{
    partial class CpxSettings
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CpxSettings));
            this.pnlWorkspace = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radBtnGeoFence = new System.Windows.Forms.RadioButton();
            this.radCompass = new System.Windows.Forms.RadioButton();
            this.radSchedules = new System.Windows.Forms.RadioButton();
            this.radPlans = new System.Windows.Forms.RadioButton();
            this.radVehicles = new System.Windows.Forms.RadioButton();
            this.radGeneral = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblCaption = new System.Windows.Forms.Label();
            this.btnBack = new CpxLib.Controls.Common.CpxButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlWorkspace
            // 
            resources.ApplyResources(this.pnlWorkspace, "pnlWorkspace");
            this.pnlWorkspace.Name = "pnlWorkspace";
            this.pnlWorkspace.Tag = "";
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radBtnGeoFence);
            this.panel1.Controls.Add(this.radCompass);
            this.panel1.Controls.Add(this.radSchedules);
            this.panel1.Controls.Add(this.radPlans);
            this.panel1.Controls.Add(this.radVehicles);
            this.panel1.Controls.Add(this.radGeneral);
            this.panel1.Name = "panel1";
            this.panel1.Tag = "custom";
            // 
            // radBtnGeoFence
            // 
            resources.ApplyResources(this.radBtnGeoFence, "radBtnGeoFence");
            this.radBtnGeoFence.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radBtnGeoFence.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.radBtnGeoFence.ForeColor = System.Drawing.Color.White;
            this.radBtnGeoFence.Image = global::CpxLib.Properties.Resources.icons8_compass_26;
            this.radBtnGeoFence.Name = "radBtnGeoFence";
            this.radBtnGeoFence.TabStop = true;
            this.radBtnGeoFence.Tag = "custom";
            this.radBtnGeoFence.UseVisualStyleBackColor = true;
            this.radBtnGeoFence.CheckedChanged += new System.EventHandler(this.radOptions_CheckedChanged);
            // 
            // radCompass
            // 
            resources.ApplyResources(this.radCompass, "radCompass");
            this.radCompass.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radCompass.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.radCompass.ForeColor = System.Drawing.Color.White;
            this.radCompass.Image = global::CpxLib.Properties.Resources.icons8_compass_26;
            this.radCompass.Name = "radCompass";
            this.radCompass.TabStop = true;
            this.radCompass.Tag = "custom";
            this.radCompass.UseVisualStyleBackColor = true;
            this.radCompass.CheckedChanged += new System.EventHandler(this.radOptions_CheckedChanged);
            // 
            // radSchedules
            // 
            resources.ApplyResources(this.radSchedules, "radSchedules");
            this.radSchedules.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radSchedules.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.radSchedules.ForeColor = System.Drawing.Color.White;
            this.radSchedules.Image = global::CpxLib.Properties.Resources.scheduled_flight_26;
            this.radSchedules.Name = "radSchedules";
            this.radSchedules.TabStop = true;
            this.radSchedules.Tag = "custom";
            this.radSchedules.UseVisualStyleBackColor = true;
            this.radSchedules.CheckedChanged += new System.EventHandler(this.radOptions_CheckedChanged);
            // 
            // radPlans
            // 
            resources.ApplyResources(this.radPlans, "radPlans");
            this.radPlans.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radPlans.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.radPlans.ForeColor = System.Drawing.Color.White;
            this.radPlans.Image = global::CpxLib.Properties.Resources.waypoint_map_light_26;
            this.radPlans.Name = "radPlans";
            this.radPlans.TabStop = true;
            this.radPlans.Tag = "custom";
            this.radPlans.UseVisualStyleBackColor = true;
            this.radPlans.CheckedChanged += new System.EventHandler(this.radOptions_CheckedChanged);
            // 
            // radVehicles
            // 
            resources.ApplyResources(this.radVehicles, "radVehicles");
            this.radVehicles.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radVehicles.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.radVehicles.ForeColor = System.Drawing.Color.White;
            this.radVehicles.Image = global::CpxLib.Properties.Resources.take_off_light_26;
            this.radVehicles.Name = "radVehicles";
            this.radVehicles.TabStop = true;
            this.radVehicles.Tag = "custom";
            this.radVehicles.UseVisualStyleBackColor = true;
            this.radVehicles.CheckedChanged += new System.EventHandler(this.radOptions_CheckedChanged);
            // 
            // radGeneral
            // 
            resources.ApplyResources(this.radGeneral, "radGeneral");
            this.radGeneral.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.radGeneral.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.radGeneral.ForeColor = System.Drawing.Color.White;
            this.radGeneral.Image = global::CpxLib.Properties.Resources.icons8_settings_26;
            this.radGeneral.Name = "radGeneral";
            this.radGeneral.TabStop = true;
            this.radGeneral.Tag = "custom";
            this.radGeneral.UseVisualStyleBackColor = true;
            this.radGeneral.CheckedChanged += new System.EventHandler(this.radOptions_CheckedChanged);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Image = global::CpxLib.Properties.Resources.CopterPixProNew;
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            // 
            // lblCaption
            // 
            resources.ApplyResources(this.lblCaption, "lblCaption");
            this.lblCaption.Name = "lblCaption";
            // 
            // btnBack
            // 
            resources.ApplyResources(this.btnBack, "btnBack");
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBack.ButtonAction = null;
            this.btnBack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Image = global::CpxLib.Properties.Resources.icons8_back_arrow_26;
            this.btnBack.Name = "btnBack";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.BackColor = System.Drawing.Color.DimGray;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnBack);
            this.panel2.Controls.Add(this.lblCaption);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Name = "panel2";
            this.panel2.Tag = "custom";
            // 
            // CpxSettings
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlWorkspace);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "CpxSettings";
            this.Load += new System.EventHandler(this.CpxSettings_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlWorkspace;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radPlans;
        private System.Windows.Forms.RadioButton radSchedules;
        private System.Windows.Forms.RadioButton radVehicles;
        private System.Windows.Forms.RadioButton radGeneral;
        private System.Windows.Forms.RadioButton radCompass;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblCaption;
        private Common.CpxButton btnBack;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radBtnGeoFence;
    }
}