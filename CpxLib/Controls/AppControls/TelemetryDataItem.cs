﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Controls.AppControls
{
    public partial class TelemetryDataItem : UserControl
    {
        public TelemetryDataItem()
        {
            InitializeComponent();
        }

        [Category("Telemetry")]
        [Localizable(true)]
        public string Caption
        {
            get
            {
                return lblCaption.Text;
            }
            set
            {
                lblCaption.Text = value;
            }
        }
        [Category("Telemetry")]
        [Localizable(true)]
        public string Value
        {
            get
            {
                return lblValue.Text;
            }
            set
            {
                lblValue.Text = value;
            }
        }
        [Category("Telemetry")]
        [Localizable(true)]
        public Image Image
        {

            get
            {
                return pic.BackgroundImage;
            }
            set
            {
                pic.BackgroundImage = value;
                pic.BackgroundImageLayout = ImageLayout.Center;
            }
        }
        [Category("Telemetry")]
        [Localizable(true)]
        public Font CaptionFont
        {
            get
            {
                return lblCaption.Font;

            }
            set
            {
                lblCaption.Font = value;
            }
        }
        [Category("Telemetry")]
        [Localizable(true)]
        public Font ValueFont
        {
            get
            {
                return lblValue.Font;

            }
            set
            {
                lblValue.Font = value;
            }
        }

        public string Key { get; set; }
    }
}
