﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CpxLib.Core;
using System.Threading;
using System.Globalization;
using MissionPlanner;
using MissionPlanner.GCSViews;
using CpxLib.Properties;
using CpxLib.Forms;

namespace CpxLib.Controls.AppControls
{
    public partial class CpxToolbar : UserControl //DraggableHost
    {
        public string language = Properties.Settings.Default.Language;
        public event EventHandler<CpxToolbarCommandEventArgs> CommandInvoked;
        private videoState videoPlaying;
        bool adminMode;
        public bool nextVision=false;
        public CpxToolbar(int LayoutMode)
        {
            videoPlaying = videoState.None;
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(language);
            InitializeComponent();
            //nextVision = false;
            if (LayoutMode == 10)
            {
                adminMode = false;
                btnSettings.Visible = false;
                btnMoreActions.Visible = false;
            }
            else
            {
                adminMode = true;
                btnFlyByMap.Visible = true;
            }
        }

        private void btnTakeOff_Click(object sender, EventArgs e)
        {
            if (btnTakeOff.ButtonAction == "TAKEOFF")
            {
                OnCommand(CpxToolbarCommandEnum.Takeoff);
            }
            else
            {
                OnCommand(CpxToolbarCommandEnum.Land);
            }
        }

        private void btnFlyByMap_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.LoadPlan);
        }

        private void btnRtl_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.Rtl);
        }

        private void btnLoiter_Click(object sender, EventArgs e)
        {

            if (btnLoiter.ButtonAction == "LOITER")
            {
                OnCommand(CpxToolbarCommandEnum.LoiterMode);
            }
            else if (btnLoiter.ButtonAction == "START")
            {
                OnCommand(CpxToolbarCommandEnum.FlyByMap);
            }
            else
            {
                OnCommand(CpxToolbarCommandEnum.ResumeFlight);
            }
            Thread.Sleep(1000);
        }

        private void btnResumeFlight_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.ResumeFlight);
        }

        private void btnOpenSchedule_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.ScheduledFlight);
        }

        private void OnCommand(CpxToolbarCommandEnum command)
        {
            CommandInvoked?.Invoke(this, new CpxToolbarCommandEventArgs(command));
        }

        private void btnConnectMAVS_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.Connect);
        }

        private void btnShowNightCamera_Click(object sender, EventArgs e)
        {
            if (btnShowNightCamera.ButtonAction == "Start")
                videoPlaying = videoState.Night;
            else
                videoPlaying = videoState.None;
            OnCommand(CpxToolbarCommandEnum.ToggleNightCamera);
        }

        private void btnShowDayCamera_Click(object sender, EventArgs e)
        {
            if (btnShowDayCamera.ButtonAction == "Start")
                videoPlaying = videoState.Day;
            else
                videoPlaying = videoState.None;
            OnCommand(CpxToolbarCommandEnum.ToggleDayCamera);
        }

        private void btnMoreActions_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.MoreActions);
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.Settings);
        }

        private void ChangeLanguage(string lang)
        {
            foreach (Control c in this.Controls)
            {
                ComponentResourceManager resources = new ComponentResourceManager(typeof(CpxToolbar));
                resources.ApplyResources(c, c.Name, new CultureInfo(lang));
                IEnumerable<Control> allItems = GetAll(c);
                foreach (Control cn in allItems)
                {
                    resources = new ComponentResourceManager(typeof(CpxToolbar));
                    resources.ApplyResources(cn, cn.Name, new CultureInfo(lang));
                }

            }
        }
        private IEnumerable<Control> GetAll(Control control)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl))
                                      .Concat(controls);
        }

        private void btnEditPlan_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.EditPlan);
        }

        private void btnArmDisarm_Click(object sender, EventArgs e)
        {
            if (btnArmDisarm.ButtonAction == "DISARM")
            {
                var confirmForm = new ArmOrDisarm();
                confirmForm.AreYouSure.Text = confirmForm.AreYouSure.Text + " Disarm?";
                confirmForm.ShowDialog();
                if (confirmForm.DialogResult == DialogResult.OK)
                    OnCommand(CpxToolbarCommandEnum.Disarm);
            }
            else
            {
                var confirmForm = new ArmOrDisarm();
                confirmForm.AreYouSure.Text = confirmForm.AreYouSure.Text + " Arm?";
                confirmForm.ShowDialog();
                if (confirmForm.DialogResult == DialogResult.OK)
                    OnCommand(CpxToolbarCommandEnum.Arm);
            }
        }
        internal void EnableControls(CurrentState state)
        {
            if (!state.connected)
            {
                btnArmDisarm.Visible = false;
                btnTakeOff.Visible = false;
                btnRtl.Visible = false;
                btnShowDayCamera.Visible = false;
                btnShowNightCamera.Visible = false;
                btnLoiter.Visible = false;
                btnResumeFlight.Visible = false;
                btnFlyByMap.Visible = true;
                btnSchedule.Visible = false;
                btnConnectMAVS.Image = Resources.connect_vehicles;
                btnNV.Visible = false;
            }
            else
            {
                btnConnectMAVS.Image = Resources.connected;
                if (nextVision == false)
                {
                    btnShowDayCamera.Visible = true;
                    btnShowNightCamera.Visible = true;
                    //btnShowDayCamera.Visible = false;
                    //btnShowNightCamera.Visible = false;
                }
                else
                {
                    btnShowDayCamera.Visible = false;
                    btnShowNightCamera.Visible = false;
                }
                btnArmDisarm.Visible = true;
                btnTakeOff.Visible = true;
                btnLoiter.Visible = true;
                btnSchedule.Visible = false;
                if (videoPlaying == videoState.Day)
                {
                    if (btnShowDayCamera.ButtonAction == "Start")
                    {
                        btnShowDayCamera.ButtonAction = "Stop";
                        if (btnShowNightCamera.ButtonAction == "Stop")
                        {
                            btnShowNightCamera.Image = Resources.night_camera;
                            btnShowNightCamera.ButtonAction = "Start";
                        }
                        btnShowDayCamera.Image = Resources.dayPlaying;
                    }
                    else
                    {
                        btnShowNightCamera.Image = Resources.night_camera;
                        btnShowDayCamera.Image = Resources.dayPlaying;
                    }
                }
                else if (videoPlaying == videoState.Night)
                {
                    if (btnShowNightCamera.ButtonAction == "Start")
                    {
                        btnShowNightCamera.ButtonAction = "Stop";
                        if (btnShowDayCamera.ButtonAction == "Stop")
                        {
                            btnShowDayCamera.Image = Resources.day_camera;
                            btnShowDayCamera.ButtonAction = "Start";
                        }
                        btnShowNightCamera.Image = Resources.nightPlaying;
                    }
                    else
                    {
                        btnShowNightCamera.Image = Resources.nightPlaying;
                        btnShowDayCamera.Image = Resources.day_camera;
                    }
                }
                else
                {
                    btnShowDayCamera.ButtonAction = "Start";
                    btnShowNightCamera.ButtonAction = "Start";
                    btnShowDayCamera.Image = Resources.day_camera;
                    btnShowNightCamera.Image = Resources.night_camera;
                }

                if (state.armed)
                {
                    btnArmDisarm.Image = Resources.disarm;
                    btnArmDisarm.ButtonAction = "DISARM";
                    btnRtl.Visible = true;
                    btnLoiter.Visible = true;
                    if (!adminMode)
                        btnFlyByMap.Visible = true;
                }
                else
                {
                    btnArmDisarm.Image = Resources.arm;
                    btnArmDisarm.ButtonAction = "ARM";
                    btnRtl.Visible = false;
                    //btnLoiter.Visible = false;
                    btnResumeFlight.Visible = false;
                    if (!adminMode)
                        btnFlyByMap.Visible = true;
                }

                if (state.IsInFlight())
                {
                    btnTakeOff.Image = Resources.land;
                    btnTakeOff.ButtonAction = "LAND";
                    //btnRtl.Visible = true;

                    //btnShowDayCamera.Visible = true;
                    //btnShowNightCamera.Visible = true;
                    //btnLoiter.Visible = true;
                    //if (!adminMode)
                    //    btnFlyByMap.Visible = true;
                    //if (!state.armed)
                    //{
                    //    btnLoiter.Image = Resources.start1;
                    //    btnLoiter.ButtonAction = "START";
                    //}
                    if(state.mode.ToLower() == "auto")
                    {
                        btnLoiter.Image = Resources.pause;
                        btnLoiter.ButtonAction = "LOITER";
                    }
                    else
                    {
                        btnLoiter.Image = Resources.resume;
                        btnLoiter.ButtonAction = "RESUME";
                    }
                    //if ((state.mode.ToLower() == "loiter" )|| (state.mode.ToLower() == "guided" && VehicleConnection.isInMission))
                    //{
                    //    btnLoiter.Image = Resources.resume;
                    //    btnLoiter.ButtonAction = "RESUME";
                    //}
                    ////else
                    ////{
                    ////    btnLoiter.Image = Resources.pause;
                    ////    btnLoiter.ButtonAction = "LOITER";
                    ////}
                    //else if (state.mode.ToLower() == "auto")
                    //{
                    //    btnLoiter.Image = Resources.pause;
                    //    btnLoiter.ButtonAction = "LOITER";
                    //}
                    //else
                    //{
                        
                    //}
                }
                else
                {
                    if (state.mode.ToLower() == "auto")
                    {
                        btnLoiter.Image = Resources.pause;
                        btnLoiter.ButtonAction = "LOITER";
                    }
                    else
                    {
                        btnLoiter.Image = Resources.resume;
                        btnLoiter.ButtonAction = "RESUME";
                    }
                    //btnLoiter.Image = Resources.start1;
                    //btnLoiter.ButtonAction = "START";
                    btnTakeOff.Image = Resources.take_off;
                    btnTakeOff.ButtonAction = "TAKEOFF";

                    //btnRtl.Visible = false;
                    ////btnLoiter.Visible = false;
                    //btnResumeFlight.Visible = false;
                    //if (!adminMode)
                    //    btnFlyByMap.Visible = true;
                }
            }
        }
        private void btnSchedule_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.ScheduledFlight);
        }
        public void isCamerasExist(bool dayCamera, bool nightCamera)
        {
            if (dayCamera)
                btnShowDayCamera.Visible = false;
            else
                btnShowDayCamera.Visible = false;

            if (nightCamera)
                btnShowNightCamera.Visible = false;
            else
                btnShowNightCamera.Visible = false;
        }

        private void cpxButton1_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.NextVisionConnect);
        }
    }
    public class CpxToolbarCommandEventArgs : EventArgs
    {
        private CpxToolbarCommandEnum _command = CpxToolbarCommandEnum.NotSet;

        public CpxToolbarCommandEventArgs(CpxToolbarCommandEnum command)
        {
            _command = command;
        }

        public CpxToolbarCommandEnum Command { get { return _command; } }
    }
    enum videoState { Day, Night, None }

}
