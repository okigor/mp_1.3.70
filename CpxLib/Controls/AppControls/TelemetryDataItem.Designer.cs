﻿using CpxLib.Controls.Common;

namespace CpxLib.Controls.AppControls
{
    partial class TelemetryDataItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TelemetryDataItem));
            this.pic = new System.Windows.Forms.PictureBox();
            this.lblValue = new CpxLib.Controls.Common.CpxLabel();
            this.lblCaption = new CpxLib.Controls.Common.CpxLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            this.SuspendLayout();
            // 
            // pic
            // 
            resources.ApplyResources(this.pic, "pic");
            this.pic.BackColor = System.Drawing.Color.Transparent;
            this.pic.Name = "pic";
            this.pic.TabStop = false;
            // 
            // lblValue
            // 
            resources.ApplyResources(this.lblValue, "lblValue");
            this.lblValue.Name = "lblValue";
            // 
            // lblCaption
            // 
            resources.ApplyResources(this.lblCaption, "lblCaption");
            this.lblCaption.Name = "lblCaption";
            // 
            // TelemetryDataItem
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblValue);
            this.Controls.Add(this.lblCaption);
            this.Controls.Add(this.pic);
            this.Name = "TelemetryDataItem";
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public CpxLabel lblCaption;
        public CpxLabel lblValue;
        public System.Windows.Forms.PictureBox pic;
    }
}
