﻿using System;

namespace CpxLib.Controls.AppControls
{
    partial class gimbalControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gimbalControl));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.zoomBar = new CpxLib.Controls.Common.ColorSlider();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.zoomBar);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // zoomBar
            // 
            this.zoomBar.BackColor = System.Drawing.Color.Transparent;
            this.zoomBar.BarInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.BarPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.BarPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.BorderRoundRectSize = new System.Drawing.Size(8, 8);
            resources.ApplyResources(this.zoomBar, "zoomBar");
            this.zoomBar.ElapsedInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.ElapsedPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.ElapsedPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.zoomBar.ForeColor = System.Drawing.Color.White;
            this.zoomBar.LargeChange = ((uint)(5u));
            this.zoomBar.Name = "zoomBar";
            this.zoomBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.zoomBar.ScaleDivisions = 10;
            this.zoomBar.ScaleSubDivisions = 5;
            this.zoomBar.ShowDivisionsText = true;
            this.zoomBar.ShowSmallScale = false;
            this.zoomBar.SmallChange = ((uint)(1u));
            this.zoomBar.ThumbImage = global::CpxLib.Properties.Resources.zoom_button;
            this.zoomBar.ThumbInnerColor = System.Drawing.Color.Transparent;
            this.zoomBar.ThumbOuterColor = System.Drawing.Color.Transparent;
            this.zoomBar.ThumbPenColor = System.Drawing.Color.Transparent;
            this.zoomBar.ThumbRoundRectSize = new System.Drawing.Size(16, 16);
            this.zoomBar.ThumbSize = new System.Drawing.Size(16, 16);
            this.zoomBar.TickAdd = 0F;
            this.zoomBar.TickColor = System.Drawing.Color.White;
            this.zoomBar.TickDivide = 0F;
            this.zoomBar.Value = 0;
            this.zoomBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.zoomBar_Scroll);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Name = "label1";
            // 
            // gimbalControl
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "gimbalControl";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        public Common.ColorSlider zoomBar;
        private System.Windows.Forms.Panel panel3;
        
    }
}
