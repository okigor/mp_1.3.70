﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CpxLib.Core;
using CpxLib.Forms;

namespace CpxLib.Controls.AppControls
{
    public partial class CpxFlightPlannerToolBar : UserControl
    {
        public event EventHandler<CpxToolbarCommandEventArgs> CommandInvoked;

        public CpxFlightPlannerToolBar()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.ReturnHome);
        
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.SavePlan);


         

        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OnCommand(CpxToolbarCommandEnum.LoadPlan);
        }

        private void OnCommand(CpxToolbarCommandEnum command)
        {
            CommandInvoked?.Invoke(this, new CpxToolbarCommandEventArgs(command));
        }


    }
 
}
