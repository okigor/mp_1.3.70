﻿
using CpxLib.Controls.Common;
using CpxLib.Core;
using CpxLib.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Controls.AppControls
{
    public class TimePanel : ShapedUserControl
    {
        public CpxLabel lblValue;
        public CpxLabel lblCaption;

        public TimePanel()
        {
            InitializeComponent();

            lblCaption.Text = Resources.TelemetryFlightTime;
            lblValue.Text = "00:00";


            lblValue.Font = CpxFonts.GetFont(CpxFontsEnum.Rubik_Medium, 15.0F);
            lblCaption.Font = CpxFonts.GetFont(CpxFontsEnum.Rubik_Medium, 10.0F);

        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            var offset = DoubleToInt(this.BorderWidth / 2);
            System.Drawing.Pen borderPen2 = new System.Drawing.Pen(this.BackColor, this.BorderWidth + 1);

            e.Graphics.DrawLine(borderPen2, offset, offset, this.ClientRectangle.Width - offset, offset);
            borderPen2.Dispose();



        }

        private void InitializeComponent()
        {
            this.lblValue = new CpxLib.Controls.Common.CpxLabel();
            this.lblCaption = new CpxLib.Controls.Common.CpxLabel();
            this.SuspendLayout();
            // 
            // lblValue
            // 
            this.lblValue.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblValue.BackColor = System.Drawing.Color.Transparent;
            this.lblValue.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValue.ForeColor = System.Drawing.Color.White;
            this.lblValue.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblValue.Location = new System.Drawing.Point(17, 29);
            this.lblValue.Name = "lblValue";
            this.lblValue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblValue.Size = new System.Drawing.Size(190, 32);
            this.lblValue.TabIndex = 10;
            this.lblValue.Text = "VALUE";
            this.lblValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblValue.Click += new System.EventHandler(this.lblValue_Click);
            // 
            // lblCaption
            // 
            this.lblCaption.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCaption.BackColor = System.Drawing.Color.Transparent;
            this.lblCaption.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.ForeColor = System.Drawing.Color.White;
            this.lblCaption.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCaption.Location = new System.Drawing.Point(17, 0);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblCaption.Size = new System.Drawing.Size(190, 29);
            this.lblCaption.TabIndex = 9;
            this.lblCaption.Text = "CAPTION";
            this.lblCaption.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // TimePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(47)))), ((int)(((byte)(56)))));
            this.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(169)))), ((int)(((byte)(1)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BorderWidth = 6;
            this.Controls.Add(this.lblValue);
            this.Controls.Add(this.lblCaption);
            this.Curvature = 20;
            this.CurveMode = ((CpxLib.Controls.Common.CornerCurveMode)((CpxLib.Controls.Common.CornerCurveMode.BottomLeft | CpxLib.Controls.Common.CornerCurveMode.BottomRight)));
            this.Name = "TimePanel";
            this.Size = new System.Drawing.Size(224, 70);
            this.ResumeLayout(false);

        }

        private string FormatTime(double timeinair)
        {

            //var minutes = Math.Truncate(timeinair);
            //var seconds = (timeinair - minutes) * 100;

            //return string.Format("{0:00}:{1:00}", minutes, seconds);
            TimeSpan t = TimeSpan.FromSeconds(timeinair);

            string str = string.Format("{0:D2}:{1:D2}",
                            (t.Hours * 60) + t.Minutes,
                            t.Seconds);
            //here backslash is must to tell that colon is
            //not the part of format, it just a character that we want in output
            return str;


        }
        public void ShowFlightTime(double flightTime)
        {
            lblValue.Text = FormatTime(flightTime);
        }
        public void UpdateFlightTime(double flightTime)
        {
            try
            {
                Action<double> Delegate_ShowFlightTime = ShowFlightTime;
                Invoke(Delegate_ShowFlightTime, flightTime);
            }
            catch
            {


            }

        }

        private void lblValue_Click(object sender, EventArgs e)
        {

        }
    }
}
