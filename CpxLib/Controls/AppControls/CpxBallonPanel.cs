﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Controls.AppControls
{
    public class CpxBallonPanel : Panel
    {

        private int _borderWidth;
        private int _curvature = 1;
        private string _text = "";

        public CpxBallonPanel() : base()
        {
            this.SetDefaultControlStyles();
        }

       // [Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
        [System.ComponentModel.DefaultValueAttribute(typeof(string), "0"), System.ComponentModel.CategoryAttribute("Appearance"), System.ComponentModel.DescriptionAttribute("Text")]
        public string MessageText
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
                if (this.DesignMode == true)
                {

                    this.Invalidate();
                }
            }
        }
        [System.ComponentModel.DefaultValueAttribute(typeof(int), "0"), System.ComponentModel.CategoryAttribute("Appearance"), System.ComponentModel.DescriptionAttribute("The radius of the curve used to paint the corners of the control.")]
        public int Curvature
        {
            get
            {
                return this._curvature;
            }
            set
            {
                this._curvature = value;
                if (this.DesignMode == true)
                {
                    this.Invalidate();
                }
            }
        }
        [System.ComponentModel.DefaultValueAttribute(typeof(int), "1"), System.ComponentModel.CategoryAttribute("Appearance"), System.ComponentModel.DescriptionAttribute("The width of the border used to paint the control.")]
        public int BorderWidth
        {
            get
            {
                return this._borderWidth;
            }
            set
            {
                this._borderWidth = value;
                if (this.DesignMode == true)
                {
                    this.Invalidate();
                }
            }
        }


        private int adjustedCurve
        {
            get
            {
                int curve = 0;

                if (this._curvature > (this.ClientRectangle.Width / 2))
                {
                    curve = DoubleToInt(this.ClientRectangle.Width / 2);
                }
                else
                {
                    curve = this._curvature;
                }
                if (curve > (this.ClientRectangle.Height / 2))
                {
                    curve = DoubleToInt(this.ClientRectangle.Height / 2);
                }

                return curve;
            }
        }

        private void SetDefaultControlStyles()
        {
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, false);
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            SetStyle(ControlStyles.ContainerControl, true);
        }

        protected override void OnRightToLeftChanged(EventArgs e)
        {
            base.OnRightToLeftChanged(e);

            if (this.DesignMode == true)
            {
                this.Invalidate();
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);



            var g = e.Graphics;
            var pen = new Pen(Color.FromArgb(230, 169, 1), 6);

            var path = GetPath();
            g.DrawPath(pen, path);

            var textBrush = new SolidBrush(ForeColor);

            if (this.RightToLeft == RightToLeft.Yes)
            {
                var format = new StringFormat(StringFormatFlags.DirectionRightToLeft);
                g.DrawString(_text, this.Font, textBrush, this.ClientSize.Width - 10, 10, format);
            }
            else
            {
                g.DrawString(_text, this.Font, textBrush, 10, 10);
            }

            this.Region = new System.Drawing.Region(path);
        }



        private int DoubleToInt(double value)
        {
            return decimal.ToInt32(decimal.Floor(decimal.Parse((value).ToString())));
        }
        protected System.Drawing.Drawing2D.GraphicsPath GetPath()
        {
            var graphPath = new System.Drawing.Drawing2D.GraphicsPath();

            var offset = DoubleToInt(_borderWidth / 2);

            int rectWidth = this.ClientSize.Width - 1 - offset;
            int rectHeight = this.ClientSize.Height - 1 - offset;
            var triangleHeight = DoubleToInt(rectHeight / 4);
            int curve = adjustedCurve;

            int curveWidth = curve * 2;
            var baloonLower = rectHeight - triangleHeight - offset;


            graphPath.AddArc(rectWidth - curveWidth, offset, curveWidth, curveWidth, 270, 90);

            graphPath.AddArc(rectWidth - curveWidth, rectHeight - curveWidth - offset - triangleHeight, curveWidth, curveWidth, 0, 90);

            if (this.RightToLeft == RightToLeft.Yes)
            {
                graphPath.AddLine(rectWidth - curveWidth, baloonLower, rectWidth - 1.5f * curveWidth, rectHeight);
                graphPath.AddLine(rectWidth - 1.5f * curveWidth, rectHeight, rectWidth - 2f * curveWidth, baloonLower);
            }
            else
            {
                graphPath.AddLine(rectWidth - curveWidth, baloonLower, 2f * curveWidth, baloonLower);
                graphPath.AddLine(2f * curveWidth, baloonLower, 1.5f * curveWidth, rectHeight);
                graphPath.AddLine(1.5f * curveWidth, rectHeight, curveWidth, baloonLower);                
            }

            graphPath.AddArc(offset, baloonLower - curveWidth, curveWidth, curveWidth, 90, 90);
            graphPath.AddArc(offset, offset, curveWidth, curveWidth, 180, 90);

            graphPath.CloseFigure();



            return graphPath;
        }

    }
}
