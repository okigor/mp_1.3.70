﻿using CpxLib.Controls.Common;

namespace CpxLib.Controls.AppControls
{ 
    partial class CpxToolbar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CpxToolbar));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnMoreActions = new CpxLib.Controls.Common.CpxButton();
            this.btnSettings = new CpxLib.Controls.Common.CpxButton();
            this.btnSchedule = new CpxLib.Controls.Common.CpxButton();
            this.btnEditPlan = new CpxLib.Controls.Common.CpxButton();
            this.btnShowNightCamera = new CpxLib.Controls.Common.CpxButton();
            this.btnShowDayCamera = new CpxLib.Controls.Common.CpxButton();
            this.btnConnectMAVS = new CpxLib.Controls.Common.CpxButton();
            this.btnResumeFlight = new CpxLib.Controls.Common.CpxButton();
            this.btnLoiter = new CpxLib.Controls.Common.CpxButton();
            this.btnRtl = new CpxLib.Controls.Common.CpxButton();
            this.btnFlyByMap = new CpxLib.Controls.Common.CpxButton();
            this.btnTakeOff = new CpxLib.Controls.Common.CpxButton();
            this.btnArmDisarm = new CpxLib.Controls.Common.CpxButton();
            this.btnNV = new CpxLib.Controls.Common.CpxButton();
            this.droneLable = new CpxLib.Controls.Common.CpxLabel();
            this.SuspendLayout();
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // btnMoreActions
            // 
            resources.ApplyResources(this.btnMoreActions, "btnMoreActions");
            this.btnMoreActions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnMoreActions.ButtonAction = null;
            this.btnMoreActions.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnMoreActions.ForeColor = System.Drawing.Color.White;
            this.btnMoreActions.Image = global::CpxLib.Properties.Resources.options;
            this.btnMoreActions.Name = "btnMoreActions";
            this.toolTip1.SetToolTip(this.btnMoreActions, resources.GetString("btnMoreActions.ToolTip"));
            this.btnMoreActions.UseVisualStyleBackColor = false;
            this.btnMoreActions.Click += new System.EventHandler(this.btnMoreActions_Click);
            // 
            // btnSettings
            // 
            resources.ApplyResources(this.btnSettings, "btnSettings");
            this.btnSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnSettings.ButtonAction = null;
            this.btnSettings.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnSettings.ForeColor = System.Drawing.Color.White;
            this.btnSettings.Image = global::CpxLib.Properties.Resources.settings;
            this.btnSettings.Name = "btnSettings";
            this.toolTip1.SetToolTip(this.btnSettings, resources.GetString("btnSettings.ToolTip"));
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnSchedule
            // 
            resources.ApplyResources(this.btnSchedule, "btnSchedule");
            this.btnSchedule.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnSchedule.ButtonAction = null;
            this.btnSchedule.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnSchedule.ForeColor = System.Drawing.Color.White;
            this.btnSchedule.Image = global::CpxLib.Properties.Resources.schedual;
            this.btnSchedule.Name = "btnSchedule";
            this.toolTip1.SetToolTip(this.btnSchedule, resources.GetString("btnSchedule.ToolTip"));
            this.btnSchedule.UseVisualStyleBackColor = false;
            this.btnSchedule.Click += new System.EventHandler(this.btnSchedule_Click);
            // 
            // btnEditPlan
            // 
            resources.ApplyResources(this.btnEditPlan, "btnEditPlan");
            this.btnEditPlan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnEditPlan.ButtonAction = null;
            this.btnEditPlan.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnEditPlan.ForeColor = System.Drawing.Color.White;
            this.btnEditPlan.Image = global::CpxLib.Properties.Resources.map_editing;
            this.btnEditPlan.Name = "btnEditPlan";
            this.toolTip1.SetToolTip(this.btnEditPlan, resources.GetString("btnEditPlan.ToolTip"));
            this.btnEditPlan.UseVisualStyleBackColor = false;
            this.btnEditPlan.Click += new System.EventHandler(this.btnEditPlan_Click);
            // 
            // btnShowNightCamera
            // 
            resources.ApplyResources(this.btnShowNightCamera, "btnShowNightCamera");
            this.btnShowNightCamera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnShowNightCamera.ButtonAction = "Start";
            this.btnShowNightCamera.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnShowNightCamera.ForeColor = System.Drawing.Color.White;
            this.btnShowNightCamera.Image = global::CpxLib.Properties.Resources.night_camera;
            this.btnShowNightCamera.Name = "btnShowNightCamera";
            this.toolTip1.SetToolTip(this.btnShowNightCamera, resources.GetString("btnShowNightCamera.ToolTip"));
            this.btnShowNightCamera.UseVisualStyleBackColor = false;
            this.btnShowNightCamera.Click += new System.EventHandler(this.btnShowNightCamera_Click);
            // 
            // btnShowDayCamera
            // 
            resources.ApplyResources(this.btnShowDayCamera, "btnShowDayCamera");
            this.btnShowDayCamera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnShowDayCamera.ButtonAction = "Start";
            this.btnShowDayCamera.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnShowDayCamera.ForeColor = System.Drawing.Color.White;
            this.btnShowDayCamera.Image = global::CpxLib.Properties.Resources.day_camera;
            this.btnShowDayCamera.Name = "btnShowDayCamera";
            this.toolTip1.SetToolTip(this.btnShowDayCamera, resources.GetString("btnShowDayCamera.ToolTip"));
            this.btnShowDayCamera.UseVisualStyleBackColor = false;
            this.btnShowDayCamera.Click += new System.EventHandler(this.btnShowDayCamera_Click);
            // 
            // btnConnectMAVS
            // 
            resources.ApplyResources(this.btnConnectMAVS, "btnConnectMAVS");
            this.btnConnectMAVS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnConnectMAVS.ButtonAction = null;
            this.btnConnectMAVS.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnConnectMAVS.ForeColor = System.Drawing.Color.White;
            this.btnConnectMAVS.Image = global::CpxLib.Properties.Resources.connect_vehicles;
            this.btnConnectMAVS.Name = "btnConnectMAVS";
            this.toolTip1.SetToolTip(this.btnConnectMAVS, resources.GetString("btnConnectMAVS.ToolTip"));
            this.btnConnectMAVS.UseVisualStyleBackColor = false;
            this.btnConnectMAVS.Click += new System.EventHandler(this.btnConnectMAVS_Click);
            // 
            // btnResumeFlight
            // 
            resources.ApplyResources(this.btnResumeFlight, "btnResumeFlight");
            this.btnResumeFlight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnResumeFlight.ButtonAction = null;
            this.btnResumeFlight.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnResumeFlight.ForeColor = System.Drawing.Color.White;
            this.btnResumeFlight.Image = global::CpxLib.Properties.Resources.resume;
            this.btnResumeFlight.Name = "btnResumeFlight";
            this.toolTip1.SetToolTip(this.btnResumeFlight, resources.GetString("btnResumeFlight.ToolTip"));
            this.btnResumeFlight.UseVisualStyleBackColor = false;
            this.btnResumeFlight.Click += new System.EventHandler(this.btnResumeFlight_Click);
            // 
            // btnLoiter
            // 
            resources.ApplyResources(this.btnLoiter, "btnLoiter");
            this.btnLoiter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnLoiter.ButtonAction = null;
            this.btnLoiter.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnLoiter.ForeColor = System.Drawing.Color.White;
            this.btnLoiter.Image = global::CpxLib.Properties.Resources.pause;
            this.btnLoiter.Name = "btnLoiter";
            this.toolTip1.SetToolTip(this.btnLoiter, resources.GetString("btnLoiter.ToolTip"));
            this.btnLoiter.UseVisualStyleBackColor = false;
            this.btnLoiter.Click += new System.EventHandler(this.btnLoiter_Click);
            // 
            // btnRtl
            // 
            resources.ApplyResources(this.btnRtl, "btnRtl");
            this.btnRtl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnRtl.ButtonAction = null;
            this.btnRtl.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnRtl.ForeColor = System.Drawing.Color.White;
            this.btnRtl.Image = global::CpxLib.Properties.Resources.return_home;
            this.btnRtl.Name = "btnRtl";
            this.toolTip1.SetToolTip(this.btnRtl, resources.GetString("btnRtl.ToolTip"));
            this.btnRtl.UseVisualStyleBackColor = false;
            this.btnRtl.Click += new System.EventHandler(this.btnRtl_Click);
            // 
            // btnFlyByMap
            // 
            resources.ApplyResources(this.btnFlyByMap, "btnFlyByMap");
            this.btnFlyByMap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnFlyByMap.ButtonAction = null;
            this.btnFlyByMap.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnFlyByMap.ForeColor = System.Drawing.Color.White;
            this.btnFlyByMap.Image = global::CpxLib.Properties.Resources.fly_by_path;
            this.btnFlyByMap.Name = "btnFlyByMap";
            this.toolTip1.SetToolTip(this.btnFlyByMap, resources.GetString("btnFlyByMap.ToolTip"));
            this.btnFlyByMap.UseCompatibleTextRendering = true;
            this.btnFlyByMap.UseVisualStyleBackColor = false;
            this.btnFlyByMap.Click += new System.EventHandler(this.btnFlyByMap_Click);
            // 
            // btnTakeOff
            // 
            resources.ApplyResources(this.btnTakeOff, "btnTakeOff");
            this.btnTakeOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnTakeOff.ButtonAction = null;
            this.btnTakeOff.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnTakeOff.ForeColor = System.Drawing.Color.White;
            this.btnTakeOff.Image = global::CpxLib.Properties.Resources.take_off;
            this.btnTakeOff.Name = "btnTakeOff";
            this.toolTip1.SetToolTip(this.btnTakeOff, resources.GetString("btnTakeOff.ToolTip"));
            this.btnTakeOff.UseVisualStyleBackColor = false;
            this.btnTakeOff.Click += new System.EventHandler(this.btnTakeOff_Click);
            // 
            // btnArmDisarm
            // 
            resources.ApplyResources(this.btnArmDisarm, "btnArmDisarm");
            this.btnArmDisarm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnArmDisarm.ButtonAction = null;
            this.btnArmDisarm.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnArmDisarm.ForeColor = System.Drawing.Color.White;
            this.btnArmDisarm.Image = global::CpxLib.Properties.Resources.arm;
            this.btnArmDisarm.Name = "btnArmDisarm";
            this.toolTip1.SetToolTip(this.btnArmDisarm, resources.GetString("btnArmDisarm.ToolTip"));
            this.btnArmDisarm.UseVisualStyleBackColor = false;
            this.btnArmDisarm.Click += new System.EventHandler(this.btnArmDisarm_Click);
            // 
            // btnNV
            // 
            resources.ApplyResources(this.btnNV, "btnNV");
            this.btnNV.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnNV.ButtonAction = null;
            this.btnNV.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.btnNV.ForeColor = System.Drawing.Color.White;
            this.btnNV.Name = "btnNV";
            this.toolTip1.SetToolTip(this.btnNV, resources.GetString("btnNV.ToolTip"));
            this.btnNV.UseVisualStyleBackColor = false;
            this.btnNV.Click += new System.EventHandler(this.cpxButton1_Click);
            // 
            // droneLable
            // 
            resources.ApplyResources(this.droneLable, "droneLable");
            this.droneLable.ForeColor = System.Drawing.Color.White;
            this.droneLable.Name = "droneLable";
            this.toolTip1.SetToolTip(this.droneLable, resources.GetString("droneLable.ToolTip"));
            // 
            // CpxToolbar
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.Controls.Add(this.btnNV);
            this.Controls.Add(this.droneLable);
            this.Controls.Add(this.btnMoreActions);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnSchedule);
            this.Controls.Add(this.btnEditPlan);
            this.Controls.Add(this.btnShowNightCamera);
            this.Controls.Add(this.btnShowDayCamera);
            this.Controls.Add(this.btnConnectMAVS);
            this.Controls.Add(this.btnResumeFlight);
            this.Controls.Add(this.btnLoiter);
            this.Controls.Add(this.btnRtl);
            this.Controls.Add(this.btnFlyByMap);
            this.Controls.Add(this.btnTakeOff);
            this.Controls.Add(this.btnArmDisarm);
            this.Name = "CpxToolbar";
            this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.ResumeLayout(false);

        }

        #endregion
        private  CpxButton btnResumeFlight;
        private  CpxButton btnLoiter;
        private  CpxButton btnRtl;
        private  CpxButton btnFlyByMap;
        private  CpxButton btnTakeOff;
        private  CpxButton btnConnectMAVS;
        public  CpxButton btnShowNightCamera;
        public  CpxButton btnShowDayCamera;
        private System.Windows.Forms.ToolTip toolTip1;
        private CpxButton btnMoreActions;
        private CpxButton btnSettings;
        private CpxButton btnEditPlan;
        private CpxButton btnArmDisarm;
        private CpxButton btnSchedule;
        private Common.CpxLabel droneLable;
        private CpxButton btnNV;
    }
}
