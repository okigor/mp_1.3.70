﻿using CpxLib.Controls.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Controls.AppControls
{
    public class ZoomMapTracker : ShapedUserControl
    {
        private CpxButton btnZoomIn;
        private CpxButton btnZoomOut;
        public ColorSlider Slider;

        public event EventHandler ZoomIn;
        public event EventHandler ZoomOut;
        public int zoom { get; private set; }
        public int brightness { get; private set; }
        public int contrast { get; private set; }
        public ZoomMapTracker()
        {
            InitializeComponent();
            this.btnZoomIn.BackColor = System.Drawing.Color.FromArgb(27, 24, 23);
            this.btnZoomOut.BackColor = System.Drawing.Color.FromArgb(27, 24, 23);

            btnZoomIn.Click += BtnZoomIn_Click;
            btnZoomOut.Click += BtnZoomOut_Click;
        }

        private void BtnZoomOut_Click(object sender, EventArgs e)
        {
            ZoomOut?.Invoke(sender, e);
          

        }

        private void BtnZoomIn_Click(object sender, EventArgs e)
        {
            ZoomIn?.Invoke(sender, e);
         
        }

        private void InitializeComponent()
        {
            this.Slider = new CpxLib.Controls.Common.ColorSlider();
            this.btnZoomIn = new CpxLib.Controls.Common.CpxButton();
            this.btnZoomOut = new CpxLib.Controls.Common.CpxButton();
            this.SuspendLayout();
            // 
            // Slider
            // 
            this.Slider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.Slider.BarInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.Slider.BarPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.Slider.BarPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.Slider.BorderRoundRectSize = new System.Drawing.Size(8, 8);
            this.Slider.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Slider.ElapsedInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.Slider.ElapsedPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.Slider.ElapsedPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.Slider.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            this.Slider.ForeColor = System.Drawing.Color.Maroon;
            this.Slider.LargeChange = ((uint)(5u));
            this.Slider.Location = new System.Drawing.Point(0, 32);
            this.Slider.Margin = new System.Windows.Forms.Padding(2);
            this.Slider.Name = "Slider";
            this.Slider.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.Slider.ScaleDivisions = 10;
            this.Slider.ScaleSubDivisions = 5;
            this.Slider.ShowDivisionsText = false;
            this.Slider.ShowSmallScale = false;
            this.Slider.Size = new System.Drawing.Size(33, 318);
            this.Slider.SmallChange = ((uint)(1u));
            this.Slider.TabIndex = 0;
            this.Slider.ThumbImage = global::CpxLib.Properties.Resources.zoom_button;
            this.Slider.ThumbInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(152)))));
            this.Slider.ThumbPenColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(56)))), ((int)(((byte)(152)))));
            this.Slider.ThumbRoundRectSize = new System.Drawing.Size(23, 23);
            this.Slider.ThumbSize = new System.Drawing.Size(23, 23);
            this.Slider.TickAdd = 0F;
            this.Slider.TickColor = System.Drawing.Color.White;
            this.Slider.TickDivide = 0F;
            this.Slider.TickStyle = System.Windows.Forms.TickStyle.None;
            // 
            // btnZoomIn
            // 
            this.btnZoomIn.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnZoomIn.FlatAppearance.BorderSize = 0;
            this.btnZoomIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZoomIn.Image = global::CpxLib.Properties.Resources.zoom_in;
            this.btnZoomIn.Location = new System.Drawing.Point(0, 0);
            this.btnZoomIn.Margin = new System.Windows.Forms.Padding(2);
            this.btnZoomIn.Name = "btnZoomIn";
            this.btnZoomIn.Size = new System.Drawing.Size(33, 32);
            this.btnZoomIn.TabIndex = 1;
            this.btnZoomIn.Tag = "custom";
            this.btnZoomIn.UseVisualStyleBackColor = true;
            // 
            // btnZoomOut
            // 
            this.btnZoomOut.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnZoomOut.FlatAppearance.BorderSize = 0;
            this.btnZoomOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZoomOut.Image = global::CpxLib.Properties.Resources.zoom_out;
            this.btnZoomOut.Location = new System.Drawing.Point(0, 350);
            this.btnZoomOut.Margin = new System.Windows.Forms.Padding(2);
            this.btnZoomOut.Name = "btnZoomOut";
            this.btnZoomOut.Size = new System.Drawing.Size(33, 32);
            this.btnZoomOut.TabIndex = 2;
            this.btnZoomOut.Tag = "custom";
            this.btnZoomOut.UseVisualStyleBackColor = true;
            // 
            // ZoomMapTracker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.Slider);
            this.Controls.Add(this.btnZoomOut);
            this.Controls.Add(this.btnZoomIn);
            this.Curvature = 20;
            this.Name = "ZoomMapTracker";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Size = new System.Drawing.Size(33, 382);
            this.ResumeLayout(false);

        }
    }
}
