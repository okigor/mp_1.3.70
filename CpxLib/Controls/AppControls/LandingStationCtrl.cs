﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using CpxLib.Core;

namespace CpxLib.Controls
{
    public partial class LandingStationCtrl : UserControl
    {
        public CpxLandingStation landingStation = null;
        VehicleConnection _vehicleConnection;
        private CancellationTokenSource _cancelSource = new CancellationTokenSource();
        bool inAction = false;
        public LandingStationCtrl(VehicleConnection vehicleConnection)
        {
            InitializeComponent();
            _vehicleConnection = vehicleConnection;
        }

        private void open_Click_1(object sender, EventArgs e)
        {
            if (!inAction)
                OpenAsync();
        }
        public async void OpenAsync()
        {
            inAction = true;
            await Task.Factory.StartNew(() =>
            {
                //MessageBox.Show(_vehicleConnection.getLandingIPPort());
                _vehicleConnection.ConnectLandingStation();
                var result = _vehicleConnection.OpenLandingStation();
                if (!result)
                {
                    return;
                }
            });
            inAction = false;
        }
        private void close_Click_1(object sender, EventArgs e)
        {
            if (!inAction)
                CloseAsync();
        }
        public async void CloseAsync()
        {
            inAction = true;
            await Task.Factory.StartNew(() =>
            {
                _vehicleConnection.ConnectLandingStation();
                var result = _vehicleConnection.SafeCloseLandingStation();
                if (!result)
                {
                    return;
                }
                //landingStation.Disconnect();
            });
            inAction = false;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            _vehicleConnection.setStop(true);
            //connectToLandingStation();
            //var result = landingStation.stopLandingStation(10000);
            //if (!result)
            //{
            //    MessageBox.Show("Unable to close landing station");
            //    return;
            //}
            //landingStation.Disconnect();
        }

        private void btnCharge_Click(object sender, EventArgs e)
        {
            if (!inAction)
                ChargeAsync();
        }

        private async void ChargeAsync()
        {
            inAction = true;
            await Task.Factory.StartNew(() =>
            {
                _vehicleConnection.ConnectLandingStation();
                var result = _vehicleConnection.ChargeDrone();
                if (!result)
                {
                    return;
                }
                //landingStation.Disconnect();
            });
            inAction = false;
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            _vehicleConnection.setContinue(true);
        }

        private void LSLable_Click(object sender, EventArgs e)
        {

        }
    }
}
