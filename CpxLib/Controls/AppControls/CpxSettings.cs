﻿
using CpxLib.Controls.Settings;
using CpxLib.Core;
using MissionPlanner;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CpxLib.Controls.AppControls
{
    public partial class CpxSettings : MyUserControl
    {
          public CpxSettings()
        {
            var x = MainV2.instance;
            //Properties.Settings.Default.SettingsSaving += SettingsSaving;
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(CpxHost.Instance.Language);
            InitializeComponent();



        }

        private void SettingsSaving(object sender, CancelEventArgs e)
        {
            ChangeLanguage(((CpxLib.Properties.Settings)sender).Language);
        }

        private void ChangeLanguage(string lang)
        {
            foreach (Control c in this.Controls)
            {
                ComponentResourceManager resources = new ComponentResourceManager(typeof(CpxSettings));
                resources.ApplyResources(c, c.Name, new CultureInfo(lang));
                IEnumerable<Control> allItems = GetAll(c);
                foreach (Control cn in allItems)
                {
                    resources = new ComponentResourceManager(typeof(CpxSettings));
                    resources.ApplyResources(cn, cn.Name, new CultureInfo(lang));
                }
                
            }
        }
        private IEnumerable<Control> GetAll(Control control)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl))
                                      .Concat(controls);
        }
        private void ShowVehicles()
        {
            var ctrl = new VehicleSettings()
            {
                Dock = DockStyle.Fill
            };
            MissionPlanner.Utilities.ThemeManager.ApplyThemeTo(ctrl);
            pnlWorkspace.Controls.Clear();
            pnlWorkspace.Controls.Add(ctrl);
        }
        private void ShowGeneral()
        {
            var ctrl = new GeneralSettings()
            {
                Dock = DockStyle.Fill
            };
            UIUtils.ApplyTheme(ctrl);
            pnlWorkspace.Controls.Clear();
            pnlWorkspace.Controls.Add(ctrl);
        }

        private void ShowSchedules()
        {
            //var ctrl = new MissionPlanner.GCSViews.ConfigurationView.ConfigHWCompass()
            //{
            //    Dock = DockStyle.Fill
            //};

            var ctrl = new ScheduleSettings()
            {
                Dock = DockStyle.Fill
            };
            MissionPlanner.Utilities.ThemeManager.ApplyThemeTo(ctrl);
            pnlWorkspace.Controls.Clear();
            pnlWorkspace.Controls.Add(ctrl);
        }
        private void ShowCompass()
        {
            var ctrl = new MissionPlanner.GCSViews.ConfigurationView.ConfigHWCompass()
            {
                Dock = DockStyle.Fill
            };

            
            MissionPlanner.Utilities.ThemeManager.ApplyThemeTo(ctrl);
            pnlWorkspace.Controls.Clear();
            pnlWorkspace.Controls.Add(ctrl);
            ctrl.Activate();
        }
        private void ShowPlans()
        {
            var ctrl = new PlanSettings()
            {
                Dock = DockStyle.Fill
            };
            MissionPlanner.Utilities.ThemeManager.ApplyThemeTo(ctrl);
            pnlWorkspace.Controls.Clear();
            pnlWorkspace.Controls.Add(ctrl);
        }
        private void CpxSettings_Load(object sender, EventArgs e)
        {
            radGeneral.Checked = true;
        }

        private void radOptions_CheckedChanged(object sender, EventArgs e)
        {
            var btn = (RadioButton)sender;

            lblCaption.Text = btn.Text;
            pictureBox2.Image = btn.Image;
            if (CpxHost.Instance.IsRightToLeftLayout)
                lblCaption.Location = new Point(pictureBox2.Location.X - 70 - lblCaption.Size.Width, lblCaption.Location.Y);
            else
                lblCaption.Location = new Point(pictureBox2.Location.X + 70, lblCaption.Location.Y);
            if (radGeneral.Checked)
            {
                ShowGeneral();
            }
            else if (radVehicles.Checked)
            {
                ShowVehicles();
            }
            else if (radSchedules.Checked)
            {
                ShowSchedules();
            }
            else if (radCompass.Checked)
            {
                ShowCompass();
            }
            else if (radPlans.Checked)
            {
                ShowPlans();
            }
            else if (radBtnGeoFence.Checked)
                ShowGeoFence();
        }

        private void ShowGeoFence()
        {
            var ctrl = new MissionPlanner.GCSViews.ConfigurationView.ConfigAC_Fence()
            {
                Dock = DockStyle.Fill,
                RightToLeft = RightToLeft.No
            };
            MissionPlanner.Utilities.ThemeManager.ApplyThemeTo(ctrl);
            pnlWorkspace.Controls.Clear();
            pnlWorkspace.Controls.Add(ctrl);
            ctrl.Activate();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            UIUtils.ShowFlightData();
        }
    }
}
