﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO;

namespace HostGStreamer
{
    public partial class GStreamerWrapper : UserControl
    {
        delegate bool EnumThreadDelegate(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll")]
        static extern bool EnumThreadWindows(int dwThreadId, EnumThreadDelegate lpfn,
            IntPtr lParam);

        private IEnumerable<IntPtr> EnumerateProcessWindowHandles(int processId)
        {
            var handles = new List<IntPtr>();
            try
            {
                foreach (ProcessThread thread in Process.GetProcessById(processId).Threads)
                    EnumThreadWindows(thread.Id,
                        (hWnd, lParam) =>
                        {
                            handles.Add(hWnd);
                            return true;
                        },
                        IntPtr.Zero);
            }
            catch { }

            return handles;
        }
        [DllImport("USER32.DLL")]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        private const int SWP_NOOWNERZORDER = 0x200;
        private const int SWP_NOREDRAW = 0x8;
        private const int SWP_NOZORDER = 0x4;
        private const int SWP_SHOWWINDOW = 0x0040;
        private const int WS_EX_MDICHILD = 0x40;
        private const int SWP_FRAMECHANGED = 0x20;
        private const int SWP_NOACTIVATE = 0x10;
        private const int SWP_ASYNCWINDOWPOS = 0x4000;
        private const int SWP_NOMOVE = 0x2;
        private const int SWP_NOSIZE = 0x1;
        private const int GWL_STYLE = (-16);
        private const int WS_VISIBLE = 0x10000000;
        private const int WM_CLOSE = 0x10;
        private const int WS_CHILD = 0x40000000;
        private const int SW_HIDE = 0;
        //assorted constants needed
        public static int WS_BORDER = 0x00800000; //window with border
        public static int WS_DLGFRAME = 0x00400000; //window with double border but no title
        public static int WS_CAPTION = WS_BORDER | WS_DLGFRAME; //window with a title bar
        public static uint WM_SYSCOMMAND = 0x112;
        public static int SC_MINIMIZE = 0xF020;
        public static int WS_THICKFRAME = 262144;


        [DllImport("USER32.DLL")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern long SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll", EntryPoint = "GetWindowLongA", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hwnd, int nIndex);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool MoveWindow(IntPtr hwnd, int x, int y, int cx, int cy, bool repaint);


        private static int SW_SHOWMINIMIZED = 2;


        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        private Process _process = null;
        private IntPtr _handle;

        public GStreamerWrapper()
        {
            InitializeComponent();
        }

        private void SetGSTPath(string gstdir)
        {
            //
            //C:\gstreamer\1.0\x86_64\bin\gst-launch-1.0.exe

            if (!File.Exists(gstdir))
                return;

            gstdir = Path.GetDirectoryName(gstdir);
            gstdir = Path.GetDirectoryName(gstdir);

            // Prepend native path to environment path, to ensure the
            // right libs are being used.
            var path = Environment.GetEnvironmentVariable("PATH");
            path = Path.Combine(gstdir, "bin") + ";" + Path.Combine(gstdir, "lib") + ";" + path;
            Environment.SetEnvironmentVariable("PATH", path);

            Environment.SetEnvironmentVariable("GSTREAMER_ROOT", gstdir);

            Environment.SetEnvironmentVariable("GSTREAMER_1_0_ROOT_X86_64", gstdir);

            Environment.SetEnvironmentVariable("GST_PLUGIN_PATH", Path.Combine(gstdir, "lib"));

            Environment.SetEnvironmentVariable("GST_DEBUG_DUMP_DOT_DIR", Path.GetTempPath());
        }

        public string Pipeline { get; set; }
        public string GStreamerInstallDir { get; set; }

        public void Stop()
        {
            if (_process != null)
            {
                if (!_process.HasExited)
                {
                    _process.Kill();
                    _handle = IntPtr.Zero;
                }

            }
        }

        public async void RunAsync()
        {
            await Task.Run(() =>
            {
                Run();
            });
        }
        public void Run()
        {



            SetGSTPath(GStreamerInstallDir);

            var psi = new ProcessStartInfo(GStreamerInstallDir, Pipeline);

            psi.CreateNoWindow = true;

            //psi.UseShellExecute = false;


            //psi.RedirectStandardInput = true;
            //psi.RedirectStandardOutput = true;
            //psi.RedirectStandardError = true;

            _process = Process.Start(psi);
            _process.Exited += _process_Exited;

            //// Get the main handle
            var appWin = _process.MainWindowHandle;

            while (appWin == IntPtr.Zero)
            {
                Thread.Sleep(500);
                Application.DoEvents();
                appWin = _process.MainWindowHandle;


                ShowWindowAsync(_process.MainWindowHandle, SW_SHOWMINIMIZED);

            }


            while (_handle == IntPtr.Zero)
            {

                Thread.Sleep(500);


                Application.DoEvents();
                foreach (var handle in EnumerateProcessWindowHandles(_process.Id))
                {
                    StringBuilder stringBuilder = new StringBuilder(1024);
                    GetClassName(handle, stringBuilder, stringBuilder.Capacity);

                    if (stringBuilder.ToString() == "GstD3DVideoSinkInternalWindow")
                    {
                        SetParent(handle, this.Handle);
                        var style = GetWindowLong(handle, GWL_STYLE);
                        SetWindowLong(handle, GWL_STYLE, (style & ~WS_CAPTION & ~WS_BORDER & ~WS_THICKFRAME));
                        MoveWindow(handle, 0, 0, Width, Height, true);

                        _handle = handle;
                    }

                }
            }

        }

        private void _process_Exited(object sender, EventArgs e)
        {

        }

        protected override void OnResize(EventArgs e)
        {
            if (_handle != IntPtr.Zero)
            {
               
                MoveWindow(_handle, 0, 0, Width, Height, true);
            }
            base.OnResize(e);
        }

    }
}
