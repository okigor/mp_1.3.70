﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CpxLib.Controls.Common;
using CpxLib.Scheduling;
using CpxLib.Core;
using CpxLib.Properties;

namespace CpxLib.Controls.AppControls
{
    //TODO: change sorting when we support more than one drones
    public partial class CpxTaskList : DraggableHost
    {
        DateTime nextFlight;
        public CpxTaskList()
        {
            InitializeComponent();
            timer1.Tick += new EventHandler(timer1_Tick);
            //timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (lstTasks.Items.Count == 0)
            {
                label2.Text = "";
                timer1.Stop();
                return;
            }
            var nextRun = lstTasks.Items[0];
            if (nextRun.SubItems[1].Text == "InMission")
            {
                timer1.Stop();
                label2.Text = "";
            }
            else if (nextRun.SubItems[1].Text == "Waiting")
            {
                if (!timer1.Enabled)
                {
                    timer1.Start();
                }
                //DateTime nextflight = DateTime.Parse(nextRun.SubItems[2].Text);
                DateTime now = DateTime.Now;
                //MessageBox.Show(now.ToString());
                label2.Text = Resources.NextTask + (nextFlight - now).ToString(@"hh\:mm\:ss");
                label2.Left = this.Width - label2.Width;

            }

        }

        private void UpdateTaskStatusImpl(JobStatusChangeEventArgs args)
        {
            var item = lstTasks
                             .Items
                             .Cast<ListViewItem>()
                             .FirstOrDefault(x => x.Tag.ToString() == args.MavId);

            if (item == null)
            {
                item = new ListViewItem(args.Drone);
                item.SubItems.Add(args.Status.ToString());
                item.SubItems.Add(args.NextRunTime.ToString("dd/MM/yyyy hh:mm"));
                item.Tag = args.MavId;
                lstTasks.Items.Add(item);
                nextFlight = args.NextRunTime;

                timer1.Start();
            }
            else
            {
                item.SubItems[1].Text = args.Status.ToString();
                item.SubItems[2].Text = args.NextRunTime.ToString("dd/MM/yyyy hh:mm");
                nextFlight = args.NextRunTime;
                timer1.Start();
            }

        }

        internal void UpdateTaskStatus(JobStatusChangeEventArgs args)
        {
            Action<JobStatusChangeEventArgs> Delegate_UpdateTaskStatusImpl = UpdateTaskStatusImpl;
            DateTime now = DateTime.Now;
            //if (lstTasks.Items.Count==0 || lstTasks.Items[0].SubItems[1].Text != "waiting" || args.NextRunTime.ToString("dd/MM/yyyy hh:mm") == lstTasks.Items[0].SubItems[2].Text)
            Invoke(Delegate_UpdateTaskStatusImpl, args);
        }

        private void lstTasks_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            CpxHost.Instance.UI.StartSchedule();
        }
    }
}
