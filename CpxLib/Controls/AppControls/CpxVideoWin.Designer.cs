﻿using CpxLib.Controls.Common;

namespace CpxLib.Controls.AppControls
{
    partial class CpxVideoWin
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CpxVideoWin));
            this.btnSwapVideoAndMap = new CpxLib.Controls.Common.CpxButton();
            this.cpxVideo = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resizer)).BeginInit();
            this.cpxVideo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSwapVideoAndMap);
            this.panel1.Padding = new System.Windows.Forms.Padding(4);
            this.panel1.Controls.SetChildIndex(this.label1, 0);
            this.panel1.Controls.SetChildIndex(this.btnSwapVideoAndMap, 0);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 12);
            this.label1.Size = new System.Drawing.Size(25, 23);
            this.label1.Text = "...";
            // 
            // btnSwapVideoAndMap
            // 
            this.btnSwapVideoAndMap.BackColor = System.Drawing.Color.Black;
            this.btnSwapVideoAndMap.ButtonAction = null;
            this.btnSwapVideoAndMap.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSwapVideoAndMap.FlatAppearance.BorderSize = 0;
            this.btnSwapVideoAndMap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSwapVideoAndMap.Image = global::CpxLib.Properties.Resources.icons8_expand_filled_26;
            this.btnSwapVideoAndMap.Location = new System.Drawing.Point(317, 4);
            this.btnSwapVideoAndMap.Name = "btnSwapVideoAndMap";
            this.btnSwapVideoAndMap.Size = new System.Drawing.Size(37, 30);
            this.btnSwapVideoAndMap.TabIndex = 3;
            this.btnSwapVideoAndMap.UseVisualStyleBackColor = false;
            this.btnSwapVideoAndMap.Click += new System.EventHandler(this.btnSwapVideoAndMap_Click);
            
            // 
            // cpxVideo
            // 
            this.cpxVideo.Dock = System.Windows.Forms.DockStyle.Top;
            this.cpxVideo.Location = new System.Drawing.Point(0, 38);
            this.cpxVideo.Name = "cpxVideo";
            this.cpxVideo.Size = new System.Drawing.Size(358, 278);
            this.cpxVideo.TabIndex = 9;
            // 
            // CpxVideoWin
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.cpxVideo);
            this.Name = "CpxVideoWin";
            this.Title = "...";
            this.Resize += new System.EventHandler(this.CpxVideoWin_Resize);
            this.Controls.SetChildIndex(this.resizer, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.cpxVideo, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resizer)).EndInit();
            this.cpxVideo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public CpxButton btnSwapVideoAndMap;
     
        public System.Windows.Forms.Panel cpxVideo;
        //public CpxVideoOut cpxVideo;
    }
}
