﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using MissionPlanner;
using CpxLib.Core;
using GMap.NET.WindowsForms;
using GMap.NET;
using GMap.NET.WindowsForms.Markers;
using MissionPlanner.Utilities;
using CpxLib.Forms;
using System.Threading;
using CpxLib.Controls.Common;

namespace CpxLib.Controls
{
    public partial class CpxMissionPlanning : UserControl
    {
        //List<CpxCommand> cpxCommands;
        GMapOverlay MapOverlay;
        Common.CpxTextbox currentTextbox;
        int _lat;
        int _lng;
        int _alt;
        int _param1;
        int _param2;
        int _easting;
        int _northing;
        int _zone;

        private int currentIndex;
        int _command;
        //bool changedcell;
        bool createNew;
        private GMapOverlay GeoFenceOverlay;
        public CpxMissionPlanning()
        {
            GeoFenceOverlay = new GMapOverlay();
            InitializeComponent();
            //changedcell = false;
            //this.BackColor = Color.FromArgb(100, 0, 0, 0);
            //cpxCommands = new List<CpxCommand>();
            MapOverlay = new GMapOverlay();
            CpxHost.Instance.FlightPlanner.MainMap.Overlays.Add(MapOverlay);
            //ChangeMapClick();
            CpxHost.Instance.FlightPlanner.Commands.RowsAdded += Commands_RowsAdded;
            CpxHost.Instance.FlightPlanner.Commands.CellValueChanged += Commands_CellValueChanged;
            _lat = CpxHost.Instance.FlightPlanner.FlightPlannerBase.Lat.Index;
            _lng = CpxHost.Instance.FlightPlanner.FlightPlannerBase.Lon.Index;
            _alt = CpxHost.Instance.FlightPlanner.FlightPlannerBase.Alt.Index;
            _param1 = CpxHost.Instance.FlightPlanner.FlightPlannerBase.Param1.Index;
            _param2 = CpxHost.Instance.FlightPlanner.FlightPlannerBase.Param2.Index;
            _easting = CpxHost.Instance.FlightPlanner.FlightPlannerBase.coordEasting.Index;
            _northing = CpxHost.Instance.FlightPlanner.FlightPlannerBase.coordNorthing.Index;
            _zone = CpxHost.Instance.FlightPlanner.Commands.Columns["coordZone"].Index;

            _command = CpxHost.Instance.FlightPlanner.FlightPlannerBase.Command.Index;
            txtDefaultAlt.TextChanged += TxtDefaultAlt_TextChanged;
            comboBox1.SelectedItem = CpxHost.Instance.FlightPlanner.coords1.System;
            this.VisibleChanged += CpxMissionPlanning_VisibleChanged;
            CpxHost.Instance.FlightPlanner.MainMap.MouseDown += MainMap_MouseDown;
            var x = CpxHost.Instance.FlightPlanner.MainMap;
            createNew = true;
            currentIndex = -1;
            panel1.Height = comboBox1.Top + comboBox1.Height + 10;
            btnAdd.ButtonShape = CpxButton.ButtonShapeEnum.Ellipse;
            //CpxHost.Instance.FlightPlanner.contextMenuStripPoly.VisibleChanged += ContextMenuStripPoly_VisibleChanged;
            //CpxHost.Instance.FlightPlanner.MainMap.MouseUp += MainMap_MouseUp;
            CpxHost.Instance.FlightPlanner.contextMenuStripPoly.Opening += (s, e) =>
            {
                CpxHost.Instance.FlightPlanner.FlightPlannerBase.addPolygonPointToolStripMenuItem_Click(null, null);
                e.Cancel = true;
            };
        }

        private void MainMap_MouseDown(object sender, MouseEventArgs e)
        {
            if (CpxHost.Instance.FlightPlanner.MainMap.IsMouseOverMarker)
            {
                foreach (var marker in CpxHost.Instance.FlightPlanner.MainMap.Overlays[1].Markers)
                {
                    if (marker.IsMouseOver && marker.Tag!="H")
                    {
                        CpxCommand_Click(flowLayoutPanel1.Controls[Int32.Parse(marker.Tag.ToString()) - 1], e);
                        break;
                    }
                    //else if (marker.Tag == "H")
                    //{

                    //}
                }
            }
        }

        private void CpxMissionPlanning_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                txtDefaultAlt.Text = CpxHost.Instance.FlightPlanner.TXT_DefaultAlt.Text;
                comboBox1.SelectedItem = CpxHost.Instance.FlightPlanner.coords1.System;
                //CpxHost.Instance.FlightPlanner.MainMap.BringToFront();
                if (CpxHost.Instance.ComPort.MAV.cs.connected)
                    CpxHost.Instance.FlightPlanner.FlightPlannerBase.GeoFencedownloadToolStripMenuItem_Click(sender, e);
            }
        }
        private void TxtDefaultAlt_TextChanged(object sender, EventArgs e)
        {
            if (txtDefaultAlt.Text.Length > 0)
            {
                CpxHost.Instance.FlightPlanner.TXT_DefaultAlt.Text = txtDefaultAlt.Text;
            }
        }
        private void Commands_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (CpxHost.Instance.FlightPlanner.Commands.RowCount > flowLayoutPanel1.Controls.Count)
                return;
            if (CpxHost.Instance.FlightPlanner.cmb_missiontype.Text == "FENCE")
            {
                return;
            }
            if (e.RowIndex == -1)
                return;
            CpxCommand c = (CpxCommand)flowLayoutPanel1.Controls[e.RowIndex];
            if (e.ColumnIndex == _lat)
            {
                c.txtLatitude.Text = CpxHost.Instance.FlightPlanner.Commands[_lat, e.RowIndex].Value.ToString();
            }
            else if (e.ColumnIndex == _command)
            {
                c.cmbCommand.Text = CpxHost.Instance.FlightPlanner.Commands[_command, e.RowIndex].Value.ToString();
            }
            else if (e.ColumnIndex == _lng)
            {
                c.txtLongitude.Text = CpxHost.Instance.FlightPlanner.Commands[_lng, e.RowIndex].Value.ToString();
            }
            else if (e.ColumnIndex == _alt)
            {
                c.txtAltitude.Text = CpxHost.Instance.FlightPlanner.Commands[_alt, e.RowIndex].Value.ToString();
            }
            else if (e.ColumnIndex == _param1)
            {
                c.txtP1.Text = CpxHost.Instance.FlightPlanner.Commands[_param1, e.RowIndex].Value.ToString();
            }
            else if (e.ColumnIndex == _param2)
            {
                c.txtP2.Text = CpxHost.Instance.FlightPlanner.Commands[_param2, e.RowIndex].Value.ToString();
            }
            else if (e.ColumnIndex == _easting)
            {
                c.txtEasting.Text = CpxHost.Instance.FlightPlanner.Commands[_easting, e.RowIndex].Value.ToString();
            }
            else if (e.ColumnIndex == _northing)
            {
                c.txtNorthing.Text = CpxHost.Instance.FlightPlanner.Commands[_northing, e.RowIndex].Value.ToString();
            }
            else if (e.ColumnIndex == _zone)
            {
                c.txtZone.Text = CpxHost.Instance.FlightPlanner.Commands[_zone, e.RowIndex].Value.ToString();
            }
        }
        private void TxtZone_Enter(object sender, EventArgs e)
        {
            if (currentIndex == -1)
            {
                currentIndex = _zone;
                return;
            }
            if (currentIndex == _zone)
                return;
            FindRightCommand((Common.CpxTextbox)sender, e);
            currentIndex = _zone;
        }
        private void setZone()
        {
            if (currentIndex == -1)
            {
                currentIndex = _zone;
                return;
            }
            if (currentIndex == _zone)
                return;
            currentIndex = _zone;
        }
        private void Commands_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (CpxHost.Instance.FlightPlanner.Commands[_command, e.RowIndex].Value.ToString() == "UNKNOWN")
            {
                CpxHost.Instance.FlightPlanner.Commands.Rows.Remove(CpxHost.Instance.FlightPlanner.Commands.Rows[e.RowIndex]);
                return;
            }
            if (createNew && CpxHost.Instance.FlightPlanner.cmb_missiontype.Text != "FENCE")
            {
                CpxCommand cpxCommand = new CpxCommand(comboBox1.Text);
                flowLayoutPanel1.Controls.Add(cpxCommand);
                cpxCommand.lblWpNo.Text = "#" + flowLayoutPanel1.Controls.Count;
                cpxCommand._command = CpxHost.Instance.FlightPlanner.Commands.Rows[e.RowIndex];
                cpxCommand.btnDelete.Click += BtnDelete_Click;
                cpxCommand.btnUp.Click += BtnUp_Click;
                cpxCommand.btnDown.Click += BtnDown_Click;
                cpxCommand.Click += CpxCommand_Click;
                cpxCommand.lblWpNo.Click += CpxCommand_Click;
                cpxCommand.cmbCommand.Click += CpxCommand_Click;
                cpxCommand.lblCommand.Click += CpxCommand_Click;
                cpxCommand.cmbCommand.SelectedIndexChanged += CmbCommand_SelectedIndexChanged;
                cpxCommand.txtLatitude.Enter += TxtLatitude_Enter;
                cpxCommand.txtLongitude.Enter += TxtLongitude_Enter;
                cpxCommand.txtAltitude.Enter += TxtAltitude_Enter;
                cpxCommand.txtP1.Enter += TxtP1_Enter;
                cpxCommand.txtP2.Enter += TxtP2_Enter;
                cpxCommand.txtZone.Enter += TxtZone_Enter;
                cpxCommand.txtEasting.Enter += TxtEasting_Enter;
                cpxCommand.txtNorthing.Enter += TxtNorthing_Enter;
                CpxCommand_Click(cpxCommand, new EventArgs());
                if (cpxCommand.txtZone.Text == "0")
                {
                    cpxCommand.txtZone.Text = CpxHost.Instance.ComPort.MAV.cs.HomeLocation.GetUTMZone() + "";
                    setZone();
                }
            }
            else
            {
                createNew = true;
            }


        }

        private void TxtNorthing_Enter(object sender, EventArgs e)
        {
            if (currentIndex == -1)
            {
                currentIndex = _northing;
                return;
            }
            if (currentIndex == _northing)
                return;
            FindRightCommand((Common.CpxTextbox)sender, e);
            //Common.CpxTextbox cpxTextbox = (Common.CpxTextbox)sender;
            //for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            //{
            //    if (flowLayoutPanel1.Controls[i] == cpxTextbox.Parent)
            //    {

            //        CpxCommand command = (CpxCommand)flowLayoutPanel1.Controls[i];
            //        ChangeTextInCommands(command, i, sender);
            //        command.textBox_Click(sender, e);
            //    }
            //}
            currentIndex = _northing;
        }

        private void TxtEasting_Enter(object sender, EventArgs e)
        {
            if (currentIndex == -1)
            {
                currentIndex = _easting;
                return;
            }
            if (currentIndex == _easting)
                return;
            FindRightCommand((Common.CpxTextbox)sender, e);
            //Common.CpxTextbox cpxTextbox = (Common.CpxTextbox)sender;
            //for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            //{
            //    if (flowLayoutPanel1.Controls[i] == cpxTextbox.Parent)
            //    {

            //        CpxCommand command = (CpxCommand)flowLayoutPanel1.Controls[i];
            //        ChangeTextInCommands(command, i, sender);
            //        command.textBox_Click(sender, e);
            //    }
            //}
            currentIndex = _easting;
        }

        private void TxtP2_Enter(object sender, EventArgs e)
        {
            if (currentIndex == -1)
            {
                currentIndex = _param2;
                return;
            }
            if (currentIndex == _param2)
                return;
            FindRightCommand((Common.CpxTextbox)sender, e);
            //Common.CpxTextbox cpxTextbox = (Common.CpxTextbox)sender;
            //for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            //{
            //    if (flowLayoutPanel1.Controls[i] == cpxTextbox.Parent)
            //    {

            //        CpxCommand command = (CpxCommand)flowLayoutPanel1.Controls[i];
            //        ChangeTextInCommands(command, i, sender);
            //        command.textBox_Click(sender, e);
            //    }
            //}
            currentIndex = _param2;
        }

        private void TxtP1_Enter(object sender, EventArgs e)
        {
            if (currentIndex == -1)
            {
                currentIndex = _param1;
                return;
            }
            if (currentIndex == _param1)
                return;
            FindRightCommand((Common.CpxTextbox)sender, e);
            Common.CpxTextbox cpxTextbox = (Common.CpxTextbox)sender;
            //for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            //{
            //    if (flowLayoutPanel1.Controls[i] == cpxTextbox.Parent)
            //    {

            //        CpxCommand command = (CpxCommand)flowLayoutPanel1.Controls[i];
            //        ChangeTextInCommands(command, i, sender);
            //        command.textBox_Click(sender, e);
            //    }
            //}
            currentIndex = _param1;

        }

        private void TxtAltitude_Enter(object sender, EventArgs e)
        {
            if (currentIndex == -1)
            {
                currentIndex = _alt;
                return;
            }
            if (currentIndex == _alt)
                return;
            FindRightCommand((Common.CpxTextbox)sender, e);
            //Common.CpxTextbox cpxTextbox = (Common.CpxTextbox)sender;
            //for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            //{
            //    if (flowLayoutPanel1.Controls[i] == cpxTextbox.Parent)
            //    {

            //        CpxCommand command = (CpxCommand)flowLayoutPanel1.Controls[i];
            //        ChangeTextInCommands(command, i, sender);
            //        command.textBox_Click(sender, e);
            //    }
            //}
            currentIndex = _alt;
        }

        private void TxtLongitude_Enter(object sender, EventArgs e)
        {
            if (currentIndex == -1)
            {
                currentIndex = _lng;
                return;
            }
            if (currentIndex == _lng)
                return;
            FindRightCommand((Common.CpxTextbox)sender, e);
            //Common.CpxTextbox cpxTextbox = (Common.CpxTextbox)sender;
            //for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            //{
            //    if (flowLayoutPanel1.Controls[i] == cpxTextbox.Parent)
            //    {

            //        CpxCommand command = (CpxCommand)flowLayoutPanel1.Controls[i];
            //        ChangeTextInCommands(command, i, sender);
            //        command.textBox_Click(sender, e);
            //    }
            //}
            currentIndex = _lng;
        }
        private void FindRightCommand(CpxTextbox cpxTextbox, EventArgs e)
        {
            for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            {
                if (flowLayoutPanel1.Controls[i] == cpxTextbox.Parent)
                {
                    CpxCommand command = (CpxCommand)flowLayoutPanel1.Controls[i];
                    ChangeTextInCommands(command, i, cpxTextbox);
                    command.textBox_Click(cpxTextbox, e);
                }
            }
        }
        private void TxtLatitude_Enter(object sender, EventArgs e)
        {
            if (currentIndex == -1)
            {
                currentIndex = _lat;
                return;
            }
            if (currentIndex == _lat)
                return;
            FindRightCommand((Common.CpxTextbox)sender, e);
            //Common.CpxTextbox cpxTextbox = ;

            currentIndex = _lat;
        }
        private void ChangeTextInCommands(CpxCommand command, int i, object sender)
        {
            //changedcell = true;
            if (currentIndex == -1)
                return;
            if (currentIndex == _lat)
            {
                CpxHost.Instance.FlightPlanner.Commands[currentIndex, i].Value = command.txtLatitude.Text;
            }
            else if (currentIndex == _lng)
            {
                CpxHost.Instance.FlightPlanner.Commands[currentIndex, i].Value = command.txtLongitude.Text;
            }
            else if (currentIndex == _alt)
            {
                CpxHost.Instance.FlightPlanner.Commands[currentIndex, i].Value = command.txtAltitude.Text;
            }
            else if (currentIndex == _param1)
            {
                CpxHost.Instance.FlightPlanner.Commands[currentIndex, i].Value = command.txtP1.Text;
            }
            else if (currentIndex == _param2)
            {
                CpxHost.Instance.FlightPlanner.Commands[currentIndex, i].Value = command.txtP2.Text;
            }
            else if (currentIndex == _easting)
            {
                CpxHost.Instance.FlightPlanner.Commands[currentIndex, i].Value = command.txtEasting.Text;
            }
            else if (currentIndex == _northing)
            {
                CpxHost.Instance.FlightPlanner.Commands[currentIndex, i].Value = command.txtNorthing.Text;
            }
            else if (currentIndex == _zone)
            {
                CpxHost.Instance.FlightPlanner.Commands[currentIndex, i].Value = command.txtZone.Text;
            }
            CpxHost.Instance.FlightPlanner.FlightPlannerBase.Commands_CellEndEdit(sender, new DataGridViewCellEventArgs(currentIndex, i));
        }
        private void CmbCommand_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            {
                CpxCommand temp = (CpxCommand)flowLayoutPanel1.Controls[i];
                if (temp.cmbCommand == comboBox)
                {
                    CpxHost.Instance.FlightPlanner.Commands[CpxHost.Instance.FlightPlanner.FlightPlannerBase.Command.Index, i].Value = comboBox.Text;
                }
            }
        }

        private void CpxCommand_Click(object sender, EventArgs e)
        {

            CpxCommand cpxCommand = null;
            if (sender is CpxCommand)
            {
                cpxCommand = (CpxCommand)sender;
            }
            else
            {
                if (sender is Common.CpxLabel)
                    cpxCommand = (CpxCommand)((Common.CpxLabel)sender).Parent;
                else
                    cpxCommand = (CpxCommand)((ComboBox)sender).Parent;
            }
            for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            {

                CpxCommand temp = (CpxCommand)flowLayoutPanel1.Controls[i];
                if (temp.Height > temp.cmbCommand.Top + temp.cmbCommand.Height + 20)
                {
                    ChangeTextInCommands(temp, i, sender);
                    temp.CpxCommand_LostFocus();
                    break;
                }
            }
            for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            {
                if (flowLayoutPanel1.Controls[i] == cpxCommand)
                {
                    //if (changedcell)
                    //{
                    cpxCommand.CpxCommand_Click();
                    //changedcell = false;
                    //}

                    break;
                }
            }
            currentIndex = -1;
        }
        GraphicsPath GetRoundPath(Common.CpxButton btn, int radius)
        {
            float r2 = radius / 2f;
            GraphicsPath GraphPath = new GraphicsPath();
            GraphPath.AddArc(btn.Top, btn.Left, radius, radius, 180, 90);
            GraphPath.AddLine(btn.Top + r2, btn.Left, btn.Width - r2, btn.Left);
            GraphPath.AddArc(btn.Top + btn.Width - radius, btn.Left, radius, radius, 270, 90);
            GraphPath.AddLine(btn.Width, btn.Left + r2, btn.Width, btn.Height - r2);
            GraphPath.AddArc(btn.Top + btn.Width - radius,
                             btn.Left + btn.Height - radius, radius, radius, 0, 90);
            GraphPath.AddLine(btn.Width - r2, btn.Height, btn.Top + r2, btn.Height);
            GraphPath.AddArc(btn.Top, btn.Left + btn.Height - radius, radius, radius, 90, 90);
            GraphPath.AddLine(btn.Top, btn.Height - r2, btn.Top, btn.Left + r2);
            GraphPath.CloseFigure();
            return GraphPath;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (flowLayoutPanel1.Controls.Count > 0)
                CpxHost.Instance.FlightPlanner.FlightPlannerBase.InsertCommand(flowLayoutPanel1.Controls.Count, MAVLink.MAV_CMD.WAYPOINT, 0, 0, 0, 0, 0, 0, 0);
            else
                CpxHost.Instance.FlightPlanner.FlightPlannerBase.InsertCommand(0, MAVLink.MAV_CMD.WAYPOINT, 0, 0, 0, 0, 0, 0, 0);
            RearrangeNumbers();
        }
        private void BtnDown_Click(object sender, EventArgs e)
        {
            Common.CpxButton cpxButton = (Common.CpxButton)sender;
            for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            {
                if (flowLayoutPanel1.Controls[i] == cpxButton.Parent && flowLayoutPanel1.Controls.Count > 1 && i < flowLayoutPanel1.Controls.Count - 1)
                {
                    CpxCommand first = (CpxCommand)flowLayoutPanel1.Controls[i];
                    CpxCommand_Click(first, new EventArgs());
                    //Thread.Sleep(1000);
                    CpxCommand second = (CpxCommand)flowLayoutPanel1.Controls[i + 1];
                    flowLayoutPanel1.Controls.SetChildIndex(second, i);
                    flowLayoutPanel1.Controls.SetChildIndex(first, i + 1);
                    createNew = false;
                    //CpxHost.Instance.FlightPlanner.FlightPlannerBase.Commands_CellContentClick(sender, new DataGridViewCellEventArgs(_down, i));
                    DataGridViewRow myrow = CpxHost.Instance.FlightPlanner.Commands.Rows[i];
                    CpxHost.Instance.FlightPlanner.Commands.Rows.Remove(myrow);
                    CpxHost.Instance.FlightPlanner.Commands.Rows.Insert(i + 1, myrow);
                    CpxHost.Instance.FlightPlanner.FlightPlannerBase.writeKML();
                }
            }
            RearrangeNumbers();
        }

        private void BtnUp_Click(object sender, EventArgs e)
        {
            Common.CpxButton cpxButton = (Common.CpxButton)sender;
            for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            {
                if (flowLayoutPanel1.Controls[i] == cpxButton.Parent && flowLayoutPanel1.Controls.Count > 1 && i > 0)
                { 
                    CpxCommand first = (CpxCommand)flowLayoutPanel1.Controls[i];
                    CpxCommand_Click(first, new EventArgs());
                    //Thread.Sleep(1000);
                    CpxCommand second = (CpxCommand)flowLayoutPanel1.Controls[i - 1];
                    flowLayoutPanel1.Controls.SetChildIndex(second, i);
                    flowLayoutPanel1.Controls.SetChildIndex(first, i - 1);
                    first.lblWpNo.Text = "#" + (i);
                    second.lblWpNo.Text = "#" + (i + 1);
                    createNew = false;
                    DataGridViewRow myrow = CpxHost.Instance.FlightPlanner.Commands.Rows[i];
                    CpxHost.Instance.FlightPlanner.Commands.Rows.Remove(myrow);
                    CpxHost.Instance.FlightPlanner.Commands.Rows.Insert(i - 1, myrow);
                    CpxHost.Instance.FlightPlanner.FlightPlannerBase.writeKML();
                }
            }
            RearrangeNumbers();
        }

        private void RearrangeNumbers()
        {
            for (int i= 0; i < flowLayoutPanel1.Controls.Count; i++)
            {
                CpxCommand panel = (CpxCommand)flowLayoutPanel1.Controls[i];
                panel.lblWpNo.Text = "#" + (i+1);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            Common.CpxButton cpxButton = (Common.CpxButton)sender;
            foreach (CpxCommand cpxCommand in flowLayoutPanel1.Controls)
            {

                if (cpxCommand == cpxButton.Parent)
                {
                    CpxHost.Instance.FlightPlanner.Commands.Rows.Remove(cpxCommand._command);
                    flowLayoutPanel1.Controls.Remove(cpxCommand);
                    break;
                }
            }
            RearrangeNumbers();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CpxHost.Instance.FlightPlanner.coords1.System = comboBox1.Text;
            foreach (CpxCommand cpxCommand in flowLayoutPanel1.Controls)
            {
                cpxCommand.ChangeCoordsType(comboBox1.Text);
            }
        }

        private void cpxButton_Click(object sender, EventArgs e)
        {
            CpxButton cpxButton = (CpxButton)sender;
            txtDefaultAlt.Focus();
            SendKeys.Send(cpxButton.Text);
        }

        private void cpxButtonLeft_Click(object sender, EventArgs e)
        {
            txtDefaultAlt.Focus();
            SendKeys.Send("{LEFT}");
        }

        private void cpxButtonDelete_Click(object sender, EventArgs e)
        {
            txtDefaultAlt.Focus();
            SendKeys.Send("{BS}");
        }

        private void cpxButtonRight_Click(object sender, EventArgs e)
        {
            txtDefaultAlt.Focus();
            SendKeys.Send("{RIGHT}");
        }

        private void cpxButton12_Click(object sender, EventArgs e)
        {
            panel1.Height = comboBox1.Top + comboBox1.Height + 10;
        }

        private void txtDefaultAlt_Click(object sender, EventArgs e)
        {
            panel1.Height = cpxButton8.Top + cpxButton8.Height + 10;
        }
    }
}
