﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CpxLib.Controls.Common;
using CpxLib.Core;
using GMap.NET.WindowsForms;
using MissionPlanner.Controls;

namespace CpxLib.Controls
{
    public partial class CpxCommand : ShapedUserControl
    {
        //public int CommandsIndex { get; set; }
        public DataGridViewRow _command { get; set; }
        private Point LocLblLat;
        private Point LocLblLng;
        private Point LocTxtLat;
        private Point LocTxtLng;

        public Point LocLblP1;
        public Point LocLblP2;
        public Point LocTxtP1;
        public Point LocTxtP2;
        private int CommandHeight;
        int _lat;
        int _lng;
        int _alt;
        int _param1;
        int _param2;
        public Common.CpxTextbox currentTextbox;
        int currentTxt;
        string _coords;
        public CpxCommand(string coords1)
        {
            InitializeComponent();
            ChangeCoordsType(coords1);
            LocLblLat = lblLat.Location;
            LocLblLng = lblLng.Location;
            LocTxtLat = txtLatitude.Location;
            LocTxtLng = txtLongitude.Location;
            LocLblP1 = lblP1.Location;
            LocLblP2 = lblP2.Location;
            LocTxtP1 = txtP1.Location;
            LocTxtP2 = txtP2.Location;


            //cmbCommand.DataSource = CpxHost.Instance.FlightPlanner.FlightPlannerBase.Command.DataSource;
            cmbCommand.Items.Add("WAYPOINT");
            cmbCommand.Items.Add("RETURN_TO_LAUNCH");
            cmbCommand.Items.Add("LAND");
            cmbCommand.Items.Add("DO_JUMP");
            cmbCommand.Items.Add("DO_CHANGE_SPEED");
            cmbCommand.Items.Add("DO_GRIPPER");
            cmbCommand.SelectedIndexChanged += CmbCommand_SelectedIndexChanged;
            cmbCommand.SelectedIndex = 0;
            currentTextbox = new CpxTextbox();
            btnDown.ButtonShape = CpxButton.ButtonShapeEnum.Ellipse;
            btnUp.ButtonShape = CpxButton.ButtonShapeEnum.Ellipse;
            btnDelete.ButtonShape = CpxButton.ButtonShapeEnum.Ellipse;
        }

        public void ChangeCoordsType(string coords1)
        {
            _coords = coords1;
            if (_coords == "GEO")
            {
                lblLat.Visible = true;
                txtLatitude.Visible = true;
                lblLng.Visible = true;
                txtLongitude.Visible = true;

                lblEasting.Visible = false;
                txtEasting.Visible = false;
                lblNorthing.Visible = false;
                txtNorthing.Visible = false;
                lblZone.Visible = false;
                txtZone.Visible = false;
                //lblP1.Location = LocLblP1;
                //txtP1.Location = LocTxtP1;
            }
            else
            {
                lblLat.Visible = false;
                txtLatitude.Visible = false;
                lblLng.Visible = false;
                txtLongitude.Visible = false;

                lblEasting.Visible = true;
                txtEasting.Visible = true;
                lblNorthing.Visible = true;
                txtNorthing.Visible = true;
                lblZone.Visible = true;
                txtZone.Visible = true;
                //MessageBox.Show(LocTxtP1.X + ", " + LocTxtP1.Y);
                //lblZone.Location = LocLblP1;
                //txtZone.Location = LocTxtP1;
                //lblP1.Location = LocLblP2;
                //txtP1.Location = LocTxtP2;
            }
        }

        public void CpxCommand_LostFocus()
        {
            this.Height = cmbCommand.Top + cmbCommand.Height + 20;
        }

        public void CpxCommand_Click()
        {
            if (this.Height < CommandHeight)
                this.Height = CommandHeight;
        }

        private void CmbCommand_SelectedIndexChanged(object sender, EventArgs e)
        {
            //var x=CpxHost.Instance.FlightPlanner.FlightPlannerBase.InvokePrivateMethod("Commands_SelectionChangeCommitted",sender, e);
            switch ((string)cmbCommand.SelectedItem)
            {
                case "WAYPOINT":
                    ChangeCoordsType(_coords);

                    lblAlt.Visible = true;
                    txtAltitude.Visible = true;

                    lblP1.Visible = true;
                    lblP1.Text = CpxLib.Properties.Resources.MavCMDDelay;
                    txtP1.Visible = true;

                    lblP2.Visible = false;
                    txtP2.Visible = false;

                    this.Height = txtP1.Top + txtP1.Height + 20;

                    break;
                case "RETURN_TO_LAUNCH":
                    ChangeCoordsType(_coords);

                    lblAlt.Visible = false;
                    txtAltitude.Visible = false;

                    lblP1.Visible = false;
                    txtP1.Visible = false;

                    lblP2.Visible = false;
                    txtP2.Visible = false;

                    this.Height = cmbCommand.Top + cmbCommand.Height + 20;
                    break;
                case "LAND":
                    ChangeCoordsType(_coords);

                    lblAlt.Visible = true;
                    txtAltitude.Visible = true;

                    lblP1.Visible = false;
                    txtP1.Visible = false;

                    lblP2.Visible = false;
                    txtP2.Visible = false;

                    this.Height = txtLatitude.Top + txtZone.Height + 20;
                    break;
                case "DO_JUMP":
                    HideAllCoords();

                    lblP1.Visible = true;
                    lblP1.Text = CpxLib.Properties.Resources.MavCMDWpNo;
                    txtP1.Visible = true;
                    lblP1.Location = LocLblLat;
                    txtP1.Location = LocTxtLat;

                    lblP2.Visible = true;
                    lblP2.Text = CpxLib.Properties.Resources.MavCMDRepeats;
                    txtP2.Visible = true;
                    lblP2.Location = LocLblLng;
                    txtP2.Location = LocTxtLng;
                    this.Height = txtLatitude.Top + txtLatitude.Height + 20;
                    break;
                case "DO_CHANGE_SPEED":

                    HideAllCoords();
                    lblP1.Visible = true;
                    lblP1.Text = CpxLib.Properties.Resources.MavCMDSpeedType;
                    txtP1.Visible = true;
                    lblP1.Location = LocLblLat;
                    txtP1.Location = LocTxtLat;

                    lblP2.Visible = true;
                    lblP2.Text = CpxLib.Properties.Resources.MavCMDSpeed;
                    txtP2.Visible = true;
                    lblP2.Location = LocLblLng;
                    txtP2.Location = LocTxtLng;
                    this.Height = txtLatitude.Top + txtLatitude.Height + 20;
                    break;
                case "DO_GRIPPER":
                    lblLat.Visible = false;
                    txtLatitude.Visible = false;

                    lblLng.Visible = false;
                    txtLongitude.Visible = false;

                    lblEasting.Visible = false;
                    txtEasting.Visible = false;

                    lblNorthing.Visible = false;
                    txtNorthing.Visible = false;

                    lblZone.Visible = false;
                    txtZone.Visible = false;

                    lblAlt.Visible = false;
                    txtAltitude.Visible = false;

                    lblP1.Visible = true;
                    lblP1.Text = CpxLib.Properties.Resources.MavCMDGripperNo;
                    txtP1.Visible = true;
                    lblP1.Location = LocLblLat;
                    txtP1.Location = LocTxtLat;

                    lblP2.Visible = true;
                    lblP2.Text = CpxLib.Properties.Resources.MavCMDDrop;
                    txtP2.Visible = true;
                    lblP2.Location = LocLblLng;
                    txtP2.Location = LocTxtLng;
                    this.Height = txtLatitude.Top + txtLatitude.Height + 20;
                    break;
            }
            CommandHeight = this.Height;
        }
        private void HideAllCoords()
        {
            lblLat.Visible = false;
            txtLatitude.Visible = false;

            lblLng.Visible = false;
            txtLongitude.Visible = false;

            lblEasting.Visible = false;
            txtEasting.Visible = false;

            lblNorthing.Visible = false;
            txtNorthing.Visible = false;

            lblZone.Visible = false;
            txtZone.Visible = false;

            lblAlt.Visible = false;
            txtAltitude.Visible = false;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            var offset = DoubleToInt(this.BorderWidth / 2);
            System.Drawing.Pen borderPen2 = new System.Drawing.Pen(this.BackColor, this.BorderWidth + 1);

            //e.Graphics.DrawLine(borderPen2, offset, offset, this.ClientRectangle.Width - offset, offset);
            borderPen2.Dispose();



        }

        private void cpxButton_Click(object sender, EventArgs e)
        {
            CpxButton cpxButton = (CpxButton)sender;
            currentTextbox.Focus();
            SendKeys.Send(cpxButton.Text);

        }

        private void cpxButtonLeft_Click(object sender, EventArgs e)
        {
            currentTextbox.Focus();
            SendKeys.Send("{LEFT}");
        }

        private void cpxButtonDelete_Click(object sender, EventArgs e)
        {
            currentTextbox.Focus();
            SendKeys.Send("{BS}");
        }

        private void cpxButtonRight_Click(object sender, EventArgs e)
        {
            currentTextbox.Focus();
            SendKeys.Send("{RIGHT}");
        }

        public void textBox_Click(object sender, EventArgs e)
        {
            CpxTextbox cpxTextbox = (CpxTextbox)sender;
            currentTextbox = cpxTextbox;
            this.Height = cpxButtonDelete.Height + cpxButtonDelete.Top + 20;
            cpxButton12.Visible = true;
        }

        private void cpxButton12_Click(object sender, EventArgs e)
        {
            cpxButton12.Visible = false;
            this.Height = CommandHeight;
        }

    }
}
