﻿namespace CpxLib.Controls
{
    partial class CpxMissionPlanning
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CpxMissionPlanning));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cpxButtonRight = new CpxLib.Controls.Common.CpxButton();
            this.cpxButtonDelete = new CpxLib.Controls.Common.CpxButton();
            this.cpxButtonLeft = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton11 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton10 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton9 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton8 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton7 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton6 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton5 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton4 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton3 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton2 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton13 = new CpxLib.Controls.Common.CpxButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cpxButton12 = new CpxLib.Controls.Common.CpxButton();
            this.btnAdd = new CpxLib.Controls.Common.CpxButton();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.cpxLabel2 = new CpxLib.Controls.Common.CpxLabel();
            this.txtDefaultAlt = new CpxLib.Controls.Common.CpxTextbox();
            this.cpxLabel1 = new CpxLib.Controls.Common.CpxLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Name = "panel1";
            // 
            // cpxButtonRight
            // 
            resources.ApplyResources(this.cpxButtonRight, "cpxButtonRight");
            this.cpxButtonRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButtonRight.ButtonAction = null;
            this.cpxButtonRight.ForeColor = System.Drawing.Color.White;
            this.cpxButtonRight.Image = global::CpxLib.Properties.Resources.right;
            this.cpxButtonRight.Name = "cpxButtonRight";
            this.cpxButtonRight.TabStop = false;
            this.cpxButtonRight.UseVisualStyleBackColor = false;
            this.cpxButtonRight.Click += new System.EventHandler(this.cpxButtonRight_Click);
            // 
            // cpxButtonDelete
            // 
            resources.ApplyResources(this.cpxButtonDelete, "cpxButtonDelete");
            this.cpxButtonDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButtonDelete.ButtonAction = null;
            this.cpxButtonDelete.ForeColor = System.Drawing.Color.White;
            this.cpxButtonDelete.Image = global::CpxLib.Properties.Resources.delete_white;
            this.cpxButtonDelete.Name = "cpxButtonDelete";
            this.cpxButtonDelete.TabStop = false;
            this.cpxButtonDelete.UseVisualStyleBackColor = false;
            this.cpxButtonDelete.Click += new System.EventHandler(this.cpxButtonDelete_Click);
            // 
            // cpxButtonLeft
            // 
            resources.ApplyResources(this.cpxButtonLeft, "cpxButtonLeft");
            this.cpxButtonLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButtonLeft.ButtonAction = null;
            this.cpxButtonLeft.ForeColor = System.Drawing.Color.White;
            this.cpxButtonLeft.Name = "cpxButtonLeft";
            this.cpxButtonLeft.TabStop = false;
            this.cpxButtonLeft.UseVisualStyleBackColor = false;
            this.cpxButtonLeft.Click += new System.EventHandler(this.cpxButtonLeft_Click);
            // 
            // cpxButton11
            // 
            resources.ApplyResources(this.cpxButton11, "cpxButton11");
            this.cpxButton11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton11.ButtonAction = null;
            this.cpxButton11.ForeColor = System.Drawing.Color.White;
            this.cpxButton11.Name = "cpxButton11";
            this.cpxButton11.TabStop = false;
            this.cpxButton11.UseVisualStyleBackColor = false;
            this.cpxButton11.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton10
            // 
            resources.ApplyResources(this.cpxButton10, "cpxButton10");
            this.cpxButton10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton10.ButtonAction = null;
            this.cpxButton10.ForeColor = System.Drawing.Color.White;
            this.cpxButton10.Name = "cpxButton10";
            this.cpxButton10.TabStop = false;
            this.cpxButton10.UseVisualStyleBackColor = false;
            this.cpxButton10.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton9
            // 
            resources.ApplyResources(this.cpxButton9, "cpxButton9");
            this.cpxButton9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton9.ButtonAction = null;
            this.cpxButton9.ForeColor = System.Drawing.Color.White;
            this.cpxButton9.Name = "cpxButton9";
            this.cpxButton9.TabStop = false;
            this.cpxButton9.UseVisualStyleBackColor = false;
            this.cpxButton9.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton8
            // 
            resources.ApplyResources(this.cpxButton8, "cpxButton8");
            this.cpxButton8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton8.ButtonAction = null;
            this.cpxButton8.ForeColor = System.Drawing.Color.White;
            this.cpxButton8.Name = "cpxButton8";
            this.cpxButton8.TabStop = false;
            this.cpxButton8.UseVisualStyleBackColor = false;
            this.cpxButton8.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton7
            // 
            resources.ApplyResources(this.cpxButton7, "cpxButton7");
            this.cpxButton7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton7.ButtonAction = null;
            this.cpxButton7.ForeColor = System.Drawing.Color.White;
            this.cpxButton7.Name = "cpxButton7";
            this.cpxButton7.TabStop = false;
            this.cpxButton7.UseVisualStyleBackColor = false;
            this.cpxButton7.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton6
            // 
            resources.ApplyResources(this.cpxButton6, "cpxButton6");
            this.cpxButton6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton6.ButtonAction = null;
            this.cpxButton6.ForeColor = System.Drawing.Color.White;
            this.cpxButton6.Name = "cpxButton6";
            this.cpxButton6.TabStop = false;
            this.cpxButton6.UseVisualStyleBackColor = false;
            this.cpxButton6.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton5
            // 
            resources.ApplyResources(this.cpxButton5, "cpxButton5");
            this.cpxButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton5.ButtonAction = null;
            this.cpxButton5.ForeColor = System.Drawing.Color.White;
            this.cpxButton5.Name = "cpxButton5";
            this.cpxButton5.TabStop = false;
            this.cpxButton5.UseVisualStyleBackColor = false;
            this.cpxButton5.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton4
            // 
            resources.ApplyResources(this.cpxButton4, "cpxButton4");
            this.cpxButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton4.ButtonAction = null;
            this.cpxButton4.ForeColor = System.Drawing.Color.White;
            this.cpxButton4.Name = "cpxButton4";
            this.cpxButton4.TabStop = false;
            this.cpxButton4.UseVisualStyleBackColor = false;
            this.cpxButton4.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton3
            // 
            resources.ApplyResources(this.cpxButton3, "cpxButton3");
            this.cpxButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton3.ButtonAction = null;
            this.cpxButton3.ForeColor = System.Drawing.Color.White;
            this.cpxButton3.Name = "cpxButton3";
            this.cpxButton3.TabStop = false;
            this.cpxButton3.UseVisualStyleBackColor = false;
            this.cpxButton3.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton2
            // 
            resources.ApplyResources(this.cpxButton2, "cpxButton2");
            this.cpxButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton2.ButtonAction = null;
            this.cpxButton2.ForeColor = System.Drawing.Color.White;
            this.cpxButton2.Name = "cpxButton2";
            this.cpxButton2.TabStop = false;
            this.cpxButton2.UseVisualStyleBackColor = false;
            this.cpxButton2.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton13
            // 
            resources.ApplyResources(this.cpxButton13, "cpxButton13");
            this.cpxButton13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton13.ButtonAction = null;
            this.cpxButton13.ForeColor = System.Drawing.Color.White;
            this.cpxButton13.Name = "cpxButton13";
            this.cpxButton13.TabStop = false;
            this.cpxButton13.UseVisualStyleBackColor = false;
            this.cpxButton13.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.cpxButtonRight);
            this.groupBox1.Controls.Add(this.cpxButton12);
            this.groupBox1.Controls.Add(this.cpxButtonDelete);
            this.groupBox1.Controls.Add(this.btnAdd);
            this.groupBox1.Controls.Add(this.cpxButtonLeft);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.cpxButton11);
            this.groupBox1.Controls.Add(this.cpxLabel2);
            this.groupBox1.Controls.Add(this.cpxButton10);
            this.groupBox1.Controls.Add(this.txtDefaultAlt);
            this.groupBox1.Controls.Add(this.cpxButton9);
            this.groupBox1.Controls.Add(this.cpxLabel1);
            this.groupBox1.Controls.Add(this.cpxButton8);
            this.groupBox1.Controls.Add(this.cpxButton7);
            this.groupBox1.Controls.Add(this.cpxButton13);
            this.groupBox1.Controls.Add(this.cpxButton6);
            this.groupBox1.Controls.Add(this.cpxButton2);
            this.groupBox1.Controls.Add(this.cpxButton5);
            this.groupBox1.Controls.Add(this.cpxButton3);
            this.groupBox1.Controls.Add(this.cpxButton4);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // cpxButton12
            // 
            resources.ApplyResources(this.cpxButton12, "cpxButton12");
            this.cpxButton12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.cpxButton12.ButtonAction = null;
            this.cpxButton12.FlatAppearance.BorderSize = 0;
            this.cpxButton12.ForeColor = System.Drawing.Color.White;
            this.cpxButton12.Image = global::CpxLib.Properties.Resources.rect835;
            this.cpxButton12.Name = "cpxButton12";
            this.cpxButton12.UseVisualStyleBackColor = false;
            this.cpxButton12.Click += new System.EventHandler(this.cpxButton12_Click);
            // 
            // btnAdd
            // 
            resources.ApplyResources(this.btnAdd, "btnAdd");
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnAdd.ButtonAction = null;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Image = global::CpxLib.Properties.Resources.add_command;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // comboBox1
            // 
            resources.ApplyResources(this.comboBox1, "comboBox1");
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.comboBox1.DisplayMember = "1";
            this.comboBox1.ForeColor = System.Drawing.Color.White;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            resources.GetString("comboBox1.Items"),
            resources.GetString("comboBox1.Items1")});
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // cpxLabel2
            // 
            resources.ApplyResources(this.cpxLabel2, "cpxLabel2");
            this.cpxLabel2.Name = "cpxLabel2";
            // 
            // txtDefaultAlt
            // 
            resources.ApplyResources(this.txtDefaultAlt, "txtDefaultAlt");
            this.txtDefaultAlt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtDefaultAlt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDefaultAlt.ForeColor = System.Drawing.Color.White;
            this.txtDefaultAlt.Name = "txtDefaultAlt";
            this.txtDefaultAlt.Click += new System.EventHandler(this.txtDefaultAlt_Click);
            // 
            // cpxLabel1
            // 
            resources.ApplyResources(this.cpxLabel1, "cpxLabel1");
            this.cpxLabel1.Name = "cpxLabel1";
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Controls.Add(this.flowLayoutPanel1);
            this.panel2.Name = "panel2";
            // 
            // flowLayoutPanel1
            // 
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // CpxMissionPlanning
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "CpxMissionPlanning";
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private Common.CpxLabel cpxLabel1;
        private System.Windows.Forms.Panel panel2;
        private Common.CpxTextbox txtDefaultAlt;
        private Common.CpxLabel cpxLabel2;
        public System.Windows.Forms.ComboBox comboBox1;
        private Common.CpxButton btnAdd;
        public System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public Common.CpxButton cpxButtonRight;
        public Common.CpxButton cpxButtonDelete;
        public Common.CpxButton cpxButtonLeft;
        public Common.CpxButton cpxButton11;
        public Common.CpxButton cpxButton10;
        public Common.CpxButton cpxButton9;
        public Common.CpxButton cpxButton8;
        public Common.CpxButton cpxButton7;
        public Common.CpxButton cpxButton6;
        public Common.CpxButton cpxButton5;
        public Common.CpxButton cpxButton4;
        public Common.CpxButton cpxButton3;
        public Common.CpxButton cpxButton2;
        public Common.CpxButton cpxButton13;
        private Common.CpxButton cpxButton12;
    }
}
