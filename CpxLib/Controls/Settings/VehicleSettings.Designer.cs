﻿using CpxLib.Controls.Common;
using System.Windows.Forms;

namespace CpxLib.Controls.Settings
{
    partial class VehicleSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VehicleSettings));
            this.lstVehicles = new System.Windows.Forms.ListView();
            this.colID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMavPort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnDeleteMAV = new CpxLib.Controls.Common.CpxButton();
            this.btnEditMAV = new CpxLib.Controls.Common.CpxButton();
            this.btnNewMAV = new CpxLib.Controls.Common.CpxButton();
            this.SuspendLayout();
            // 
            // lstVehicles
            // 
            resources.ApplyResources(this.lstVehicles, "lstVehicles");
            this.lstVehicles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colID,
            this.colName,
            this.colMavPort});
            this.lstVehicles.FullRowSelect = true;
            this.lstVehicles.GridLines = true;
            this.lstVehicles.HideSelection = false;
            this.lstVehicles.Name = "lstVehicles";
            this.lstVehicles.UseCompatibleStateImageBehavior = false;
            this.lstVehicles.View = System.Windows.Forms.View.Details;
            this.lstVehicles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstVehicles_MouseDoubleClick);
            // 
            // colID
            // 
            resources.ApplyResources(this.colID, "colID");
            // 
            // colName
            // 
            resources.ApplyResources(this.colName, "colName");
            // 
            // colMavPort
            // 
            resources.ApplyResources(this.colMavPort, "colMavPort");
            // 
            // btnDeleteMAV
            // 
            resources.ApplyResources(this.btnDeleteMAV, "btnDeleteMAV");
            this.btnDeleteMAV.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnDeleteMAV.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDeleteMAV.Name = "btnDeleteMAV";
            this.btnDeleteMAV.UseVisualStyleBackColor = false;
            this.btnDeleteMAV.Click += new System.EventHandler(this.btnDeleteMAV_Click);
            // 
            // btnEditMAV
            // 
            resources.ApplyResources(this.btnEditMAV, "btnEditMAV");
            this.btnEditMAV.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnEditMAV.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnEditMAV.Name = "btnEditMAV";
            this.btnEditMAV.UseVisualStyleBackColor = false;
            this.btnEditMAV.Click += new System.EventHandler(this.btnEditMAV_Click);
            // 
            // btnNewMAV
            // 
            resources.ApplyResources(this.btnNewMAV, "btnNewMAV");
            this.btnNewMAV.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnNewMAV.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnNewMAV.Name = "btnNewMAV";
            this.btnNewMAV.UseVisualStyleBackColor = false;
            this.btnNewMAV.Click += new System.EventHandler(this.btnNewMAV_Click);
            // 
            // VehicleSettings
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnDeleteMAV);
            this.Controls.Add(this.btnEditMAV);
            this.Controls.Add(this.btnNewMAV);
            this.Controls.Add(this.lstVehicles);
            this.Name = "VehicleSettings";
            this.Tag = "custom";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstVehicles;
        private System.Windows.Forms.ColumnHeader colID;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colMavPort;
        private CpxButton btnDeleteMAV;
        private CpxButton btnEditMAV;
        private CpxButton btnNewMAV;
    }
}
