﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CpxLib.Model;
using System.Globalization;
using System.Threading;
using CpxLib.Properties;

namespace CpxLib.Controls.Settings
{
    public partial class PlanSettings : UserControl
    {
        public string language = Properties.Settings.Default.Language;
        private FlightRoute _waypointFile;

        public PlanSettings()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            InitializeComponent();
            FillList();

            tvwPlans.NodeMouseClick += TvwPlans_NodeMouseClick;  
        }

        private void TvwPlans_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            _waypointFile = e.Node.Tag as FlightRoute;
            FillFields();
        }

        private void FillList()
        {


            foreach (var fileInfo in ModelExtensions.GetFlightRoutes())
            {
           
                var treeNode = new TreeNode(fileInfo.Name);
                treeNode.Tag = fileInfo;
                tvwPlans.Nodes.Add(treeNode);

            }

            if (tvwPlans.Nodes.Count > 0)
            {
                tvwPlans.SelectedNode = tvwPlans.Nodes[0];

                _waypointFile = tvwPlans.SelectedNode.Tag as FlightRoute;
                FillFields();
            }
            else
            {

            }
        }

        private void FillFields()
        {
            txtPathName.Text = _waypointFile.Name;
            lstWaypoints.Items.Clear();
            foreach (MavCommand item in _waypointFile.Commands)
            {
                AddItemToList(item);
            }

        }

        private void AddItemToList(MavCommand command)
        {
            var listItem = new ListViewItem();
            listItem.Text = command.Lat.ToString();
            listItem.SubItems.Add(command.Lng.ToString());
            listItem.SubItems.Add(command.Alt.ToString());
            listItem.Tag = command;
            lstWaypoints.Items.Add(listItem);

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                _waypointFile.Name = txtPathName.Text;
                _waypointFile.Save();
                tvwPlans.SelectedNode.Text = _waypointFile.Name;
                CustomMessageBox.Show(Resources.SuccessfulOperation);
            }
            catch(Exception ex)
            {
                CustomMessageBox.Show(Resources.NotSuccessfulOperation);
            }
            
        }

        private void btnDeleteTrigger_Click(object sender, EventArgs e)
        {
            try
            {
                _waypointFile.Name = txtPathName.Text;
                _waypointFile.Delete();
                CustomMessageBox.Show(_waypointFile.Id);
                tvwPlans.Nodes.Clear();
                FillList();
                tvwPlans.SelectedNode.Text = _waypointFile.Name;

            }
            catch (Exception ex)
            {
            }
        }

        private void btnEditTrigger_Click(object sender, EventArgs e)
        {

        }
    }
}
