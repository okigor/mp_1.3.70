﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CpxLib.Model;
using CpxLib.Forms;
using System.Threading;
using System.Globalization;

namespace CpxLib.Controls.Settings
{
    public partial class VehicleSettings : UserControl
    {
        public string language = Properties.Settings.Default.Language;
        public VehicleSettings()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            InitializeComponent();
            FillList();
        }

        private void FillList()
        {
            lstVehicles.Items.Clear();
            foreach (var item in ModelExtensions.GetVehicleFiles())
            {
                AddItemToList(item);
            }
        }


        private void btnNewMAV_Click(object sender, EventArgs e)
        {
            var frm = new frmEditVehicle();

            if (frm.ShowDialog() == DialogResult.OK)
            {
                var vehicle = new Vehicle();
                vehicle.MavId = frm.Vehicle.MavId;
                vehicle.Name = frm.Vehicle.Name;
                vehicle.MavPort = frm.Vehicle.MavPort;
                vehicle.NightCameraPort = frm.Vehicle.NightCameraPort;
                vehicle.DayCameraPort = frm.Vehicle.DayCameraPort;
                vehicle.MavIP = frm.Vehicle.MavIP;
                vehicle.NightCameraIP = frm.Vehicle.NightCameraIP;
                vehicle.DayCameraIP = frm.Vehicle.DayCameraIP;
                //AddItemToList(vehicle);
                FillList();
            }
        }

        private void AddItemToList(Vehicle vehicle)
        {
            var listItem = new ListViewItem();
            listItem.Text = vehicle.MavId.ToString();
            listItem.SubItems.Add(vehicle.Name);
            listItem.SubItems.Add(vehicle.MavPort);
            listItem.Tag = vehicle;
            lstVehicles.Items.Add(listItem);
        }

        private void btnEditMAV_Click(object sender, EventArgs e)
        {
            if (lstVehicles.SelectedItems.Count > 0)
            {
                var item = lstVehicles.SelectedItems[0];

                EditItem(item);
            }
        }

        private void btnDeleteMAV_Click(object sender, EventArgs e)
        {
            if (lstVehicles.SelectedItems.Count > 0)
            {
                var item = lstVehicles.SelectedItems[0];

                Vehicle vehicle = (Vehicle)item.Tag;
                vehicle.Delete();
                FillList();
            }

        }
        private void lstVehicles_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var senderList = (ListView)sender;
            var clickedItem = senderList.HitTest(e.Location).Item;
            if (clickedItem != null)
            {
                EditItem(clickedItem);
            }
        }


        private void EditItem(ListViewItem item)
        {
            var vehicle = item.Tag as Vehicle;

            var frm = new frmEditVehicle(vehicle);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                vehicle.MavId = frm.Vehicle.MavId;
                vehicle.Name = frm.Vehicle.Name;
                vehicle.MavPort = frm.Vehicle.MavPort;
                vehicle.NightCameraPort = frm.Vehicle.NightCameraPort;
                vehicle.DayCameraPort = frm.Vehicle.DayCameraPort;
                vehicle.MavIP = frm.Vehicle.MavIP;
                vehicle.NightCameraIP = frm.Vehicle.NightCameraIP;
                vehicle.DayCameraIP = frm.Vehicle.DayCameraIP;
                vehicle.CommunicationType = frm.Vehicle.CommunicationType;
                vehicle.WirelessName = frm.Vehicle.WirelessName;
                vehicle.Protocol = frm.Vehicle.Protocol;

                item.Text = vehicle.MavId.ToString();
                item.SubItems[1].Text = vehicle.Name;
                item.SubItems[2].Text = vehicle.MavPort;
            }
        }
    }
}
