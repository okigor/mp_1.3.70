﻿namespace CpxLib.Controls.Settings
{
    partial class GeneralSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneralSettings));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtGStreamerDir = new CpxLib.Controls.Common.CpxTextbox();
            this.btnBrowseGSteamerFolder = new CpxLib.Controls.Common.CpxButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.radCopterPix = new System.Windows.Forms.RadioButton();
            this.radDefault = new System.Windows.Forms.RadioButton();
            this.lngCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtLTEIP = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtLandingStationPort = new CpxLib.Controls.Common.CpxTextbox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtLandingStationIP = new System.Windows.Forms.MaskedTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnSave = new CpxLib.Controls.Common.CpxButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnVideo = new CpxLib.Controls.Common.CpxButton();
            this.btnOpenVPN = new CpxLib.Controls.Common.CpxButton();
            this.txtOpenVPN = new CpxLib.Controls.Common.CpxTextbox();
            this.txtVideo = new CpxLib.Controls.Common.CpxTextbox();
            this.button1 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel10.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel9.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.panel11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // panel11
            // 
            resources.ApplyResources(this.panel11, "panel11");
            this.panel11.Controls.Add(this.txtGStreamerDir);
            this.panel11.Controls.Add(this.btnBrowseGSteamerFolder);
            this.panel11.Controls.Add(this.panel1);
            this.panel11.Name = "panel11";
            // 
            // txtGStreamerDir
            // 
            resources.ApplyResources(this.txtGStreamerDir, "txtGStreamerDir");
            this.txtGStreamerDir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtGStreamerDir.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGStreamerDir.ForeColor = System.Drawing.Color.White;
            this.txtGStreamerDir.Name = "txtGStreamerDir";
            // 
            // btnBrowseGSteamerFolder
            // 
            resources.ApplyResources(this.btnBrowseGSteamerFolder, "btnBrowseGSteamerFolder");
            this.btnBrowseGSteamerFolder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnBrowseGSteamerFolder.ButtonAction = null;
            this.btnBrowseGSteamerFolder.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnBrowseGSteamerFolder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnBrowseGSteamerFolder.Name = "btnBrowseGSteamerFolder";
            this.btnBrowseGSteamerFolder.UseVisualStyleBackColor = false;
            this.btnBrowseGSteamerFolder.Click += new System.EventHandler(this.btnBrowseGSteamerFolder_Click);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Name = "panel1";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // groupBox3
            // 
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Controls.Add(this.panel10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // panel10
            // 
            resources.ApplyResources(this.panel10, "panel10");
            this.panel10.Controls.Add(this.radCopterPix);
            this.panel10.Controls.Add(this.radDefault);
            this.panel10.Name = "panel10";
            // 
            // radCopterPix
            // 
            resources.ApplyResources(this.radCopterPix, "radCopterPix");
            this.radCopterPix.Name = "radCopterPix";
            this.radCopterPix.TabStop = true;
            this.radCopterPix.UseVisualStyleBackColor = true;
            this.radCopterPix.CheckedChanged += new System.EventHandler(this.radCopterPix_CheckedChanged);
            // 
            // radDefault
            // 
            resources.ApplyResources(this.radDefault, "radDefault");
            this.radDefault.Name = "radDefault";
            this.radDefault.TabStop = true;
            this.radDefault.UseVisualStyleBackColor = true;
            this.radDefault.CheckedChanged += new System.EventHandler(this.radDefault_CheckedChanged);
            // 
            // lngCombo
            // 
            resources.ApplyResources(this.lngCombo, "lngCombo");
            this.lngCombo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.lngCombo.ForeColor = System.Drawing.Color.White;
            this.lngCombo.FormattingEnabled = true;
            this.lngCombo.Items.AddRange(new object[] {
            resources.GetString("lngCombo.Items"),
            resources.GetString("lngCombo.Items1")});
            this.lngCombo.Name = "lngCombo";
            this.lngCombo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Controls.Add(this.panel9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // panel9
            // 
            resources.ApplyResources(this.panel9, "panel9");
            this.panel9.Controls.Add(this.txtLTEIP);
            this.panel9.Controls.Add(this.label7);
            this.panel9.Name = "panel9";
            // 
            // txtLTEIP
            // 
            resources.ApplyResources(this.txtLTEIP, "txtLTEIP");
            this.txtLTEIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtLTEIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLTEIP.ForeColor = System.Drawing.Color.White;
            this.txtLTEIP.Name = "txtLTEIP";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // groupBox4
            // 
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Controls.Add(this.panel4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // panel4
            // 
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Controls.Add(this.lngCombo);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Name = "panel4";
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Name = "panel2";
            // 
            // groupBox5
            // 
            resources.ApplyResources(this.groupBox5, "groupBox5");
            this.groupBox5.Controls.Add(this.panel8);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.TabStop = false;
            // 
            // panel8
            // 
            resources.ApplyResources(this.panel8, "panel8");
            this.panel8.Controls.Add(this.txtLandingStationPort);
            this.panel8.Controls.Add(this.label9);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.txtLandingStationIP);
            this.panel8.Name = "panel8";
            // 
            // txtLandingStationPort
            // 
            resources.ApplyResources(this.txtLandingStationPort, "txtLandingStationPort");
            this.txtLandingStationPort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtLandingStationPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLandingStationPort.ForeColor = System.Drawing.Color.White;
            this.txtLandingStationPort.Name = "txtLandingStationPort";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // txtLandingStationIP
            // 
            resources.ApplyResources(this.txtLandingStationIP, "txtLandingStationIP");
            this.txtLandingStationIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtLandingStationIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLandingStationIP.ForeColor = System.Drawing.Color.White;
            this.txtLandingStationIP.Name = "txtLandingStationIP";
            // 
            // panel3
            // 
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Controls.Add(this.groupBox4);
            this.panel3.Name = "panel3";
            // 
            // panel5
            // 
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Controls.Add(this.btnSave);
            this.panel5.Name = "panel5";
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnSave.ButtonAction = null;
            this.btnSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel6
            // 
            resources.ApplyResources(this.panel6, "panel6");
            this.panel6.Controls.Add(this.groupBox6);
            this.panel6.Name = "panel6";
            // 
            // groupBox6
            // 
            resources.ApplyResources(this.groupBox6, "groupBox6");
            this.groupBox6.Controls.Add(this.panel7);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.TabStop = false;
            // 
            // panel7
            // 
            resources.ApplyResources(this.panel7, "panel7");
            this.panel7.Controls.Add(this.btnVideo);
            this.panel7.Controls.Add(this.btnOpenVPN);
            this.panel7.Controls.Add(this.txtOpenVPN);
            this.panel7.Controls.Add(this.txtVideo);
            this.panel7.Controls.Add(this.button1);
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Name = "panel7";
            // 
            // btnVideo
            // 
            resources.ApplyResources(this.btnVideo, "btnVideo");
            this.btnVideo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnVideo.ButtonAction = null;
            this.btnVideo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnVideo.Name = "btnVideo";
            this.btnVideo.UseVisualStyleBackColor = false;
            this.btnVideo.Click += new System.EventHandler(this.btnVideo_Click);
            // 
            // btnOpenVPN
            // 
            resources.ApplyResources(this.btnOpenVPN, "btnOpenVPN");
            this.btnOpenVPN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnOpenVPN.ButtonAction = null;
            this.btnOpenVPN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnOpenVPN.Name = "btnOpenVPN";
            this.btnOpenVPN.UseVisualStyleBackColor = false;
            this.btnOpenVPN.Click += new System.EventHandler(this.btnOpenVPN_Click);
            // 
            // txtOpenVPN
            // 
            resources.ApplyResources(this.txtOpenVPN, "txtOpenVPN");
            this.txtOpenVPN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtOpenVPN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOpenVPN.ForeColor = System.Drawing.Color.White;
            this.txtOpenVPN.Name = "txtOpenVPN";
            // 
            // txtVideo
            // 
            resources.ApplyResources(this.txtVideo, "txtVideo");
            this.txtVideo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtVideo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVideo.ForeColor = System.Drawing.Color.White;
            this.txtVideo.Name = "txtVideo";
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // GeneralSettings
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox1);
            this.Name = "GeneralSettings";
            this.groupBox1.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radCopterPix;
        private System.Windows.Forms.RadioButton radDefault;
        private Common.CpxButton btnSave;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
 
        public System.Windows.Forms.MaskedTextBox txtLTEIP;
        private System.Windows.Forms.ComboBox lngCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox txtLandingStationIP;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private Common.CpxButton btnBrowseGSteamerFolder;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private Common.CpxTextbox txtLandingStationPort;
        private Common.CpxTextbox txtOpenVPN;
        private Common.CpxTextbox txtVideo;
        private Common.CpxButton btnOpenVPN;
        private Common.CpxButton btnVideo;
        private System.Windows.Forms.Panel panel11;
        private Common.CpxTextbox txtGStreamerDir;
    }
}
