﻿using CpxLib.Controls.Common;

namespace CpxLib.Controls.Settings
{
    partial class ScheduleSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScheduleSettings));
            this.tvvSchedules = new System.Windows.Forms.TreeView();
            this.lstTriggers = new System.Windows.Forms.ListView();
            this.colTrigger = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDetail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.txtPathName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnDeleteTrigger = new CpxLib.Controls.Common.CpxButton();
            this.btnEditTrigger = new CpxLib.Controls.Common.CpxButton();
            this.btnNewTrigger = new CpxLib.Controls.Common.CpxButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnNewSchedule = new CpxLib.Controls.Common.CpxButton();
            this.btnSave = new CpxLib.Controls.Common.CpxButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvvSchedules
            // 
            resources.ApplyResources(this.tvvSchedules, "tvvSchedules");
            this.tvvSchedules.HideSelection = false;
            this.tvvSchedules.Name = "tvvSchedules";
            // 
            // lstTriggers
            // 
            this.lstTriggers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTrigger,
            this.colDetail,
            this.colStatus});
            resources.ApplyResources(this.lstTriggers, "lstTriggers");
            this.lstTriggers.FullRowSelect = true;
            this.lstTriggers.GridLines = true;
            this.lstTriggers.HideSelection = false;
            this.lstTriggers.Name = "lstTriggers";
            this.lstTriggers.UseCompatibleStateImageBehavior = false;
            this.lstTriggers.View = System.Windows.Forms.View.Details;
            this.lstTriggers.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstTriggers_MouseDoubleClick);
            // 
            // colTrigger
            // 
            resources.ApplyResources(this.colTrigger, "colTrigger");
            // 
            // colDetail
            // 
            resources.ApplyResources(this.colDetail, "colDetail");
            // 
            // colStatus
            // 
            resources.ApplyResources(this.colStatus, "colStatus");
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Name = "label2";
            // 
            // txtPathName
            // 
            resources.ApplyResources(this.txtPathName, "txtPathName");
            this.txtPathName.Name = "txtPathName";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.groupBox2);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.panel1.Tag = "custom";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel5);
            this.groupBox2.Controls.Add(this.panel6);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lstTriggers);
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            // 
            // btnDeleteTrigger
            // 
            this.btnDeleteTrigger.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnDeleteTrigger.ButtonAction = null;
            this.btnDeleteTrigger.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.btnDeleteTrigger, "btnDeleteTrigger");
            this.btnDeleteTrigger.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDeleteTrigger.Name = "btnDeleteTrigger";
            this.btnDeleteTrigger.UseVisualStyleBackColor = false;
            this.btnDeleteTrigger.Click += new System.EventHandler(this.btnDeleteTrigger_Click);
            // 
            // btnEditTrigger
            // 
            this.btnEditTrigger.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnEditTrigger.ButtonAction = null;
            this.btnEditTrigger.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.btnEditTrigger, "btnEditTrigger");
            this.btnEditTrigger.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnEditTrigger.Name = "btnEditTrigger";
            this.btnEditTrigger.UseVisualStyleBackColor = false;
            this.btnEditTrigger.Click += new System.EventHandler(this.btnEditTrigger_Click);
            // 
            // btnNewTrigger
            // 
            this.btnNewTrigger.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnNewTrigger.ButtonAction = null;
            this.btnNewTrigger.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.btnNewTrigger, "btnNewTrigger");
            this.btnNewTrigger.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnNewTrigger.Name = "btnNewTrigger";
            this.btnNewTrigger.UseVisualStyleBackColor = false;
            this.btnNewTrigger.Click += new System.EventHandler(this.btnNewTrigger_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnNewSchedule);
            this.panel2.Controls.Add(this.btnSave);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnNewSchedule
            // 
            resources.ApplyResources(this.btnNewSchedule, "btnNewSchedule");
            this.btnNewSchedule.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnNewSchedule.ButtonAction = null;
            this.btnNewSchedule.FlatAppearance.BorderSize = 0;
            this.btnNewSchedule.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnNewSchedule.Name = "btnNewSchedule";
            this.btnNewSchedule.UseVisualStyleBackColor = false;
            this.btnNewSchedule.Click += new System.EventHandler(this.btnNewSchedule_Click);
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSave.ButtonAction = null;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel3
            // 
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBox1);
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.panel2);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.txtPathName);
            this.panel6.Controls.Add(this.btnDeleteTrigger);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.btnEditTrigger);
            this.panel6.Controls.Add(this.btnNewTrigger);
            resources.ApplyResources(this.panel6, "panel6");
            this.panel6.Name = "panel6";
            // 
            // ScheduleSettings
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.tvvSchedules);
            this.Name = "ScheduleSettings";
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView tvvSchedules;
        private System.Windows.Forms.ListView lstTriggers;
        private System.Windows.Forms.ColumnHeader colTrigger;
        private System.Windows.Forms.ColumnHeader colDetail;
        private System.Windows.Forms.ColumnHeader colStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPathName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private CpxButton btnSave;
        private CpxButton btnDeleteTrigger;
        private CpxButton btnEditTrigger;
        private CpxButton btnNewTrigger;
        private System.Windows.Forms.Panel panel3;
        private CpxButton btnNewSchedule;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
    }
}
