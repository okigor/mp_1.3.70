﻿using CpxLib.Controls.Common;

namespace CpxLib.Controls.Settings
{
    partial class PlanSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlanSettings));
            this.tvwPlans = new System.Windows.Forms.TreeView();
            this.lstWaypoints = new System.Windows.Forms.ListView();
            this.colLat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colLng = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAlt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtPathName = new System.Windows.Forms.TextBox();
            this.btnDeleteTrigger = new CpxLib.Controls.Common.CpxButton();
            this.label2 = new System.Windows.Forms.Label();
            this.btnEditTrigger = new CpxLib.Controls.Common.CpxButton();
            this.btnNewTrigger = new CpxLib.Controls.Common.CpxButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSave = new CpxLib.Controls.Common.CpxButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvwPlans
            // 
            resources.ApplyResources(this.tvwPlans, "tvwPlans");
            this.tvwPlans.HideSelection = false;
            this.tvwPlans.Name = "tvwPlans";
            // 
            // lstWaypoints
            // 
            resources.ApplyResources(this.lstWaypoints, "lstWaypoints");
            this.lstWaypoints.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colLat,
            this.colLng,
            this.colAlt});
            this.lstWaypoints.FullRowSelect = true;
            this.lstWaypoints.GridLines = true;
            this.lstWaypoints.HideSelection = false;
            this.lstWaypoints.Name = "lstWaypoints";
            this.lstWaypoints.UseCompatibleStateImageBehavior = false;
            this.lstWaypoints.View = System.Windows.Forms.View.Details;
            // 
            // colLat
            // 
            resources.ApplyResources(this.colLat, "colLat");
            // 
            // colLng
            // 
            resources.ApplyResources(this.colLng, "colLng");
            // 
            // colAlt
            // 
            resources.ApplyResources(this.colAlt, "colAlt");
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.txtPathName);
            this.panel1.Controls.Add(this.btnDeleteTrigger);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnEditTrigger);
            this.panel1.Controls.Add(this.btnNewTrigger);
            this.panel1.Name = "panel1";
            this.panel1.Tag = "custom";
            // 
            // txtPathName
            // 
            resources.ApplyResources(this.txtPathName, "txtPathName");
            this.txtPathName.Name = "txtPathName";
            // 
            // btnDeleteTrigger
            // 
            resources.ApplyResources(this.btnDeleteTrigger, "btnDeleteTrigger");
            this.btnDeleteTrigger.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnDeleteTrigger.ButtonAction = null;
            this.btnDeleteTrigger.FlatAppearance.BorderSize = 0;
            this.btnDeleteTrigger.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDeleteTrigger.Name = "btnDeleteTrigger";
            this.btnDeleteTrigger.UseVisualStyleBackColor = false;
            this.btnDeleteTrigger.Click += new System.EventHandler(this.btnDeleteTrigger_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // btnEditTrigger
            // 
            resources.ApplyResources(this.btnEditTrigger, "btnEditTrigger");
            this.btnEditTrigger.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnEditTrigger.ButtonAction = null;
            this.btnEditTrigger.FlatAppearance.BorderSize = 0;
            this.btnEditTrigger.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnEditTrigger.Name = "btnEditTrigger";
            this.btnEditTrigger.UseVisualStyleBackColor = false;
            this.btnEditTrigger.Click += new System.EventHandler(this.btnEditTrigger_Click);
            // 
            // btnNewTrigger
            // 
            resources.ApplyResources(this.btnNewTrigger, "btnNewTrigger");
            this.btnNewTrigger.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnNewTrigger.ButtonAction = null;
            this.btnNewTrigger.FlatAppearance.BorderSize = 0;
            this.btnNewTrigger.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnNewTrigger.Name = "btnNewTrigger";
            this.btnNewTrigger.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Name = "panel2";
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSave.ButtonAction = null;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // PlanSettings
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lstWaypoints);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tvwPlans);
            this.Name = "PlanSettings";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView tvwPlans;
        private System.Windows.Forms.ListView lstWaypoints;
        private System.Windows.Forms.ColumnHeader colLat;
        private System.Windows.Forms.ColumnHeader colLng;
        private System.Windows.Forms.ColumnHeader colAlt;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtPathName;
        private System.Windows.Forms.Label label2;
        private CpxButton btnNewTrigger;
        private System.Windows.Forms.Panel panel2;
        private CpxButton btnSave;
        private CpxButton btnDeleteTrigger;
        private CpxButton btnEditTrigger;
    }
}
