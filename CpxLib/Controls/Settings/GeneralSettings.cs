﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CpxLib.Core;
using System.Globalization;
using System.Threading;
using MissionPlanner;

namespace CpxLib.Controls.Settings
{
    public partial class GeneralSettings : UserControl
    {
        private Configuration _configuration;
        public GeneralSettings()
        {
            //Properties.Settings.Default.SettingsSaving += SettingsSaving;
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(CpxHost.Instance.Language);
            InitializeComponent();
            LoadData();
            //txtGStreamerDir.Left =groupBox1.Width- txtGStreamerDir.Width-9;
        }

        private IEnumerable<Control> GetAll(Control control)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl))
                                      .Concat(controls);
        }
        private void LoadData()
        {
            _configuration = AppSettings.LoadConfiguration();

            radDefault.Checked = _configuration.LayoutMode == 2;
            radCopterPix.Checked = _configuration.LayoutMode == 1;
            txtGStreamerDir.Text = _configuration.GStreamerDir;
            txtLTEIP.Text = _configuration.LTEAddress;
            txtLandingStationIP.Text = _configuration.LandingStationIP;
            txtLandingStationPort.Text = _configuration.LandingStationPort;
            txtVideo.Text = _configuration.VideoControlPath;
            txtOpenVPN.Text = _configuration.OpenVPNPath;
        }
        private void btnBrowseGSteamerFolder_Click(object sender, EventArgs e)
        {
            var dlg = new FolderBrowserDialog();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                txtGStreamerDir.Text = dlg.SelectedPath;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _configuration.GStreamerDir = txtGStreamerDir.Text;
            _configuration.LayoutMode = radDefault.Checked ? 2 : 1;
            _configuration.LTEAddress = txtLTEIP.Text;
            _configuration.LandingStationIP = txtLandingStationIP.Text;
            _configuration.LandingStationPort = txtLandingStationPort.Text;
            _configuration.VideoControlPath = txtVideo.Text;
            _configuration.OpenVPNPath = txtOpenVPN.Text;

            AppSettings.SaveConfiguration(_configuration);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lngCombo.SelectedItem.ToString() == "Hebrew")
            {
                CpxHost.Instance.Language = "he-IL";
            }
            else
            {
                CpxHost.Instance.Language = "en-US";
            }
            //MainV2.instance.changelanguage(new System.Globalization.CultureInfo(language));
            Properties.Settings.Default.Save();
            MessageBox.Show("Please restart Mission Planner");
            MainV2.instance.Close();
        }

        private void btnVideo_Click(object sender, EventArgs e)
        {
            //var dlg = new OpenFileDialog();
            var dlg = new FolderBrowserDialog();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                txtVideo.Text = dlg.SelectedPath;
            }
        }

        private void btnOpenVPN_Click(object sender, EventArgs e)
        {
            var dlg = new FolderBrowserDialog();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                txtOpenVPN.Text = dlg.SelectedPath;
            }
        }

        private void radDefault_CheckedChanged(object sender, EventArgs e)
        {
            if(_configuration.LayoutMode == 1)
            {
                btnSave_Click(null, null);
                Properties.Settings.Default.Save();
                MessageBox.Show("Please restart Mission Planner");
                MainV2.instance.Close();
            }

        }

        private void radCopterPix_CheckedChanged(object sender, EventArgs e)
        {
            if(_configuration.LayoutMode == 2)
            {
                btnSave_Click(null, null);
                Properties.Settings.Default.Save();
                MessageBox.Show("Please restart Mission Planner");
                MainV2.instance.Close();
            }

        }
    }
}
