﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CpxLib.Model;
using CpxLib.Forms;
using CpxLib.Properties;

namespace CpxLib.Controls.Settings
{
    public partial class ScheduleSettings : UserControl
    {
        private Schedule _schedule;
        private bool _isNewSchedule;

        public ScheduleSettings()
        {
            InitializeComponent();
            FillList();

            tvvSchedules.NodeMouseClick += TvvSchedules_NodeMouseClick;

        }

        private void TableLayoutPanel1_MouseClick(object sender, MouseEventArgs e)
        {
            Control control = this.GetChildAtPoint(new Point(e.Location.X, e.Location.Y));
            MessageBox.Show(control.ToString());
        }

        private void TvvSchedules_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            _schedule = e.Node.Tag as Schedule;
            FillFields();
            _isNewSchedule = false;
        }
        private void FillFields()
        {
            txtPathName.Text = _schedule.Name;
            lstTriggers.Items.Clear();
            foreach (Trigger item in _schedule.Triggers)
            {
                AddItemToList(item);
            }

        }

        private void AddItemToList(Trigger trigger)
        {
            var listItem = new ListViewItem();
            listItem.Text = trigger.TriggerType.ToString();
            listItem.SubItems.Add(trigger.Details);
            listItem.SubItems.Add(trigger.Status);
            listItem.Tag = trigger;
            lstTriggers.Items.Add(listItem);

        }
        private void FillList()
        {
            foreach (var fileInfo in ModelExtensions.GetScheduleFiles())
            {
                var treeNode = new TreeNode(fileInfo.Name);
                treeNode.Tag = fileInfo;
                tvvSchedules.Nodes.Add(treeNode);

            }

            if (tvvSchedules.Nodes.Count > 0)
            {
                tvvSchedules.SelectedNode = tvvSchedules.Nodes[0];

                _schedule = tvvSchedules.SelectedNode.Tag as Schedule;
                FillFields();
                _isNewSchedule = false;

            }
            else
            {
                _schedule = new Schedule();
                FillFields();
                _isNewSchedule = true;

            }
        }


        private void lstTriggers_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var senderList = (ListView)sender;
            var clickedItem = senderList.HitTest(e.Location).Item;
            if (clickedItem != null)
            {
                var trigger = clickedItem.Tag as Trigger;

                var frm = new frmEditTrigger(trigger);

                if (frm.ShowDialog() == DialogResult.OK)
                {
                    trigger.TriggerType = frm.Trigger.TriggerType;
                    trigger.StartTime = frm.Trigger.StartTime;
                    trigger.RepeatInterval = frm.Trigger.RepeatInterval;
                    trigger.FinishTime = frm.Trigger.FinishTime;
                    trigger.Enabled = frm.Trigger.Enabled;
                    trigger.ActionId = frm.Trigger.ActionId;
                    trigger.VehicleId = frm.Trigger.VehicleId;
                    clickedItem.Text = trigger.TriggerType.ToString();
                    clickedItem.SubItems[1].Text = trigger.Details;
                    clickedItem.SubItems[2].Text = trigger.Status;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            _schedule.Name = txtPathName.Text;
            _schedule.Triggers = new List<Trigger>();

            foreach (ListViewItem item in lstTriggers.Items)
            {
                var trigger = item.Tag as Trigger;

                _schedule.Triggers.Add(trigger);


            }

            _schedule.Save();
            if (_isNewSchedule)
            {
                var treeNode = new TreeNode(_schedule.Name);
                treeNode.Tag = _schedule;
                tvvSchedules.Nodes.Add(treeNode);
                tvvSchedules.SelectedNode = treeNode;
                _isNewSchedule = false;
            }
            else
            {
                tvvSchedules.SelectedNode.Text = _schedule.Name;
            }
        }

        private void btnNewTrigger_Click(object sender, EventArgs e)
        {

            var trigger = new Trigger();

            var frm = new frmEditTrigger(trigger);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                trigger.TriggerType = frm.Trigger.TriggerType;
                trigger.StartTime = frm.Trigger.StartTime;
                trigger.RepeatInterval = frm.Trigger.RepeatInterval;
                trigger.FinishTime = frm.Trigger.FinishTime;
                trigger.Enabled = frm.Trigger.Enabled;
                trigger.ActionId = frm.Trigger.ActionId;
                AddItemToList(trigger);

            }

        }

        private void btnDeleteTrigger_Click(object sender, EventArgs e)
        {

            if (lstTriggers.SelectedItems.Count > 0)
            {
                var clickedItem = lstTriggers.SelectedItems[0];
                var trigger = clickedItem.Tag as Trigger;
                _schedule.Triggers.Remove(trigger);
                lstTriggers.Items.Remove(clickedItem);
            }
        }

        private void btnEditTrigger_Click(object sender, EventArgs e)
        {

            if (lstTriggers.SelectedItems.Count > 0)
            {
                var clickedItem = lstTriggers.SelectedItems[0];
                var trigger = clickedItem.Tag as Trigger;

                var frm = new frmEditTrigger(trigger);

                if (frm.ShowDialog() == DialogResult.OK)
                {
                    trigger.TriggerType = frm.Trigger.TriggerType;
                    trigger.StartTime = frm.Trigger.StartTime;
                    trigger.RepeatInterval = frm.Trigger.RepeatInterval;
                    trigger.FinishTime = frm.Trigger.FinishTime;
                    trigger.Enabled = frm.Trigger.Enabled;
                    trigger.ActionId = frm.Trigger.ActionId;
                    clickedItem.Text = trigger.TriggerType.ToString();
                    clickedItem.SubItems[1].Text = trigger.Details;
                    clickedItem.SubItems[2].Text = trigger.Status;
                }
            }
        }

        private void btnNewSchedule_Click(object sender, EventArgs e)
        {
            _schedule = new Schedule();
            _isNewSchedule = true;

            FillFields();

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {
            //Control c=this.GetChildAtPoint(new Point(e.))
            //var clickedItem = senderList.HitTest(e.Location).Item;
            //if (clickedItem != null)
            //{
            //    var trigger = clickedItem.Tag as Trigger;

            //    var frm = new frmEditTrigger(trigger);

            //    if (frm.ShowDialog() == DialogResult.OK)
            //    {
            //        trigger.TriggerType = frm.Trigger.TriggerType;
            //        trigger.StartTime = frm.Trigger.StartTime;
            //        trigger.RepeatInterval = frm.Trigger.RepeatInterval;
            //        trigger.FinishTime = frm.Trigger.FinishTime;
            //        trigger.Enabled = frm.Trigger.Enabled;
            //        trigger.ActionId = frm.Trigger.ActionId;
            //        trigger.VehicleId = frm.Trigger.VehicleId;
            //        clickedItem.Text = trigger.TriggerType.ToString();
            //        clickedItem.SubItems[1].Text = trigger.Details;
            //        clickedItem.SubItems[2].Text = trigger.Status;
            //    }
            //}
        }

        private void cpxButton1_Click(object sender, EventArgs e)
        {
            //tableLayoutPanel1.row
        }
    }
}
