﻿namespace CpxLib.Controls
{
    partial class CpxCommand
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CpxCommand));
            this.lblCommand = new CpxLib.Controls.Common.CpxLabel();
            this.cmbCommand = new System.Windows.Forms.ComboBox();
            this.btnDelete = new CpxLib.Controls.Common.CpxButton();
            this.lblLat = new CpxLib.Controls.Common.CpxLabel();
            this.txtLatitude = new CpxLib.Controls.Common.CpxTextbox();
            this.txtLongitude = new CpxLib.Controls.Common.CpxTextbox();
            this.lblLng = new CpxLib.Controls.Common.CpxLabel();
            this.txtAltitude = new CpxLib.Controls.Common.CpxTextbox();
            this.lblAlt = new CpxLib.Controls.Common.CpxLabel();
            this.txtP1 = new CpxLib.Controls.Common.CpxTextbox();
            this.lblP1 = new CpxLib.Controls.Common.CpxLabel();
            this.txtP2 = new CpxLib.Controls.Common.CpxTextbox();
            this.lblP2 = new CpxLib.Controls.Common.CpxLabel();
            this.btnDown = new CpxLib.Controls.Common.CpxButton();
            this.btnUp = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton1 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton2 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton3 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton4 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton5 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton6 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton7 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton8 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton9 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton10 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton11 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButtonLeft = new CpxLib.Controls.Common.CpxButton();
            this.cpxButtonDelete = new CpxLib.Controls.Common.CpxButton();
            this.cpxButtonRight = new CpxLib.Controls.Common.CpxButton();
            this.lblWpNo = new CpxLib.Controls.Common.CpxLabel();
            this.cpxButton12 = new CpxLib.Controls.Common.CpxButton();
            this.txtNorthing = new CpxLib.Controls.Common.CpxTextbox();
            this.lblNorthing = new CpxLib.Controls.Common.CpxLabel();
            this.txtEasting = new CpxLib.Controls.Common.CpxTextbox();
            this.lblEasting = new CpxLib.Controls.Common.CpxLabel();
            this.txtZone = new CpxLib.Controls.Common.CpxTextbox();
            this.lblZone = new CpxLib.Controls.Common.CpxLabel();
            this.SuspendLayout();
            // 
            // lblCommand
            // 
            resources.ApplyResources(this.lblCommand, "lblCommand");
            this.lblCommand.ForeColor = System.Drawing.Color.White;
            this.lblCommand.Name = "lblCommand";
            // 
            // cmbCommand
            // 
            this.cmbCommand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            resources.ApplyResources(this.cmbCommand, "cmbCommand");
            this.cmbCommand.ForeColor = System.Drawing.Color.White;
            this.cmbCommand.FormattingEnabled = true;
            this.cmbCommand.Name = "cmbCommand";
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnDelete.ButtonAction = null;
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnDelete.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.btnDelete, "btnDelete");
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // lblLat
            // 
            resources.ApplyResources(this.lblLat, "lblLat");
            this.lblLat.ForeColor = System.Drawing.Color.White;
            this.lblLat.Name = "lblLat";
            // 
            // txtLatitude
            // 
            this.txtLatitude.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtLatitude.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtLatitude, "txtLatitude");
            this.txtLatitude.ForeColor = System.Drawing.Color.White;
            this.txtLatitude.Name = "txtLatitude";
            this.txtLatitude.Click += new System.EventHandler(this.textBox_Click);
            // 
            // txtLongitude
            // 
            this.txtLongitude.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtLongitude.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtLongitude, "txtLongitude");
            this.txtLongitude.ForeColor = System.Drawing.Color.White;
            this.txtLongitude.Name = "txtLongitude";
            this.txtLongitude.Click += new System.EventHandler(this.textBox_Click);
            // 
            // lblLng
            // 
            resources.ApplyResources(this.lblLng, "lblLng");
            this.lblLng.ForeColor = System.Drawing.Color.White;
            this.lblLng.Name = "lblLng";
            // 
            // txtAltitude
            // 
            this.txtAltitude.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtAltitude.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtAltitude, "txtAltitude");
            this.txtAltitude.ForeColor = System.Drawing.Color.White;
            this.txtAltitude.Name = "txtAltitude";
            this.txtAltitude.Click += new System.EventHandler(this.textBox_Click);
            // 
            // lblAlt
            // 
            resources.ApplyResources(this.lblAlt, "lblAlt");
            this.lblAlt.ForeColor = System.Drawing.Color.White;
            this.lblAlt.Name = "lblAlt";
            // 
            // txtP1
            // 
            this.txtP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtP1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtP1, "txtP1");
            this.txtP1.ForeColor = System.Drawing.Color.White;
            this.txtP1.Name = "txtP1";
            this.txtP1.Click += new System.EventHandler(this.textBox_Click);
            // 
            // lblP1
            // 
            resources.ApplyResources(this.lblP1, "lblP1");
            this.lblP1.ForeColor = System.Drawing.Color.White;
            this.lblP1.Name = "lblP1";
            // 
            // txtP2
            // 
            this.txtP2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtP2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtP2, "txtP2");
            this.txtP2.ForeColor = System.Drawing.Color.White;
            this.txtP2.Name = "txtP2";
            this.txtP2.Click += new System.EventHandler(this.textBox_Click);
            // 
            // lblP2
            // 
            resources.ApplyResources(this.lblP2, "lblP2");
            this.lblP2.ForeColor = System.Drawing.Color.White;
            this.lblP2.Name = "lblP2";
            // 
            // btnDown
            // 
            this.btnDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnDown.ButtonAction = null;
            this.btnDown.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnDown.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.btnDown, "btnDown");
            this.btnDown.Name = "btnDown";
            this.btnDown.UseVisualStyleBackColor = false;
            // 
            // btnUp
            // 
            this.btnUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnUp.ButtonAction = null;
            this.btnUp.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnUp.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.btnUp, "btnUp");
            this.btnUp.Name = "btnUp";
            this.btnUp.UseVisualStyleBackColor = false;
            // 
            // cpxButton1
            // 
            this.cpxButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton1.ButtonAction = null;
            resources.ApplyResources(this.cpxButton1, "cpxButton1");
            this.cpxButton1.ForeColor = System.Drawing.Color.White;
            this.cpxButton1.Name = "cpxButton1";
            this.cpxButton1.TabStop = false;
            this.cpxButton1.UseVisualStyleBackColor = false;
            this.cpxButton1.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton2
            // 
            this.cpxButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton2.ButtonAction = null;
            resources.ApplyResources(this.cpxButton2, "cpxButton2");
            this.cpxButton2.ForeColor = System.Drawing.Color.White;
            this.cpxButton2.Name = "cpxButton2";
            this.cpxButton2.TabStop = false;
            this.cpxButton2.UseVisualStyleBackColor = false;
            this.cpxButton2.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton3
            // 
            this.cpxButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton3.ButtonAction = null;
            resources.ApplyResources(this.cpxButton3, "cpxButton3");
            this.cpxButton3.ForeColor = System.Drawing.Color.White;
            this.cpxButton3.Name = "cpxButton3";
            this.cpxButton3.TabStop = false;
            this.cpxButton3.UseVisualStyleBackColor = false;
            this.cpxButton3.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton4
            // 
            this.cpxButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton4.ButtonAction = null;
            resources.ApplyResources(this.cpxButton4, "cpxButton4");
            this.cpxButton4.ForeColor = System.Drawing.Color.White;
            this.cpxButton4.Name = "cpxButton4";
            this.cpxButton4.TabStop = false;
            this.cpxButton4.UseVisualStyleBackColor = false;
            this.cpxButton4.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton5
            // 
            this.cpxButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton5.ButtonAction = null;
            resources.ApplyResources(this.cpxButton5, "cpxButton5");
            this.cpxButton5.ForeColor = System.Drawing.Color.White;
            this.cpxButton5.Name = "cpxButton5";
            this.cpxButton5.TabStop = false;
            this.cpxButton5.UseVisualStyleBackColor = false;
            this.cpxButton5.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton6
            // 
            this.cpxButton6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton6.ButtonAction = null;
            resources.ApplyResources(this.cpxButton6, "cpxButton6");
            this.cpxButton6.ForeColor = System.Drawing.Color.White;
            this.cpxButton6.Name = "cpxButton6";
            this.cpxButton6.TabStop = false;
            this.cpxButton6.UseVisualStyleBackColor = false;
            this.cpxButton6.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton7
            // 
            this.cpxButton7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton7.ButtonAction = null;
            resources.ApplyResources(this.cpxButton7, "cpxButton7");
            this.cpxButton7.ForeColor = System.Drawing.Color.White;
            this.cpxButton7.Name = "cpxButton7";
            this.cpxButton7.TabStop = false;
            this.cpxButton7.UseVisualStyleBackColor = false;
            this.cpxButton7.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton8
            // 
            this.cpxButton8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton8.ButtonAction = null;
            resources.ApplyResources(this.cpxButton8, "cpxButton8");
            this.cpxButton8.ForeColor = System.Drawing.Color.White;
            this.cpxButton8.Name = "cpxButton8";
            this.cpxButton8.TabStop = false;
            this.cpxButton8.UseVisualStyleBackColor = false;
            this.cpxButton8.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton9
            // 
            this.cpxButton9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton9.ButtonAction = null;
            resources.ApplyResources(this.cpxButton9, "cpxButton9");
            this.cpxButton9.ForeColor = System.Drawing.Color.White;
            this.cpxButton9.Name = "cpxButton9";
            this.cpxButton9.TabStop = false;
            this.cpxButton9.UseVisualStyleBackColor = false;
            this.cpxButton9.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton10
            // 
            this.cpxButton10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton10.ButtonAction = null;
            resources.ApplyResources(this.cpxButton10, "cpxButton10");
            this.cpxButton10.ForeColor = System.Drawing.Color.White;
            this.cpxButton10.Name = "cpxButton10";
            this.cpxButton10.TabStop = false;
            this.cpxButton10.UseVisualStyleBackColor = false;
            this.cpxButton10.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButton11
            // 
            this.cpxButton11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton11.ButtonAction = null;
            resources.ApplyResources(this.cpxButton11, "cpxButton11");
            this.cpxButton11.ForeColor = System.Drawing.Color.White;
            this.cpxButton11.Name = "cpxButton11";
            this.cpxButton11.TabStop = false;
            this.cpxButton11.UseVisualStyleBackColor = false;
            this.cpxButton11.Click += new System.EventHandler(this.cpxButton_Click);
            // 
            // cpxButtonLeft
            // 
            this.cpxButtonLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButtonLeft.ButtonAction = null;
            resources.ApplyResources(this.cpxButtonLeft, "cpxButtonLeft");
            this.cpxButtonLeft.ForeColor = System.Drawing.Color.White;
            this.cpxButtonLeft.Name = "cpxButtonLeft";
            this.cpxButtonLeft.TabStop = false;
            this.cpxButtonLeft.UseVisualStyleBackColor = false;
            this.cpxButtonLeft.Click += new System.EventHandler(this.cpxButtonLeft_Click);
            // 
            // cpxButtonDelete
            // 
            this.cpxButtonDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButtonDelete.ButtonAction = null;
            resources.ApplyResources(this.cpxButtonDelete, "cpxButtonDelete");
            this.cpxButtonDelete.ForeColor = System.Drawing.Color.White;
            this.cpxButtonDelete.Name = "cpxButtonDelete";
            this.cpxButtonDelete.TabStop = false;
            this.cpxButtonDelete.UseVisualStyleBackColor = false;
            this.cpxButtonDelete.Click += new System.EventHandler(this.cpxButtonDelete_Click);
            // 
            // cpxButtonRight
            // 
            this.cpxButtonRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButtonRight.ButtonAction = null;
            resources.ApplyResources(this.cpxButtonRight, "cpxButtonRight");
            this.cpxButtonRight.ForeColor = System.Drawing.Color.White;
            this.cpxButtonRight.Name = "cpxButtonRight";
            this.cpxButtonRight.TabStop = false;
            this.cpxButtonRight.UseVisualStyleBackColor = false;
            this.cpxButtonRight.Click += new System.EventHandler(this.cpxButtonRight_Click);
            // 
            // lblWpNo
            // 
            resources.ApplyResources(this.lblWpNo, "lblWpNo");
            this.lblWpNo.ForeColor = System.Drawing.Color.White;
            this.lblWpNo.Name = "lblWpNo";
            // 
            // cpxButton12
            // 
            this.cpxButton12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.cpxButton12.ButtonAction = null;
            this.cpxButton12.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.cpxButton12, "cpxButton12");
            this.cpxButton12.ForeColor = System.Drawing.Color.White;
            this.cpxButton12.Name = "cpxButton12";
            this.cpxButton12.UseVisualStyleBackColor = false;
            this.cpxButton12.Click += new System.EventHandler(this.cpxButton12_Click);
            // 
            // txtNorthing
            // 
            this.txtNorthing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtNorthing.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtNorthing, "txtNorthing");
            this.txtNorthing.ForeColor = System.Drawing.Color.White;
            this.txtNorthing.Name = "txtNorthing";
            // 
            // lblNorthing
            // 
            resources.ApplyResources(this.lblNorthing, "lblNorthing");
            this.lblNorthing.ForeColor = System.Drawing.Color.White;
            this.lblNorthing.Name = "lblNorthing";
            // 
            // txtEasting
            // 
            this.txtEasting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtEasting.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtEasting, "txtEasting");
            this.txtEasting.ForeColor = System.Drawing.Color.White;
            this.txtEasting.Name = "txtEasting";
            // 
            // lblEasting
            // 
            resources.ApplyResources(this.lblEasting, "lblEasting");
            this.lblEasting.ForeColor = System.Drawing.Color.White;
            this.lblEasting.Name = "lblEasting";
            // 
            // txtZone
            // 
            this.txtZone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtZone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtZone, "txtZone");
            this.txtZone.ForeColor = System.Drawing.Color.White;
            this.txtZone.Name = "txtZone";
            // 
            // lblZone
            // 
            resources.ApplyResources(this.lblZone, "lblZone");
            this.lblZone.ForeColor = System.Drawing.Color.White;
            this.lblZone.Name = "lblZone";
            // 
            // CpxCommand
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(24)))), ((int)(((byte)(23)))));
            this.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BorderWidth = 7;
            this.Controls.Add(this.txtZone);
            this.Controls.Add(this.lblZone);
            this.Controls.Add(this.txtNorthing);
            this.Controls.Add(this.lblNorthing);
            this.Controls.Add(this.txtEasting);
            this.Controls.Add(this.lblEasting);
            this.Controls.Add(this.cpxButton12);
            this.Controls.Add(this.lblWpNo);
            this.Controls.Add(this.cpxButtonRight);
            this.Controls.Add(this.cpxButtonDelete);
            this.Controls.Add(this.cpxButtonLeft);
            this.Controls.Add(this.cpxButton11);
            this.Controls.Add(this.cpxButton10);
            this.Controls.Add(this.cpxButton9);
            this.Controls.Add(this.cpxButton8);
            this.Controls.Add(this.cpxButton7);
            this.Controls.Add(this.cpxButton6);
            this.Controls.Add(this.cpxButton5);
            this.Controls.Add(this.cpxButton4);
            this.Controls.Add(this.cpxButton3);
            this.Controls.Add(this.cpxButton2);
            this.Controls.Add(this.cpxButton1);
            this.Controls.Add(this.btnUp);
            this.Controls.Add(this.btnDown);
            this.Controls.Add(this.txtP2);
            this.Controls.Add(this.lblP2);
            this.Controls.Add(this.txtP1);
            this.Controls.Add(this.lblP1);
            this.Controls.Add(this.txtAltitude);
            this.Controls.Add(this.lblAlt);
            this.Controls.Add(this.txtLongitude);
            this.Controls.Add(this.lblLng);
            this.Controls.Add(this.txtLatitude);
            this.Controls.Add(this.lblLat);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.cmbCommand);
            this.Controls.Add(this.lblCommand);
            this.Curvature = 20;
            this.Name = "CpxCommand";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Common.CpxLabel lblCommand;
        public System.Windows.Forms.ComboBox cmbCommand;
        public Common.CpxButton btnDelete;
        private Common.CpxLabel lblLat;
        public Common.CpxTextbox txtLatitude;
        public Common.CpxTextbox txtLongitude;
        private Common.CpxLabel lblLng;
        public Common.CpxTextbox txtAltitude;
        private Common.CpxLabel lblAlt;
        public Common.CpxTextbox txtP1;
        public Common.CpxLabel lblP1;
        public Common.CpxTextbox txtP2;
        public Common.CpxLabel lblP2;
        public Common.CpxButton btnDown;
        public Common.CpxButton btnUp;
        public Common.CpxButton cpxButton1;
        public Common.CpxButton cpxButton2;
        public Common.CpxButton cpxButton3;
        public Common.CpxButton cpxButton4;
        public Common.CpxButton cpxButton5;
        public Common.CpxButton cpxButton6;
        public Common.CpxButton cpxButton7;
        public Common.CpxButton cpxButton8;
        public Common.CpxButton cpxButton9;
        public Common.CpxButton cpxButton10;
        public Common.CpxButton cpxButton11;
        public Common.CpxButton cpxButtonLeft;
        public Common.CpxButton cpxButtonDelete;
        public Common.CpxButton cpxButtonRight;
        public Common.CpxLabel lblWpNo;
        private Common.CpxButton cpxButton12;
        public Common.CpxTextbox txtNorthing;
        private Common.CpxLabel lblNorthing;
        public Common.CpxTextbox txtEasting;
        private Common.CpxLabel lblEasting;
        public Common.CpxTextbox txtZone;
        private Common.CpxLabel lblZone;
    }
}
