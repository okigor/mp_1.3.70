﻿
using CpxLib.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CpxLib.Forms
{
    public partial class frmOpenSchedule : frmBase     
    {
        public Schedule SelectedFile { get; private set; }

        public frmOpenSchedule()
        {
            InitializeComponent();
            FillList();
        }

        private void FillList()
        {
            foreach (var fileInfo in ModelExtensions.GetScheduleFiles())
            {
                var modifiedDate = fileInfo.LastWriteTime.ToString("dd/MM/yyyy hh:mm");
                var listItem = new ListViewItem(new[] { fileInfo.Name, modifiedDate, fileInfo.Id });
                listItem.Tag = fileInfo;
                lstFiles.Items.Add(listItem);

            }

        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (lstFiles.SelectedItems.Count > 0)
            {
                SelectedFile = lstFiles.SelectedItems[0].Tag as Schedule;
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
