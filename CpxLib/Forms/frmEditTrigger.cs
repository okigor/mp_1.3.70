﻿
using CpxLib.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Forms
{
    public partial class frmEditTrigger : frmBase
    {
        private Trigger _trigger;

        public frmEditTrigger()
        {
            InitializeComponent();
            FillActions();
            FillVehicles();
            chkEnabled.Checked = true;
            EnableControls();
        }
        public frmEditTrigger(Trigger trigger)
        {
            InitializeComponent();
            FillActions();
            FillVehicles();
            _trigger = trigger;
            FillFields();
            EnableControls();
        }

        private void FillFields()
        {
            if (_trigger != null)
            {
                dtStartTime.Value = _trigger.StartTime;
                chkEnabled.Checked = _trigger.Enabled;
                if (_trigger.TriggerType == TriggerTypeEnum.OneTime)
                {
                    radOneTime.Checked = true;
                }
                else
                {
                    radRepeat.Checked = true;
                    txtMin.Value = _trigger.RepeatInterval;
                    dtFinishTime.Value = _trigger.FinishTime;
                }

                var i = 0;

                foreach (var item in cmbPlan.Items.OfType<FlightRoute>())
                {
                    if (item.Id == _trigger.ActionId)
                    {
                        cmbPlan.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                i = 0;

                foreach (var item in cmbVehicle.Items.OfType<Vehicle>())
                {
                    if (item.Id == _trigger.VehicleId)
                    {
                        cmbVehicle.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                chkSunday.Checked = _trigger.Days[0] == 1;
                chkMonday.Checked = _trigger.Days[1] == 1;
                chkTuesday.Checked = _trigger.Days[2] == 1;
                chkWednesday.Checked = _trigger.Days[3] == 1;
                chkThursday.Checked = _trigger.Days[4] == 1;
                chkFriday.Checked = _trigger.Days[5] == 1;
                chkSaturday.Checked = _trigger.Days[6] == 1;

            }
        }

        private void FillVehicles()
        {
            cmbVehicle.Items.Clear();
            cmbVehicle.DisplayMember = "Name";
            cmbVehicle.ValueMember = "Id";

            foreach (var fileInfo in ModelExtensions.GetVehicleFiles())
            {
                cmbVehicle.Items.Add(fileInfo);
            }
        }
        private void FillActions()
        {
            cmbPlan.Items.Clear();
            cmbPlan.DisplayMember = "Name";
            cmbPlan.ValueMember = "Id";

            foreach (var fileInfo in ModelExtensions.GetFlightRoutes())
            {
                cmbPlan.Items.Add(fileInfo);
            }
        }
        private void ReadData()
        {
            if (_trigger == null)
            {
                _trigger = new Trigger();
            }

            if (radOneTime.Checked)
            {
                _trigger.TriggerType = TriggerTypeEnum.OneTime;
            }
            else if (radRepeat.Checked)
            {
                _trigger.TriggerType = TriggerTypeEnum.Repeating;
            }
            else
            {
                _trigger.TriggerType = TriggerTypeEnum.NotSet;
            }

            _trigger.StartTime = dtStartTime.Value;
            _trigger.Enabled = chkEnabled.Checked;

            if (_trigger.TriggerType == TriggerTypeEnum.Repeating)
            {
                _trigger.FinishTime = dtFinishTime.Value;
                _trigger.RepeatInterval = Convert.ToInt32(txtMin.Value);
            }

            if (cmbPlan.SelectedItem != null)
            {
                var action = cmbPlan.SelectedItem as FlightRoute;

                _trigger.ActionId = action.Id;
            }
            if (cmbVehicle.SelectedItem != null)
            {
                var vehicle = cmbVehicle.SelectedItem as Vehicle;

                _trigger.VehicleId = vehicle.Id;
            }

            _trigger.Days[0] = chkSunday.Checked ? 1 : 0;
            _trigger.Days[1] = chkMonday.Checked ? 1 : 0;
            _trigger.Days[2] = chkTuesday.Checked ? 1 : 0;
            _trigger.Days[3] = chkWednesday.Checked ? 1 : 0;
            _trigger.Days[4] = chkThursday.Checked ? 1 : 0;
            _trigger.Days[5] = chkFriday.Checked ? 1 : 0;
            _trigger.Days[6] = chkSaturday.Checked ? 1 : 0;
        }

        public Trigger Trigger
        {
            get
            {
                return _trigger;
            }
        }
        private void triggerType_CheckedChanged(object sender, EventArgs e)
        {
            EnableControls();
        }

        private void EnableControls()
        {
            lblFinish.Visible = radRepeat.Checked;
            lblMin.Visible = radRepeat.Checked;
            lblRepeat.Visible = radRepeat.Checked;
            txtMin.Visible = radRepeat.Checked;
            dtFinishTime.Visible = radRepeat.Checked;

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            ReadData();
            //_trigger.save();
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
