﻿namespace CpxLib.Forms
{
    partial class frmActions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmActions));
            this.btnSimulation = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton1 = new CpxLib.Controls.Common.CpxButton();
            this.btnJoystick = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton3 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton4 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton5 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton6 = new CpxLib.Controls.Common.CpxButton();
            this.btnLogExtractor = new CpxLib.Controls.Common.CpxButton();
            this.btnSDRtoLTE = new CpxLib.Controls.Common.CpxButton();
            this.SuspendLayout();
            // 
            // btnSimulation
            // 
            this.btnSimulation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSimulation.ButtonAction = null;
            this.btnSimulation.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.btnSimulation, "btnSimulation");
            this.btnSimulation.ForeColor = System.Drawing.Color.White;
            this.btnSimulation.Name = "btnSimulation";
            this.btnSimulation.UseVisualStyleBackColor = false;
            this.btnSimulation.Click += new System.EventHandler(this.btnSimulation_Click);
            // 
            // cpxButton1
            // 
            this.cpxButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cpxButton1.ButtonAction = null;
            this.cpxButton1.FlatAppearance.BorderSize = 0;
            this.cpxButton1.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.cpxButton1, "cpxButton1");
            this.cpxButton1.Name = "cpxButton1";
            this.cpxButton1.UseVisualStyleBackColor = false;
            this.cpxButton1.Click += new System.EventHandler(this.cpxButton1_Click);
            // 
            // btnJoystick
            // 
            this.btnJoystick.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnJoystick.ButtonAction = null;
            this.btnJoystick.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.btnJoystick, "btnJoystick");
            this.btnJoystick.ForeColor = System.Drawing.Color.White;
            this.btnJoystick.Name = "btnJoystick";
            this.btnJoystick.UseVisualStyleBackColor = false;
            this.btnJoystick.Click += new System.EventHandler(this.cpxButton2_Click);
            // 
            // cpxButton3
            // 
            this.cpxButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cpxButton3.ButtonAction = null;
            this.cpxButton3.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.cpxButton3, "cpxButton3");
            this.cpxButton3.ForeColor = System.Drawing.Color.White;
            this.cpxButton3.Name = "cpxButton3";
            this.cpxButton3.UseVisualStyleBackColor = false;
            this.cpxButton3.Click += new System.EventHandler(this.cpxButton3_Click);
            // 
            // cpxButton4
            // 
            this.cpxButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cpxButton4.ButtonAction = "start";
            this.cpxButton4.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.cpxButton4, "cpxButton4");
            this.cpxButton4.ForeColor = System.Drawing.Color.White;
            this.cpxButton4.Name = "cpxButton4";
            this.cpxButton4.UseVisualStyleBackColor = false;
            this.cpxButton4.Click += new System.EventHandler(this.cpxButton4_Click);
            // 
            // cpxButton5
            // 
            this.cpxButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cpxButton5.ButtonAction = null;
            resources.ApplyResources(this.cpxButton5, "cpxButton5");
            this.cpxButton5.ForeColor = System.Drawing.Color.White;
            this.cpxButton5.Name = "cpxButton5";
            this.cpxButton5.UseVisualStyleBackColor = false;
            this.cpxButton5.Click += new System.EventHandler(this.cpxButton5_Click);
            // 
            // cpxButton6
            // 
            this.cpxButton6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cpxButton6.ButtonAction = null;
            this.cpxButton6.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.cpxButton6, "cpxButton6");
            this.cpxButton6.Name = "cpxButton6";
            this.cpxButton6.UseVisualStyleBackColor = false;
            this.cpxButton6.Click += new System.EventHandler(this.cpxButton6_Click);
            // 
            // btnLogExtractor
            // 
            this.btnLogExtractor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnLogExtractor.ButtonAction = null;
            this.btnLogExtractor.Cursor = System.Windows.Forms.Cursors.No;
            this.btnLogExtractor.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.btnLogExtractor, "btnLogExtractor");
            this.btnLogExtractor.Name = "btnLogExtractor";
            this.btnLogExtractor.UseVisualStyleBackColor = false;
            this.btnLogExtractor.Click += new System.EventHandler(this.BtnLogExtractor_Click);
            // 
            // btnSDRtoLTE
            // 
            this.btnSDRtoLTE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSDRtoLTE.ButtonAction = null;
            this.btnSDRtoLTE.FlatAppearance.BorderSize = 0;
            this.btnSDRtoLTE.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.btnSDRtoLTE, "btnSDRtoLTE");
            this.btnSDRtoLTE.Name = "btnSDRtoLTE";
            this.btnSDRtoLTE.UseVisualStyleBackColor = false;
            // 
            // frmActions
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSDRtoLTE);
            this.Controls.Add(this.btnLogExtractor);
            this.Controls.Add(this.cpxButton6);
            this.Controls.Add(this.cpxButton5);
            this.Controls.Add(this.cpxButton4);
            this.Controls.Add(this.cpxButton3);
            this.Controls.Add(this.btnJoystick);
            this.Controls.Add(this.cpxButton1);
            this.Controls.Add(this.btnSimulation);
            this.Name = "frmActions";
            this.Ttile = "Additional Actions";
            this.Load += new System.EventHandler(this.frmActions_Load);
            this.Controls.SetChildIndex(this.btnSimulation, 0);
            this.Controls.SetChildIndex(this.cpxButton1, 0);
            this.Controls.SetChildIndex(this.btnJoystick, 0);
            this.Controls.SetChildIndex(this.cpxButton3, 0);
            this.Controls.SetChildIndex(this.cpxButton4, 0);
            this.Controls.SetChildIndex(this.cpxButton5, 0);
            this.Controls.SetChildIndex(this.cpxButton6, 0);
            this.Controls.SetChildIndex(this.btnLogExtractor, 0);
            this.Controls.SetChildIndex(this.btnSDRtoLTE, 0);
            this.ResumeLayout(false);

        }
        #endregion

        private Controls.Common.CpxButton btnSimulation;
        private Controls.Common.CpxButton cpxButton1;
        private Controls.Common.CpxButton btnJoystick;
        private Controls.Common.CpxButton cpxButton3;
        private Controls.Common.CpxButton cpxButton4;
        private Controls.Common.CpxButton cpxButton5;
        private Controls.Common.CpxButton cpxButton6;
        private Controls.Common.CpxButton btnLogExtractor;
        public Controls.Common.CpxButton btnSDRtoLTE;
    }
}