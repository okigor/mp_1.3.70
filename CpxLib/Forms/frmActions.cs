﻿using CpxLib.Core;
using CpxLib.Forms.Test;
using MissionPlanner;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Forms
{
    public partial class frmActions : frmBase
    {
        public frmActions()
        {
            InitializeComponent();
        }

        private void btnSimulation_Click(object sender, EventArgs e)
        {
            UIUtils.ShowSimulation();
            this.Close();
        }

        private void cpxButton1_Click(object sender, EventArgs e)
        {
            //var frm = new frmGStreamerTest();
            //frm.ShowDialog();

            this.Close();

        }

        private void frmActions_Load(object sender, EventArgs e)
        {
           
        }

        private void cpxButton2_Click(object sender, EventArgs e)
        {
            CpxHost.Instance.UI.EnableJoystik();
        }

        private void cpxButton3_Click(object sender, EventArgs e)
        {
            UIUtils.InvokePrivateMethod(CpxHost.Instance.MainForm, "MenuConnect_Click", null, EventArgs.Empty);
        }

        private void cpxButton4_Click(object sender, EventArgs e)
        {
            CpxHost.Instance.UI.StartSchedule();
            if(cpxButton4.ButtonAction=="start")
            {
                cpxButton4.Text = "הפסק משימה מתוזמנת";
                cpxButton4.ButtonAction = "stop";
            }
            else
            {
                cpxButton4.Text = "התחל משימה מתוזמנת";
                cpxButton4.ButtonAction = "start";
            }
                
            this.Close();
        }

        private void cpxButton5_Click(object sender, EventArgs e)
        {
            CpxHost.Instance.ComPort.setParam((byte)MainV2.comPort.sysidcurrent, (byte)MainV2.comPort.compidcurrent, "MNT_RC_IN_TILT", 8);
            CpxHost.Instance.ComPort.setParam((byte)MainV2.comPort.sysidcurrent, (byte)MainV2.comPort.compidcurrent, "MNT_STAB_TILT", 0);
        }

        private void cpxButton6_Click(object sender, EventArgs e)
        {
            CpxHost.Instance.ComPort.setParam((byte)MainV2.comPort.sysidcurrent, (byte)MainV2.comPort.compidcurrent, "MNT_RC_IN_TILT", 6);
            CpxHost.Instance.ComPort.setParam((byte)MainV2.comPort.sysidcurrent, (byte)MainV2.comPort.compidcurrent, "MNT_STAB_TILT", 1);
        }

        private void BtnLogExtractor_Click(object sender, EventArgs e)
        {
            if (!MainV2.comPort.BaseStream.IsOpen)
            {
                CustomMessageBox.Show( "connect to drone first");
                return;
            }
            try
            {
                Process logEx = new Process();
                logEx.StartInfo.CreateNoWindow = true;
                logEx.StartInfo.WorkingDirectory = @"C:\Program Files (x86)\Mission Planner\plugins\LogExtractor";
                logEx.StartInfo.FileName = @"LogExtractor.exe";
                //logEx.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                logEx.Start();
                logEx.WaitForExit(9000);
            }
            catch
            {
                CpxHost.Instance.UI.ShowMessage(10, "download failed");
            }
            CpxHost.Instance.UI.ShowMessage(10, "download done");
        }

        private void cpxButton2_Click_1(object sender, EventArgs e)
        {
            bool result = MainV2.comPort.doCommand(1, 25, MAVLink.MAV_CMD.USER_1, 0, 0, 0, 0, 0, 0, 0);
            MessageBox.Show(result + "");
        }
    }
}
