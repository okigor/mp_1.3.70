﻿using CpxLib.Controls.Common;

namespace CpxLib.Forms
{
    partial class frmManagePlans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManagePlans));
            this.lstPoints = new System.Windows.Forms.ListView();
            this.colNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colLat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colLng = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnCancel = new CpxLib.Controls.Common.CpxButton();
            this.btnSave = new CpxLib.Controls.Common.CpxButton();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPathName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lstPoints
            // 
            resources.ApplyResources(this.lstPoints, "lstPoints");
            this.lstPoints.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNum,
            this.colLat,
            this.colLng});
            this.lstPoints.FullRowSelect = true;
            this.lstPoints.GridLines = true;
            this.lstPoints.HideSelection = false;
            this.lstPoints.Name = "lstPoints";
            this.lstPoints.UseCompatibleStateImageBehavior = false;
            this.lstPoints.View = System.Windows.Forms.View.Details;
            // 
            // colNum
            // 
            resources.ApplyResources(this.colNum, "colNum");
            // 
            // colLat
            // 
            resources.ApplyResources(this.colLat, "colLat");
            // 
            // colLng
            // 
            resources.ApplyResources(this.colLng, "colLng");
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCancel.ButtonAction = null;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSave.ButtonAction = null;
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // txtPathName
            // 
            resources.ApplyResources(this.txtPathName, "txtPathName");
            this.txtPathName.Name = "txtPathName";
            // 
            // frmManagePlans
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPathName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lstPoints);
            this.Name = "frmManagePlans";
            this.Ttile = "שמור תוכנית טיסה";
            this.Controls.SetChildIndex(this.lstPoints, 0);
            this.Controls.SetChildIndex(this.btnSave, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.txtPathName, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lstPoints;
        private System.Windows.Forms.ColumnHeader colNum;
        private System.Windows.Forms.ColumnHeader colLat;
        private System.Windows.Forms.ColumnHeader colLng;
        private CpxButton btnCancel;
        private CpxButton btnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPathName;
    }
}