﻿using CpxLib.Controls.Common;

namespace CpxLib.Forms
{
    partial class frmTakeoff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTakeoff));
            this.label2 = new System.Windows.Forms.Label();
            this.btnTakeoff = new CpxLib.Controls.Common.CpxButton();
            this.btnCancel = new CpxLib.Controls.Common.CpxButton();
            this.colorSlider1 = new CpxLib.Controls.Common.ColorSlider();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtAlt = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // btnTakeoff
            // 
            resources.ApplyResources(this.btnTakeoff, "btnTakeoff");
            this.btnTakeoff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnTakeoff.ButtonAction = null;
            this.btnTakeoff.FlatAppearance.BorderSize = 0;
            this.btnTakeoff.ForeColor = System.Drawing.Color.White;
            this.btnTakeoff.Name = "btnTakeoff";
            this.btnTakeoff.UseVisualStyleBackColor = false;
            this.btnTakeoff.Click += new System.EventHandler(this.btnTakeOff_Click);
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCancel.ButtonAction = null;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // colorSlider1
            // 
            resources.ApplyResources(this.colorSlider1, "colorSlider1");
            this.colorSlider1.BackColor = System.Drawing.Color.Transparent;
            this.colorSlider1.BarInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.colorSlider1.BarPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.colorSlider1.BarPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.colorSlider1.BorderRoundRectSize = new System.Drawing.Size(8, 8);
            this.colorSlider1.ElapsedInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.colorSlider1.ElapsedPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.colorSlider1.ElapsedPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.colorSlider1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.colorSlider1.LargeChange = ((uint)(5u));
            this.colorSlider1.Maximum = 500;
            this.colorSlider1.Name = "colorSlider1";
            this.colorSlider1.ScaleDivisions = 10;
            this.colorSlider1.ScaleSubDivisions = 5;
            this.colorSlider1.ShowDivisionsText = true;
            this.colorSlider1.ShowSmallScale = true;
            this.colorSlider1.SmallChange = ((uint)(1u));
            this.colorSlider1.ThumbImage = global::CpxLib.Properties.Resources.zoom_button;
            this.colorSlider1.ThumbInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.colorSlider1.ThumbPenColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.colorSlider1.ThumbRoundRectSize = new System.Drawing.Size(23, 23);
            this.colorSlider1.ThumbSize = new System.Drawing.Size(23, 23);
            this.colorSlider1.TickAdd = 0F;
            this.colorSlider1.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.colorSlider1.TickDivide = 0F;
            this.colorSlider1.ValueChanged += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Controls.Add(this.btnCancel);
            this.panel2.Controls.Add(this.colorSlider1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnTakeoff);
            this.panel2.Controls.Add(this.txtAlt);
            this.panel2.Name = "panel2";
            // 
            // txtAlt
            // 
            resources.ApplyResources(this.txtAlt, "txtAlt");
            this.txtAlt.Name = "txtAlt";
            this.txtAlt.TextChanged += new System.EventHandler(this.txtAlt_TextChanged);
            // 
            // frmTakeoff
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Name = "frmTakeoff";
            this.Ttile = "גובה להמראה";
            this.Controls.SetChildIndex(this.panel2, 0);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        public  CpxButton btnTakeoff;
        private  CpxButton btnCancel;
        public ColorSlider colorSlider1;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.TextBox txtAlt;
    }
}