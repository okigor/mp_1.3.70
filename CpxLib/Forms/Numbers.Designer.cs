﻿
namespace CpxLib.Forms
{
    partial class Numbers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.cpxButton13 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton12 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton11 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton10 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton9 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton8 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton7 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton6 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton5 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton4 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton3 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton2 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton1 = new CpxLib.Controls.Common.CpxButton();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cpxButton13);
            this.panel2.Controls.Add(this.cpxButton12);
            this.panel2.Controls.Add(this.cpxButton11);
            this.panel2.Controls.Add(this.cpxButton10);
            this.panel2.Controls.Add(this.cpxButton9);
            this.panel2.Controls.Add(this.cpxButton8);
            this.panel2.Controls.Add(this.cpxButton7);
            this.panel2.Controls.Add(this.cpxButton6);
            this.panel2.Controls.Add(this.cpxButton5);
            this.panel2.Controls.Add(this.cpxButton4);
            this.panel2.Controls.Add(this.cpxButton3);
            this.panel2.Controls.Add(this.cpxButton2);
            this.panel2.Controls.Add(this.cpxButton1);
            this.panel2.Location = new System.Drawing.Point(0, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(245, 402);
            this.panel2.TabIndex = 0;
            // 
            // cpxButton13
            // 
            this.cpxButton13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton13.ButtonAction = null;
            this.cpxButton13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton13.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton13.Location = new System.Drawing.Point(84, 327);
            this.cpxButton13.Name = "cpxButton13";
            this.cpxButton13.Size = new System.Drawing.Size(75, 75);
            this.cpxButton13.TabIndex = 12;
            this.cpxButton13.Text = "Enter";
            this.cpxButton13.UseVisualStyleBackColor = false;
            this.cpxButton13.Click += new System.EventHandler(this.cpxButton13_Click);
            // 
            // cpxButton12
            // 
            this.cpxButton12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton12.ButtonAction = null;
            this.cpxButton12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton12.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton12.Location = new System.Drawing.Point(165, 246);
            this.cpxButton12.Name = "cpxButton12";
            this.cpxButton12.Size = new System.Drawing.Size(75, 75);
            this.cpxButton12.TabIndex = 11;
            this.cpxButton12.Text = "Delete";
            this.cpxButton12.UseVisualStyleBackColor = false;
            // 
            // cpxButton11
            // 
            this.cpxButton11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton11.ButtonAction = null;
            this.cpxButton11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton11.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton11.Location = new System.Drawing.Point(84, 246);
            this.cpxButton11.Name = "cpxButton11";
            this.cpxButton11.Size = new System.Drawing.Size(75, 75);
            this.cpxButton11.TabIndex = 10;
            this.cpxButton11.Text = "0";
            this.cpxButton11.UseVisualStyleBackColor = false;
            // 
            // cpxButton10
            // 
            this.cpxButton10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton10.ButtonAction = null;
            this.cpxButton10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton10.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton10.Location = new System.Drawing.Point(3, 246);
            this.cpxButton10.Name = "cpxButton10";
            this.cpxButton10.Size = new System.Drawing.Size(75, 75);
            this.cpxButton10.TabIndex = 9;
            this.cpxButton10.Text = ".";
            this.cpxButton10.UseVisualStyleBackColor = false;
            // 
            // cpxButton9
            // 
            this.cpxButton9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton9.ButtonAction = null;
            this.cpxButton9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton9.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton9.Location = new System.Drawing.Point(165, 165);
            this.cpxButton9.Name = "cpxButton9";
            this.cpxButton9.Size = new System.Drawing.Size(75, 75);
            this.cpxButton9.TabIndex = 8;
            this.cpxButton9.Text = "9";
            this.cpxButton9.UseVisualStyleBackColor = false;
            // 
            // cpxButton8
            // 
            this.cpxButton8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton8.ButtonAction = null;
            this.cpxButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton8.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton8.Location = new System.Drawing.Point(84, 165);
            this.cpxButton8.Name = "cpxButton8";
            this.cpxButton8.Size = new System.Drawing.Size(75, 75);
            this.cpxButton8.TabIndex = 7;
            this.cpxButton8.Text = "8";
            this.cpxButton8.UseVisualStyleBackColor = false;
            // 
            // cpxButton7
            // 
            this.cpxButton7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton7.ButtonAction = null;
            this.cpxButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton7.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton7.Location = new System.Drawing.Point(3, 165);
            this.cpxButton7.Name = "cpxButton7";
            this.cpxButton7.Size = new System.Drawing.Size(75, 75);
            this.cpxButton7.TabIndex = 6;
            this.cpxButton7.Text = "7";
            this.cpxButton7.UseVisualStyleBackColor = false;
            // 
            // cpxButton6
            // 
            this.cpxButton6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton6.ButtonAction = null;
            this.cpxButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton6.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton6.Location = new System.Drawing.Point(165, 84);
            this.cpxButton6.Name = "cpxButton6";
            this.cpxButton6.Size = new System.Drawing.Size(75, 75);
            this.cpxButton6.TabIndex = 5;
            this.cpxButton6.Text = "6";
            this.cpxButton6.UseVisualStyleBackColor = false;
            // 
            // cpxButton5
            // 
            this.cpxButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton5.ButtonAction = null;
            this.cpxButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton5.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton5.Location = new System.Drawing.Point(84, 84);
            this.cpxButton5.Name = "cpxButton5";
            this.cpxButton5.Size = new System.Drawing.Size(75, 75);
            this.cpxButton5.TabIndex = 4;
            this.cpxButton5.Text = "5";
            this.cpxButton5.UseVisualStyleBackColor = false;
            // 
            // cpxButton4
            // 
            this.cpxButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton4.ButtonAction = null;
            this.cpxButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton4.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton4.Location = new System.Drawing.Point(3, 84);
            this.cpxButton4.Name = "cpxButton4";
            this.cpxButton4.Size = new System.Drawing.Size(75, 75);
            this.cpxButton4.TabIndex = 3;
            this.cpxButton4.Text = "4";
            this.cpxButton4.UseVisualStyleBackColor = false;
            // 
            // cpxButton3
            // 
            this.cpxButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton3.ButtonAction = null;
            this.cpxButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton3.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton3.Location = new System.Drawing.Point(165, 3);
            this.cpxButton3.Name = "cpxButton3";
            this.cpxButton3.Size = new System.Drawing.Size(75, 75);
            this.cpxButton3.TabIndex = 2;
            this.cpxButton3.Text = "3";
            this.cpxButton3.UseVisualStyleBackColor = false;
            // 
            // cpxButton2
            // 
            this.cpxButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton2.ButtonAction = null;
            this.cpxButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton2.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton2.Location = new System.Drawing.Point(84, 3);
            this.cpxButton2.Name = "cpxButton2";
            this.cpxButton2.Size = new System.Drawing.Size(75, 75);
            this.cpxButton2.TabIndex = 1;
            this.cpxButton2.Text = "2";
            this.cpxButton2.UseVisualStyleBackColor = false;
            // 
            // cpxButton1
            // 
            this.cpxButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.cpxButton1.ButtonAction = null;
            this.cpxButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpxButton1.Font = new System.Drawing.Font("Rubik", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpxButton1.Location = new System.Drawing.Point(3, 3);
            this.cpxButton1.Name = "cpxButton1";
            this.cpxButton1.Size = new System.Drawing.Size(75, 75);
            this.cpxButton1.TabIndex = 0;
            this.cpxButton1.Text = "1";
            this.cpxButton1.UseVisualStyleBackColor = false;
            this.cpxButton1.Click += new System.EventHandler(this.cpxButton1_Click);
            // 
            // Numbers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.ClientSize = new System.Drawing.Size(245, 438);
            this.Controls.Add(this.panel2);
            this.Name = "Numbers";
            this.Text = "Numbers";
            this.Controls.SetChildIndex(this.panel2, 0);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        public Controls.Common.CpxButton cpxButton1;
        public Controls.Common.CpxButton cpxButton13;
        public Controls.Common.CpxButton cpxButton12;
        public Controls.Common.CpxButton cpxButton11;
        public Controls.Common.CpxButton cpxButton10;
        public Controls.Common.CpxButton cpxButton9;
        public Controls.Common.CpxButton cpxButton8;
        public Controls.Common.CpxButton cpxButton7;
        public Controls.Common.CpxButton cpxButton6;
        public Controls.Common.CpxButton cpxButton5;
        public Controls.Common.CpxButton cpxButton4;
        public Controls.Common.CpxButton cpxButton3;
        public Controls.Common.CpxButton cpxButton2;
    }
}