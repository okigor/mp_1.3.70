﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Forms
{
    public partial class frmConnectingLTE : frmBase
    {
        private int counter=30;
        string LogFile;
        public frmConnectingLTE(string file)
        {
            LogFile = file;
            InitializeComponent();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 1000;
            timer1.Start();
            label3.Text = "Timout in " + counter.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Stream stream = File.Open(LogFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            //File.OpenRead(seclogPath1);
            StreamReader streamReader = new StreamReader(stream);
            string log = streamReader.ReadToEnd();
            streamReader.Close();
            counter--;
            progressBar1.Increment(100/30);
            if (counter == 0 || log.Contains("CONNECTED,SUCCESS"))
            {
                timer1.Stop();
                if (counter == 0)
                    this.DialogResult = DialogResult.Cancel;
                else
                    this.DialogResult = DialogResult.OK;
                this.Close();
            }
                
            label3.Text = "Timout in "+counter.ToString();

        }
    }
}
