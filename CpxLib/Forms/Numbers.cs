﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Forms
{
    public partial class Numbers : frmBase
    {
        //protected override bool ShowWithoutActivation
        //{
        //    get { return true; }
        //}
        const int WS_EX_NOACTIVATE = 0x8000000;
        public Numbers()
        {
            InitializeComponent();
        }

        private void cpxButton1_Click(object sender, EventArgs e)
        {

        }

        private void cpxButton13_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle = WS_EX_NOACTIVATE;
                return cp;
            }
        }
    }
}
