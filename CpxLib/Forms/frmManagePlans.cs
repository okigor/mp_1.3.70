﻿
using CpxLib.Core;
using CpxLib.Model;
using CpxLib.Properties;
using MissionPlanner.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Forms
{
    public partial class frmManagePlans : frmBase
    {
        public string language = Properties.Settings.Default.Language;
        private List<MavCommand> _pointList;
        private bool _exisingFile;

        public frmManagePlans()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            InitializeComponent();
        }
        public frmManagePlans(List<MavCommand> pointlist, string name/*, bool exisingFile*/)
        {
            InitializeComponent();
            _pointList = pointlist;
            //_exisingFile = exisingFile;
            var i = 1;
            foreach (var item in _pointList)
            {

                var listItem = lstPoints.Items.Add((i++).ToString());
                listItem.SubItems.Add(item.Lat.ToString());
                listItem.SubItems.Add(item.Lng.ToString());
            }


            txtPathName.Text = name;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            bool isExist = false;
            string existingFile = "";
            foreach (var flightRoute in ModelExtensions.GetFlightRoutes())
            {
                if (flightRoute.Name == txtPathName.Text)
                {
                    isExist = true;
                    existingFile = flightRoute.Id;
                    break;
                }
            }
            if (isExist)
            {
                if (MessageBox.Show(Resources.Ovewrite, "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
                {
                    return;
                }
                var filePath = AppSettings.CopterPixDataFolder + System.IO.Path.DirectorySeparatorChar + existingFile + ".wps";
                System.IO.File.Delete(filePath);
            }
            DialogResult = DialogResult.OK;
            Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        public string PathName
        {
            get
            {
                return txtPathName.Text;
            }
        }

    }
}
