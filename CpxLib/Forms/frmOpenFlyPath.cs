﻿
using CpxLib.Controls.Common;
using CpxLib.Model;
using CpxLib.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace CpxLib.Forms
{
    public partial class frmOpenFlyPath : frmBase
    {
        public FlightRoute SelectedFile { get; private set; }
        public string language = Properties.Settings.Default.Language;
        public frmOpenFlyPath()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            InitializeComponent();

            FillList();
        }

        private void FillList()
        {
            foreach (var flightRoute in ModelExtensions.GetFlightRoutes())
            {
                AddItemToList(flightRoute);

            }

        }
        private void AddItemToList(FlightRoute flightRoute)
        {
            var btn = new CpxButton
            {
                BackColor = System.Drawing.Color.FromArgb(64, 64, 64),
                FlatStyle = System.Windows.Forms.FlatStyle.Flat,
                ForeColor = System.Drawing.Color.White,
                Dock = DockStyle.Top,
                Margin = new System.Windows.Forms.Padding(3, 4, 3, 4),
                Size = new System.Drawing.Size(172, 56),
                Text = flightRoute.Name,
                UseVisualStyleBackColor = false,
                Tag = flightRoute,
                Font = new Font(this.Font, FontStyle.Bold),
                Image = Resources.waypoint_map_light_26,
                TextImageRelation = TextImageRelation.ImageBeforeText,
                ImageAlign = ContentAlignment.MiddleLeft
            };

            btn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            btn.Click += new System.EventHandler(this.btnOpen_Click);
            pnlPlans.Controls.Add(btn);

        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            var btn = sender as CpxButton;
            SelectedFile = btn.Tag as FlightRoute;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
