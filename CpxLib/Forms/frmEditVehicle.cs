﻿
using CpxLib.Core;
using CpxLib.Model;
using CpxLib.Properties;
using NativeWifi;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CpxLib.Forms
{
    public partial class frmEditVehicle : frmBase
    {
        private Vehicle _vehicle;
        public string language = Properties.Settings.Default.Language;

        public frmEditVehicle()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            InitializeComponent();
            radEthernet.Checked = true;
        }
        public frmEditVehicle(Vehicle vehicle)
        {
            InitializeComponent();

            //lblMavID.Left = gbDroneConnection.Width - 5 - lblMavID.Width;
            _vehicle = vehicle;
            //lblMavID.Left = gbDroneConnection.Width - 5 - lblMavID.Width;
            //lblMavPort.Left = gbDroneConnection.Width - 5 - lblMavPort.Width;
            //lblCommunication.Left = gbDroneConnection.Width - 5 - lblCommunication.Width;
            //lblDayCamPipeline.Left = gbCameras.Width - 5 - lblDayCamPipeline.Width;
            //lblNightCamPipeline.Left = gbCameras.Width - 5 - lblNightCamPipeline.Width;
            //lblMaxBattery.Left = gbBattetry.Width - 5 - lblMaxBattery.Width;
            //lblLSHost.Left = gbLandingStation.Width - 5 - lblLSHost.Width;
            FillFields();
        }
        private List<string> GetAvailableVPN()
        {
            string folderpath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            return Directory.GetDirectories(folderpath + @"\OpenVPN\config").ToList();
        }
        private List<string> GetAvailableWifi()
        {
            var networkList = new List<string>();

            var client = new WlanClient();
            foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
            {

                // Lists all networks with WEP security
                Wlan.WlanAvailableNetwork[] networks = wlanIface.GetAvailableNetworkList(Wlan.WlanGetAvailableNetworkFlags.IncludeAllAdhocProfiles);
                foreach (Wlan.WlanAvailableNetwork network in networks)
                {

                    Wlan.Dot11Ssid ssid = network.dot11Ssid;
                    string networkname = Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
                    if (networkname != "")
                    {
                        networkList.Add(networkname.ToString());
                    }

                    if (networkname == "MADAN_2.5")
                    {
                        wlanIface.Connect(Wlan.WlanConnectionMode.Auto, Wlan.Dot11BssType.Any, "MADAN_2.5");
                    }

                }
            }

            return networkList;
        }
        private void FillFields()
        {
            txtMavId.Text = _vehicle.MavId;
            txtVehicleName.Text = _vehicle.Name;
            txtMavPort.Text = _vehicle.MavPort;

            txtMavIP.Text = _vehicle.MavIP;
            txtBoxMaxBat.Text = _vehicle.MaximumBattery;
            txtBoxMinBat.Text = _vehicle.MinimumBattery;
            txtBoxTIN.Text = _vehicle.TimeInAir;
            txtCamDev.Text = _vehicle.CameraDeviation;
            if (_vehicle.NextVisionIP == "")
            {
                txtTripIP.Text = "192.168.0.201";
            }
            else
            {
                txtTripIP.Text = _vehicle.NextVisionIP;
            }

            if (_vehicle.MobilicomIP == "")
            {
                txtMobilicomIP.Text = "192.168.0.241";
            }
            else
            {
                txtMobilicomIP.Text = _vehicle.MobilicomIP;
            }
            if (_vehicle.MobilicomAirIP == "")
            {
                txtMobDroneIP.Text = "192.168.0.242";
            }
            else
            {
                txtMobDroneIP.Text = _vehicle.MobilicomAirIP;
            }
            if (_vehicle.Protocol == "UDP")
            {
                radUDP.Checked = true;
            }
            else if (_vehicle.Protocol == "UDPCl")
            {
                radUDPCl.Checked = true;
            }

            else
            {
                radTCP.Checked = true;
            }



            if (_vehicle.CommunicationType == "LTE")
            {
                radLTE.Checked = true;
            }
            else if (_vehicle.CommunicationType == "WIFI")
            {
                radWIFI.Checked = true;
            }
            else if (_vehicle.CommunicationType == "ETHERNET")
            {
                radEthernet.Checked = true;
            }
            else
            {
                radMobilicom.Checked = true;
            }

            if (_vehicle.CommunicationType != "ETHERNET" && _vehicle.CommunicationType != "Mobilicom")
            {
                cmbWifi.Enabled = true;
                cmbWifi.SelectedIndex = 0;

                for (int i = 0; i < cmbWifi.Items.Count; i++)
                {
                    if (cmbWifi.Items[i].ToString() == _vehicle.WirelessName)
                    {
                        cmbWifi.SelectedIndex = i;
                        break;
                    }
                }
            }
            else
            {
                cmbWifi.Enabled = false;
            }
            if (_vehicle.CameraType == "nextvision")
            {
                radNextvision.Checked = true;
            }
            else
            {
                radCopterpix.Checked = true;
            }


            txtNightCameraPipeline.Text = _vehicle.NightCameraPipline;
            txtDayCameraPipeline.Text = _vehicle.DayCameraPipline;
            txtLandingIP.Text = _vehicle.LandingStationIp;
            txtLandingPort.Text = _vehicle.LandingStationPort;
        }

        private void ReadData()
        {
            if (_vehicle == null)
            {
                _vehicle = new Vehicle();
            }

            _vehicle.MavId = txtMavId.Text;
            _vehicle.Name = txtVehicleName.Text;
            _vehicle.MavPort = txtMavPort.Text;
            
            _vehicle.MaximumBattery = txtBoxMaxBat.Text;
            _vehicle.MinimumBattery = txtBoxMinBat.Text;
            _vehicle.TimeInAir = txtBoxTIN.Text;

            if (radUDPCl.Checked)
            {
                _vehicle.Protocol = "UDPCl";
            }
            else
            {
                _vehicle.Protocol = radUDP.Checked ? "UDP" : "TCP";
            }
            _vehicle.MavIP = txtMavIP.Text;

            _vehicle.NightCameraPipline = txtNightCameraPipeline.Text;
            _vehicle.DayCameraPipline = txtDayCameraPipeline.Text;
            _vehicle.LandingStationIp = txtLandingIP.Text;
            _vehicle.LandingStationPort = txtLandingPort.Text;
            _vehicle.NextVisionIP = txtTripIP.Text;
            _vehicle.MobilicomIP = txtMobilicomIP.Text;
            _vehicle.MobilicomAirIP = txtMobDroneIP.Text;
            if (radLTE.Checked)
            {
                _vehicle.CommunicationType = "LTE";
                _vehicle.WirelessName = "";
                _vehicle.WirelessName = cmbWifi.SelectedItem.ToString();
            }
            else if (radEthernet.Checked)
            {
                _vehicle.CommunicationType = "ETHERNET";
            }
            else if (radWIFI.Checked)
            {
                _vehicle.CommunicationType = "WIFI";

                if (cmbWifi.SelectedItem != null)
                {
                    _vehicle.WirelessName = cmbWifi.SelectedItem.ToString();
                }
            }
            else if (radMobilicom.Checked)
            {
                _vehicle.CommunicationType = "Mobilicom";
            }
            else
            {
                _vehicle.CommunicationType = "";
                _vehicle.WirelessName = "";
            }
            if (radNextvision.Checked)
                _vehicle.CameraType = "nextvision";
            else
                _vehicle.CameraType = "copterpix";

            _vehicle.CameraDeviation = txtCamDev.Text;
        }

        public Vehicle Vehicle { get { return _vehicle; } }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            ReadData();
            _vehicle.Save();
            DialogResult = DialogResult.OK;
            Close();
        }
        private void radWIFI_CheckedChanged(object sender, EventArgs e)
        {
            cmbWifi.Enabled = true;
            cmbWifi.Items.Clear();
            cmbWifi.Items.Add(Resources.SelectValue);
            var availableNetworks = WifiUtilities.GetAvailbleNetworks();

            if (availableNetworks != null)
            {
                foreach (var item in availableNetworks)
                {
                    cmbWifi.Items.Add(item.ProfileName);
                }

            }
            cmbWifi.SelectedIndex = 0;
        }

        private void radLTE_CheckedChanged(object sender, EventArgs e)
        {
            cmbWifi.Enabled = true;
            cmbWifi.Items.Clear();
            cmbWifi.Items.Add(Resources.SelectValue);
            List<string> allVPNs = GetAvailableVPN();
            if (allVPNs.Count > 0)
            {
                foreach (string item in allVPNs)
                {
                    int pos = item.LastIndexOf("\\") + 1;
                    cmbWifi.Items.Add(item.Substring(pos, item.Length - pos));
                }
            }
            cmbWifi.SelectedIndex = 0;
        }

        private void radEthernet_CheckedChanged(object sender, EventArgs e)
        {
            cmbWifi.Enabled = false;
        }
        private void radNextvision_CheckedChanged(object sender, EventArgs e)
        {
            if (radNextvision.Checked)
            {
                txtDayCameraPipeline.Visible = false;
                txtNightCameraPipeline.Visible = false;
                lblDayCamPipeline.Visible = false;
                lblNightCamPipeline.Visible = false;

                lblTripIP.Visible = true;
                txtTripIP.Visible = true;
                btnNextVisionSettings.Visible = true;
            }

        }

        private void btnNextVisionSettings_Click(object sender, EventArgs e)
        {
            try
            {
                Ping ping = new Ping();
                PingReply pingReply = ping.Send(txtTripIP.Text);
                if (pingReply.Status == IPStatus.Success)
                {
                    frmWebPage frmWebPage = new frmWebPage();
                    frmWebPage.Name = "Nextvison";
                    frmWebPage.webBrowser1.Url = new Uri(@"http://" + txtTripIP.Text);
                    //System.Diagnostics.Process.Start(@"http://" + txtMobilicomIP.Text);
                    frmWebPage.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Something went wrong :(\nPlease check your connection or the host of Mobilicom");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("EXCEPTION: Something went wrong :(\nPlease check your connection or the host of Mobilicom");
            }
        }

        private void radMobilicom_CheckedChanged(object sender, EventArgs e)
        {
            if (radMobilicom.Checked)
            {
                lblMobilicomIP.Visible = true;
                txtMobilicomIP.Visible = true;
                btnMobilicomGUI.Visible = true;
                lblMobDroneIP.Visible = true;
                txtMobDroneIP.Visible = true;
                btnMobDroneGUI.Visible = true;
                cmbWifi.Visible = false;
            }
            else
            {
                lblMobilicomIP.Visible = false;
                txtMobilicomIP.Visible = false;
                btnMobilicomGUI.Visible = false;
                lblMobDroneIP.Visible = false;
                txtMobDroneIP.Visible = false;
                btnMobDroneGUI.Visible = false;
                cmbWifi.Visible = true;
                txtMobilicomIP.Text = "";
            }
        }

        private void radCopterpix_CheckedChanged(object sender, EventArgs e)
        {
            if (radCopterpix.Checked)
            {
                txtDayCameraPipeline.Visible = true;
                txtNightCameraPipeline.Visible = true;
                lblDayCamPipeline.Visible = true;
                lblNightCamPipeline.Visible = true;

                lblTripIP.Visible = false;
                txtTripIP.Visible = false;
                btnNextVisionSettings.Visible = false;
            }
            else
            {
                txtDayCameraPipeline.Visible = false;
                txtNightCameraPipeline.Visible = false;
                lblDayCamPipeline.Visible = false;
                lblNightCamPipeline.Visible = false;

                lblTripIP.Visible = true;
                txtTripIP.Visible = true;
                btnNextVisionSettings.Visible = true;
            }
        }
        private void btnMobilicomGUI_Click(object sender, EventArgs e)
        {
            try
            {
                Ping ping = new Ping();
                PingReply pingReply = ping.Send(txtMobilicomIP.Text);
                if (pingReply.Status == IPStatus.Success)
                {
                    frmWebPage frmWebPage = new frmWebPage();
                    frmWebPage.Name = "Mobilicom Ground";
                    frmWebPage.webBrowser1.Url = new Uri(@"http://" + txtMobilicomIP.Text);
                    //System.Diagnostics.Process.Start(@"http://" + txtMobilicomIP.Text);
                    frmWebPage.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Something went wrong :(\nPlease check your connection or the host of Mobilicom");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("EXCEPTION: Something went wrong :(\nPlease check your connection or the host of Mobilicom");
            }
        }

        private void btnMobDroneGUI_Click(object sender, EventArgs e)
        {
            try
            {
                Ping ping = new Ping();
                PingReply pingReply = ping.Send(txtMobDroneIP.Text);
                if (pingReply.Status == IPStatus.Success)
                {
                    frmWebPage frmWebPage = new frmWebPage();
                    frmWebPage.Name = "Mobilicom Drone";
                    frmWebPage.webBrowser1.Url = new Uri(@"http://" + txtMobDroneIP.Text);
                    //System.Diagnostics.Process.Start(@"http://" + txtMobilicomIP.Text);
                    frmWebPage.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Something went wrong :(\nPlease check your connection or the host of Mobilicom");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("EXCEPTION: Something went wrong :(\nPlease check your connection or the host of Mobilicom");
            }
        }
    }
}
