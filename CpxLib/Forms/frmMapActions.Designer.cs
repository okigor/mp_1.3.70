﻿using System.Drawing;

namespace CpxLib.Forms
{
    partial class frmMapActions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMapActions));
            this.pnlVehicles = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cpxButton1 = new CpxLib.Controls.Common.CpxButton();
            this.btnFlightToHere = new CpxLib.Controls.Common.CpxButton();
            this.btnNewPointCamera = new CpxLib.Controls.Common.CpxButton();
            this.pnlVehicles.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlVehicles
            // 
            resources.ApplyResources(this.pnlVehicles, "pnlVehicles");
            this.pnlVehicles.Controls.Add(this.tableLayoutPanel1);
            this.pnlVehicles.Name = "pnlVehicles";
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.cpxButton1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnFlightToHere, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNewPointCamera, 1, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // cpxButton1
            // 
            resources.ApplyResources(this.cpxButton1, "cpxButton1");
            this.cpxButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cpxButton1.ButtonAction = null;
            this.cpxButton1.ForeColor = System.Drawing.Color.White;
            this.cpxButton1.Name = "cpxButton1";
            this.cpxButton1.UseVisualStyleBackColor = false;
            this.cpxButton1.Click += new System.EventHandler(this.cpxButton1_Click);
            // 
            // btnFlightToHere
            // 
            resources.ApplyResources(this.btnFlightToHere, "btnFlightToHere");
            this.btnFlightToHere.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnFlightToHere.ButtonAction = null;
            this.btnFlightToHere.ForeColor = System.Drawing.Color.White;
            this.btnFlightToHere.Name = "btnFlightToHere";
            this.btnFlightToHere.UseVisualStyleBackColor = false;
            this.btnFlightToHere.Click += new System.EventHandler(this.btnFlightToHere_Click);
            // 
            // btnNewPointCamera
            // 
            resources.ApplyResources(this.btnNewPointCamera, "btnNewPointCamera");
            this.btnNewPointCamera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnNewPointCamera.ButtonAction = null;
            this.btnNewPointCamera.ForeColor = System.Drawing.Color.White;
            this.btnNewPointCamera.Name = "btnNewPointCamera";
            this.btnNewPointCamera.UseVisualStyleBackColor = false;
            this.btnNewPointCamera.Click += new System.EventHandler(this.btnNewPointCamera_Click);
            // 
            // frmMapActions
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.Controls.Add(this.pnlVehicles);
            this.Name = "frmMapActions";
            this.Ttile = "בחר/י פעולה";
            this.Controls.SetChildIndex(this.pnlVehicles, 0);
            this.pnlVehicles.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlVehicles;
        private Controls.Common.CpxButton btnFlightToHere;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Controls.Common.CpxButton cpxButton1;
        private Controls.Common.CpxButton btnNewPointCamera;
    }
}