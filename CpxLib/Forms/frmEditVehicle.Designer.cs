﻿using CpxLib.Controls.Common;

namespace CpxLib.Forms
{
    partial class frmEditVehicle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditVehicle));
            this.lblName = new System.Windows.Forms.Label();
            this.lblMavPort = new System.Windows.Forms.Label();
            this.lblMavHost = new System.Windows.Forms.Label();
            this.lblDayCamPipeline = new System.Windows.Forms.Label();
            this.lblNightCamPipeline = new System.Windows.Forms.Label();
            this.lblLSPort = new System.Windows.Forms.Label();
            this.lblLSHost = new System.Windows.Forms.Label();
            this.radTCP = new System.Windows.Forms.RadioButton();
            this.radUDP = new System.Windows.Forms.RadioButton();
            this.lblProtocol = new System.Windows.Forms.Label();
            this.radUDPCl = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radMobilicom = new System.Windows.Forms.RadioButton();
            this.radEthernet = new System.Windows.Forms.RadioButton();
            this.radLTE = new System.Windows.Forms.RadioButton();
            this.radWIFI = new System.Windows.Forms.RadioButton();
            this.lblCommunication = new System.Windows.Forms.Label();
            this.cmbWifi = new System.Windows.Forms.ComboBox();
            this.lblMaxBattery = new System.Windows.Forms.Label();
            this.lblMinBaterry = new System.Windows.Forms.Label();
            this.lblTimeInAir = new System.Windows.Forms.Label();
            this.lblMavID = new System.Windows.Forms.Label();
            this.txtLandingIP = new CpxLib.Controls.Common.CpxTextbox();
            this.btnCancel = new CpxLib.Controls.Common.CpxButton();
            this.btnSave = new CpxLib.Controls.Common.CpxButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.gbLandingStation = new System.Windows.Forms.GroupBox();
            this.txtLandingPort = new CpxLib.Controls.Common.CpxTextbox();
            this.gbBattetry = new System.Windows.Forms.GroupBox();
            this.txtBoxTIN = new CpxLib.Controls.Common.CpxTextbox();
            this.txtBoxMinBat = new CpxLib.Controls.Common.CpxTextbox();
            this.txtBoxMaxBat = new CpxLib.Controls.Common.CpxTextbox();
            this.gbCameras = new System.Windows.Forms.GroupBox();
            this.txtCamDev = new CpxLib.Controls.Common.CpxTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNightCameraPipeline = new CpxLib.Controls.Common.CpxTextbox();
            this.txtDayCameraPipeline = new CpxLib.Controls.Common.CpxTextbox();
            this.txtTripIP = new CpxLib.Controls.Common.CpxTextbox();
            this.lblTripIP = new CpxLib.Controls.Common.CpxLabel();
            this.btnNextVisionSettings = new CpxLib.Controls.Common.CpxButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.radNextvision = new System.Windows.Forms.RadioButton();
            this.radCopterpix = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.gbDroneConnection = new System.Windows.Forms.GroupBox();
            this.txtVehicleName = new CpxLib.Controls.Common.CpxTextbox();
            this.txtMavPort = new CpxLib.Controls.Common.CpxTextbox();
            this.btnMobDroneGUI = new CpxLib.Controls.Common.CpxButton();
            this.txtMobDroneIP = new CpxLib.Controls.Common.CpxTextbox();
            this.lblMobDroneIP = new CpxLib.Controls.Common.CpxLabel();
            this.txtMavId = new CpxLib.Controls.Common.CpxTextbox();
            this.lblMobilicomIP = new CpxLib.Controls.Common.CpxLabel();
            this.txtMobilicomIP = new CpxLib.Controls.Common.CpxTextbox();
            this.btnMobilicomGUI = new CpxLib.Controls.Common.CpxButton();
            this.txtMavIP = new CpxLib.Controls.Common.CpxTextbox();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.gbLandingStation.SuspendLayout();
            this.gbBattetry.SuspendLayout();
            this.gbCameras.SuspendLayout();
            this.panel6.SuspendLayout();
            this.gbDroneConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblName
            // 
            resources.ApplyResources(this.lblName, "lblName");
            this.lblName.Name = "lblName";
            // 
            // lblMavPort
            // 
            resources.ApplyResources(this.lblMavPort, "lblMavPort");
            this.lblMavPort.Name = "lblMavPort";
            // 
            // lblMavHost
            // 
            resources.ApplyResources(this.lblMavHost, "lblMavHost");
            this.lblMavHost.Name = "lblMavHost";
            // 
            // lblDayCamPipeline
            // 
            resources.ApplyResources(this.lblDayCamPipeline, "lblDayCamPipeline");
            this.lblDayCamPipeline.Name = "lblDayCamPipeline";
            // 
            // lblNightCamPipeline
            // 
            resources.ApplyResources(this.lblNightCamPipeline, "lblNightCamPipeline");
            this.lblNightCamPipeline.Name = "lblNightCamPipeline";
            // 
            // lblLSPort
            // 
            resources.ApplyResources(this.lblLSPort, "lblLSPort");
            this.lblLSPort.Name = "lblLSPort";
            // 
            // lblLSHost
            // 
            resources.ApplyResources(this.lblLSHost, "lblLSHost");
            this.lblLSHost.Name = "lblLSHost";
            // 
            // radTCP
            // 
            resources.ApplyResources(this.radTCP, "radTCP");
            this.radTCP.Name = "radTCP";
            this.radTCP.TabStop = true;
            this.radTCP.UseVisualStyleBackColor = true;
            // 
            // radUDP
            // 
            resources.ApplyResources(this.radUDP, "radUDP");
            this.radUDP.Name = "radUDP";
            this.radUDP.TabStop = true;
            this.radUDP.UseVisualStyleBackColor = true;
            // 
            // lblProtocol
            // 
            resources.ApplyResources(this.lblProtocol, "lblProtocol");
            this.lblProtocol.Name = "lblProtocol";
            // 
            // radUDPCl
            // 
            resources.ApplyResources(this.radUDPCl, "radUDPCl");
            this.radUDPCl.Name = "radUDPCl";
            this.radUDPCl.TabStop = true;
            this.radUDPCl.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radUDPCl);
            this.panel2.Controls.Add(this.radUDP);
            this.panel2.Controls.Add(this.radTCP);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radMobilicom);
            this.panel3.Controls.Add(this.radEthernet);
            this.panel3.Controls.Add(this.radLTE);
            this.panel3.Controls.Add(this.radWIFI);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // radMobilicom
            // 
            resources.ApplyResources(this.radMobilicom, "radMobilicom");
            this.radMobilicom.Name = "radMobilicom";
            this.radMobilicom.TabStop = true;
            this.radMobilicom.UseVisualStyleBackColor = true;
            this.radMobilicom.CheckedChanged += new System.EventHandler(this.radMobilicom_CheckedChanged);
            // 
            // radEthernet
            // 
            resources.ApplyResources(this.radEthernet, "radEthernet");
            this.radEthernet.Checked = true;
            this.radEthernet.Name = "radEthernet";
            this.radEthernet.TabStop = true;
            this.radEthernet.UseVisualStyleBackColor = true;
            this.radEthernet.CheckedChanged += new System.EventHandler(this.radEthernet_CheckedChanged);
            // 
            // radLTE
            // 
            resources.ApplyResources(this.radLTE, "radLTE");
            this.radLTE.Name = "radLTE";
            this.radLTE.UseVisualStyleBackColor = true;
            this.radLTE.CheckedChanged += new System.EventHandler(this.radLTE_CheckedChanged);
            // 
            // radWIFI
            // 
            resources.ApplyResources(this.radWIFI, "radWIFI");
            this.radWIFI.Name = "radWIFI";
            this.radWIFI.UseVisualStyleBackColor = true;
            this.radWIFI.CheckedChanged += new System.EventHandler(this.radWIFI_CheckedChanged);
            // 
            // lblCommunication
            // 
            resources.ApplyResources(this.lblCommunication, "lblCommunication");
            this.lblCommunication.Name = "lblCommunication";
            // 
            // cmbWifi
            // 
            this.cmbWifi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cmbWifi, "cmbWifi");
            this.cmbWifi.FormattingEnabled = true;
            this.cmbWifi.Name = "cmbWifi";
            // 
            // lblMaxBattery
            // 
            resources.ApplyResources(this.lblMaxBattery, "lblMaxBattery");
            this.lblMaxBattery.Name = "lblMaxBattery";
            // 
            // lblMinBaterry
            // 
            resources.ApplyResources(this.lblMinBaterry, "lblMinBaterry");
            this.lblMinBaterry.Name = "lblMinBaterry";
            // 
            // lblTimeInAir
            // 
            resources.ApplyResources(this.lblTimeInAir, "lblTimeInAir");
            this.lblTimeInAir.Name = "lblTimeInAir";
            // 
            // lblMavID
            // 
            resources.ApplyResources(this.lblMavID, "lblMavID");
            this.lblMavID.Name = "lblMavID";
            // 
            // txtLandingIP
            // 
            this.txtLandingIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtLandingIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLandingIP.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtLandingIP, "txtLandingIP");
            this.txtLandingIP.Name = "txtLandingIP";
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCancel.ButtonAction = null;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSave.ButtonAction = null;
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnSave);
            this.panel4.Controls.Add(this.btnCancel);
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.gbLandingStation);
            this.panel5.Controls.Add(this.gbBattetry);
            this.panel5.Controls.Add(this.gbCameras);
            this.panel5.Controls.Add(this.gbDroneConnection);
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            // 
            // gbLandingStation
            // 
            this.gbLandingStation.Controls.Add(this.txtLandingPort);
            this.gbLandingStation.Controls.Add(this.lblLSHost);
            this.gbLandingStation.Controls.Add(this.txtLandingIP);
            this.gbLandingStation.Controls.Add(this.lblLSPort);
            resources.ApplyResources(this.gbLandingStation, "gbLandingStation");
            this.gbLandingStation.Name = "gbLandingStation";
            this.gbLandingStation.TabStop = false;
            // 
            // txtLandingPort
            // 
            this.txtLandingPort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtLandingPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLandingPort.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtLandingPort, "txtLandingPort");
            this.txtLandingPort.Name = "txtLandingPort";
            // 
            // gbBattetry
            // 
            this.gbBattetry.Controls.Add(this.txtBoxTIN);
            this.gbBattetry.Controls.Add(this.txtBoxMinBat);
            this.gbBattetry.Controls.Add(this.txtBoxMaxBat);
            this.gbBattetry.Controls.Add(this.lblMaxBattery);
            this.gbBattetry.Controls.Add(this.lblMinBaterry);
            this.gbBattetry.Controls.Add(this.lblTimeInAir);
            resources.ApplyResources(this.gbBattetry, "gbBattetry");
            this.gbBattetry.Name = "gbBattetry";
            this.gbBattetry.TabStop = false;
            // 
            // txtBoxTIN
            // 
            this.txtBoxTIN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtBoxTIN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBoxTIN.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtBoxTIN, "txtBoxTIN");
            this.txtBoxTIN.Name = "txtBoxTIN";
            // 
            // txtBoxMinBat
            // 
            this.txtBoxMinBat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtBoxMinBat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBoxMinBat.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtBoxMinBat, "txtBoxMinBat");
            this.txtBoxMinBat.Name = "txtBoxMinBat";
            // 
            // txtBoxMaxBat
            // 
            this.txtBoxMaxBat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtBoxMaxBat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBoxMaxBat.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtBoxMaxBat, "txtBoxMaxBat");
            this.txtBoxMaxBat.Name = "txtBoxMaxBat";
            // 
            // gbCameras
            // 
            resources.ApplyResources(this.gbCameras, "gbCameras");
            this.gbCameras.Controls.Add(this.txtCamDev);
            this.gbCameras.Controls.Add(this.label3);
            this.gbCameras.Controls.Add(this.txtNightCameraPipeline);
            this.gbCameras.Controls.Add(this.txtDayCameraPipeline);
            this.gbCameras.Controls.Add(this.txtTripIP);
            this.gbCameras.Controls.Add(this.lblTripIP);
            this.gbCameras.Controls.Add(this.btnNextVisionSettings);
            this.gbCameras.Controls.Add(this.panel6);
            this.gbCameras.Controls.Add(this.label2);
            this.gbCameras.Controls.Add(this.lblDayCamPipeline);
            this.gbCameras.Controls.Add(this.lblNightCamPipeline);
            this.gbCameras.Name = "gbCameras";
            this.gbCameras.TabStop = false;
            // 
            // txtCamDev
            // 
            this.txtCamDev.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtCamDev.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCamDev.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtCamDev, "txtCamDev");
            this.txtCamDev.Name = "txtCamDev";
            this.txtCamDev.ShortcutsEnabled = false;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // txtNightCameraPipeline
            // 
            this.txtNightCameraPipeline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtNightCameraPipeline.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNightCameraPipeline.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtNightCameraPipeline, "txtNightCameraPipeline");
            this.txtNightCameraPipeline.Name = "txtNightCameraPipeline";
            // 
            // txtDayCameraPipeline
            // 
            this.txtDayCameraPipeline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtDayCameraPipeline.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDayCameraPipeline.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtDayCameraPipeline, "txtDayCameraPipeline");
            this.txtDayCameraPipeline.Name = "txtDayCameraPipeline";
            // 
            // txtTripIP
            // 
            this.txtTripIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtTripIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTripIP.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtTripIP, "txtTripIP");
            this.txtTripIP.Name = "txtTripIP";
            // 
            // lblTripIP
            // 
            resources.ApplyResources(this.lblTripIP, "lblTripIP");
            this.lblTripIP.Name = "lblTripIP";
            // 
            // btnNextVisionSettings
            // 
            resources.ApplyResources(this.btnNextVisionSettings, "btnNextVisionSettings");
            this.btnNextVisionSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnNextVisionSettings.ButtonAction = null;
            this.btnNextVisionSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnNextVisionSettings.Name = "btnNextVisionSettings";
            this.btnNextVisionSettings.UseVisualStyleBackColor = false;
            this.btnNextVisionSettings.Click += new System.EventHandler(this.btnNextVisionSettings_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.radNextvision);
            this.panel6.Controls.Add(this.radCopterpix);
            resources.ApplyResources(this.panel6, "panel6");
            this.panel6.Name = "panel6";
            // 
            // radNextvision
            // 
            resources.ApplyResources(this.radNextvision, "radNextvision");
            this.radNextvision.Name = "radNextvision";
            this.radNextvision.TabStop = true;
            this.radNextvision.UseVisualStyleBackColor = true;
            this.radNextvision.CheckedChanged += new System.EventHandler(this.radNextvision_CheckedChanged);
            // 
            // radCopterpix
            // 
            resources.ApplyResources(this.radCopterpix, "radCopterpix");
            this.radCopterpix.Name = "radCopterpix";
            this.radCopterpix.TabStop = true;
            this.radCopterpix.UseVisualStyleBackColor = true;
            this.radCopterpix.CheckedChanged += new System.EventHandler(this.radCopterpix_CheckedChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // gbDroneConnection
            // 
            resources.ApplyResources(this.gbDroneConnection, "gbDroneConnection");
            this.gbDroneConnection.Controls.Add(this.txtVehicleName);
            this.gbDroneConnection.Controls.Add(this.txtMavPort);
            this.gbDroneConnection.Controls.Add(this.btnMobDroneGUI);
            this.gbDroneConnection.Controls.Add(this.txtMobDroneIP);
            this.gbDroneConnection.Controls.Add(this.lblMobDroneIP);
            this.gbDroneConnection.Controls.Add(this.txtMavId);
            this.gbDroneConnection.Controls.Add(this.lblMobilicomIP);
            this.gbDroneConnection.Controls.Add(this.txtMobilicomIP);
            this.gbDroneConnection.Controls.Add(this.btnMobilicomGUI);
            this.gbDroneConnection.Controls.Add(this.txtMavIP);
            this.gbDroneConnection.Controls.Add(this.panel3);
            this.gbDroneConnection.Controls.Add(this.cmbWifi);
            this.gbDroneConnection.Controls.Add(this.lblCommunication);
            this.gbDroneConnection.Controls.Add(this.lblProtocol);
            this.gbDroneConnection.Controls.Add(this.lblMavHost);
            this.gbDroneConnection.Controls.Add(this.lblMavPort);
            this.gbDroneConnection.Controls.Add(this.lblName);
            this.gbDroneConnection.Controls.Add(this.lblMavID);
            this.gbDroneConnection.Controls.Add(this.panel2);
            this.gbDroneConnection.Name = "gbDroneConnection";
            this.gbDroneConnection.TabStop = false;
            // 
            // txtVehicleName
            // 
            this.txtVehicleName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtVehicleName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVehicleName.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtVehicleName, "txtVehicleName");
            this.txtVehicleName.Name = "txtVehicleName";
            // 
            // txtMavPort
            // 
            this.txtMavPort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtMavPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMavPort.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtMavPort, "txtMavPort");
            this.txtMavPort.Name = "txtMavPort";
            // 
            // btnMobDroneGUI
            // 
            resources.ApplyResources(this.btnMobDroneGUI, "btnMobDroneGUI");
            this.btnMobDroneGUI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnMobDroneGUI.ButtonAction = null;
            this.btnMobDroneGUI.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnMobDroneGUI.Name = "btnMobDroneGUI";
            this.btnMobDroneGUI.UseVisualStyleBackColor = false;
            this.btnMobDroneGUI.Click += new System.EventHandler(this.btnMobDroneGUI_Click);
            // 
            // txtMobDroneIP
            // 
            this.txtMobDroneIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtMobDroneIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMobDroneIP.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtMobDroneIP, "txtMobDroneIP");
            this.txtMobDroneIP.Name = "txtMobDroneIP";
            this.txtMobDroneIP.ShortcutsEnabled = false;
            // 
            // lblMobDroneIP
            // 
            resources.ApplyResources(this.lblMobDroneIP, "lblMobDroneIP");
            this.lblMobDroneIP.Name = "lblMobDroneIP";
            // 
            // txtMavId
            // 
            this.txtMavId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtMavId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMavId.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtMavId, "txtMavId");
            this.txtMavId.Name = "txtMavId";
            // 
            // lblMobilicomIP
            // 
            resources.ApplyResources(this.lblMobilicomIP, "lblMobilicomIP");
            this.lblMobilicomIP.Name = "lblMobilicomIP";
            // 
            // txtMobilicomIP
            // 
            this.txtMobilicomIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtMobilicomIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMobilicomIP.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtMobilicomIP, "txtMobilicomIP");
            this.txtMobilicomIP.Name = "txtMobilicomIP";
            this.txtMobilicomIP.ShortcutsEnabled = false;
            // 
            // btnMobilicomGUI
            // 
            resources.ApplyResources(this.btnMobilicomGUI, "btnMobilicomGUI");
            this.btnMobilicomGUI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0)))));
            this.btnMobilicomGUI.ButtonAction = null;
            this.btnMobilicomGUI.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(46)))), ((int)(((byte)(56)))));
            this.btnMobilicomGUI.Name = "btnMobilicomGUI";
            this.btnMobilicomGUI.UseVisualStyleBackColor = false;
            this.btnMobilicomGUI.Click += new System.EventHandler(this.btnMobilicomGUI_Click);
            // 
            // txtMavIP
            // 
            this.txtMavIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(68)))), ((int)(((byte)(69)))));
            this.txtMavIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMavIP.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.txtMavIP, "txtMavIP");
            this.txtMavIP.Name = "txtMavIP";
            // 
            // frmEditVehicle
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Name = "frmEditVehicle";
            this.Ttile = "Vehicle Properties";
            this.Controls.SetChildIndex(this.panel4, 0);
            this.Controls.SetChildIndex(this.panel5, 0);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.gbLandingStation.ResumeLayout(false);
            this.gbLandingStation.PerformLayout();
            this.gbBattetry.ResumeLayout(false);
            this.gbBattetry.PerformLayout();
            this.gbCameras.ResumeLayout(false);
            this.gbCameras.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.gbDroneConnection.ResumeLayout(false);
            this.gbDroneConnection.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblMavPort;
        private System.Windows.Forms.Label lblMavHost;
        private System.Windows.Forms.Label lblDayCamPipeline;
        private System.Windows.Forms.Label lblNightCamPipeline;
        private System.Windows.Forms.Label lblLSPort;
        private System.Windows.Forms.Label lblLSHost;
        private System.Windows.Forms.RadioButton radTCP;
        private System.Windows.Forms.RadioButton radUDP;
        private System.Windows.Forms.Label lblProtocol;
        private System.Windows.Forms.RadioButton radUDPCl;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radLTE;
        private System.Windows.Forms.RadioButton radWIFI;
        private System.Windows.Forms.Label lblCommunication;
        private System.Windows.Forms.ComboBox cmbWifi;
        private System.Windows.Forms.Label lblMaxBattery;
        private System.Windows.Forms.Label lblMinBaterry;
        private System.Windows.Forms.Label lblTimeInAir;
        private System.Windows.Forms.RadioButton radEthernet;
        private System.Windows.Forms.Label lblMavID;
        private CpxTextbox txtLandingIP;
        private CpxButton btnCancel;
        private CpxButton btnSave;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox gbBattetry;
        private System.Windows.Forms.GroupBox gbLandingStation;
        private System.Windows.Forms.GroupBox gbCameras;
        private System.Windows.Forms.GroupBox gbDroneConnection;
        private CpxTextbox txtMavIP;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RadioButton radNextvision;
        private System.Windows.Forms.RadioButton radCopterpix;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radMobilicom;
        private CpxButton btnNextVisionSettings;
        private CpxLabel lblTripIP;
        private CpxTextbox txtTripIP;
        private CpxLabel lblMobilicomIP;
        private CpxTextbox txtMobilicomIP;
        private CpxButton btnMobilicomGUI;
        private CpxTextbox txtMavId;
        private CpxTextbox txtMobDroneIP;
        private CpxLabel lblMobDroneIP;
        private CpxButton btnMobDroneGUI;
        private CpxTextbox txtNightCameraPipeline;
        private CpxTextbox txtDayCameraPipeline;
        private CpxTextbox txtVehicleName;
        private CpxTextbox txtMavPort;
        private CpxTextbox txtLandingPort;
        private CpxTextbox txtBoxTIN;
        private CpxTextbox txtBoxMinBat;
        private CpxTextbox txtBoxMaxBat;
        private CpxTextbox txtCamDev;
        private System.Windows.Forms.Label label3;
    }
}