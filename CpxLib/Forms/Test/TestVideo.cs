﻿using CpxLib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Forms.Test
{
    public partial class TestVideo : Form
    {
        public TestVideo()
        {
            InitializeComponent();
            CpxHost.Instance.NewImage += 
                Instance_NewImage;
        }

        private void btnTestWebCamera_Click(object sender, EventArgs e)
        {

            var config = CpxLib.Core.AppSettings.LoadConfiguration();
            var dataDirectory = config.GStreamerDir;
            var gstdir = Path.Combine(dataDirectory, @"1.0\x86_64\bin\gst-launch-1.0.exe");


            cpxVideoOut1.Pipeline = txtDayPipeline.Text;
            cpxVideoOut1.GStreamerInstallDir = gstdir;
            cpxVideoOut1.Run();
        }

        private void Instance_NewImage(object sender, NewImageEventArgs e)
        {
            
           
        }

        private void btnPauseWebCamera_Click(object sender, EventArgs e)
        {
            
        }

        private void btnTestNightWebCamera_Click(object sender, EventArgs e)
        {
            var config = CpxLib.Core.AppSettings.LoadConfiguration();
            var dataDirectory = config.GStreamerDir;
            var gstdir = Path.Combine(dataDirectory, @"1.0\x86_64\bin\gst-launch-1.0.exe");


            cpxVideoOut2.Pipeline = txtNightPipeline.Text;
            cpxVideoOut2.GStreamerInstallDir = gstdir;
            cpxVideoOut2.Run();

        }
    }
}
