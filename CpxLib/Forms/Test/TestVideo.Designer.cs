﻿namespace CpxLib.Forms.Test
{
    partial class TestVideo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTestDayWebCamera = new System.Windows.Forms.Button();
            this.btnPauseWebCamera = new System.Windows.Forms.Button();
            this.btnTestNightWebCamera = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txtDayPipeline = new System.Windows.Forms.TextBox();
            this.txtNightPipeline = new System.Windows.Forms.TextBox();
            this.cpxVideoOut1 = new HostGStreamer.GStreamerWrapper();
            this.cpxVideoOut2 = new HostGStreamer.GStreamerWrapper();
            this.SuspendLayout();
            // 
            // btnTestDayWebCamera
            // 
            this.btnTestDayWebCamera.Location = new System.Drawing.Point(28, 387);
            this.btnTestDayWebCamera.Name = "btnTestDayWebCamera";
            this.btnTestDayWebCamera.Size = new System.Drawing.Size(160, 45);
            this.btnTestDayWebCamera.TabIndex = 1;
            this.btnTestDayWebCamera.Text = "Start WebCamera";
            this.btnTestDayWebCamera.UseVisualStyleBackColor = true;
            this.btnTestDayWebCamera.Click += new System.EventHandler(this.btnTestWebCamera_Click);
            // 
            // btnPauseWebCamera
            // 
            this.btnPauseWebCamera.Location = new System.Drawing.Point(194, 387);
            this.btnPauseWebCamera.Name = "btnPauseWebCamera";
            this.btnPauseWebCamera.Size = new System.Drawing.Size(160, 45);
            this.btnPauseWebCamera.TabIndex = 2;
            this.btnPauseWebCamera.Text = "Pause WebCamera";
            this.btnPauseWebCamera.UseVisualStyleBackColor = true;
            this.btnPauseWebCamera.Click += new System.EventHandler(this.btnPauseWebCamera_Click);
            // 
            // btnTestNightWebCamera
            // 
            this.btnTestNightWebCamera.Location = new System.Drawing.Point(542, 387);
            this.btnTestNightWebCamera.Name = "btnTestNightWebCamera";
            this.btnTestNightWebCamera.Size = new System.Drawing.Size(160, 45);
            this.btnTestNightWebCamera.TabIndex = 4;
            this.btnTestNightWebCamera.Text = "Start WebCamera";
            this.btnTestNightWebCamera.UseVisualStyleBackColor = true;
            this.btnTestNightWebCamera.Click += new System.EventHandler(this.btnTestNightWebCamera_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(708, 387);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(160, 45);
            this.button2.TabIndex = 5;
            this.button2.Text = "Pause WebCamera";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // txtDayPipeline
            // 
            this.txtDayPipeline.Location = new System.Drawing.Point(28, 315);
            this.txtDayPipeline.Multiline = true;
            this.txtDayPipeline.Name = "txtDayPipeline";
            this.txtDayPipeline.Size = new System.Drawing.Size(479, 66);
            this.txtDayPipeline.TabIndex = 6;
            this.txtDayPipeline.Text = "udpsrc port=5600 buffer-size=90000 ! application/x-rtp ! rtph264depay ! avdec_h26" +
    "4 ! queue leaky=2 ! videoconvert ! video/x-raw,format=BGRA ! autovideosink";
            // 
            // txtNightPipeline
            // 
            this.txtNightPipeline.Location = new System.Drawing.Point(542, 315);
            this.txtNightPipeline.Multiline = true;
            this.txtNightPipeline.Name = "txtNightPipeline";
            this.txtNightPipeline.Size = new System.Drawing.Size(479, 66);
            this.txtNightPipeline.TabIndex = 7;
            this.txtNightPipeline.Text = "udpsrc port=5601 buffer-size=90000 ! application/x-rtp ! rtph264depay ! avdec_h26" +
    "4 ! queue leaky=2 ! videoconvert ! video/x-raw,format=BGRA ! autovideosink";
            // 
            // cpxVideoOut1
            // 
            this.cpxVideoOut1.GStreamerInstallDir = null;
            this.cpxVideoOut1.Location = new System.Drawing.Point(28, 30);
            this.cpxVideoOut1.Name = "cpxVideoOut1";
            this.cpxVideoOut1.Pipeline = null;
            this.cpxVideoOut1.Size = new System.Drawing.Size(479, 279);
            this.cpxVideoOut1.TabIndex = 8;
            // 
            // cpxVideoOut2
            // 
            this.cpxVideoOut2.GStreamerInstallDir = null;
            this.cpxVideoOut2.Location = new System.Drawing.Point(558, 46);
            this.cpxVideoOut2.Name = "cpxVideoOut2";
            this.cpxVideoOut2.Pipeline = null;
            this.cpxVideoOut2.Size = new System.Drawing.Size(360, 235);
            this.cpxVideoOut2.TabIndex = 9;
            // 
            // TestVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 450);
            this.Controls.Add(this.cpxVideoOut2);
            this.Controls.Add(this.cpxVideoOut1);
            this.Controls.Add(this.txtNightPipeline);
            this.Controls.Add(this.txtDayPipeline);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnTestNightWebCamera);
            this.Controls.Add(this.btnPauseWebCamera);
            this.Controls.Add(this.btnTestDayWebCamera);
            this.Name = "TestVideo";
            this.Text = "TestVideo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnTestDayWebCamera;
        private System.Windows.Forms.Button btnPauseWebCamera;
        private System.Windows.Forms.Button btnTestNightWebCamera;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtDayPipeline;
        private System.Windows.Forms.TextBox txtNightPipeline;
        private HostGStreamer.GStreamerWrapper cpxVideoOut1;
        private HostGStreamer.GStreamerWrapper cpxVideoOut2;
    }
}