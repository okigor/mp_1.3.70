﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CpxLib.Controls.Common;
using CpxLib.Model;
using CpxLib.Properties;

namespace CpxLib.Forms
{
    public partial class frmSelectVehicle : frmBase
    {
        public string language = Properties.Settings.Default.Language;
        public Vehicle SelectedVehicle { get; private set; }

        public frmSelectVehicle()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            InitializeComponent();
        }

        private void frmSelectVehicle_Load(object sender, EventArgs e)
        {
            FillList();
        }

        private void FillList()
        {


            foreach (var item in ModelExtensions.GetVehicleFiles())
            {
                AddItemToList(item);
            }
        }

        private void AddItemToList(Vehicle vehicle)
        {
            var btn = new CpxButton
            {
                BackColor = System.Drawing.Color.FromArgb(64, 64, 64),
                FlatStyle = System.Windows.Forms.FlatStyle.Flat,
                ForeColor = System.Drawing.Color.White,
                Dock = DockStyle.Top,
                Margin = new System.Windows.Forms.Padding(3, 4, 3, 4),
                Size = new System.Drawing.Size(172, 56),
                Text = vehicle.Name,
                UseVisualStyleBackColor = false,
                Tag = vehicle,
                Font = new Font(this.Font, FontStyle.Bold),
                Image = Resources.icons8_drone_26,
                TextImageRelation = TextImageRelation.ImageBeforeText,
                ImageAlign = ContentAlignment.MiddleLeft
            };

            btn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            btn.Click += new System.EventHandler(this.btnOk_Click);
            pnlVehicles.Controls.Add(btn);

        }


        private void btnOk_Click(object sender, EventArgs e)
        {
            var btn = sender as CpxButton;

            SelectedVehicle = btn.Tag as Vehicle;
            DialogResult = DialogResult.OK;
            Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
