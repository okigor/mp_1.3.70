﻿namespace CpxLib.Forms
{
    partial class ArmOrDisarm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ArmOrDisarm));
            this.AreYouSure = new System.Windows.Forms.Label();
            this.cpxButton2 = new CpxLib.Controls.Common.CpxButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cpxButton1 = new CpxLib.Controls.Common.CpxButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // AreYouSure
            // 
            resources.ApplyResources(this.AreYouSure, "AreYouSure");
            this.AreYouSure.Name = "AreYouSure";
            this.AreYouSure.Click += new System.EventHandler(this.label1_Click);
            // 
            // cpxButton2
            // 
            this.cpxButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cpxButton2.ButtonAction = null;
            this.cpxButton2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cpxButton2.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.cpxButton2, "cpxButton2");
            this.cpxButton2.ForeColor = System.Drawing.Color.White;
            this.cpxButton2.Name = "cpxButton2";
            this.cpxButton2.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.AreYouSure);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cpxButton2);
            this.panel2.Controls.Add(this.cpxButton1);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // cpxButton1
            // 
            this.cpxButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cpxButton1.ButtonAction = null;
            this.cpxButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cpxButton1.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.cpxButton1, "cpxButton1");
            this.cpxButton1.ForeColor = System.Drawing.Color.White;
            this.cpxButton1.Name = "cpxButton1";
            this.cpxButton1.UseVisualStyleBackColor = false;
            this.cpxButton1.Click += new System.EventHandler(this.cpxButton1_Click);
            // 
            // ArmOrDisarm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "ArmOrDisarm";
            this.Ttile = "Arm/Disarm";
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label AreYouSure;
        private Controls.Common.CpxButton cpxButton2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Controls.Common.CpxButton cpxButton1;
    }
}