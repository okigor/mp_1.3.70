﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Forms
{
    public partial class frmTakeoff : frmBase
    {
        public string language = Properties.Settings.Default.Language;
        private bool _ignoreEvents = false;

        public float Altitude { get; private set; }

        public frmTakeoff()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            InitializeComponent();

            colorSlider1.Value = 12;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;

            _ignoreEvents = true;
            txtAlt.Text = colorSlider1.Value.ToString();
            _ignoreEvents = false;
        }

        private void btnTakeOff_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAlt.Text))
            {
                MessageBox.Show("Enter altitude value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!float.TryParse(txtAlt.Text, out float alt))
            {
                MessageBox.Show("Invalid altitude value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (alt < 0)
            {
                MessageBox.Show("Invalid altitude value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Altitude = alt;

            DialogResult = DialogResult.OK;
            Close();

        }

        private void txtAlt_TextChanged(object sender, EventArgs e)
        {

            if (_ignoreEvents) return;

            _ignoreEvents = true;
            int.TryParse(txtAlt.Text, out int alt);
            if (alt > colorSlider1.Maximum)
            {
                MessageBox.Show(Properties.Resources.AltNotGood);
                _ignoreEvents = false;
                return;
            }
            colorSlider1.Value = alt;

            _ignoreEvents = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
