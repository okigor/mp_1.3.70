﻿using CpxLib.Controls.Common;

namespace CpxLib.Forms
{
    partial class frmSelectVehicle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSelectVehicle));
            this.btnCancel = new CpxLib.Controls.Common.CpxButton();
            this.pnlVehicles = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCancel.ButtonAction = null;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pnlVehicles
            // 
            resources.ApplyResources(this.pnlVehicles, "pnlVehicles");
            this.pnlVehicles.Name = "pnlVehicles";
            // 
            // frmSelectVehicle
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlVehicles);
            this.Controls.Add(this.btnCancel);
            this.Name = "frmSelectVehicle";
            this.Ttile = "Choose Drone";
            this.Load += new System.EventHandler(this.frmSelectVehicle_Load);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.pnlVehicles, 0);
            this.ResumeLayout(false);

        }

        #endregion
        private  CpxButton btnCancel;
        private System.Windows.Forms.Panel pnlVehicles;
    }
}