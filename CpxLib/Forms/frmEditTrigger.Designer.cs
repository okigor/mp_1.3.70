﻿using CpxLib.Controls.Common;

namespace CpxLib.Forms
{
    partial class frmEditTrigger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditTrigger));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkEnabled = new System.Windows.Forms.CheckBox();
            this.lblFinish = new System.Windows.Forms.Label();
            this.lblMin = new System.Windows.Forms.Label();
            this.lblRepeat = new System.Windows.Forms.Label();
            this.lblStart = new System.Windows.Forms.Label();
            this.txtMin = new System.Windows.Forms.NumericUpDown();
            this.dtFinishTime = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtStartTime = new System.Windows.Forms.DateTimePicker();
            this.radRepeat = new System.Windows.Forms.RadioButton();
            this.radOneTime = new System.Windows.Forms.RadioButton();
            this.cmbPlan = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkSaturday = new System.Windows.Forms.CheckBox();
            this.chkFriday = new System.Windows.Forms.CheckBox();
            this.chkWednesday = new System.Windows.Forms.CheckBox();
            this.chkThursday = new System.Windows.Forms.CheckBox();
            this.chkTuesday = new System.Windows.Forms.CheckBox();
            this.chkMonday = new System.Windows.Forms.CheckBox();
            this.chkSunday = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbVehicle = new System.Windows.Forms.ComboBox();
            this.btnSave = new CpxLib.Controls.Common.CpxButton();
            this.btnCancel = new CpxLib.Controls.Common.CpxButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMin)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkEnabled);
            this.groupBox1.Controls.Add(this.lblFinish);
            this.groupBox1.Controls.Add(this.lblMin);
            this.groupBox1.Controls.Add(this.lblRepeat);
            this.groupBox1.Controls.Add(this.lblStart);
            this.groupBox1.Controls.Add(this.txtMin);
            this.groupBox1.Controls.Add(this.dtFinishTime);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.dtStartTime);
            this.groupBox1.Controls.Add(this.radRepeat);
            this.groupBox1.Controls.Add(this.radOneTime);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // chkEnabled
            // 
            resources.ApplyResources(this.chkEnabled, "chkEnabled");
            this.chkEnabled.Name = "chkEnabled";
            this.chkEnabled.UseVisualStyleBackColor = true;
            // 
            // lblFinish
            // 
            resources.ApplyResources(this.lblFinish, "lblFinish");
            this.lblFinish.Name = "lblFinish";
            // 
            // lblMin
            // 
            resources.ApplyResources(this.lblMin, "lblMin");
            this.lblMin.Name = "lblMin";
            // 
            // lblRepeat
            // 
            resources.ApplyResources(this.lblRepeat, "lblRepeat");
            this.lblRepeat.Name = "lblRepeat";
            // 
            // lblStart
            // 
            resources.ApplyResources(this.lblStart, "lblStart");
            this.lblStart.Name = "lblStart";
            // 
            // txtMin
            // 
            resources.ApplyResources(this.txtMin, "txtMin");
            this.txtMin.Name = "txtMin";
            // 
            // dtFinishTime
            // 
            resources.ApplyResources(this.dtFinishTime, "dtFinishTime");
            this.dtFinishTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFinishTime.Name = "dtFinishTime";
            this.dtFinishTime.ShowUpDown = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // dtStartTime
            // 
            resources.ApplyResources(this.dtStartTime, "dtStartTime");
            this.dtStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStartTime.Name = "dtStartTime";
            this.dtStartTime.ShowUpDown = true;
            // 
            // radRepeat
            // 
            resources.ApplyResources(this.radRepeat, "radRepeat");
            this.radRepeat.Name = "radRepeat";
            this.radRepeat.UseVisualStyleBackColor = true;
            this.radRepeat.CheckedChanged += new System.EventHandler(this.triggerType_CheckedChanged);
            // 
            // radOneTime
            // 
            resources.ApplyResources(this.radOneTime, "radOneTime");
            this.radOneTime.Checked = true;
            this.radOneTime.Name = "radOneTime";
            this.radOneTime.TabStop = true;
            this.radOneTime.UseVisualStyleBackColor = true;
            this.radOneTime.CheckedChanged += new System.EventHandler(this.triggerType_CheckedChanged);
            // 
            // cmbPlan
            // 
            this.cmbPlan.FormattingEnabled = true;
            resources.ApplyResources(this.cmbPlan, "cmbPlan");
            this.cmbPlan.Name = "cmbPlan";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkSaturday);
            this.groupBox2.Controls.Add(this.chkFriday);
            this.groupBox2.Controls.Add(this.chkWednesday);
            this.groupBox2.Controls.Add(this.chkThursday);
            this.groupBox2.Controls.Add(this.chkTuesday);
            this.groupBox2.Controls.Add(this.chkMonday);
            this.groupBox2.Controls.Add(this.chkSunday);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // chkSaturday
            // 
            resources.ApplyResources(this.chkSaturday, "chkSaturday");
            this.chkSaturday.Name = "chkSaturday";
            this.chkSaturday.UseVisualStyleBackColor = true;
            // 
            // chkFriday
            // 
            resources.ApplyResources(this.chkFriday, "chkFriday");
            this.chkFriday.Name = "chkFriday";
            this.chkFriday.UseVisualStyleBackColor = true;
            // 
            // chkWednesday
            // 
            resources.ApplyResources(this.chkWednesday, "chkWednesday");
            this.chkWednesday.Name = "chkWednesday";
            this.chkWednesday.UseVisualStyleBackColor = true;
            // 
            // chkThursday
            // 
            resources.ApplyResources(this.chkThursday, "chkThursday");
            this.chkThursday.Name = "chkThursday";
            this.chkThursday.UseVisualStyleBackColor = true;
            // 
            // chkTuesday
            // 
            resources.ApplyResources(this.chkTuesday, "chkTuesday");
            this.chkTuesday.Name = "chkTuesday";
            this.chkTuesday.UseVisualStyleBackColor = true;
            // 
            // chkMonday
            // 
            resources.ApplyResources(this.chkMonday, "chkMonday");
            this.chkMonday.Name = "chkMonday";
            this.chkMonday.UseVisualStyleBackColor = true;
            // 
            // chkSunday
            // 
            resources.ApplyResources(this.chkSunday, "chkSunday");
            this.chkSunday.Name = "chkSunday";
            this.chkSunday.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // cmbVehicle
            // 
            this.cmbVehicle.FormattingEnabled = true;
            resources.ApplyResources(this.cmbVehicle, "cmbVehicle");
            this.cmbVehicle.Name = "cmbVehicle";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSave.ButtonAction = null;
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCancel.ButtonAction = null;
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmEditTrigger
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbVehicle);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbPlan);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmEditTrigger";
            this.Ttile = "EDIT TRIGGER";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.cmbPlan, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.cmbVehicle, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSave, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMin)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radRepeat;
        private System.Windows.Forms.RadioButton radOneTime;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker dtStartTime;
        private System.Windows.Forms.Label lblFinish;
        private System.Windows.Forms.Label lblMin;
        private System.Windows.Forms.Label lblRepeat;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.NumericUpDown txtMin;
        private System.Windows.Forms.DateTimePicker dtFinishTime;
        private System.Windows.Forms.CheckBox chkEnabled;
        private System.Windows.Forms.ComboBox cmbPlan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkSaturday;
        private System.Windows.Forms.CheckBox chkFriday;
        private System.Windows.Forms.CheckBox chkWednesday;
        private System.Windows.Forms.CheckBox chkThursday;
        private System.Windows.Forms.CheckBox chkTuesday;
        private System.Windows.Forms.CheckBox chkMonday;
        private System.Windows.Forms.CheckBox chkSunday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbVehicle;
        private CpxButton btnSave;
        private CpxButton btnCancel;
    }
}