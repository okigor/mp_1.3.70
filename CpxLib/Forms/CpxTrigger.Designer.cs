﻿namespace CpxLib.Forms
{
    partial class CpxTrigger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.gbRoute = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.gbSettings = new System.Windows.Forms.GroupBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cpxTextbox1 = new CpxLib.Controls.Common.CpxTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rbOneTime = new System.Windows.Forms.RadioButton();
            this.rbRepeating = new System.Windows.Forms.RadioButton();
            this.gbDays = new System.Windows.Forms.GroupBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cpxButton2 = new CpxLib.Controls.Common.CpxButton();
            this.cpxButton1 = new CpxLib.Controls.Common.CpxButton();
            this.panel4.SuspendLayout();
            this.gbRoute.SuspendLayout();
            this.gbSettings.SuspendLayout();
            this.gbDays.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.gbRoute);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 164);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(883, 62);
            this.panel4.TabIndex = 3;
            // 
            // gbRoute
            // 
            this.gbRoute.Controls.Add(this.comboBox1);
            this.gbRoute.Controls.Add(this.label5);
            this.gbRoute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbRoute.Location = new System.Drawing.Point(0, 0);
            this.gbRoute.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbRoute.Name = "gbRoute";
            this.gbRoute.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbRoute.Size = new System.Drawing.Size(883, 62);
            this.gbRoute.TabIndex = 0;
            this.gbRoute.TabStop = false;
            this.gbRoute.Text = "Route";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(126, 24);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(469, 28);
            this.comboBox1.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Choose Route";
            // 
            // gbSettings
            // 
            this.gbSettings.Controls.Add(this.dateTimePicker2);
            this.gbSettings.Controls.Add(this.dateTimePicker1);
            this.gbSettings.Controls.Add(this.label4);
            this.gbSettings.Controls.Add(this.cpxTextbox1);
            this.gbSettings.Controls.Add(this.label3);
            this.gbSettings.Controls.Add(this.label2);
            this.gbSettings.Controls.Add(this.label1);
            this.gbSettings.Controls.Add(this.rbOneTime);
            this.gbSettings.Controls.Add(this.rbRepeating);
            this.gbSettings.Dock = System.Windows.Forms.DockStyle.Left;
            this.gbSettings.Location = new System.Drawing.Point(0, 0);
            this.gbSettings.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbSettings.Name = "gbSettings";
            this.gbSettings.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbSettings.Size = new System.Drawing.Size(570, 164);
            this.gbSettings.TabIndex = 0;
            this.gbSettings.TabStop = false;
            this.gbSettings.Text = "Settings";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker2.Location = new System.Drawing.Point(373, 61);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.ShowUpDown = true;
            this.dateTimePicker2.Size = new System.Drawing.Size(116, 26);
            this.dateTimePicker2.TabIndex = 8;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker1.Location = new System.Drawing.Point(126, 61);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowUpDown = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(116, 26);
            this.dateTimePicker1.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(243, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "min.";
            // 
            // cpxTextbox1
            // 
            this.cpxTextbox1.Location = new System.Drawing.Point(126, 95);
            this.cpxTextbox1.Name = "cpxTextbox1";
            this.cpxTextbox1.Size = new System.Drawing.Size(116, 26);
            this.cpxTextbox1.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Repeat every";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(302, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Finish";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Start";
            // 
            // rbOneTime
            // 
            this.rbOneTime.AutoSize = true;
            this.rbOneTime.Location = new System.Drawing.Point(168, 28);
            this.rbOneTime.Name = "rbOneTime";
            this.rbOneTime.Size = new System.Drawing.Size(95, 24);
            this.rbOneTime.TabIndex = 1;
            this.rbOneTime.TabStop = true;
            this.rbOneTime.Text = "One Time";
            this.rbOneTime.UseVisualStyleBackColor = true;
            // 
            // rbRepeating
            // 
            this.rbRepeating.AutoSize = true;
            this.rbRepeating.Location = new System.Drawing.Point(13, 28);
            this.rbRepeating.Name = "rbRepeating";
            this.rbRepeating.Size = new System.Drawing.Size(101, 24);
            this.rbRepeating.TabIndex = 0;
            this.rbRepeating.TabStop = true;
            this.rbRepeating.Text = "Repeating";
            this.rbRepeating.UseVisualStyleBackColor = true;
            // 
            // gbDays
            // 
            this.gbDays.Controls.Add(this.checkBox7);
            this.gbDays.Controls.Add(this.checkBox6);
            this.gbDays.Controls.Add(this.checkBox5);
            this.gbDays.Controls.Add(this.checkBox4);
            this.gbDays.Controls.Add(this.checkBox3);
            this.gbDays.Controls.Add(this.checkBox2);
            this.gbDays.Controls.Add(this.checkBox1);
            this.gbDays.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbDays.Location = new System.Drawing.Point(570, 0);
            this.gbDays.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbDays.Name = "gbDays";
            this.gbDays.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbDays.Size = new System.Drawing.Size(313, 164);
            this.gbDays.TabIndex = 0;
            this.gbDays.TabStop = false;
            this.gbDays.Text = "Days";
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(169, 96);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(92, 24);
            this.checkBox7.TabIndex = 6;
            this.checkBox7.Text = "Saturday";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(169, 62);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(71, 24);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.Text = "Friday";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(169, 28);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(93, 24);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "Thurdsay";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(8, 130);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(112, 24);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "Wednesday";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(8, 96);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(88, 24);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "Tuesday";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(8, 62);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(84, 24);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "Monday";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(8, 28);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(82, 24);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Sunday";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.gbDays);
            this.panel3.Controls.Add(this.gbSettings);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(883, 164);
            this.panel3.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cpxButton2);
            this.panel1.Controls.Add(this.cpxButton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 226);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(883, 48);
            this.panel1.TabIndex = 10;
            // 
            // cpxButton2
            // 
            this.cpxButton2.ButtonAction = null;
            this.cpxButton2.Location = new System.Drawing.Point(789, 13);
            this.cpxButton2.Name = "cpxButton2";
            this.cpxButton2.Size = new System.Drawing.Size(75, 23);
            this.cpxButton2.TabIndex = 1;
            this.cpxButton2.Text = "cpxButton2";
            this.cpxButton2.UseVisualStyleBackColor = true;
            // 
            // cpxButton1
            // 
            this.cpxButton1.ButtonAction = null;
            this.cpxButton1.Location = new System.Drawing.Point(691, 13);
            this.cpxButton1.Name = "cpxButton1";
            this.cpxButton1.Size = new System.Drawing.Size(75, 23);
            this.cpxButton1.TabIndex = 0;
            this.cpxButton1.Text = "cpxButton1";
            this.cpxButton1.UseVisualStyleBackColor = true;
            // 
            // CpxTrigger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(883, 274);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "CpxTrigger";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CpxTrigger";
            this.panel4.ResumeLayout(false);
            this.gbRoute.ResumeLayout(false);
            this.gbRoute.PerformLayout();
            this.gbSettings.ResumeLayout(false);
            this.gbSettings.PerformLayout();
            this.gbDays.ResumeLayout(false);
            this.gbDays.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbSettings;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label4;
        private Controls.Common.CpxTextbox cpxTextbox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbOneTime;
        private System.Windows.Forms.RadioButton rbRepeating;
        private System.Windows.Forms.GroupBox gbDays;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox gbRoute;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private Controls.Common.CpxButton cpxButton2;
        private Controls.Common.CpxButton cpxButton1;
    }
}