﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Forms
{
    public partial class frmMapActions : frmBase
    {
        public enum MapActionEnum
        {
            NotSet,
            FlightToHere,
            ChangeAlt,
            PointCameraHere,
            UnPointCamera,
            NewPointCamera
        }

        public MapActionEnum MapAction { get; private set; } = MapActionEnum.NotSet;
        public frmMapActions()
        {
            //SetDesktopLocation(x, y);
            InitializeComponent();
        }

        private void btnFlightToHere_Click(object sender, EventArgs e)
        {
            MapAction = MapActionEnum.FlightToHere;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnChangeAlt_Click(object sender, EventArgs e)
        {
            MapAction = MapActionEnum.ChangeAlt;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnPointCameraHere_Click(object sender, EventArgs e)
        {
            MapAction = MapActionEnum.PointCameraHere;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cpxButton1_Click(object sender, EventArgs e)
        {
            MapAction = MapActionEnum.UnPointCamera;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnNewPointCamera_Click(object sender, EventArgs e)
        {
            MapAction = MapActionEnum.NewPointCamera;
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
