﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.IO.Pipes;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
//using log4net;
using guint = System.UInt32;
using GstClockTime = System.UInt64;
using gsize = System.UInt64;
 
namespace CpxLib
{
    public partial class GStreamerCpx
    {
        public static class NativeMethods
        {
            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_init(ref int argc, ref IntPtr[] argv);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_init(IntPtr argc, IntPtr argv);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_init(ref int argc, string[] argv);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern bool gst_init_check(ref int argc, ref IntPtr[] argv, out IntPtr error);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern bool gst_init_check(IntPtr argc, IntPtr argv, out IntPtr error);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_version(ref guint major,
                ref guint minor,
                ref guint micro,
                ref guint nano);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern UIntPtr gst_buffer_extract(IntPtr raw, UIntPtr offset, byte[] dest, UIntPtr size);


            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_buffer_extract_dup(IntPtr raw, UIntPtr offset, UIntPtr size, out IntPtr dest,
                out UIntPtr dest_size);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr gst_pipeline_new(string name);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr gst_element_factory_make(string factoryname, string name);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_message_parse_error(IntPtr msg, out IntPtr err, out IntPtr debug);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr gst_message_get_stream_status_object(IntPtr raw);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_element_set_state(IntPtr pipeline, GstState gST_STATE_PLAYING);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr gst_parse_launch(string cmdline, out IntPtr error);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr gst_bus_timed_pop_filtered(IntPtr bus, ulong gST_CLOCK_TIME_NONE,
                GstMessageType gstMessageType);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr gst_element_get_bus(IntPtr pipeline);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_debug_bin_to_dot_file(IntPtr pipeline, GstDebugGraphDetails details, string file_name);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_message_set_stream_status_object(IntPtr raw, IntPtr value);

            [DllImport("libgstapp-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr gst_app_sink_try_pull_sample(IntPtr appsink,
                GstClockTime timeout);

            [DllImport("libgstapp-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr
                gst_app_sink_get_caps(IntPtr appsink);

            [DllImport("libgstapp-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_app_sink_set_max_buffers(IntPtr appsink, guint max);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr gst_bin_get_by_name(IntPtr pipeline, string name);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr gst_sample_get_buffer(IntPtr sample);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr
                gst_sample_get_caps(IntPtr sample);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr
                gst_sample_get_info(IntPtr sample);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern
                StringBuilder
                gst_structure_to_string(IntPtr structure);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern bool
                gst_structure_get_int(IntPtr structure,
                    string fieldname,
                    out int value);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern IntPtr
                gst_caps_get_structure(IntPtr caps,
                    guint index);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern
                IntPtr gst_caps_to_string(IntPtr caps);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern bool gst_buffer_map(IntPtr buffer, out GstMapInfo info, GstMapFlags GstMapFlags);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_buffer_unmap(IntPtr buffer, out GstMapInfo info);

            public static void gst_sample_unref(IntPtr sample)
            {
                gst_mini_object_unref(sample);
            }

            public static void gst_buffer_unref(IntPtr buffer)
            {
                gst_mini_object_unref(buffer);
            }

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.StdCall)]
            public static extern void
                gst_caps_unref(IntPtr caps);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_structure_free(IntPtr structure);

            [DllImport("libgstreamer-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void
                gst_mini_object_unref(IntPtr mini_object);

            [DllImport("libgstapp-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern bool gst_app_sink_is_eos(IntPtr appsink);

            [DllImport("libgstapp-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_app_sink_set_drop(IntPtr appsink, bool v);

            [DllImport("libgstapp-1.0-0.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void gst_app_sink_set_callbacks(IntPtr appsink, GstAppSinkCallbacks callbacks,
                ref int testdata, object p);
        }

        [StructLayout(LayoutKind.Sequential, Size = 200)]
        public struct GstMapInfo
        {
            public IntPtr memory;
            public GstMapFlags flags;
            public IntPtr data;
            public gsize size;
            public gsize maxsize;
            //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)] public IntPtr[] user_data; //4
            //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)] public IntPtr[] _gst_reserved; //4
        }

        public struct GstAppSinkCallbacks
        {
            public eos eos; //void (* eos) (GstAppSink* sink, gpointer user_data);
            public new_preroll new_preroll; //GstFlowReturn(*new_preroll)      (GstAppSink* sink, gpointer user_data);
            public new_buffer new_buffer; //GstFlowReturn(*new_buffer)       (GstAppSink* sink, gpointer user_data);
            public new_buffer_list new_buffer_list; //GstFlowReturn(*new_buffer_list)  (GstAppSink* sink, gpointer user_data);
        }

        public enum GstFlowReturn
        {
            /* custom success starts here */
            GST_FLOW_CUSTOM_SUCCESS_2 = 102,
            GST_FLOW_CUSTOM_SUCCESS_1 = 101,
            GST_FLOW_CUSTOM_SUCCESS = 100,

            /* core predefined */
            GST_FLOW_RESEND = 1,
            GST_FLOW_OK = 0,
            /* expected failures */
            GST_FLOW_NOT_LINKED = -1,
            GST_FLOW_WRONG_STATE = -2,
            /* error cases */
            GST_FLOW_UNEXPECTED = -3,
            GST_FLOW_NOT_NEGOTIATED = -4,
            GST_FLOW_ERROR = -5,
            GST_FLOW_NOT_SUPPORTED = -6,

            /* custom error starts here */
            GST_FLOW_CUSTOM_ERROR = -100,
            GST_FLOW_CUSTOM_ERROR_1 = -101,
            GST_FLOW_CUSTOM_ERROR_2 = -102
        }


        public enum GstDebugGraphDetails
        {

            GST_DEBUG_GRAPH_SHOW_MEDIA_TYPE = (1 << 0),

            GST_DEBUG_GRAPH_SHOW_CAPS_DETAILS = (1 << 1),

            GST_DEBUG_GRAPH_SHOW_NON_DEFAULT_PARAMS = (1 << 2),

            GST_DEBUG_GRAPH_SHOW_STATES = (1 << 3),

            GST_DEBUG_GRAPH_SHOW_FULL_PARAMS = (1 << 4),

            GST_DEBUG_GRAPH_SHOW_ALL = ((1 << 4) - 1),

            GST_DEBUG_GRAPH_SHOW_VERBOSE = (-1)

        }

        public enum GstMapFlags
        {
            GST_MAP_READ = 1,
            GST_MAP_WRITE = 2,
            GST_MAP_FLAG_LAST = 65536
        }

        public enum GstState
        {
            GST_STATE_VOID_PENDING = 0,
            GST_STATE_NULL = 1,
            GST_STATE_READY = 2,
            GST_STATE_PAUSED = 3,
            GST_STATE_PLAYING = 4
        }

        public enum GstMessageType
        {
            GST_MESSAGE_UNKNOWN = 0,
            GST_MESSAGE_EOS = (1 << 0),
            GST_MESSAGE_ERROR = (1 << 1),
            GST_MESSAGE_WARNING = (1 << 2),
            GST_MESSAGE_INFO = (1 << 3),
            GST_MESSAGE_TAG = (1 << 4),
            GST_MESSAGE_BUFFERING = (1 << 5),
            GST_MESSAGE_STATE_CHANGED = (1 << 6),
            GST_MESSAGE_STATE_DIRTY = (1 << 7),
            GST_MESSAGE_STEP_DONE = (1 << 8),
            GST_MESSAGE_CLOCK_PROVIDE = (1 << 9),
            GST_MESSAGE_CLOCK_LOST = (1 << 10),
            GST_MESSAGE_NEW_CLOCK = (1 << 11),
            GST_MESSAGE_STRUCTURE_CHANGE = (1 << 12),
            GST_MESSAGE_STREAM_STATUS = (1 << 13),
            GST_MESSAGE_APPLICATION = (1 << 14),
            GST_MESSAGE_ELEMENT = (1 << 15),
            GST_MESSAGE_SEGMENT_START = (1 << 16),
            GST_MESSAGE_SEGMENT_DONE = (1 << 17),
            GST_MESSAGE_DURATION = (1 << 18),
            GST_MESSAGE_LATENCY = (1 << 19),
            GST_MESSAGE_ASYNC_START = (1 << 20),
            GST_MESSAGE_ASYNC_DONE = (1 << 21),
            GST_MESSAGE_REQUEST_STATE = (1 << 22),
            GST_MESSAGE_STEP_START = (1 << 23),
            GST_MESSAGE_QOS = (1 << 24),
            GST_MESSAGE_PROGRESS = (1 << 25),
            GST_MESSAGE_ANY = ~0
        }


        [StructLayout(LayoutKind.Sequential)]
        public struct GstObject
        {
            IntPtr _lock;
            public string name;
            public Object parent;
            public uint flags;
            IntPtr controlBindings;
            public int control_rate;
            public int last_sync;

            private IntPtr[] _gstGstReserved;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct GError
        {
            public UInt32 domain; // typedef guint32 GQuark;
            public int code;
            public string message;
        }
    }
}
