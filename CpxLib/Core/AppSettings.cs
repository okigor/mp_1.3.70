﻿using CpxLib.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Core
{
    public class AppSettings
    {
        public static void WriteToLog(string text, string ex)
        {

            string newLine = DateTime.Now.ToString() + "," + text + "," + ex + "\n";
            File.AppendAllText(LogFilePath, newLine);
            //using (CsvFileWriter writer = new CsvFileWriter(@"E:\SharedDoc1\WriteTest.csv"))
            //{
            //}
        }
        public static Configuration LoadConfiguration()
        {
            var config = new Configuration();

            if (System.IO.File.Exists(SettingsFilePath))
            {
                var content = System.IO.File.ReadAllText(SettingsFilePath);

                config.Deserialize(content);
            }

            return config;
        }
        public static void SaveConfiguration(Configuration config)
        {
            try
            {
                InitCopterPixWorkspace();

                var content = config.Serialize();

                System.IO.File.WriteAllText(SettingsFilePath, content);
                CustomMessageBox.Show(Resources.SuccessfulOperation);
            }
            catch (Exception e)
            {
                CustomMessageBox.Show(Resources.NotSuccessfulOperation);
            }
        }

        public static void InitCopterPixWorkspace()
        {
            if (!System.IO.Directory.Exists(CopterPixFolder))
            {
                System.IO.Directory.CreateDirectory(CopterPixFolder);
            }
            if (!System.IO.Directory.Exists(CopterPixDataFolder))
            {
                System.IO.Directory.CreateDirectory(CopterPixDataFolder);
            }
        }
        public static string LogFilePath
        {
            get
            {
                return System.IO.Path.Combine(CopterPixFolder, "log.csv");
            }
        }
        public static string SettingsFilePath
        {
            get
            {
                return System.IO.Path.Combine(CopterPixFolder, "cpx.config");
            }
        }
        public static string CopterPixFolder
        {
            get
            {
                var path = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                     System.IO.Path.DirectorySeparatorChar +
                     "CPX";

                return path;
            }
        }
        public static string CopterPixDataFolder
        {
            get
            {
                return CopterPixFolder +
                     System.IO.Path.DirectorySeparatorChar +
                     "Data";
            }
        }
    }
}
