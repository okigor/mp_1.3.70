﻿using CpxLib.Properties;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Core
{
    public static class CpxConstants
    {
        public const int MSG_NO_MESSAGES = 0;
        public const int MSG_NO_JOYSTICK = 1;
        public const int MSG_COMMUNICATION_PROBLEMS = 2;
        public const int MSG_LAW_BATTERY = 3;
        public const int MSG_JOYSTICK_CONNECTED = 4;
        public const int MSG_JOYSTICK_DISABLED = 5;
        public const int MSG_WIFI_UNAVAILBLE = 6;
        public const int MSG_CONNECTED = 7;
        public const int MSG_WIFI_UNABLE_TO_CONNECT = 8;
        public const int MSG_WIFI_UNDEFINED = 9;
        public const int MESSAGE_HIGH = 10;
        public const int MSG_PLEASE_CONNECT = 11;
        public const int MSG_WAITING_FOR_CONNECTION = 12;
        public const int MSG_SHOW_LAST_THREE = 13;
        public const int MSG_VIDEO_NOT_AVAILABLE = 14;
        public const int MSG_NO_NAVIGATION = 15;
        public const int MSG_LANDING_NOT_OPEN = 16;
        public const int MSG_LANDING_NOT_CLOSE = 17;

        public const int EXC_LAND_MODE = 18;
        public const int EXC_GIMBAL_DOWN = 19;
        public const int EXC_GIMBAL_FLIGHT = 20;
        public const int EXC_RTL_MODE = 21;
        public const int EXC_LOITER_MODE = 22;
        public const int EXC_AUTO_MODE = 23;
        public const int EXC_ARM = 24;
        public const int EXC_DISARM = 25;
        public const int EXC_GUIDED_MODE = 26;
        public const int EXC_TAKEOFF = 27;
        public const int EXC_DRAW_FENCE = 28;
        public const int EXC_OPEN_VPN = 29;
        public const int EXC_MISSION_START = 30;

        public const int MSG_INCORRECT_POS = 31;
        public const int MSG_LANDING_NOT_STOP = 32;
        public const int EXC_CAMERA_SETTINGS = 33;
        private static readonly CpxMessage[] _messages;
        static CpxConstants()
        {
            _messages = new[]
            {
                new CpxMessage(MSG_NO_MESSAGES, Resources.MessageNoMessages ),
                new CpxMessage(MSG_NO_JOYSTICK, Resources.MessageJoystickDisconnected,5000 ),
                new CpxMessage(MSG_COMMUNICATION_PROBLEMS, Resources.MessageCommunicationProblems ),
                new CpxMessage(MSG_LAW_BATTERY, Resources.MessageLowBattery ),
                new CpxMessage(MSG_JOYSTICK_CONNECTED, Resources.MessageJoystickConnected,  5000 ),
                new CpxMessage(MSG_JOYSTICK_DISABLED, Resources.MessageJoystickDisabled, 5000 ),
                new CpxMessage(MSG_WIFI_UNAVAILBLE, Resources.MessageWifiUnavalable, 7000 ),
                new CpxMessage(MSG_CONNECTED, Resources.MessageVehicleConnected, 7000 ),
                new CpxMessage(MSG_WIFI_UNABLE_TO_CONNECT, Resources.MessageUnableToConnectToNetwork, 7000),
                new CpxMessage(MSG_WIFI_UNDEFINED, Resources.MessageWifiUndefined, 7000 ),
                new CpxMessage(MESSAGE_HIGH, "Message High", 7000 ),
                new CpxMessage(MSG_PLEASE_CONNECT, Resources.MessagePleaseConnect, 10000 ),
                new CpxMessage(MSG_WAITING_FOR_CONNECTION, Resources.MessageWaitingForConnection, 30000),
                new CpxMessage(MSG_SHOW_LAST_THREE,"", 5000),
                new CpxMessage(MSG_VIDEO_NOT_AVAILABLE, Resources.MessageVideoNotAvailable, 5000),
                new CpxMessage(MSG_NO_NAVIGATION, Resources.MessageNoNavigation, 5000),
                new CpxMessage(MSG_LANDING_NOT_OPEN, Resources.MessageLandingNotOpen, 5000),
                new CpxMessage(MSG_LANDING_NOT_CLOSE, Resources.MessageLandingNotClose, 5000),
                new CpxMessage(EXC_LAND_MODE, Resources.ExceptionLandMode, 5000),
                new CpxMessage(EXC_GIMBAL_DOWN, Resources.ExceptionGimbalDown, 5000),
                new CpxMessage(EXC_GIMBAL_FLIGHT, Resources.ExceptionGimbalFlight, 5000),
                new CpxMessage(EXC_RTL_MODE, Resources.ExceptionRTLMode, 5000),
                new CpxMessage(EXC_LOITER_MODE, Resources.ExcpetionLoiterMode, 5000),
                new CpxMessage(EXC_AUTO_MODE, Resources.ExceptionAutoMode, 5000),
                new CpxMessage(EXC_ARM, Resources.ExceptionArm, 5000),
                new CpxMessage(EXC_DISARM, Resources.ExceptionDisarm, 5000),
                new CpxMessage(EXC_GUIDED_MODE, Resources.ExceptionGuidedMode,5000),
                new CpxMessage(EXC_TAKEOFF, Resources.ExceptionTakeoff, 5000),
                new CpxMessage(EXC_DRAW_FENCE, Resources.ExceptionDrawFence, 5000),
                new CpxMessage(EXC_OPEN_VPN, Resources.ExceptionOpenVPN, 5000),
                new CpxMessage(EXC_MISSION_START, Resources.ExceptionMissionStart, 5000),
                new CpxMessage(MSG_INCORRECT_POS, Resources.MassageIncorrectPosition, 5000),
                new CpxMessage(MSG_LANDING_NOT_STOP, Resources.MessageLandingNotStop, 5000),
                new CpxMessage(EXC_CAMERA_SETTINGS, Resources.MessageLandingNotStop, 5000),
                
          };
        }
        public static Color CopterPixForeColor
        {
            get
            {
                return Color.FromArgb(230, 169, 1);
            }
        }

        public static CpxMessage[] Messages
        {
            get
            {
                return _messages;
            }
        }

        public static Font SmallFont
        {
            get
            {
                return CpxFonts.GetFont(CpxFontsEnum.Rubik_Medium, 10.0F);
            }
        }
        public static Font BigFont
        {
            get
            {
                return CpxFonts.GetFont(CpxFontsEnum.Rubik_Medium, 13.0F);
            }
        }



    }
}
