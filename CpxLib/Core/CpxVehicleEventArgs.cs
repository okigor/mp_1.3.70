﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Core
{
    public class VehicleConnectionEventArgs : EventArgs
    {
        public VehicleConnectionEventArgs(string message)
        {
            Message = message;
            IsException = false;
        }
        public VehicleConnectionEventArgs(Exception exception)
        {
            Exception = Exception;
            IsException = true;
        }

        public bool IsException { get; private set; }
        public string Message { get; private set; }

        public Exception Exception { get; private set; }
    }
}
