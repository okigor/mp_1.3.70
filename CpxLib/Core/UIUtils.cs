﻿using CpxLib.Controls.Common;
using CpxLib.Model;
using CpxLib.Properties;
using MissionPlanner;
using MissionPlanner.Comms;
using MissionPlanner.Controls;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Core
{
    public static class UIUtils
    {
        /// <summary>
        /// Add button to the MissionPlanner main toolbar
        /// </summary>
        /// <param name="text">Caption</param>
        /// <param name="name">Id of the button</param>
        /// <param name="image">Drawn image</param>
        /// <param name="place">Location - zero-based index</param>
        /// <returns></returns>
        public static ToolStripButton AddToolstripButton(string text, string name, Image image, int place)
        {
            var button = new ToolStripButton()
            {
                DisplayStyle = ToolStripItemDisplayStyle.ImageAndText,
                Text = text,
                ForeColor = System.Drawing.Color.White,
                Image = image,
                TextImageRelation = TextImageRelation.ImageAboveText,
                ImageScaling = ToolStripItemImageScaling.None,
                Margin = new Padding(0),
                Name = name,
                Size = new System.Drawing.Size(60, 60),
                AutoSize = true,
                Font = CpxHost.Instance.MainForm.MainMenu.Items[0].Font,
                Visible = true
            };

            // add the button to the tool bar
            CpxHost.Instance.MainForm.MainMenu.Items.Insert(place, button);
            CpxHost.Instance.MainForm.MainMenu.Update();

            return button;
        }

        public static int EnableJoystick()
        {
            var connectedJoystick = GetConnectedJoysicks().Select(x => x.GetDeviceName()).FirstOrDefault();

            if (string.IsNullOrEmpty(connectedJoystick)) return CpxConstants.MSG_NO_JOYSTICK;


            if (MainV2.joystick == null || MainV2.joystick.enabled == false)
            {
                try
                {
                    if (MainV2.joystick != null)
                    {
                        MainV2.joystick.UnAcquireJoyStick();
                    }
                }
                catch
                {
                }
                // all config is loaded from the xmls
                var joy = new MissionPlanner.Joystick.Joystick(() => MainV2.comPort);

                //show error message if a joystick is not connected when Enable is clicked
                if (!joy.start(connectedJoystick))
                {
                    joy.Dispose();
                    return CpxConstants.MSG_NO_JOYSTICK;
                }

                MissionPlanner.Utilities.Settings.Instance["joystick_name"] = connectedJoystick;

                MainV2.joystick = joy;
                MainV2.joystick.enabled = true;
                return CpxConstants.MSG_JOYSTICK_CONNECTED;
            }
            else
            {
                MainV2.joystick.enabled = false;

                MainV2.joystick.clearRCOverride();

                MainV2.joystick = null;

                return CpxConstants.MSG_JOYSTICK_DISABLED;
            }
        }
        public static IList<SharpDX.DirectInput.DeviceInstance> GetConnectedJoysicks()
        {
            return MissionPlanner.Joystick.Joystick.getDevices();
        }

        public static string GetDeviceName(this SharpDX.DirectInput.DeviceInstance device)
        {
            return Regex.Replace(device.ProductName, @"[^\u0020-\u007E]", string.Empty);
        }

        public static void SetCopterPixLogo(Image logo)
        {
            // hide ArduPilot Logo
            CpxHost.Instance.MainForm.MainMenu.Items["MenuArduPilot"].Visible = false;

            // create new button with the CopterPix Logo
            var copterPixButton = new ToolStripButton()
            {
                Alignment = ToolStripItemAlignment.Right,
                BackColor = System.Drawing.Color.White,
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Text = "CopterPix",
                ForeColor = System.Drawing.Color.White,
                Image = logo,
                ImageAlign = System.Drawing.ContentAlignment.MiddleCenter,
                ImageScaling = ToolStripItemImageScaling.None,
                TextImageRelation = TextImageRelation.Overlay,
                Size = new System.Drawing.Size(173, 60),
                AutoSize = false,
                Margin = new Padding(0),
                Name = "MenuCopterPix",
                Overflow = ToolStripItemOverflow.Never,
                Visible = true,

            };

            // add the button to the tool bar
            CpxHost.Instance.MainForm.MainMenu.Items.Add(copterPixButton);
        }

        public static void RegisterMissionPlannerView(string name, MyUserControl control, bool persistent = false)
        {
            MainV2.View.AddScreen(new MainSwitcher.Screen(name, control, persistent));
        }

        public static void ShowSimulation()
        {
            UIUtils.ShowView("Simulation", false);
        }

        public static void ShowFlightData()
        {
            UIUtils.ShowView("FlightData", false);
        }

        public static void ShowSettings()
        {
            UIUtils.ShowView("CpxConfig", false);
        }

        public static void ShowFlightPlanner()
        {
            UIUtils.ShowView("FlightPlanner", true);
        }

        public static void ShowView(string name, bool hostInCpxView)
        {
            MainV2.instance.SuspendLayout();


            var currentViewControl = MainV2.View.current.Control;
            var container = currentViewControl.Parent;


            if (container.Name == "pnlWorkspace")
            {
                container.Controls.Remove(currentViewControl);
                container.Parent.Parent.Controls.Add(currentViewControl);
                container.Parent.Parent.Controls.Remove(container.Parent);
            }


            MainV2.View.ShowScreen(name);


            if (hostInCpxView)
            {
                var viewHost = new CpxViewHost
                {
                    Dock = DockStyle.Fill
                };

                currentViewControl = MainV2.View.current.Control;
                container = currentViewControl.Parent;

                container.Controls.Remove(currentViewControl);
                container.Controls.Add(viewHost);
                viewHost.HostedControl = currentViewControl;
            }

            MainV2.instance.ResumeLayout();
        }
        public static void ConnectToVideoStream(ref TcpClient tcpClient, ref NetworkStream networkStream, string IP)
        {
            try
            {
                tcpClient = new TcpClient(IP, 14600);
                networkStream = tcpClient.GetStream();
                //CpxHost.Instance.UI.restartVideo(5601, false);
            }
            catch (Exception e)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_VIDEO_NOT_AVAILABLE);
                AppSettings.WriteToLog(Resources.MessageVideoNotAvailable, e.Message);
            }
        }

        public static void DisconnectFromVideoStream(ref TcpClient tcpClient, ref NetworkStream networkStream)
        {
            try
            {
                if (tcpClient != null && tcpClient.Connected)
                {
                    networkStream.Close();
                    tcpClient.Close();
                    tcpClient = null;
                    networkStream = null;
                }
            }
            catch (Exception e)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_VIDEO_NOT_AVAILABLE);
                AppSettings.WriteToLog(Resources.MessageVideoNotAvailable, e.Message);
            }
        }

        public static void Arm(bool force, ref VehicleConnection connection)
        {
            try
            {
                var comport = CpxHost.Instance.Plugin.Host.comPort;
                comport.doARM(true, force);
                //ConnectToVideoStream(ref connection.tcpClient, ref connection.stream, Regex.Replace(connection.Vehicle.MavIP, " ", ""));
                //int index = connection.Vehicle.DayCameraPipline.IndexOf("port");
                //string port = connection.Vehicle.DayCameraPipline.Substring(index + 5, 4);

                //CpxHost.Instance.UI.restartVideo(Int32.Parse(port), false);
            }
            catch (Exception e)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_ARM);
                AppSettings.WriteToLog(Resources.ExceptionArm, e.Message);
            }
        }

        public static void Arm(bool force)
        {
            try
            {
                var comport = CpxHost.Instance.Plugin.Host.comPort;
                comport.doARM(true, force);

            }
            catch (Exception e)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_ARM);
                AppSettings.WriteToLog(Resources.ExceptionArm, e.Message);
            }
        }

        public static void Disarm(bool force, ref VehicleConnection connection)
        {
            try
            {
                var comport = CpxHost.Instance.Plugin.Host.comPort;
                comport.doARM(false, force);
                //DisconnectFromVideoStream(ref connection.tcpClient, ref connection.stream);
            }
            catch (Exception e)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_DISARM);
                AppSettings.WriteToLog(Resources.ExceptionDisarm, e.Message);
            }
        }

        public static void Disarm(bool force)
        {
            try
            {
                var comport = CpxHost.Instance.Plugin.Host.comPort;
                comport.doARM(false, force);
            }
            catch (Exception e)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_DISARM);
                AppSettings.WriteToLog(Resources.ExceptionDisarm, e.Message);
            }
        }

        internal static void TakeOff(float altitude)
        {
            var comport = CpxHost.Instance.Plugin.Host.comPort;
            SetGuidedMode();
            if (!comport.MAV.cs.armed)
            {
                Arm(false);
            }
            try
            {
                Thread.Sleep(4000);
                comport.doCommand(MAVLink.MAV_CMD.TAKEOFF, 0, 0, 0, 0, 0, 0, altitude, true);

            }
            catch (Exception e)
            {
                Core.CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_TAKEOFF);
                AppSettings.WriteToLog(Resources.ExceptionTakeoff, e.Message);
            }

        }

        public static void HideMainMenu()
        {
            var mainForm = CpxHost.Instance.MainForm;
            var mainMenu = mainForm.MainMenu;
            var mainMenuContainer = mainMenu.Parent;

            mainForm.SuspendLayout();
            mainMenuContainer.Dock = DockStyle.None;
            mainMenuContainer.Visible = false;
            mainForm.ResumeLayout();
        }

        public static void setChildIndex(Control control, int index)
        {
            CpxHost.Instance.FlightData.Controls.SetChildIndex(control, index);
        }

        public static int getChildIndex(Control control)
        {
            return CpxHost.Instance.FlightData.Controls.IndexOf(control);
        }

        public static void AddControlToFlightData(Control control)
        {
            CpxHost.Instance.FlightData.Controls.Add(control);
        }

        public static void RemoveControlFromFlightData(Control control)
        {
            CpxHost.Instance.FlightData.Controls.Remove(control);
        }

        public static Control FindMapZoomSlider()
        {
            return CpxHost.Instance.FlightData.FindFirstControl(x => x.Name == "TRK_zoom");
        }

        public static Control FindMapContainer()
        {
            return CpxHost.Instance.FlightData.FindFirstControl(x => x.Name == "tableMap");
        }
        public static Control FindMapContainerFlightPlanner()
        {
            return CpxHost.Instance.FlightPlanner.FindFirstControl(x => x.Name == "panelMap");
        }
        public static void ApplyTheme(Control control)
        {
            MissionPlanner.Utilities.ThemeManager.ApplyThemeTo(control);
        }

        public static Control FindControl(this Control rootControl, string name)
        {
            return rootControl.FindFirstControl(x => x.Name == name);
        }

        public static Control FindFirstControl(this Control rootControl, Func<Control, bool> condition)
        {
            foreach (Control child in rootControl.Controls)
            {
                if (condition(child))
                {
                    return child;
                }
            }

            foreach (Control child in rootControl.Controls.Cast<Control>().Where(x => x.HasChildren))
            {
                var ctrl = child.FindFirstControl(condition);

                if (ctrl != null)
                {
                    return ctrl;
                }

            }

            return null;
        }

        public static MAVLinkInterface ConnectMAV(Vehicle vehicle)
        {

            //var mav = new MAVLinkInterface();

            if (vehicle.Protocol == "UDPCl")
            {
                MainV2._connectionControl.CMB_serialport.Text = "UDPCl";
                var udc = new CpxUdpSerialConnect(vehicle.MavIP.Replace(" ", ""), vehicle.MavPort);
                udc.Port = vehicle.MavPort;
                udc.RemoteIpEndPoint = new IPEndPoint(IPAddress.Parse(vehicle.MavIP.Replace(" ", "")), int.Parse(vehicle.MavPort));
                Thread.Sleep(2000);
                udc.Open();
                MainV2.comPort.BaseStream = udc;
                MainV2.comPort.BaseStream.BaudRate = 115200;
                MainV2.comPort.BaseStream.PortName = "UDPCl" + int.Parse(vehicle.MavPort);
            }
            else if (vehicle.Protocol == "UDP")
            {
                var udp = new UdpSerial
                {
                    client = new UdpClient(int.Parse(vehicle.MavPort))
                };
                MainV2.comPort.BaseStream = udp;
            }
            else
            {
                var tc = new MissionPlanner.Comms.TcpSerial
                {
                    client = new TcpClient(vehicle.MavIP.Replace(" ", ""), int.Parse(vehicle.MavPort))

                };
                MainV2.comPort.BaseStream = tc;
            }

            MainV2.instance.BeginInvoke((Action)delegate
            {
                //MainV2.instance.doConnect(MainV2.comPort, "preset", "115200");
                MainV2.instance.doConnect(MainV2.comPort, "preset", "0", true);
                MainV2.Comports.Add(MainV2.comPort);
                DrawFence();
            });

            return MainV2.comPort;
        }

        public static void DrawFence()
        {
            try
            {
                if (MainV2.comPort.MAV.param.ContainsKey("FENCE_TOTAL") &&
                    int.Parse(MainV2.comPort.MAV.param["FENCE_TOTAL"].ToString()) > 1 &&
                    MainV2.comPort.MAV.param.ContainsKey("FENCE_ACTION"))
                {
                    CpxHost.Instance.FlightPlanner.FlightPlannerBase.GeoFencedownloadToolStripMenuItem_Click(null, null);
                }
            }
            catch (Exception e)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_DRAW_FENCE);
                AppSettings.WriteToLog(Resources.ExceptionDrawFence, e.Message);
            }
        }

        public static myGMAP GetMapControl()
        {
            return CpxHost.Instance.FlightData.gMapControl1;
        }

        public static void ReplaceMapContextMenu(Action openSelectionMethod)
        {
            CpxHost.Instance.FlightData.contextMenuStripMap.Opening += (s, e) =>
            {
                e.Cancel = true;
                openSelectionMethod();
            };
        }

        public static void ConnectMAVS(Vehicle[] vehicles)
        {
            var mavs = new ConcurrentBag<MAVLinkInterface>();


            Parallel.ForEach(vehicles, mavFile =>
           {
               var mav = new MAVLinkInterface();

               var tc = new TcpSerial
               {
                   client = new TcpClient(mavFile.MavIP.Replace(" ", ""), int.Parse(mavFile.MavPort))
               };
               mav.BaseStream = (MissionPlanner.Comms.ICommsSerial)tc;

               mavs.Add(mav);

           });

            foreach (var mav in mavs)
            {
                MainV2.instance.BeginInvoke((Action)delegate
                {
                    MainV2.instance.doConnect(mav, "preset", "0", false);
                    MainV2.Comports.Add(mav);
                    Dictionary<string, double> hel = mav.getParamList(mav.MAV.sysid, mav.MAV.compid);
                });
            }
        }


        public static object InvokePrivateMethod(this object o, string methodName, params object[] args)
        {
            var mi = o.GetType().GetMethod(methodName, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            if (mi != null)
            {
                return mi.Invoke(o, args);
            }
            return null;
        }
        public static void InvokeInUIThread<T>(this T control, Action<T> action) where T : Control
        {
            if (control.IsHandleCreated)
            {
                control.Invoke(action, control);
            }
            else
            {
                control.HandleCreated += (s, e) =>
                {
                    control.Invoke(action, control);
                };
            }
        }
        public static void SafeInvoke(this Control uiElement, Action updater, bool forceSynchronous)
        {
            if (uiElement == null)
            {
                throw new ArgumentNullException("uiElement");
            }

            if (uiElement.InvokeRequired)
            {
                if (forceSynchronous)
                {
                    uiElement.Invoke((Action)delegate { SafeInvoke(uiElement, updater, forceSynchronous); });
                }
                else
                {
                    uiElement.BeginInvoke((Action)delegate { SafeInvoke(uiElement, updater, forceSynchronous); });
                }
            }
            else
            {
                if (uiElement.IsDisposed)
                {
                    throw new ObjectDisposedException("Control is already disposed.");
                }

                updater();
            }
        }

        private static void ClearCommands()
        {
            var flightPlanner = CpxHost.Instance.FlightPlanner;

            //var grd = flightPlanner.FindControl("Commands") as DataGridView;
            var grd = CpxHost.Instance.FlightPlanner.Commands;
            // mono fix
            try { grd.CurrentCell = null; }
            catch (Exception e) { }
            grd.Rows.Clear();
            flightPlanner.FlightPlannerBase.writeKML();
        }
        private static void FillCommands(FlightRoute flightRoute)
        {
            var flightPlanner = CpxHost.Instance.FlightPlanner;

            ClearCommands();

            MainV2.comPort.MAV.cs.HomeLocation.Lat = flightRoute.HomeLocation.Lat;
            MainV2.comPort.MAV.cs.HomeLocation.Lng = flightRoute.HomeLocation.Lng;
            MainV2.comPort.MAV.cs.HomeLocation.Alt = flightRoute.HomeLocation.Alt;

            flightPlanner.FlightPlannerBase.updateHome();

            foreach (var item in flightRoute.Commands)
            {
                flightPlanner.FlightPlannerBase.AddCommand((MAVLink.MAV_CMD)item.Command, item.Param1,
                    item.Param2, item.Param3, item.Param4, item.Lng, item.Lat, item.Alt);
            }
            flightPlanner.FlightPlannerBase.BUT_write_Click(null, EventArgs.Empty);
        }
        public static void SetGuidedMode()
        {
            try
            {
                CpxHost.Instance.Plugin.Host.comPort.setMode("GUIDED");
            }
            catch (Exception e)
            {
                Core.CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_GUIDED_MODE);
                AppSettings.WriteToLog(Resources.ExceptionGuidedMode, e.Message);
            }
        }
        public static void SetLandMode()
        {
            try
            {
                CpxHost.Instance.Plugin.Host.comPort.setMode("LAND");
            }
            catch (Exception e)
            {
                Core.CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_LAND_MODE);
                AppSettings.WriteToLog(Resources.ExceptionLandMode, e.Message);
            }

        }
        public static void SetLoiterMode()
        {
            try
            {
                CpxHost.Instance.Plugin.Host.comPort.setMode("LOITER");
            }
            catch (Exception e)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_LOITER_MODE);
                AppSettings.WriteToLog(Resources.ExcpetionLoiterMode, e.Message);
            }

        }
        public static void SetRtlMode()
        {
            try
            {
                CpxHost.Instance.Plugin.Host.comPort.setMode("RTL");
            }
            catch (Exception e)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_RTL_MODE);
                AppSettings.WriteToLog(Resources.ExceptionRTLMode, e.Message);
            }
        }
        public static void SetAutoMode()
        {
            try
            {
                CpxHost.Instance.Plugin.Host.comPort.setMode("AUTO");
            }
            catch (Exception e)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_AUTO_MODE);
                AppSettings.WriteToLog(Resources.ExceptionAutoMode, e.Message);
            }
        }

        public static void LoadPlanToPlanner(FlightRoute flightRoute)
        {
            var flightPlanner = CpxHost.Instance.FlightPlanner;

            if (!flightPlanner.IsHandleCreated)
            {
                flightPlanner.HandleCreated += (s1, e1) =>
                {
                    FillCommands(flightRoute);
                };
                var handle = flightPlanner.Handle;
            }
            else
            {
                FillCommands(flightRoute);
            }
        }
        public static FlightRoute ReadFlightPlanner()
        {
            var flightRoute = new FlightRoute();

            var flightPlanner = CpxHost.Instance.FlightPlanner;

            var grd = flightPlanner.Commands;
            var cmbAltMode = flightPlanner.CMB_altmode;
            var txtHomeLat = flightPlanner.TXT_homelat;
            var txtHomeLng = flightPlanner.TXT_homelng;
            var txtHomeAlt = flightPlanner.TXT_homealt;

            var altMode = (MAVLink.MAV_FRAME)((int)cmbAltMode.SelectedValue);
            var homeLocation = new MavCommand
            {
                Command = (ushort)MAVLink.MAV_CMD.WAYPOINT,
                Lat = (double.Parse(txtHomeLat.Text)),
                Lng = (double.Parse(txtHomeLng.Text)),
                Alt = (float.Parse(txtHomeAlt.Text) / CurrentState.multiplierdist)
            };



            var cols = grd.Columns.Cast<DataGridViewColumn>().ToDictionary(x => x.Name, y => y.Index);
            var commands = new List<MavCommand>();

            // read grid
            for (int index = 0; index < grd.Rows.Count - 0; index++)
            {
                var cmd = new MavCommand();
                var row = grd.Rows[index];

                if (row.Cells[0].Value.ToString() == "UNKNOWN")
                {
                    cmd.Command = (ushort)row.Cells[cols["Command"]].Tag;
                }
                else
                {
                    cmd.Command =
                    (ushort)
                        (MAVLink.MAV_CMD)
                            Enum.Parse(typeof(MAVLink.MAV_CMD), row.Cells[cols["Command"]].Value.ToString());
                }

                cmd.Param1 = double.Parse(row.Cells[cols["Param1"]].Value.ToString());
                cmd.Param2 = double.Parse(row.Cells[cols["Param2"]].Value.ToString());
                cmd.Param3 = double.Parse(row.Cells[cols["Param3"]].Value.ToString());
                cmd.Param4 = double.Parse(row.Cells[cols["Param4"]].Value.ToString());
                cmd.Lat = double.Parse(row.Cells[cols["Lat"]].Value.ToString());
                cmd.Lng = double.Parse(row.Cells[cols["Lon"]].Value.ToString());
                cmd.Alt = double.Parse(row.Cells[cols["Alt"]].Value.ToString());

                commands.Add(cmd);
            }



            flightRoute.Commands = commands;
            flightRoute.HomeLocation = homeLocation;
            flightRoute.AltMode = altMode;

            return flightRoute;
        }


        public static bool IsInFlight(this CurrentState state)
        {
            if ((state.ch3percent > 12 || state.groundspeed > 3.0) && state.armed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static Control FindHud()
        {
            var y = CpxHost.Instance.FlightData.MainHcopy.FindFirstControl(x => x.Name == "hud1");
            return y;
        }
        public static Control FindCoordsCMB()
        {
            var y = CpxHost.Instance.FlightData.MainHcopy.FindFirstControl(x => x.Name == "coords1");
            var z = y.FindFirstControl(x => x.Name == "CMB_coordsystem");
            return z;
        }
        public static Control FindSubMainLeft()
        {
            var y = CpxHost.Instance.FlightData.MainHcopy.FindFirstControl(x => x.Name == "SubMainLeft");
            return y;
        }
    }
}
