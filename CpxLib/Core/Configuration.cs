﻿using CpxLib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CpxLib.Core
{
    public class Configuration
    {
        public Configuration()
        {
            LayoutMode = 1;
        }


        /// <summary>
        /// 
        /// </summary>
        public string GStreamerDir { get; set; }


        /// <summary>
        /// 1 - CopterPix
        /// 2 - Default
        /// </summary>
        public int LayoutMode { get; set; }
        public string LTEAddress { get; internal set; }
        public string LandingStationIP { get; internal set; }
        public string LandingStationPort { get; internal set; }
        public string VideoControlPath { get; set; }
        public string OpenVPNPath { get; set; }
        public int FailsafeTimeout { get; set; }
        public void Deserialize(string source)
        {
            var doc = XDocument.Parse(source);

            var copterPixConfig = doc.Descendants("CopterPix").FirstOrDefault();



            GStreamerDir = copterPixConfig.GetAttributeValue("GStreamerDir");
            LayoutMode = Convert.ToInt32(copterPixConfig.GetAttributeValue("LayoutMode", "1"));
            LTEAddress = copterPixConfig.GetAttributeValue("LTEAddress", "");
            LandingStationIP = copterPixConfig.GetAttributeValue("LandingStationIP", "");
            LandingStationPort = copterPixConfig.GetAttributeValue("LandingStationPort", "");
            VideoControlPath = copterPixConfig.GetAttributeValue("VideoControlPath", "");
            OpenVPNPath = copterPixConfig.GetAttributeValue("OpenVPNPath", "");
            FailsafeTimeout = Convert.ToInt32(copterPixConfig.GetAttributeValue("FailsafeTimeout", "3"));


        }

        public string Serialize()
        {

            var document = new XDocument(new XElement("Settings",
                       new XElement("CopterPix", new XAttribute("GStreamerDir", GStreamerDir),
                                                 new XAttribute("LayoutMode", LayoutMode),
                                                 new XAttribute("LTEAddress", LTEAddress),
                                                 new XAttribute("LandingStationIP", LandingStationIP),
                                                 new XAttribute("LandingStationPort", LandingStationPort),
                                                 new XAttribute("VideoControlPath", VideoControlPath),
                                                 new XAttribute("OpenVPNPath", OpenVPNPath),
                                                 new XAttribute("FailsafeTimeout",FailsafeTimeout)
                                               )
                                               ));
            return document.ToString();
        }
    }
}
