﻿using MissionPlanner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Core
{
    public class DisplayTelemetryData
    {
        public float alt;
        public float altasl;
        public double lng;
        public double lat;
        public float distToWP;
        public float distToMAV;
        public float gimbalLAT;
        public float gimbalLng;
        public string mode;
        public float satcount;
        public int battery_remaining;
        public double battery_voltage;
        internal float wpno;
        internal bool isArmed;
        internal MAVLinkInterface comport;
        internal float timeinair;
        internal float groundspeed;
        internal float gpsstatus2;
        internal float gpsstatus;
        internal bool isinflight;
        public bool isInCharging=false;
    }
}
