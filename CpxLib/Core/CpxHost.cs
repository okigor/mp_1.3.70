﻿using DirectShow;
using MissionPlanner;
using MissionPlanner.GCSViews;
using MissionPlanner.Utilities;
using OpenTK.Platform;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebCamService;

namespace CpxLib.Core
{
    public class CpxHost
    {
        private UIController _ui = null;
        private CpxPlugin _plugin = null;
        private static CpxHost _host = null;

        public event EventHandler<FlightStatusEventArgs> FlightStatusChanged;
        public event EventHandler<NewImageEventArgs> NewImage;


        public static void Create(CpxPlugin plugin)
        {
            _host = new CpxHost(plugin);
        }
        private CpxHost(CpxPlugin plugin)
        {
            _plugin = plugin;
        }


        /// <summary>
        /// referernce to the Main form instance
        /// </summary>
        public MainV2 MainForm
        {
            get
            {
                return _plugin.Host.MainForm;
            }
        }
        public FlightData FlightData
        {
            get
            {
                return _plugin.Host.MainForm.FlightData;
            }
        }
        public MAVLinkInterface ComPort
        {
            get
            {
                return _plugin.Host.comPort;
            }
        }

        public FlightPlanner FlightPlanner
        {
            get
            {
                return _plugin.Host.MainForm.FlightPlanner;
            }
        }

        public static CpxHost Instance
        {
            get
            {
                return _host;
            }
        }

        /// <summary>
        /// reference to the current plugin instance
        /// </summary>
        public CpxPlugin Plugin
        {
            get
            {
                return _plugin;
            }
        }
        public UIController UI
        {
            get
            {
                if (_ui == null)
                {
                    _ui = new UIController();
                }

                return _ui;
            }

        }


        public bool IsRightToLeftLayout
        {
            get
            {
                return Language.ToLower() == "he-il";
            }
        }
        public string Language
        {
            get
            {
                return Properties.Settings.Default.Language;
            }
            set
            {
                Properties.Settings.Default.Language = value;
            }
        }


        public CancellationTokenSource StartGStreamer(string pipeline, string name)
        {

            var videoCancelationToken = new CancellationTokenSource();
            // CpxGStreamer.StartA("udpsrc port=5600 buffer-size=90000 ! application/x-rtp ! rtph264depay ! avdec_h264 ! queue leaky=2 ! videoconvert ! video/x-raw,format=BGRA ! appsink name=outsink sync=false", "Day");
            return videoCancelationToken;

        }

        public async void TelemetryLoop()
        {
            await Task.Delay(7000);

            await Task.Factory.StartNew(() =>
            {
                var currentTelemetry = default(DisplayTelemetryData);

                while (true)
                {
                    if (FlightStatusChanged == null) continue;

                    Task.Delay(500);
                    var host = _plugin.Host;
                    if (!host.comPort.MAV.cs.connected) continue;
                    var telemetry = new DisplayTelemetryData()
                    {
                        alt = host.cs.alt,
                        distToWP = host.cs.wp_dist,
                        distToMAV = host.cs.DistToHome,
                        gimbalLAT = host.cs.gimballat,
                        gimbalLng = host.cs.gimballng,
                        lat = host.cs.lat,
                        lng = host.cs.lng,
                        mode = host.cs.mode,
                        battery_remaining = host.cs.battery_remaining,
                        battery_voltage = host.cs.battery_voltage,
                        wpno = host.cs.wpno,
                        isArmed = host.cs.armed,
                        comport = host.comPort
                    };

                    if (HasDataChanged(currentTelemetry, telemetry))
                    {
                        currentTelemetry = telemetry;

                        OnDataChanged(telemetry);
                    }
                }
            });
        }

        private void OnDataChanged(DisplayTelemetryData data)
        {
            if (FlightStatusChanged != null)
            {
                var args = new FlightStatusEventArgs(data);

                FlightStatusChanged(this, args);
            }
        }

        private bool HasDataChanged(DisplayTelemetryData oldData, DisplayTelemetryData newData)
        {
            if (oldData == null && newData != null) return true;

            if (oldData != null && newData == null) return true;

            if (oldData == null && newData == null) return false;

            return oldData.alt != newData.alt ||
                     oldData.distToWP != newData.distToWP ||
                     oldData.distToMAV != newData.distToMAV ||
                     oldData.gimbalLAT != newData.gimbalLAT ||
                     oldData.gimbalLng != newData.gimbalLng ||
                     oldData.lat != newData.lat ||
                     oldData.lng != newData.lng ||
                     oldData.mode != newData.mode ||
                     oldData.battery_remaining != newData.battery_remaining ||
                     oldData.battery_voltage != newData.battery_voltage ||
                     oldData.wpno != newData.wpno ||
                     oldData.isArmed != newData.isArmed;
        }
        public void OnNewImage(Image image, string name)
        {
            if (NewImage != null)
            {
                NewImage.Invoke(this, new NewImageEventArgs(image, name));
            }
        }
    }
}
