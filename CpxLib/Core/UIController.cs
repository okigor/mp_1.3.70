﻿using CpxLib.Controls;
using CpxLib.Controls.AppControls;
using CpxLib.Controls.Common;
using CpxLib.Forms;
using CpxLib.Forms.Test;
using CpxLib.LTE;
using CpxLib.Model;
using CpxLib.Properties;
using CpxLib.Scheduling;
using GeoUtility.GeoSystem;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using MissionPlanner;
using MissionPlanner.Controls;
using MissionPlanner.GCSViews;
using MissionPlanner.Joystick;
using MissionPlanner.Utilities;
using System;
using System.Device;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static CpxLib.Controls.AppControls.CpxJoystick2;
using System.Device.Location;
using static MAVLink;
using System.Timers;

namespace CpxLib.Core
{
    public class UIController
    {
        #region Declarations
        MAVLink.mavlink_attitude_t attitude;
        private string coordsType;

        private ZoomMapTracker _slider;
        private ToolStripButton _savePathButton;
        private ToolStripButton _loadPathButton;
        private ToolStripButton _settingsButton;
        private CpxButton _messagesButton;
        private CpxButton _cameraProperties;
        private CpxInfo _telemetryPanel;
        private CpxToolbar _flightToolbar;
        private Control _flightDataMapContainer;
        private Control _flightPlanner;
        private FlightRoute _currentFlightRoute;
        private readonly List<VehicleConnection> _connections = new List<VehicleConnection>();
        private GMapOverlay Markers;
        private myGMAP map2;
        private CpxVideoWin _videoOut;
        private Panel _videoControl;
        private LandingStationCtrl LandingStationCtrl;
        private CancellationTokenSource _monitorLTELoopCancellationToken;
        private volatile CancellationTokenSource _telemetryLoopCancellationToken;
        private Dictionary<ScheduleRunner, CancellationTokenSource> scheduleCanceltokenDictionary;
        private Panel _controlContainer;
        private TimePanel _flightTimePanel;
        //private CpxJoystick2 _joystick;
        private Task _monitorLTELoopTask;
        private Task _telemtryLoopTask;
        private CpxLabel _locationLabel;
        private CpxBallonPanel _message;
        private PointLatLng _mapMouseDownPosition;
        private CpxCameraProperties _winCameraProperties;
        private CpxNextVision _winNextVision;
        private Process videoProcess;
        private bool gps;
        private bool isBatteryUpdated;
        public bool enableStopCharging = true;
        private double maxBattery;
        private double minBattery;
        private const double conntinueChargingBattery = 7.8;
        private string[] LastThreeMessages;
        CpxMissionPlanning cpxMissionPlanning;
        private HUD hud;
        private SplitContainer sc;
        private System.Timers.Timer flightTime;
        int timer;
        DateTime armedTime;
        #endregion

        #region View Actions Additions
        public void Run()
        {
            timer = 0;
            flightTime = new System.Timers.Timer(1000);
            flightTime.Elapsed += FlightTime_Elapsed;
            ComboBox coordsCMB = (ComboBox)UIUtils.FindCoordsCMB();
            coordsType = coordsCMB.SelectedValue.ToString();
            coordsCMB.SelectedIndexChanged += CoordsCMB_SelectedIndexChanged;
            attitude = new MAVLink.mavlink_attitude_t();
            gps = false;
            scheduleCanceltokenDictionary = new Dictionary<ScheduleRunner, CancellationTokenSource>();
            var config = AppSettings.LoadConfiguration();
            LastThreeMessages = new string[] { "", "", "" };
            if (config.LayoutMode == 1 || config.LayoutMode == 10)
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(CpxHost.Instance.Language);
                RenderCopterPixLayout(config.LayoutMode);

                var retval = UIUtils.EnableJoystick();
                ShowMessage(retval);

                StartTelemetryLoop();
                StartMonitorLTELoop();
                SubscribeMapMouseDownEvent();
                map2 = UIUtils.GetMapControl();
                Markers = new GMapOverlay();
                map2.Overlays.Add(Markers);
            }
            else
            {
                RenderDefaultLayout();
            }
        }

        private void CoordsCMB_SelectedIndexChanged(object sender, EventArgs e)
        {


            ComboBox cmb = (ComboBox)sender;
            coordsType = cmb.SelectedValue.ToString();
        }

        private void FlightTime_Elapsed(object sender, ElapsedEventArgs e)
        {
            timer++;
            _flightTimePanel.UpdateFlightTime(timer);
        }

        public void Stop()
        {
            if (_monitorLTELoopCancellationToken != null)
            {
                _monitorLTELoopCancellationToken.Cancel();
            }
            if (_telemetryLoopCancellationToken != null)
            {
                _telemetryLoopCancellationToken.Cancel();
            }
            //TODO: VIDEO CONTROL STOP PROCESS
            if (_videoControl != null)
            {
                //  _videoControl.Stop();

            }

            Task.Delay(5000);
        }
        private bool HasDataChanged(DisplayTelemetryData oldData, DisplayTelemetryData newData)
        {
            if (oldData == null && newData != null) return true;

            if (oldData != null && newData == null) return true;

            if (oldData == null && newData == null) return false;

            return oldData.alt != newData.alt ||
                     oldData.distToWP != newData.distToWP ||
                     oldData.distToMAV != newData.distToMAV ||
                     oldData.gimbalLAT != newData.gimbalLAT ||
                     oldData.gimbalLng != newData.gimbalLng ||
                     oldData.lat != newData.lat ||
                     oldData.lng != newData.lng ||
                     oldData.mode != newData.mode ||
                     oldData.battery_remaining != newData.battery_remaining ||
                     oldData.battery_voltage != newData.battery_voltage ||
                     oldData.wpno != newData.wpno ||
                     oldData.isArmed != newData.isArmed ||
                     oldData.altasl != newData.altasl ||
                     oldData.satcount != newData.satcount ||
                     oldData.timeinair != newData.timeinair ||
                     oldData.groundspeed != newData.groundspeed ||
                     oldData.gpsstatus != newData.gpsstatus ||
                     oldData.gpsstatus2 != newData.gpsstatus2 ||
                     oldData.isinflight != newData.isinflight
                     ;
        }
        private void MonitorLTELoop(CancellationToken token)
        {
            HuaweiLTE LTEConnection = new HuaweiLTE();
            CpxMobilicom cpxMobilicom = null;
            while (true)
            {
                if (token.IsCancellationRequested)
                    break;

                Thread.Sleep(500);

                var comport = CpxHost.Instance.Plugin.Host.comPort;

                if (comport == null) continue;

                var connection = _connections.FirstOrDefault(x => x.ComPort == comport);

                if (connection == null) continue;
                if (!connection.ComPort.MAV.cs.connected) continue;
                if (connection.Vehicle.CommunicationType == "WIFI")
                {
                    try
                    {
                        var state = WifiUtilities.GetNetworkState(connection.Vehicle.WirelessName);
                        _telemetryPanel.UpdateWifiState(state);

                    }
                    catch (Exception e)
                    {
                        ShowMessage(CpxConstants.MSG_WIFI_UNAVAILBLE);
                        AppSettings.WriteToLog(Resources.MessageWifiUnavalable, e.Message);
                    }
                }
                else if (connection.Vehicle.CommunicationType == "Mobilicom" && connection.ComPort.MAV.cs.connected)
                {
                    if (cpxMobilicom == null)
                        cpxMobilicom = new CpxMobilicom(connection.Vehicle.MobilicomIP, 1234, connection.Vehicle.MobilicomAirIP, 1234);
                    int ans = cpxMobilicom.getSignalStrength();
                    //MessageBox.Show(ans + "");
                    _telemetryPanel.UpdateMobilicomInfo(ans);

                }
                else
                {
                    try
                    {
                        var status = LTEConnection.GetStatus();
                        _telemetryPanel.UpdateLTEInfo(status);
                    }
                    catch (Exception e)
                    {
                        ShowMessage(CpxConstants.MSG_COMMUNICATION_PROBLEMS);
                        AppSettings.WriteToLog(Resources.MessageCommunicationProblems, e.Message);
                    }
                }
            }
        }
        private async void StartMonitorLTELoop()
        {
            await Task.Delay(7000);
            _monitorLTELoopCancellationToken = new CancellationTokenSource();
            _monitorLTELoopTask = new Task(() => MonitorLTELoop(_monitorLTELoopCancellationToken.Token),
                                                     _monitorLTELoopCancellationToken.Token,
                                                     TaskCreationOptions.LongRunning | TaskCreationOptions.PreferFairness);
            _monitorLTELoopTask.Start();
        }
        private async void StartTelemetryLoop()
        {
            await Task.Delay(7000);
            CpxHost.Instance.Plugin.Host.comPort.requestDatastream(MAVLink.MAV_DATA_STREAM.EXTRA1, 2, 71, 67);
            CpxHost.Instance.Plugin.Host.comPort.requestDatastream(MAVLink.MAV_DATA_STREAM.EXTENDED_STATUS, 2, 1, 25);
            CpxHost.Instance.Plugin.Host.comPort.OnPacketReceived += MavOnPacketReceived;
            _telemetryLoopCancellationToken = new CancellationTokenSource();
            _telemtryLoopTask = new Task(() => TelemetryLoop(_telemetryLoopCancellationToken.Token),
                                                     _telemetryLoopCancellationToken.Token,
                                                     TaskCreationOptions.LongRunning | TaskCreationOptions.PreferFairness);
            _telemtryLoopTask.Start();

        }
        private void MavOnPacketReceived(object sender, MAVLink.MAVLinkMessage e)
        {
            MAVLink.MAVLinkMessage mAVLinkMessage = CpxHost.Instance.ComPort.MAVlist[1, 25].getPacket((uint)MAVLink.MAVLINK_MSG_ID.STATUSTEXT);
            if (mAVLinkMessage != null)
            {
                MAVLink.mavlink_statustext_t statustext = mAVLinkMessage.ToStructure<MAVLink.mavlink_statustext_t>();
                string result = System.Text.Encoding.UTF8.GetString(statustext.text);
                if (result.Substring(0, result.IndexOf('\n')) != LastThreeMessages[0] || result.Substring(0, result.IndexOf('\n')) != LastThreeMessages[1] || result.Substring(0, result.IndexOf('\n')) != LastThreeMessages[2])
                    ShowMessage(1, result.Substring(0, result.IndexOf('\n')));

            }
            UpdateGimbalPointing(CpxHost.Instance.Plugin.Host);
        }


        //TODO: CHECK HOW TO SHORT
        private void TelemetryLoop(CancellationToken token)
        {
            bool WasArmed = false;
            bool StopTimer = true;
            int failsafeTimout = AppSettings.LoadConfiguration().FailsafeTimeout;
            DateTime startTime = new DateTime();
            var currentTelemetry = default(DisplayTelemetryData);
            var point = new Geographic();
            var host = CpxHost.Instance.Plugin.Host;
            var connected = host.comPort.MAV.cs.connected;
            int batteryRemain;
            CpxMobilicom cpxMobilicom = null;
            EnableControls(host.comPort.MAV.cs);

            while (true)
            {
                if (token.IsCancellationRequested)
                    break;

                Thread.Sleep(500);
                if (host.cs.ekfstatus <= 0.8)
                    gps = false;
                else
                {
                    if (gps == false)
                    {
                        gps = true;
                    }
                }
                //MessageBox.Show(isBatteryUpdated.ToString());
                if (isBatteryUpdated)
                    batteryRemain = (int)((host.cs.battery_voltage - minBattery) / (maxBattery - minBattery) * 100);
                else
                    batteryRemain = 0;
                //check if the battery voltage is at the maximum
                var connection = _connections.FirstOrDefault(x => x.ComPort == host.comPort);
                if (connection != null && connection.Vehicle.CommunicationType == "cpxMobilicom")
                {
                    if (cpxMobilicom == null)
                        cpxMobilicom = new CpxMobilicom(connection.Vehicle.MobilicomIP, 1234, connection.Vehicle.MobilicomAirIP, 1234);
                }
                if (connection != null && connection.IsLandingStationDefined() && host.cs.battery_voltage >= maxBattery && enableStopCharging)
                {
                    connection.ConnectLandingStation();
                    connection.StopLandingStation();
                    enableStopCharging = false;
                }
                if (host.cs.battery_voltage < maxBattery)
                    enableStopCharging = true;
                if (WasArmed != host.comPort.MAV.cs.armed)
                {
                    if (!WasArmed)
                    {
                        StopTimer = false;
                        _flightTimePanel.UpdateFlightTime(0);
                        timer = 0;
                        armedTime = DateTime.Now;
                        //flightTime.Start();
                        //armedTime = DateTime.Now;
                    }
                    else
                    {
                        StopTimer = true;
                        //flightTime.Stop();
                    }
                    WasArmed = host.comPort.MAV.cs.armed;
                }
                if (connected != host.comPort.MAV.cs.connected)
                {
                    if (!host.comPort.MAV.cs.connected)
                    {
                        startTime = DateTime.Now;
                    }
                    else
                    {
                        startTime = new DateTime();
                    }
                    EnableControls(host.comPort.MAV.cs);
                    connected = host.comPort.MAV.cs.connected;
                }
                if (!host.comPort.MAV.cs.connected)
                {
                    if (startTime.AddSeconds(failsafeTimout) >= DateTime.Now)
                    {
                        if (host.comPort.MAV.cs.mode == "RTL")
                        {
                            if (connection != null && connection.IsLandingStationDefined())
                            {
                                connection.ConnectLandingStation();
                                connection.StopLandingStation();
                            }

                        }
                    }
                    continue;
                }
                var telemetry = new DisplayTelemetryData()
                {
                    alt = host.cs.alt,
                    distToWP = host.cs.wp_dist,
                    distToMAV = host.cs.DistToHome,
                    gimbalLAT = host.cs.gimballat,
                    gimbalLng = host.cs.gimballng,
                    lat = host.cs.lat,
                    lng = host.cs.lng,
                    mode = host.cs.mode,
                    battery_remaining = batteryRemain,
                    battery_voltage = host.cs.battery_voltage,
                    wpno = host.cs.wpno,
                    isArmed = host.cs.armed,
                    comport = host.comPort,
                    altasl = host.cs.altasl,
                    satcount = host.cs.satcount,
                    timeinair = host.cs.timeInAirMinSec,
                    groundspeed = host.cs.groundspeed,
                    gpsstatus = host.cs.gpsstatus,
                    gpsstatus2 = host.cs.gpsstatus2,
                };
                //MessageBox.Show(telemetry.alt.ToString());
                telemetry.isinflight = host.cs.IsInFlight();
                if (host.cs.messageHigh != "" && host.cs.messageHigh != LastThreeMessages[0])
                {
                    ShowMessage(CpxConstants.MESSAGE_HIGH, host.cs.messageHigh);
                }
                if (HasDataChanged(currentTelemetry, telemetry))
                {
                    EnableControls(host.comPort.MAV.cs);
                    currentTelemetry = telemetry;
                    _telemetryPanel.UpdateInfo(telemetry);
                    _locationLabel.SafeInvoke(() =>
                    {
                        point.Latitude = telemetry.lat;
                        point.Longitude = telemetry.lng;

                        if (point.Latitude > 84 || point.Latitude < -80 || point.Longitude >= 180 || point.Longitude <= -180)
                            return;

                        if (coordsType == "UTM")
                        {
                            PointLatLngAlt pointLatLngAlt = new PointLatLngAlt(point.Latitude, point.Longitude);
                            double[] utm = pointLatLngAlt.ToUTM();
                            if (pointLatLngAlt.GetUTMZone() > 0)
                                _locationLabel.Text = pointLatLngAlt.GetUTMZone() + "R " + Math.Round(utm[1], 0) + " " + Math.Round(utm[0], 0);
                            else
                                _locationLabel.Text = Math.Abs(pointLatLngAlt.GetUTMZone()) + "H " + Math.Round(utm[1], 0) + " " + Math.Round(utm[0], 0);
                        }
                        else
                            _locationLabel.Text = point.ToString();
                    }, true);
                }
                if (!StopTimer)
                    _flightTimePanel.UpdateFlightTime((DateTime.Now - armedTime).TotalSeconds);
                //UpdateGimbalPointing(host);
                //if(!StopTimer)
                //_flightTimePanel.UpdateFlightTime((DateTime.Now-armedTime).TotalSeconds);

                //if (!host.cs.IsInFlight() && host.cs.timeInAir != 0 && !connection.inFlight)
                //{
                //    _flightTimePanel.UpdateFlightTime(0);
                //    host.cs.timeInAir = 0;
                //    host.cs.timeSinceArmInAir = 0;

                //}
            }
        }
        private void UpdateGimbalPointing(MissionPlanner.Plugin.PluginHost host)
        {
            MAVLink.MAVLinkMessage mAVLinkMessage = host.comPort.MAVlist[1, 154].getPacket((uint)MAVLink.MAVLINK_MSG_ID.ATTITUDE);
            if (mAVLinkMessage != null)
            {
                var connection = _connections.FirstOrDefault(x => x.ComPort == host.comPort);
                attitude = mAVLinkMessage.ToStructure<MAVLink.mavlink_attitude_t>();
                double yaw_d = host.comPort.MAV.cs.yaw * Math.PI / 180 + Math.PI;

                double lat = host.comPort.MAV.cs.lat;
                double lng = host.comPort.MAV.cs.lng;

                double alt_z = host.comPort.MAV.cs.alt;
                //double alt_z = 100;
                if (alt_z < 0)
                    alt_z = 0;

                double pitch = attitude.pitch;
                double roll = attitude.roll;
                //double yaw_g = attitude.yaw;
                double temp = pitch - Double.Parse(connection.Vehicle.CameraDeviation) * Math.PI / 180 - Math.PI / 2;
                double d = alt_z * Math.Tan(temp);
                double dn = Math.Cos(yaw_d) * d;
                double de = Math.Sin(yaw_d) * d;
                double R = 6378137;
                double dLat = dn / R;
                double dLon = de / (R * Math.Cos(Math.PI * lat / 180));
                double lat_g = lat + dLat * 180 / Math.PI;
                double lng_g = lng + dLon * 180 / Math.PI;
                PointLatLng point = new PointLatLng(lat_g, lng_g);
                GMarkerGoogle m = new GMarkerGoogle(point, GMarkerGoogleType.lightblue);
                Markers.Markers.Clear();
                GMapMarkerRect gMapMarker = new GMapMarkerRect(point);
                {

                    gMapMarker.InnerMarker = m;
                    try
                    {
                        gMapMarker.wprad =
                            (int)(MissionPlanner.Utilities.Settings.Instance.GetFloat("TXT_WPRad") / CurrentState.multiplierdist);
                    }
                    catch
                    {
                    }
                    gMapMarker.Color = Color.Black;
                    if (coordsType == "UTM")
                    {
                        PointLatLngAlt pointLatLngAlt = new PointLatLngAlt(lat_g, lng_g);
                        double[] utm = pointLatLngAlt.ToUTM();
                        gMapMarker.ToolTipText = pointLatLngAlt.GetUTMZone() + "R " + Math.Round(utm[1], 0) + "," + Math.Round(utm[0], 0);
                        gMapMarker.Tag = Math.Round(utm[1], 0) + "," + Math.Round(utm[0], 0);
                    }
                    else
                    {
                        gMapMarker.ToolTipText = Math.Round(lat_g, 6) + "," + Math.Round(lng_g, 6);
                        gMapMarker.Tag = Math.Round(lat_g, 6) + "," + Math.Round(lng_g, 6);
                    }
                    //gMapMarker.ToolTipText = Math.Round(lat_g, 6) + "," + Math.Round(lng_g, 6);
                    //gMapMarker.Tag = Math.Round(lat_g, 6) + "," + Math.Round(lng_g, 6);
                    //gMapMarker.
                    gMapMarker.ToolTipMode = MarkerTooltipMode.Always;
                }
                Markers.Markers.Add(m);
                Markers.Markers.Add(gMapMarker);
            }
        }
        private void OpenMapActions()
        {
            var frm = new frmMapActions();
            FormCollection fc = Application.OpenForms;

            foreach (Form fr in fc)
            {
                //iterate through
                if (fr.Name == "frmMapActions")
                {
                    fr.Close();
                }
            }
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                switch (frm.MapAction)
                {

                    case frmMapActions.MapActionEnum.FlightToHere:
                        DoFlightToHere();
                        break;
                    case frmMapActions.MapActionEnum.PointCameraHere:
                        DoPointCameraHere();
                        break;
                    case frmMapActions.MapActionEnum.UnPointCamera:
                        DoUnPointCameraHere();
                        break;
                    case frmMapActions.MapActionEnum.NewPointCamera:
                        DoNewPointCamera();
                        break;
                    default:
                        break;
                }
            }
        }

        private void DoNewPointCamera()
        {
            CpxHost.Instance.FlightData.InvokePrivateMethod("pointCameraHereToolStripMenuItem_Click", null, EventArgs.Empty);
            //MainV2.comPort.setParam("MNT_MODE", 4);
            //MAVLink.mavlink_attitude_t attitude = mAVLinkMessage.ToStructure<MAVLink.mavlink_attitude_t>();
            var firstCoord = new GeoCoordinate(CpxHost.Instance.Plugin.Host.comPort.MAV.cs.lat, CpxHost.Instance.Plugin.Host.comPort.MAV.cs.lng);
            var secondCoord = new GeoCoordinate(_mapMouseDownPosition.Lat, _mapMouseDownPosition.Lng);
            var connection = _connections.FirstOrDefault(x => x.ComPort == CpxHost.Instance.ComPort);
            var pitch = Math.Atan(firstCoord.GetDistanceTo(secondCoord));
            if (CpxHost.Instance.Plugin.Host.comPort.MAV.cs.alt > 1)
                pitch = Math.Atan(firstCoord.GetDistanceTo(secondCoord) / CpxHost.Instance.Plugin.Host.comPort.MAV.cs.alt);
            //var pointing = pitch - Double.Parse(connection.Vehicle.CameraDeviation) * Math.PI / 180;
            var pointing = pitch * 180 / Math.PI - Double.Parse(connection.Vehicle.CameraDeviation) - 90;
            pointing = -pointing;
            var yaw = Math.Atan2(Math.Sin(secondCoord.Longitude - firstCoord.Longitude) * Math.Cos(secondCoord.Latitude),
                Math.Cos(firstCoord.Latitude) * Math.Sin(secondCoord.Latitude) - Math.Sin(firstCoord.Latitude) * Math.Cos(secondCoord.Latitude) * Math.Cos(secondCoord.Longitude - firstCoord.Longitude));
            Task.Run(() =>
            {
                Thread.Sleep(5000);

                //ShowMessage(1, firstCoord.GetDistanceTo(secondCoord) + ", " + pitch * 180 / Math.PI);
                //MainV2.comPort.setMountControl(-pitch * 180 / Math.PI * 100.0f, attitude.roll * 180 / Math.PI * 100.0f, attitude.yaw * 180 / Math.PI * 100.0f, false);
                MainV2.comPort.setMountConfigure(MAVLink.MAV_MOUNT_MODE.MAVLINK_TARGETING, false, false, false);
                MainV2.comPort.setMountControl(pointing * 100.0f, attitude.roll * 180 / Math.PI * 100.0f, MainV2.comPort.MAV.cs.yaw * 100.0f, false);
                Thread.Sleep(3000);
                ShowMessage(1, "dist: " + Math.Round(firstCoord.GetDistanceTo(secondCoord), 2) + ", alt: " + Math.Round(CpxHost.Instance.Plugin.Host.comPort.MAV.cs.alt, 2) + ", pointing: " + Math.Round(pointing, 2));
            });

        }

        private void SubscribeMapMouseDownEvent()
        {
            var map = UIUtils.GetMapControl();

            map.MouseDown += Map_MouseDown;
        }
        #endregion

        #region Actions
        private void DoReturnHome()
        {
            UIUtils.ShowFlightData();
        }
        private void DoLand()
        {
            UIUtils.SetLandMode();
        }
        private async void DoRTL()
        {
            var host = CpxHost.Instance.Plugin.Host;
            if (host.comPort.MAV.cs.mode == "RTL")
                return;
            var comport = CpxHost.Instance.Plugin.Host.comPort;
            var connection = _connections.FirstOrDefault(x => x.ComPort == comport);

            await Task.Factory.StartNew(() =>
            {
                var result = true;
                //UIUtils.SetGimbalDown();
                if (connection != null)
                {
                    if (connection.IsLandingStationDefined())
                    {
                        connection.ConnectLandingStation();
                        result = connection.OpenLandingStation();
                    }

                }
                UIUtils.SetRtlMode();
            });
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                while (comport.MAV.cs.armed) { }
                if (!comport.MAV.cs.armed && connection.IsLandingStationDefined())
                {
                    connection.inFlight = false;
                    connection.SafeCloseLandingStation();
                    UIUtils.SetLoiterMode();
                }

            }).Start();

        }
        private void DoLoiter()
        {
            UIUtils.SetLoiterMode();
        }
        private void DoResumeFlight()
        {
            UIUtils.SetAutoMode();
        }
        private void DoShowSettings()
        {
            UIUtils.ShowSettings();
        }
        private void DoToggleDayCamera()
        {
            ToggleCamera("day", "Day Camera");
        }
        private void DoToggleNightCamera()
        {
            ToggleCamera("night", "Night Camera");
        }
        private void ToggleCamera(string name, string caption)
        {


            var flightData = CpxHost.Instance.FlightData;
            var comport = CpxHost.Instance.ComPort;
            Control currnetWindow = null;

            if (_videoOut != null)
            {
                if (_winNextVision.Enabled && _winNextVision.Visible)
                {
                    _cameraProperties_Click(null, null);
                }
                else if (_winCameraProperties.Enabled && _winCameraProperties.Visible)
                {
                    _cameraProperties_Click(null, null);
                }
                var cameraName = _videoOut.CameraName;
                if (_flightDataMapContainer.Parent != _controlContainer)
                {
                    currnetWindow = _flightDataMapContainer.Parent;
                    _flightDataMapContainer.Parent = _controlContainer;
                    _controlContainer.Controls.Remove(_videoControl);
                }
                if (!_winNextVision.Enabled && videoProcess != null && !videoProcess.HasExited)
                {
                    videoProcess.Kill();
                    videoProcess = null;
                }
                flightData.Controls.Remove(_videoOut);
                _videoOut = null;

                if (cameraName == name)
                {
                    return;
                }


            }
            var connection = _connections.FirstOrDefault(x => x.ComPort == comport);
            if (connection == null) return;
            _videoOut = new CpxVideoWin(name);
            flightData.Controls.Add(_videoOut);
            DayCameraProperties(flightData);
            if (name == "nextvision")
            {

                //DayCameraProperties(flightData);
                hud = (HUD)UIUtils.FindHud();
                _videoOut.cpxVideo.Controls.Add(hud);
                hud.DoubleClick += Hud_DoubleClick;
                _videoOut.BringToFront();
                _videoOut.Title = caption;
                _videoControl = _videoOut.cpxVideo;
                _videoOut.SwapVideoAndMap += VideoOut_SwapVideoAndMap;
                if (currnetWindow != null)
                    VideoOut_SwapVideoAndMap(null, null);
                return;
            }
            if (name == "day")
            {
                try
                {
                    if (connection.tcpClient == null || !connection.tcpClient.Connected)
                        UIUtils.ConnectToVideoStream(ref connection.tcpClient, ref connection.stream, Regex.Replace(connection.Vehicle.MavIP, " ", ""));
                    //MessageBox.Show("1");
                    ChangeCameraProperties("day");
                    //MessageBox.Show("2");
                    sendCameraSettings("day");
                    //MessageBox.Show("3");
                    int index = connection.Vehicle.DayCameraPipline.IndexOf("port");
                    string port = connection.Vehicle.DayCameraPipline.Substring(index + 5, 4);
                    restartVideo(Int32.Parse(port), false);
                    //MessageBox.Show("4");
                }
                catch (Exception e)
                {
                    ShowMessage(CpxConstants.MSG_VIDEO_NOT_AVAILABLE);
                    AppSettings.WriteToLog(Resources.MessageVideoNotAvailable, e.Message);

                }

            }

            else

            {
                _videoOut.Size = new Size(400, 400 * 2 / 3 + 31);
                _videoOut.cpxVideo.Size = new Size(400, 400 * 2 / 3);
                try
                {
                    if (connection.tcpClient == null || !connection.tcpClient.Connected)
                        UIUtils.ConnectToVideoStream(ref connection.tcpClient, ref connection.stream, Regex.Replace(connection.Vehicle.MavIP, " ", ""));

                    ChangeCameraProperties("night");
                    sendCameraSettings("night");
                    int index = connection.Vehicle.NightCameraPipline.IndexOf("port");
                    string port = connection.Vehicle.NightCameraPipline.Substring(index + 5, 4);
                    restartVideo(Int32.Parse(port), false);
                }
                catch (Exception e)
                {
                    ShowMessage(CpxConstants.MSG_VIDEO_NOT_AVAILABLE);
                    AppSettings.WriteToLog(Resources.MessageVideoNotAvailable, e.Message);

                }
            }

            _videoOut.Title = caption;
            _videoControl = _videoOut.cpxVideo;
            _videoOut.SwapVideoAndMap += VideoOut_SwapVideoAndMap;
            if (currnetWindow != null)
                VideoOut_SwapVideoAndMap(null, null);
        }

        private void Hud_DoubleClick(object sender, EventArgs e)
        {
            hud.DoubleClick -= Hud_DoubleClick;
            ToggleCamera("nextvision", "NextVision");
            sc = (SplitContainer)UIUtils.FindSubMainLeft();
            sc.Panel1.ControlAdded += Panel1_ControlAdded;
        }

        private void Panel1_ControlAdded(object sender, ControlEventArgs e)
        {
            if (e.Control is HUD)
            {
                ToggleCamera("nextvision", "NextVision");
                sc.Panel1.ControlAdded -= Panel1_ControlAdded;
            }
        }

        private void ChangeCameraProperties(string cameraType)
        {
            int tempZoom = _winCameraProperties.lastZoom;
            int tempBrightness = _winCameraProperties.lastBrightness;
            int tempContrast = _winCameraProperties.lastContrast;
            string tempPolarity = _winCameraProperties.lastPolarity;
            _winCameraProperties.cameraType = cameraType;
            _winCameraProperties.lastZoom = _winCameraProperties.zoomBar.Value;
            _winCameraProperties.lastBrightness = _winCameraProperties.BrightnessBar.Value;
            _winCameraProperties.lastContrast = _winCameraProperties.contrastBar.Value;
            _winCameraProperties.lastPolarity = _winCameraProperties.Polarity.ButtonAction;

            _winCameraProperties.zoomBar.Value = tempZoom;
            _winCameraProperties.BrightnessBar.Value = tempBrightness;
            _winCameraProperties.contrastBar.Value = tempContrast;
            if (cameraType == "day")
            {
                _winCameraProperties.zoomBar.Maximum = 100;
                _winCameraProperties.zoomBar.ScaleDivisions = 10;
                _winCameraProperties.zoomBar.LargeChange = 1;
                _winCameraProperties.Polarity.Enabled = false;
            }
            else
            {
                _winCameraProperties.zoomBar.Maximum = 4;
                _winCameraProperties.zoomBar.ScaleDivisions = 2;
                _winCameraProperties.zoomBar.LargeChange = 2;
                _winCameraProperties.Polarity.Enabled = true;
            }
        }
        private void sendCameraSettings(string currentCamera)
        {
            try
            {
                var connection = _connections.FirstOrDefault(x => x.ComPort == MainV2.comPort);
                string sendCamera = "{\"camera_type\":\"" + currentCamera +
                    "\",\"zoom\":" + _winCameraProperties.zoomBar.Value +
                    ",\"brightness\":" + _winCameraProperties.BrightnessBar.Value +
                    ",\"contrast\":" + _winCameraProperties.contrastBar.Value +
                    ",\"polarity\":" + _winCameraProperties.Polarity.ButtonAction + "}";
                Byte[] camera = System.Text.Encoding.ASCII.GetBytes(sendCamera);
                connection.stream.Write(camera, 0, camera.Length);
            }
            catch (Exception e)
            {
                ShowMessage(CpxConstants.EXC_CAMERA_SETTINGS);
                AppSettings.WriteToLog(Resources.MessageLandingNotStop, e.Message);
            }
        }
        private void DayCameraProperties(FlightData flightData)
        {
            _videoOut.Size = new Size(400, 400 * 9 / 16 + 31);
            _videoOut.cpxVideo.Size = new Size(400, 400 * 9 / 16);
            _videoOut.cpxVideo.Dock = DockStyle.Top;
            if (CpxHost.Instance.IsRightToLeftLayout)
            {
                _videoOut.Location = new Point(20, flightData.Height - _videoOut.Height - 20);
            }
            else
            {
                _videoOut.Location = new Point(flightData.Width - _videoOut.Width - 20, flightData.Height - _videoOut.Height - 20);
            }
            _videoOut.cpxVideo.Top = 0;
            _videoOut.Anchor = AnchorStyles.None;
            _videoOut.Dock = DockStyle.None;
            _videoOut.BringToFront();
        }
        private void DoArmDisarm(bool arm)
        {
            var comport = CpxHost.Instance.Plugin.Host.comPort;
            var connection = _connections.FirstOrDefault(x => x.ComPort == comport);
            bool result = true;
            if (arm)
            {

                if (connection != null)
                {
                    if (connection.IsLandingStationDefined())
                    {
                        connection.ConnectLandingStation();
                        result = connection.OpenLandingStation();

                    }

                }
                if (result)
                {
                    //comport.doARM(true);
                    UIUtils.Arm(true);
                    Thread.Sleep(500);
                }
            }
            else
            {
                //comport.doARM(false, true);
                UIUtils.Disarm(true);
                if (connection.IsLandingStationDefined())
                {
                    connection.ConnectLandingStation();
                    connection.SafeCloseLandingStation();
                }
            }

        }
        private async void DoTakeOff()
        {
            await Task.Factory.StartNew(() =>
            {
                var frm = new frmTakeoff();

                frm.ShowDialog();

                if (frm.DialogResult == DialogResult.OK)
                {
                    var connection = _connections.FirstOrDefault(x => x.ComPort == CpxHost.Instance.Plugin.Host.comPort);
                    bool result = true;
                    if (connection.IsLandingStationDefined())
                    {
                        connection.ConnectLandingStation();
                        result = connection.OpenLandingStation();
                    }
                    Thread.Sleep(3000);
                    if (result)
                        UIUtils.TakeOff(frm.Altitude);
                    if (connection != null && connection.IsLandingStationDefined())
                    {
                        Thread.Sleep((int)frm.Altitude * 1000);
                        connection.CloseLandingStation(false);
                    }
                }
            });
        }
        private async Task DoFlyByMap()
        {

            var comport = CpxHost.Instance.Plugin.Host.comPort;
            var connection = _connections.FirstOrDefault(x => x.ComPort == comport);
            cpxMissionPlanning.flowLayoutPanel1.Controls.Clear(); ;
            var frm = new frmOpenFlyPath();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (connection != null)
                {
                    CancellationTokenSource source = new CancellationTokenSource();
                    await connection.DoFlyByMapAsync(frm.SelectedFile, source.Token);
                }
            }
        }
        private void DoMoreActions()
        {
            var frm = new frmActions();
            var connection = _connections.FirstOrDefault(x => x.ComPort == CpxHost.Instance.ComPort);

            if (connection != null && connection.Vehicle.CommunicationType == "Mobilicom")
            {
                CpxMobilicom cpxMobilicom = new CpxMobilicom(connection.Vehicle.MobilicomIP, 1234, connection.Vehicle.MobilicomAirIP, 1234);
                string commNow = cpxMobilicom.getCommunicationType();
                if (commNow == "LTE")
                {
                    frm.btnSDRtoLTE.Text = "SDR";
                    frm.btnSDRtoLTE.ButtonAction = "SDR";
                }
                else
                {
                    frm.btnSDRtoLTE.Text = "LTE";
                    frm.btnSDRtoLTE.ButtonAction = "LTE";
                }
                frm.btnSDRtoLTE.Click += BtnSDRtoLTE_Click;
            }
            else
            {
                frm.btnSDRtoLTE.Enabled = false;
            }
            frm.ShowDialog();
        }

        private void BtnSDRtoLTE_Click(object sender, EventArgs e)
        {
            CpxButton sdrToLTE = (CpxButton)sender;
            var connection = _connections.FirstOrDefault(x => x.ComPort == CpxHost.Instance.ComPort);
            CpxMobilicom cpxMobilicom = new CpxMobilicom(connection.Vehicle.MobilicomIP, 1234, connection.Vehicle.MobilicomAirIP, 1234);
            bool result = false;
            if (sdrToLTE.ButtonAction == "SDR")
            {
                result = cpxMobilicom.ChangeCommunicationType("0");
                if (result)
                {
                    sdrToLTE.Text = "LTE";
                    sdrToLTE.ButtonAction = "LTE";
                }
            }
            else
            {
                result = cpxMobilicom.ChangeCommunicationType("1");
                if (result)
                {
                    sdrToLTE.Text = "SDR";
                    sdrToLTE.ButtonAction = "SDR";
                }
            }
        }

        private void DisconnectVPN()
        {
            try
            {
                Process VpnProcess = new Process();
                VpnProcess.StartInfo.FileName = AppSettings.LoadConfiguration().OpenVPNPath + @"\openvpn-gui.exe";
                VpnProcess.StartInfo.Arguments = "--command disconnect_all";
                VpnProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                VpnProcess.StartInfo.UseShellExecute = false;
                VpnProcess.StartInfo.CreateNoWindow = true;
                VpnProcess.StartInfo.WorkingDirectory = AppSettings.LoadConfiguration().OpenVPNPath;
                VpnProcess.Start();
                VpnProcess.WaitForExit();
            }
            catch (Exception e) { }
        }
        private void StartProccess(string processName, string dir, string arguments)
        {
            Process[] proc = Process.GetProcessesByName(processName);
            for (int i = 0; i < proc.Length; i++)
                proc[i].Kill();

            ProcessStartInfo exestart = new ProcessStartInfo();
            exestart.FileName = dir + "\\" + processName + ".exe";
            exestart.Arguments = arguments;
            exestart.WorkingDirectory = dir;
            exestart.WindowStyle = ProcessWindowStyle.Minimized;
            exestart.UseShellExecute = true;
            Process simulator = System.Diagnostics.Process.Start(exestart);
            Thread.Sleep(3000);
        }
        private async void DoConnect()
        {
            if (MainV2.comPort.MAV.cs.connected)
            {
                UIUtils.InvokePrivateMethod(CpxHost.Instance.MainForm, "MenuConnect_Click", null, EventArgs.Empty);
                if (_videoOut != null)
                {
                    ToggleCamera(_videoOut.CameraName, "");
                }
                return;
            }

            var frm = new frmSelectVehicle();

            if (frm.ShowDialog() == DialogResult.OK)
            {
                var vehicle = frm.SelectedVehicle;
                if (vehicle.CameraType == "nextvision")
                {
                    DoConnectNextVision(vehicle.Id);
                    if (Double.TryParse(vehicle.MaximumBattery, out maxBattery) == true && Double.TryParse(vehicle.MinimumBattery, out minBattery))
                    {
                        maxBattery = Convert.ToDouble(vehicle.MaximumBattery);
                        minBattery = Convert.ToDouble(vehicle.MinimumBattery);
                        isBatteryUpdated = true;
                    }
                    else
                        isBatteryUpdated = false;
                    //return;
                }
                if (vehicle.Name == "simulator")
                {
                    try
                    {
                        string homeLocation = MainV2.comPort.MAV.cs.HomeLocation.Lat + "," + MainV2.comPort.MAV.cs.HomeLocation.Lng + "," + srtm.getAltitude(MainV2.comPort.MAV.cs.HomeLocation.Lat, MainV2.comPort.MAV.cs.HomeLocation.Lng).alt + "," + 0;
                        string myDocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        StartProccess("ArduCopter", myDocs + @"\Mission Planner\sitl", String.Format("-M{0} -O{1} -s{2} --uartA tcp:0 {3}", "+", homeLocation, 1, "--defaults \"" + myDocs + "\\Mission Planner\\sitl\\default_params/copter.parm\""));
                        //StartProccess("LandingStation", AppSettings.LoadConfiguration().VideoControlPath, "");
                    }
                    catch (Exception e) { }
                }
                if (vehicle.CommunicationType == "WIFI")
                {
                    DisconnectVPN();
                    if (string.IsNullOrEmpty(vehicle.WirelessName))
                    {
                        ShowMessage(CpxConstants.MSG_WIFI_UNAVAILBLE);
                        return;
                    }

                    if (!WifiUtilities.IsNetworkAvailable(vehicle.WirelessName))
                    {
                        ShowMessage(CpxConstants.MSG_WIFI_UNAVAILBLE);
                        return;
                    }

                    if (!WifiUtilities.ConnectToWifi(vehicle.WirelessName))
                    {
                        ShowMessage(CpxConstants.MSG_WIFI_UNABLE_TO_CONNECT, string.Format(Resources.MessageUnableToConnect, vehicle.WirelessName));
                        return;
                    };

                    Thread.Sleep(2000);
                }
                else if (vehicle.CommunicationType == "LTE")
                {
                    if (string.IsNullOrEmpty(vehicle.WirelessName))
                    {
                        ShowMessage(CpxConstants.MSG_WIFI_UNAVAILBLE);
                        return;
                    }
                    try
                    {
                        DisconnectVPN();

                        System.IO.File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\OpenVPN\log\" + vehicle.WirelessName + ".log");

                        Process VpnProcess = new Process();
                        VpnProcess.StartInfo.FileName = AppSettings.LoadConfiguration().OpenVPNPath + @"\openvpn-gui.exe";
                        VpnProcess.StartInfo.Arguments = "--command connect " + vehicle.WirelessName + " --silent_connection 1";
                        VpnProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        VpnProcess.StartInfo.UseShellExecute = false;
                        VpnProcess.StartInfo.CreateNoWindow = true;
                        VpnProcess.StartInfo.WorkingDirectory = AppSettings.LoadConfiguration().OpenVPNPath;
                        VpnProcess.Start();

                        Thread.Sleep(2000);
                    }
                    catch (Exception e)
                    {
                        ShowMessage(CpxConstants.EXC_OPEN_VPN);
                        AppSettings.WriteToLog(Resources.ExceptionOpenVPN, e.Message);
                    }
                    DateTime start = DateTime.Now;
                    frmConnectingLTE frmConnectingLTE = new frmConnectingLTE(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\OpenVPN\log\" + vehicle.WirelessName + ".log");
                    frmConnectingLTE.ShowDialog();
                    if (frmConnectingLTE.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(CpxConstants.MSG_WIFI_UNAVAILBLE);
                        return;
                    }
                }

                var mav = default(MAVLinkInterface);
                try
                {
                    mav = UIUtils.ConnectMAV(vehicle);
                }
                catch (Exception ex)
                {
                    ShowMessage(CpxConstants.MSG_WIFI_UNABLE_TO_CONNECT, ex.Message);
                    return;
                }

                //string path = @"C:\Program Files (x86)\Mission Planner\plugins\LogExtractor\LogExtractorConfig.txt";
                var connection = new VehicleConnection(mav, vehicle.Id);

                bool isDayCameraExist = vehicle.DayCameraPipline != "", isNightCameraExist = vehicle.NightCameraPipline != "";

                _cameraProperties.Visible = true;

                if (Double.TryParse(vehicle.MaximumBattery, out maxBattery) == true && Double.TryParse(vehicle.MinimumBattery, out minBattery))
                {
                    maxBattery = Convert.ToDouble(vehicle.MaximumBattery);
                    minBattery = Convert.ToDouble(vehicle.MinimumBattery);
                    isBatteryUpdated = true;
                }
                else
                    isBatteryUpdated = false;
                _connections.Add(connection);
                if (vehicle.LandingStationIp.Replace(" ", "") != "")
                {
                    if (LandingStationCtrl == null)
                        ShowLandingStationCtrl(connection);
                }
                //WriteLogExtractorConfig(vehicle);
                if (vehicle.CameraType == "nextvision")
                {
                    _winCameraProperties.Enabled = false;
                    _winNextVision.Enabled = true;
                    ToggleCamera("nextvision", "NextVision");
                    _flightToolbar.nextVision = true;
                    _flightToolbar.btnShowDayCamera.Enabled = false;
                    _flightToolbar.btnShowNightCamera.Enabled = false;
                }
                else
                {
                    _winCameraProperties.Enabled = true;
                    _winNextVision.Enabled = false;
                    if (_videoOut != null)
                        _videoOut.cpxVideo.Controls.Clear();
                    _flightToolbar.isCamerasExist(isDayCameraExist, isNightCameraExist);
                    _flightToolbar.btnShowDayCamera.Visible = true;
                    _flightToolbar.btnShowNightCamera.Visible = true;
                }
            }
        }
        private void WriteLogExtractorConfig(Vehicle vehicle)
        {

            try
            {

                string ip = Regex.Replace(vehicle.MavIP, " ", "");
                string path = @"C:\Logs\";
                File.Delete(@"C:\Program Files (x86)\Mission Planner\plugins\LogExtractor\LogExtractorConfig.txt");
                File.AppendAllText(@"C:\Program Files (x86)\Mission Planner\plugins\LogExtractor\LogExtractorConfig.txt",
                    "host=" + ip + "\nport=" + vehicle.MavPort + "\nusername=pi \npassword= raspberry \ndirPath=" + path);
            }
            catch
            {
                ShowMessage(10, "log extractor failed");
            }
        }
        private void VideoOut_SwapVideoAndMap(object sender, EventArgs e)
        {

            if (_videoOut == null) return;
            var comport = CpxHost.Instance.ComPort;
            var connection = _connections.FirstOrDefault(x => x.ComPort == comport);
            if (connection == null) return;

            if (_flightDataMapContainer.Parent == _videoOut)
            {

                _videoOut.Size = new Size(400, 400 * 2 / 3 + 31);
                _videoOut.cpxVideo.Size = new Size(400, 400 * 2 / 3);
                _videoControl.Parent = _videoOut;
                _flightDataMapContainer.Parent = _controlContainer;
                _videoControl.Dock = DockStyle.None;
                _videoOut.cpxVideo.Top = 31;
                _videoOut.cpxVideo.Left = 0;
                _videoControl.BringToFront();
                if (_winNextVision.Enabled)
                {
                    _videoOut.cpxVideo.Top = 31;
                    return;
                }
                int index = connection.Vehicle.DayCameraPipline.IndexOf("port");
                string port = connection.Vehicle.DayCameraPipline.Substring(index + 5, 4);
                if (_videoOut.Title != "Day Camera")
                {
                    _videoOut.Size = new Size(400, 400 * 2 / 3 + 31);
                    _videoOut.cpxVideo.Size = new Size(400, 400 * 2 / 3);
                    _videoOut.cpxVideo.Left = 0;
                    index = connection.Vehicle.NightCameraPipline.IndexOf("port");
                    port = connection.Vehicle.NightCameraPipline.Substring(index + 5, 4);
                }
                _videoControl.Parent = _videoOut;
                _flightDataMapContainer.Parent = _controlContainer;
                restartVideo(Int32.Parse(port), false);
            }
            else

            {
                _videoControl.Parent = _controlContainer;
                _flightDataMapContainer.Parent = _videoOut;
                _videoControl.Dock = DockStyle.None;
                _videoControl.Width = _controlContainer.Height * 16 / 9;
                _videoControl.Height = _controlContainer.Height;
                _videoControl.Top = 0;
                _videoControl.Left = (_controlContainer.Width - _videoControl.Width) / 2;
                _videoControl.BringToFront();
                if (_winNextVision.Enabled)
                {
                    //_videoOut.cpxVideo.Controls.Add(UIUtils.FindHud());
                    return;
                }
                int index = connection.Vehicle.DayCameraPipline.IndexOf("port");
                string port = connection.Vehicle.DayCameraPipline.Substring(index + 5, 4);
                if (_videoOut.Title != "Day Camera")
                {
                    _videoControl.Width = _controlContainer.Height * 3 / 2;
                    _videoControl.Height = _controlContainer.Height;
                    _videoControl.Top = 0;
                    _videoControl.Left = (_controlContainer.Width - _videoControl.Width) / 2;
                    index = connection.Vehicle.NightCameraPipline.IndexOf("port");
                    port = connection.Vehicle.NightCameraPipline.Substring(index + 5, 4);
                }
                restartVideo(Int32.Parse(port), false);
            }
        }
        private void DoSavePlan()
        {
            var flightRoute = UIUtils.ReadFlightPlanner();

            var frm = new frmManagePlans(flightRoute.Commands, _currentFlightRoute != null ? _currentFlightRoute.Name : ""/*, _currentFlightRoute != null*/);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                flightRoute.Name = frm.PathName;
                flightRoute.Save();
            }
        }
        private void DoLoadPlan()
        {
            var frm = new frmOpenFlyPath();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                cpxMissionPlanning.flowLayoutPanel1.Controls.Clear();
                _currentFlightRoute = frm.SelectedFile;

                UIUtils.LoadPlanToPlanner(_currentFlightRoute);
            }
        }
        private void DoEditPlan()
        {
            CpxHost.Instance.FlightPlanner.TXT_homealt.Text = (srtm.getAltitude(CpxHost.Instance.ComPort.MAV.cs.HomeLocation.Lat, CpxHost.Instance.ComPort.MAV.cs.HomeLocation.Lat).alt * CurrentState.multiplieralt).ToString("0");
            CpxHost.Instance.FlightPlanner.TXT_homelat.Text = CpxHost.Instance.ComPort.MAV.cs.HomeLocation.Lat.ToString();
            CpxHost.Instance.FlightPlanner.TXT_homelng.Text = CpxHost.Instance.ComPort.MAV.cs.HomeLocation.Lng.ToString();
            UIUtils.ShowFlightPlanner();

        }
        private bool DoFlyToHereAlt(bool isTakeoff)
        {
            string alt = "100";

            if (MainV2.comPort.MAV.cs.firmware == MissionPlanner.ArduPilot.Firmwares.ArduCopter2)
            {
                alt = (10 * CurrentState.multiplieralt).ToString("0");
            }
            else
            {
                alt = (100 * CurrentState.multiplieralt).ToString("0");
            }

            var frm = new frmTakeoff();
            if (!isTakeoff)
            {
                if (MainV2.comPort.MAV.cs.armed)
                {
                    if (MainV2.comPort.MAV.cs.alt > frm.colorSlider1.Maximum)
                    {
                        frm.colorSlider1.Value = (int)Math.Round(MainV2.comPort.MAV.cs.alt, 0);
                    }
                    else
                    {
                        frm.colorSlider1.Value = (int)Math.Round(MainV2.comPort.MAV.cs.alt, 0);
                    }
                }


                frm.btnTakeoff.Text = Resources.TakeoffForm;
            }
            frm.ShowDialog();

            if (frm.DialogResult != DialogResult.OK) return false;

            alt = frm.Altitude.ToString();

            int intalt = (int)(100 * CurrentState.multiplieralt);
            if (!int.TryParse(alt, out intalt))
            {
                CustomMessageBox.Show("Bad Alt");
                return false;
            }

            MainV2.comPort.MAV.GuidedMode.z = intalt / CurrentState.multiplieralt;

            if (MainV2.comPort.MAV.cs.mode == "Guided")
            {
                MainV2.comPort.setGuidedModeWP(new Locationwp
                {
                    alt = MainV2.comPort.MAV.GuidedMode.z,
                    lat = MainV2.comPort.MAV.GuidedMode.x,
                    lng = MainV2.comPort.MAV.GuidedMode.y
                });
            }
            return true;
        }
        private async void DoFlightToHere()
        {
            await Task.Factory.StartNew(() =>
            {
                if (!MainV2.comPort.BaseStream.IsOpen)
                {
                    CustomMessageBox.Show(Resources.MessagePleaseConnect, Resources.ErrorMessage);
                    return;
                }
                bool takeoffForm = DoFlyToHereAlt(false);

                if (takeoffForm)
                {
                    if (!MainV2.comPort.MAV.cs.armed)
                    {
                        var comport = CpxHost.Instance.Plugin.Host.comPort;
                        var connection = _connections.FirstOrDefault(x => x.ComPort == comport);
                        bool result = true;
                        if (connection.IsLandingStationDefined())
                        {
                            connection.inFlight = true;
                            connection.ConnectLandingStation();
                            result = connection.OpenLandingStation();
                            connection.inFlight = true;
                            if (!result)
                            {
                                return;
                            }
                        }
                        Thread.Sleep(3000);


                        while (comport.MAV.cs.gpshdop >= 1.4 || comport.MAV.cs.satcount < 9) { Thread.Sleep(5); }
                        UIUtils.TakeOff(15);
                        Thread.Sleep(3 * 1000);
                        if (connection != null)
                        {
                            while (comport.MAV.cs.alt < 5 && comport.MAV.cs.armed) { }
                            //Thread.Sleep((int)(15 / 2) * 1000);

                            if (connection.IsLandingStationDefined() && comport.MAV.cs.alt >= 5 && comport.MAV.cs.armed)
                            {
                                connection.CloseLandingStation(false);
                            }
                            else if (comport.MAV.cs.alt < 5 && comport.MAV.cs.armed)
                            {
                                comport.doARM(false);
                            }
                            if (!comport.MAV.cs.armed)
                            {
                                return;
                            }

                        }
                    }
                    if (_mapMouseDownPosition.Lat == 0 || _mapMouseDownPosition.Lng == 0)
                    {
                        CustomMessageBox.Show(Resources.BadCoords, Resources.ErrorMessage);
                        return;
                    }

                    Locationwp gotohere = new Locationwp();

                    gotohere.id = (ushort)MAVLink.MAV_CMD.WAYPOINT;
                    gotohere.alt = MainV2.comPort.MAV.GuidedMode.z; // back to m
                    gotohere.lat = (_mapMouseDownPosition.Lat);
                    gotohere.lng = (_mapMouseDownPosition.Lng);
                    try
                    {
                        MainV2.comPort.setGuidedModeWP(gotohere);
                    }
                    catch (Exception ex)
                    {
                        MainV2.comPort.giveComport = false;
                        CustomMessageBox.Show(Resources.CommandFailed + ex.Message, Resources.ErrorMessage);
                    }
                }
            });
            //  CpxHost.Instance.FlightData.InvokePrivateMethod("goHereToolStripMenuItem_Click", null, EventArgs.Empty);
        }
        private void DoChangeAlt()
        {
            CpxHost.Instance.FlightData.InvokePrivateMethod("flyToHereAltToolStripMenuItem_Click", null, EventArgs.Empty);

        }
        private void DoPointCameraHere()
        {
            CpxHost.Instance.FlightData.InvokePrivateMethod("pointCameraHereToolStripMenuItem_Click", null, EventArgs.Empty);

        }
        private void DoUnPointCameraHere()
        {
            CpxHost.Instance.ComPort.doCommand(MAVLink.MAV_CMD.DO_SET_ROI, 0, 0, 0, 0, 0, 0, 0);
            try
            {
                if (CpxHost.Instance.ComPort.MAV.param.ContainsKey("MNT_MODE"))
                {
                    CpxHost.Instance.ComPort.setParam("MNT_MODE", 2);
                }
                else
                {
                    // copter 3.3 acks with an error, but is ok
                    MainV2.comPort.doCommand(MAVLink.MAV_CMD.DO_MOUNT_CONTROL, 0, 0, 0, 0, 0, 0,
                        3);
                }
            }
            catch
            {
                ShowMessage(1, "Can't complete command");
                //CustomMessageBox.Show(Strings.ErrorNoResponce, Strings.ERROR);
            }
        }
        #endregion

        #region Toolbar Actions Additions
        private void videoProcessProperties()
        {

            videoProcess = new Process();
            videoProcess.StartInfo.CreateNoWindow = true;
            videoProcess.StartInfo.FileName = AppSettings.LoadConfiguration().VideoControlPath + @"\Project1.exe";
            videoProcess.StartInfo.WorkingDirectory = AppSettings.LoadConfiguration().VideoControlPath;
            videoProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            videoProcess.StartInfo.RedirectStandardInput = true;
            videoProcess.StartInfo.UseShellExecute = false;
            videoProcess.StartInfo.RedirectStandardOutput = true;
        }
        //TODO: call function
        public void restartVideo(int port, bool rec)
        {
            if (_videoOut != null)
            {
                if (videoProcess != null)
                {
                    //videoProcess.StandardInput.WriteLine("f");
                    if (!videoProcess.HasExited)
                        videoProcess.Kill();
                    videoProcess = null;
                }
                GStreamer.StopAll();
                videoProcess = new Process();
                videoProcess.StartInfo.CreateNoWindow = true;
                ////videoProcess.StartInfo.UseShellExecute = false;
                videoProcess.StartInfo.FileName = AppSettings.LoadConfiguration().VideoControlPath + @"\Project1.exe";
                videoProcess.StartInfo.WorkingDirectory = AppSettings.LoadConfiguration().VideoControlPath;
                videoProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                //videoProcess.StartInfo.RedirectStandardInput = true;
                //videoProcess.StartInfo.RedirectStandardOutput = true;

                if (rec)
                {
                    var fileName = DateTime.Now.ToString().Replace('/', '.');
                    fileName = fileName.Replace(' ', '_');
                    fileName = fileName.Replace(':', '.');
                    videoProcessProperties();
                    videoProcess.StartInfo.Arguments = "-p " + port + " -w " + _videoOut.cpxVideo.Handle + " -f " + fileName;
                }
                else
                    videoProcess.StartInfo.Arguments = "-p " + port + " -w " + _videoOut.cpxVideo.Handle;
                try
                {
                    //MessageBox.Show(videoProcess.StartInfo.FileName);
                    videoProcess.Start();
                }
                catch (Exception e)
                {
                    ShowMessage(CpxConstants.MSG_VIDEO_NOT_AVAILABLE);
                    AppSettings.WriteToLog(Resources.MessageVideoNotAvailable, e.Message);
                }
            }
        }
        #endregion

        #region View Actions
        private void ShowZoomSlider()
        {
            var originalSlider = UIUtils.FindMapZoomSlider() as TrackBar;

            if (originalSlider != null)
            {
                originalSlider.Visible = false;

                _slider = new ZoomMapTracker();
                UIUtils.AddControlToFlightData(_slider);
                _slider.Top = _telemetryPanel.Height + 30;
                if (!CpxHost.Instance.IsRightToLeftLayout)
                {
                    _slider.Anchor = AnchorStyles.Top | AnchorStyles.Right;
                    //MessageBox.Show(CpxHost.Instance.FlightData.Width.ToString());
                    _slider.Left = CpxHost.Instance.FlightData.Width - _slider.Width - 25;
                }
                else
                {
                    _slider.Left = 25;
                }

                _slider.Slider.LargeChange = (uint)originalSlider.LargeChange;
                _slider.Slider.Maximum = originalSlider.Maximum;
                _slider.Slider.Minimum = originalSlider.Minimum;
                _slider.Slider.Name = "";
                _slider.Slider.SmallChange = (uint)originalSlider.SmallChange;
                //   _slider.Slider.ti = 1F;
                _slider.Slider.Value = originalSlider.Value;

                originalSlider.ValueChanged += (s, e) =>
                {
                    _slider.Slider.Value = originalSlider.Value;
                };
                _slider.Slider.Scroll += (s, e) =>
                {
                    originalSlider.Value = _slider.Slider.Value;
                    CpxHost.Instance.FlightData.InvokePrivateMethod("TRK_zoom_Scroll", originalSlider, EventArgs.Empty);
                };

                _slider.ZoomIn += (s, e) =>
                {
                    var newValue = _slider.Slider.Value + (int)_slider.Slider.LargeChange;

                    if (newValue > _slider.Slider.Maximum)
                    {
                        newValue = _slider.Slider.Maximum;
                    }
                    _slider.Slider.Value = newValue;
                    originalSlider.Value = _slider.Slider.Value;
                    CpxHost.Instance.FlightData.InvokePrivateMethod("TRK_zoom_Scroll", originalSlider, EventArgs.Empty);


                };

                _slider.ZoomOut += (s, e) =>
                {
                    var newValue = _slider.Slider.Value - (int)_slider.Slider.LargeChange;

                    if (newValue < _slider.Slider.Minimum)
                    {
                        newValue = _slider.Slider.Minimum;
                    }
                    _slider.Slider.Value = newValue;
                    originalSlider.Value = _slider.Slider.Value;
                    CpxHost.Instance.FlightData.InvokePrivateMethod("TRK_zoom_Scroll", originalSlider, EventArgs.Empty);
                };


                _slider.BringToFront();
            }
        }
        private void showCameraPropertiesButton()
        {
            //Camera Properties button
            _cameraProperties = new CpxButton
            {
                Tag = "custom",
                Font = CpxFonts.GetFont(CpxFontsEnum.Rubik_Medium, 10.0F),
                AutoSize = false,
                Size = new Size(32, 32),
                Anchor = AnchorStyles.Bottom | AnchorStyles.Right,
                Image = Resources.night_video_call_filled_26,
                ImageAlign = ContentAlignment.MiddleCenter,
                ButtonShape = CpxButton.ButtonShapeEnum.Ellipse,
                FlatStyle = FlatStyle.Flat,
                ButtonAction = "SHOW",
            };

            //Initialize Camera Properties window for copterpix
            _winCameraProperties = new CpxCameraProperties
            {
                RightToLeft = CpxHost.Instance.IsRightToLeftLayout ? RightToLeft.Yes : RightToLeft.No,
                Anchor = AnchorStyles.Bottom | AnchorStyles.Right,
                Visible = false
            };
            _winCameraProperties.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - _winCameraProperties.Width - 10;
            _winCameraProperties.Top = CpxHost.Instance.FlightData.Height - _cameraProperties.Height - _winCameraProperties.Height - 10;
            UIUtils.AddControlToFlightData(_winCameraProperties);
            setEvents();
            _winCameraProperties.btnRecord.Text = "";
            _winCameraProperties.BringToFront();
            _cameraProperties.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - 40 - _cameraProperties.Width - ((_winCameraProperties.Width - _cameraProperties.Width) / 2);
            _cameraProperties.Top = CpxHost.Instance.FlightData.Height - _cameraProperties.Height - 10;
            _cameraProperties.FlatAppearance.BorderSize = 0;
            UIUtils.AddControlToFlightData(_cameraProperties);
            _cameraProperties.BringToFront();
            //Initialize camera properties for NextVision
            _winNextVision = new CpxNextVision()
            {
                RightToLeft = RightToLeft.No,
                Anchor = AnchorStyles.Bottom | AnchorStyles.Right,
                Visible = false
            };

            _winNextVision.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - _winNextVision.Width - 10;
            _winNextVision.Top = CpxHost.Instance.FlightData.Height - _cameraProperties.Height - _winNextVision.Height - 10;
            UIUtils.AddControlToFlightData(_winNextVision);
            TabPage cameraActions = new TabPage();
            foreach (TabPage tp in CpxHost.Instance.FlightData.tabControlactions.TabPages)
            {
                if (tp.Name == "tabCameraActions")
                {
                    cameraActions = tp;
                    break;
                }
            }
            _winNextVision.panel1.Controls.AddRange(cameraActions.Controls.Cast<Control>().ToArray());
            _winNextVision.BringToFront();

        }
        private void ShowMessageButton()
        {
            _messagesButton = new CpxButton
            {
                Tag = "custom",
                Font = CpxFonts.GetFont(CpxFontsEnum.Rubik_Medium, 10.0F),
                AutoSize = false,
                Size = new Size(32, 32),
                Anchor = AnchorStyles.Bottom | AnchorStyles.Right,
                Image = Resources.messages,
                ImageAlign = ContentAlignment.MiddleCenter,
                ButtonShape = CpxButton.ButtonShapeEnum.Ellipse,
                FlatStyle = FlatStyle.Flat,
            };
            _messagesButton.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - _messagesButton.Width - 58;
            _messagesButton.Top = CpxHost.Instance.FlightData.Height - _messagesButton.Height - 10;
            _messagesButton.FlatAppearance.BorderSize = 0;
            UIUtils.AddControlToFlightData(_messagesButton);
            _messagesButton.BringToFront();

            _message = new CpxBallonPanel
            {
                ForeColor = CpxConstants.CopterPixForeColor,
                Font = CpxConstants.SmallFont,
                Size = new Size(406, 170),
                RightToLeft = RightToLeft.Yes,
                //RightToLeft = CpxHost.Instance.IsRightToLeftLayout ? RightToLeft.Yes : RightToLeft.No,
                BorderWidth = 6,
                Curvature = 20,
                MessageText = "TEST",
                Anchor = AnchorStyles.Bottom | AnchorStyles.Right,
                Visible = true
            };

            _message.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - _message.Width - 10;
            _message.Top = CpxHost.Instance.FlightData.Height - _message.Height - 35;
            UIUtils.AddControlToFlightData(_message);
            _message.BringToFront();

            _messagesButton.Click += (s, e) =>
            {
                if (_message.Visible == true)
                    _message.Visible = false;
                else
                    ShowMessage(CpxConstants.MSG_SHOW_LAST_THREE);
            };

        }
        private void ShowLocationPanel()
        {
            _locationLabel = new CpxLabel();
            _locationLabel.Tag = "custom";
            _locationLabel.Font = CpxFonts.GetFont(CpxFontsEnum.Rubik_Medium, 10.0F);
            _locationLabel.Size = new System.Drawing.Size(250, 30);
            UIUtils.AddControlToFlightData(_locationLabel);
            _locationLabel.Left = (_locationLabel.Parent.Width - _locationLabel.Width - _flightToolbar.Width - _messagesButton.Width - _winCameraProperties.Width - 20);
            _locationLabel.Top = (_locationLabel.Parent.Height - _locationLabel.Height - 3);
            _locationLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _locationLabel.Text = "";
            _locationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            _locationLabel.BackColor = Color.Transparent;
            _locationLabel.AutoSize = true;
            _locationLabel.BringToFront();

            if (CpxHost.Instance.IsRightToLeftLayout)
            {
                _locationLabel.TextAlign = ContentAlignment.MiddleRight;
            }
            else
            {
                _locationLabel.TextAlign = ContentAlignment.MiddleLeft;
            }
        }
        private void ShowTelemetryPanel()
        {
            _telemetryPanel = new CpxInfo();
            _telemetryPanel.Dock = DockStyle.Top;
            UIUtils.AddControlToFlightData(_telemetryPanel);

            _telemetryPanel.SendToBack();

            _flightTimePanel = new TimePanel();
            UIUtils.AddControlToFlightData(_flightTimePanel);
            _flightTimePanel.Left = (_flightTimePanel.Parent.Width - _flightTimePanel.Width) / 2;


            _flightTimePanel.Anchor = AnchorStyles.Top;
            _flightTimePanel.Top = _telemetryPanel.Height;
            _flightTimePanel.BringToFront();

            //_joystick = new CpxJoystick2();
            //UIUtils.AddControlToFlightData(_joystick);
            //_joystick.Left = (_flightTimePanel.Parent.Width - _joystick.Width) / 2;
            //_joystick.Anchor = AnchorStyles.Bottom;
            //_joystick.Top = CpxHost.Instance.FlightData.Height - _joystick.Height - 30;
            //_joystick.BringToFront();

        }
        private void InitToolBar()
        {
            // create new button with the CopterPix Logo
            _savePathButton = UIUtils.AddToolstripButton(Resources.SavePlanButtonText, "MenuCopterPixSavePath", Resources.save_path_icon, 4);
            _loadPathButton = UIUtils.AddToolstripButton(Resources.LoadPlanButtonText, "MenuCopterPixLoadPath", Resources.load_path_icon, 4);
            _settingsButton = UIUtils.AddToolstripButton(Resources.SettingsButtonText, "MenuCopterPixSettings", Resources.icons8_settings, 4);

            _savePathButton.Click += SavePathButton_Click;
            _loadPathButton.Click += LoadPathButton_Click;
            _settingsButton.Click += SettingsButton_Click;

            UIUtils.SetCopterPixLogo(Resources.CopterPixProNew);
        }
        private void RenderCopterPixLayout(int LayoutMode)
        {
            InitToolBar();
            UIUtils.RegisterMissionPlannerView("CpxConfig", new CpxSettings(), true);

            UIUtils.HideMainMenu();

            RedrawFlightData();
            RedrawFlightPlanner(true);
            ShowCpxToolbar(LayoutMode);
            ShowTelemetryPanel();
            showCameraPropertiesButton();
            _cameraProperties.Visible = false;
            ShowMessageButton();
            ShowLocationPanel();
            ShowZoomSlider();
            UIUtils.ReplaceMapContextMenu(OpenMapActions);
            if (CpxHost.Instance.IsRightToLeftLayout)
            {
                CpxHost.Instance.FlightData.RightToLeft = RightToLeft.Yes;
            }
        }
        private void RenderDefaultLayout()
        {
            InitToolBar();
            UIUtils.RegisterMissionPlannerView("CpxConfig", new CpxSettings(), true);
        }
        /// <summary>
        /// Removes all unnessary elements from FlightData View. 
        /// Leave only map on the flight data
        /// </summary>
        private void RedrawFlightData()
        {
            _flightDataMapContainer = UIUtils.FindMapContainer();

            ((TableLayoutPanel)_flightDataMapContainer).CellBorderStyle = TableLayoutPanelCellBorderStyle.None;


            CpxHost.Instance.FlightData.Controls.Clear();

            if (CpxHost.Instance.IsRightToLeftLayout)
            {
                CpxHost.Instance.FlightData.RightToLeft = RightToLeft.Yes;


            }
            _controlContainer = new Panel();
            _controlContainer.Dock = DockStyle.Fill;
            UIUtils.AddControlToFlightData(_controlContainer);

            _controlContainer.Controls.Add(_flightDataMapContainer);
        }
        private void RedrawFlightPlanner(bool redraw)
        {
            if (redraw)
            {
                CpxHost.Instance.FlightPlanner.contextMenuStrip1.Opening += (s, e) =>
                {

                    e.Cancel = true;
                    string message = "תרצה לשנות את נקודת הבית?";
                    string title = "שינוי נקודת הבית";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult result = MessageBox.Show(message, title, buttons);
                    if (result == DialogResult.Yes)
                    {
                        CpxHost.Instance.FlightPlanner.TXT_homealt.Text = (srtm.getAltitude(_mapMouseDownPosition.Lat, _mapMouseDownPosition.Lng).alt * CurrentState.multiplieralt).ToString("0");
                        CpxHost.Instance.FlightPlanner.TXT_homelat.Text = _mapMouseDownPosition.Lat.ToString();
                        CpxHost.Instance.FlightPlanner.TXT_homelng.Text = _mapMouseDownPosition.Lng.ToString();
                    }
                };
                CpxHost.Instance.FlightPlanner.MainMap.MouseDown += (s, e) =>
                {
                    var map = CpxHost.Instance.FlightPlanner.MainMap;
                    _mapMouseDownPosition = map.FromLocalToLatLng(e.X, e.Y);
                };
                CpxHost.Instance.FlightPlanner.MainMap.DoubleClick += MainMap_DoubleClick;
                _flightPlanner = UIUtils.FindMapContainerFlightPlanner();
                CpxHost.Instance.FlightPlanner.Controls.Clear();
                //Add needed original Mission Planner controls
                Label dist = CpxHost.Instance.FlightPlanner.lbl_distance;
                dist.Top = 50;
                dist.Top = 50;
                CpxHost.Instance.FlightPlanner.Controls.Add(dist);
                Label prevdist = CpxHost.Instance.FlightPlanner.lbl_prevdist;
                prevdist.Top = dist.Top + dist.Height;
                CpxHost.Instance.FlightPlanner.Controls.Add(prevdist);
                Label homedist = CpxHost.Instance.FlightPlanner.lbl_homedist;
                homedist.Top = prevdist.Top + prevdist.Height;
                CpxHost.Instance.FlightPlanner.Controls.Add(homedist);

                //Add Cpx Save, Load, Home toolbar
                var toolBar = new CpxFlightPlannerToolBar { Dock = DockStyle.Top };
                toolBar.CommandInvoked += _flightToolbar_CommandInvoked;
                CpxHost.Instance.FlightPlanner.Controls.Add(toolBar);
                toolBar.SendToBack();

                CpxHost.Instance.FlightPlanner.Controls.Add(_flightPlanner);
                _flightPlanner.SendToBack();

                //Create CPX Flight Planner window
                cpxMissionPlanning = new CpxMissionPlanning();
                if (CpxHost.Instance.IsRightToLeftLayout)
                {
                    cpxMissionPlanning.Dock = DockStyle.Right;
                }
                else
                {
                    cpxMissionPlanning.Dock = DockStyle.Left;
                }
                CpxHost.Instance.FlightPlanner.Controls.Add(cpxMissionPlanning);
                cpxMissionPlanning.BringToFront();


                //Add Actions buttone
                try
                {
                    CpxButton actions = new CpxButton
                    {
                        BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(169)))), ((int)(((byte)(0))))),
                        ButtonAction = null,
                        ButtonShape = CpxLib.Controls.Common.CpxButton.ButtonShapeEnum.Ellipse,
                        Image = global::CpxLib.Properties.Resources.actions,
                        ImageAlign = ContentAlignment.MiddleCenter,
                        Size = new System.Drawing.Size(50, 50)
                    };
                    actions.FlatAppearance.BorderSize = 0;
                    actions.FlatStyle = FlatStyle.Flat;
                    CpxHost.Instance.FlightPlanner.Controls.Add(actions);
                    actions.BringToFront();
                    actions.Top = CpxHost.Instance.FlightPlanner.Height - actions.Height - 15;
                    if (CpxHost.Instance.IsRightToLeftLayout)
                    {
                        actions.Left = 15;
                        actions.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
                    }
                    else
                    {
                        actions.Left = CpxHost.Instance.FlightPlanner.Width - actions.Width - 15;
                        actions.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
                    }
                    actions.Visible = true;

                    //Add actions instead right click
                    CpxMissionPlanningActions cpxMissionPlanningActions = new CpxMissionPlanningActions();
                    CpxHost.Instance.FlightPlanner.Controls.Add(cpxMissionPlanningActions);
                    cpxMissionPlanningActions.Top = CpxHost.Instance.FlightData.Height - cpxMissionPlanningActions.Height - 15;
                    if (CpxHost.Instance.IsRightToLeftLayout)
                    {
                        cpxMissionPlanningActions.Left = actions.Left + actions.Width + 10;
                        cpxMissionPlanningActions.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
                    }
                    else
                    {
                        cpxMissionPlanningActions.Left = CpxHost.Instance.FlightPlanner.Width - actions.Width - cpxMissionPlanningActions.Width - 15;
                        cpxMissionPlanningActions.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
                    }
                    cpxMissionPlanningActions.Visible = false;
                    cpxMissionPlanningActions.BringToFront();
                    cpxMissionPlanningActions.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
                    //Actions events
                    actions.Click += (s, e) =>
                    {
                        if (cpxMissionPlanningActions.Visible)
                            cpxMissionPlanningActions.Visible = false;
                        else
                            cpxMissionPlanningActions.Visible = true;
                    };
                    cpxMissionPlanningActions.VisibleChanged += (s, e) =>
                    {
                        if (cpxMissionPlanningActions.Visible)
                        {
                            if (!CpxHost.Instance.ComPort.MAV.cs.connected)
                            {
                                cpxMissionPlanningActions.btnClearGeoFence.Enabled = false;
                                cpxMissionPlanningActions.btnGeoFence.Enabled = false;
                            }
                            else
                            {
                                cpxMissionPlanningActions.btnClearGeoFence.Enabled = true;
                                cpxMissionPlanningActions.btnGeoFence.Enabled = true;
                            }
                        }
                    };
                    cpxMissionPlanningActions.btnClearMission.Click += (s, e) =>
                    {
                        CpxHost.Instance.FlightPlanner.FlightPlannerBase.clearMissionToolStripMenuItem_Click(s, e);
                        cpxMissionPlanning.flowLayoutPanel1.Controls.Clear();
                        cpxMissionPlanningActions.Visible = false;
                    };
                    cpxMissionPlanningActions.btnClearPolygon.Click += (s, e) =>
                    {
                        CpxHost.Instance.FlightPlanner.FlightPlannerBase.clearPolygonToolStripMenuItem_Click(s, e);
                        cpxMissionPlanningActions.Visible = false;
                    };
                    cpxMissionPlanningActions.btnGeoFence.Click += (s, e) =>
                    {
                        //CpxHost.Instance.FlightPlanner.FlightPlannerBase.GeoFenceuploadToolStripMenuItem_Click(s, e);
                        CpxHost.Instance.FlightPlanner.cmb_missiontype.SelectedIndex = 1;
                        CpxHost.Instance.FlightPlanner.FlightPlannerBase.FenceInclusionToolStripMenuItem_Click(null, null);
                        CpxHost.Instance.FlightPlanner.FlightPlannerBase.BUT_write_Click(null, null);
                        CpxHost.Instance.FlightPlanner.cmb_missiontype.SelectedIndex = 0;
                        cpxMissionPlanningActions.Visible = false;
                    };
                    cpxMissionPlanningActions.btnSurveyGrid.Click += (s, e) =>
                    {
                        CpxHost.Instance.FlightPlanner.FlightPlannerBase.surveyGridToolStripMenuItem_Click(s, e);
                        cpxMissionPlanningActions.Visible = false;
                    };
                    cpxMissionPlanningActions.btnClearGeoFence.Click += (s, e) =>
                    {
                        CpxHost.Instance.FlightPlanner.FlightPlannerBase.clearToolStripMenuItem_Click(s, e);
                        cpxMissionPlanningActions.Visible = false;
                    };
                }
                catch (Exception ex)
                {
                    MessageBox.Show("WHAT IS HAPNENIG");
                }
            }
            else
            {
                var toolBar = new CpxFlightPlannerToolBar
                {
                    Dock = DockStyle.Top
                };
                toolBar.CommandInvoked += _flightToolbar_CommandInvoked;
                CpxHost.Instance.FlightPlanner.Controls.Add(toolBar);
                toolBar.SendToBack();

            }


        }

        private void MainMap_DoubleClick(object sender, EventArgs e)
        {


        }

        private void EnableControls(CurrentState state)
        {
            _telemetryPanel.SafeInvoke(() => { _telemetryPanel.EnableControls(state); }, true);
            _flightToolbar.SafeInvoke(() => { _flightToolbar.EnableControls(state); }, true);
        }
        private void ShowCpxToolbar(int LayoutMode)
        {



            _flightToolbar = new CpxToolbar(LayoutMode);

            if (CpxHost.Instance.IsRightToLeftLayout)
            {
                _flightToolbar.Dock = DockStyle.Right;
            }
            else
            {
                _flightToolbar.Dock = DockStyle.Left;
            }
            UIUtils.AddControlToFlightData(_flightToolbar);

            _flightToolbar.HandleCreated += (s, e) =>
            {
                _flightToolbar.InvokeInUIThread((ctrl) => ctrl.EnableControls(CpxHost.Instance.Plugin.Host.cs));
            };
            _flightToolbar.CommandInvoked += _flightToolbar_CommandInvoked;
            _flightToolbar.SendToBack();

        }
        private void ShowLandingStationCtrl(VehicleConnection vehicleConnection)
        {
            LandingStationCtrl = new LandingStationCtrl(vehicleConnection);

            if (CpxHost.Instance.IsRightToLeftLayout)
            {
                LandingStationCtrl.Dock = DockStyle.Left;
                _slider.Left = LandingStationCtrl.Width + 25;
            }
            else
            {
                LandingStationCtrl.Dock = DockStyle.Right;
                _slider.Left = CpxHost.Instance.FlightData.Width - LandingStationCtrl.Width - _slider.Width - 25;
            }
            UIUtils.AddControlToFlightData(LandingStationCtrl);
            int indexTel = UIUtils.getChildIndex(_telemetryPanel);
            int indexLand = UIUtils.getChildIndex(LandingStationCtrl);
            UIUtils.setChildIndex(_telemetryPanel, indexLand);
            UIUtils.setChildIndex(LandingStationCtrl, indexTel);
        }
        #endregion

        #region Events
        private void setEvents()
        {
            _winCameraProperties.zoomBar.ValueChanged += ZoomBar_ValueChanged;
            _winCameraProperties.BrightnessBar.ValueChanged += BrightnessBar_ValueChanged;
            _winCameraProperties.contrastBar.ValueChanged += ContrastBar_ValueChanged;
            _winCameraProperties.Polarity.Click += Polarity_Click;
            _winCameraProperties.btnRecord.Click += BtnRecord_Click;
            _winCameraProperties.btnCapture.Click += BtnCapture_Click;
            _cameraProperties.Click += _cameraProperties_Click;
        }
        private void BtnCapture_Click(object sender, EventArgs e)
        {

            Point location = _videoOut.cpxVideo.PointToScreen(Point.Empty);
            Rectangle rect = new Rectangle(0, 0, _videoOut.cpxVideo.Width, _videoOut.cpxVideo.Height);
            if (_flightDataMapContainer.Parent != _controlContainer)
            {

                _slider.SendToBack();
                _videoOut.SendToBack();
                _winCameraProperties.SendToBack();
                _cameraProperties.SendToBack();
                _locationLabel.SendToBack();
                _flightTimePanel.SendToBack();
                _messagesButton.SendToBack();
            }
            Bitmap bmp = new Bitmap(rect.Width, rect.Height, PixelFormat.Format32bppArgb);
            Graphics g = Graphics.FromImage(bmp);
            g.CopyFromScreen(location.X, location.Y, 0, 0, bmp.Size, CopyPixelOperation.SourceCopy);
            var fileName = DateTime.Now.ToString(new CultureInfo("he-IL")).Replace('/', '.');
            fileName = fileName.Replace(':', '.') + ".jpg";
            bmp.Save(fileName, ImageFormat.Jpeg);
            if (_flightDataMapContainer.Parent != _controlContainer)
            {
                _slider.BringToFront();
                _videoOut.BringToFront();
                _winCameraProperties.BringToFront();
                _cameraProperties.BringToFront();
                _locationLabel.BringToFront();
                _flightTimePanel.BringToFront();
                _messagesButton.BringToFront();
            }
        }
        private void BtnRecord_Click(object sender, EventArgs e)
        {
            if (_winCameraProperties.btnRecord.ButtonAction == "stop")
            {
                //TODO: change port
                //TODO: change field in vehicle file
                if (_videoOut != null && _videoOut.Title == "Day Camera")
                {
                    _flightToolbar.btnShowNightCamera.Enabled = false;
                    restartVideo(5601, true);
                }
                else if (_videoOut != null && _videoOut.Title == "Night Camera")
                {
                    _flightToolbar.btnShowDayCamera.Enabled = false;
                    restartVideo(9000, true);
                }

                _winCameraProperties.btnRecord.Image = global::CpxLib.Properties.Resources.stop;
                _winCameraProperties.btnRecord.ButtonAction = "rec";
                MessageBox.Show("record");
                _videoOut.btnSwapVideoAndMap.Enabled = false;

            }
            else
            {
                videoProcess.StandardInput.WriteLine("e");
                videoProcess = null;
                videoProcessProperties();
                videoProcess.StartInfo.Arguments = "-p " + 5601 + " -w " + _videoOut.cpxVideo.Handle;
                videoProcess.Start();
                _winCameraProperties.btnRecord.Image = global::CpxLib.Properties.Resources.rec;
                _winCameraProperties.btnRecord.ButtonAction = "stop";
                MessageBox.Show("stop");
                _flightToolbar.btnShowDayCamera.Enabled = true;
                _flightToolbar.btnShowNightCamera.Enabled = true;
                _videoOut.btnSwapVideoAndMap.Enabled = true;
            }
        }
        private void Polarity_Click(object sender, EventArgs e)
        {
            if (_winCameraProperties._ignoreEvents) return;
            var cameraName = _videoOut.CameraName;
            _winCameraProperties._ignoreEvents = true;
            string polarity = "";
            if (_winCameraProperties.Polarity.ButtonAction == "15")
            {
                polarity = "0";
                _winCameraProperties.Polarity.ButtonAction = "0";
                _winCameraProperties.Polarity.Image = global::CpxLib.Properties.Resources.blackHot1;
            }

            else
            {
                polarity = "15";
                _winCameraProperties.Polarity.ButtonAction = "15";
                _winCameraProperties.Polarity.Image = global::CpxLib.Properties.Resources.whiteHot1;
            }
            //var connection = _connections.FirstOrDefault(x => x.ComPort == MainV2.comPort);
            //string sendCamera = "{\"camera_type\":\"" + cameraName +
            //    "\",\"zoom\":" + _winCameraProperties.zoomBar.Value.ToString() +
            //    ",\"brightness\":" + _winCameraProperties.BrightnessBar.Value.ToString() +
            //    ",\"brightness\":" + _winCameraProperties.BrightnessBar.Value.ToString() +
            //    ",\"contrast\":" + _winCameraProperties.contrastBar.Value.ToString() +
            //    ",\"polarity\":" + polarity + "}";
            //if (cameraName == "day")
            //    _winCameraProperties.lastDay = sendCamera;
            //else
            //    _winCameraProperties.lastNight = sendCamera;
            ////MessageBox.Show(sendCamera);
            ////MessageBox.Show(sendCamera);
            //Byte[] camera = System.Text.Encoding.ASCII.GetBytes(sendCamera);
            //stream.Write(camera, 0, camera.Length);
            sendCameraSettings(cameraName);
            _winCameraProperties._ignoreEvents = false;
        }
        private void ContrastBar_ValueChanged(object sender, EventArgs e)
        {
            //var connection = _connections.FirstOrDefault(x => x.ComPort == MainV2.comPort);
            if (_winCameraProperties._ignoreEvents) return;
            var cameraName = _videoOut.CameraName;
            _winCameraProperties._ignoreEvents = true;
            //stream.Write(camera, 0, camera.Length);
            sendCameraSettings(cameraName);
            _winCameraProperties._ignoreEvents = false;
        }
        private void BrightnessBar_ValueChanged(object sender, EventArgs e)
        {
            //var connection = _connections.FirstOrDefault(x => x.ComPort == MainV2.comPort);
            if (_winCameraProperties._ignoreEvents) return;
            var cameraName = _videoOut.CameraName;
            _winCameraProperties._ignoreEvents = true;

            sendCameraSettings(cameraName);
            _winCameraProperties._ignoreEvents = false;
        }
        private void ZoomBar_ValueChanged(object sender, EventArgs e)
        {
            if (_winCameraProperties._ignoreEvents) return;
            var cameraName = _videoOut.CameraName;
            _winCameraProperties._ignoreEvents = true;
            sendCameraSettings(cameraName);
            _winCameraProperties._ignoreEvents = false;
        }
        private void _cameraProperties_Click(object sender, EventArgs e)
        {
            if (_videoOut != null)
            {
                if (_cameraProperties.ButtonAction == "SHOW")
                {
                    _cameraProperties.ButtonAction = "HIDE";
                    _winCameraProperties.takeParams = false;
                    if (_winNextVision.Enabled)
                    {
                        _messagesButton.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - _messagesButton.Width - _winNextVision.Width - 58;
                        _message.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - _message.Width - _winNextVision.Width - 10;
                        _cameraProperties.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - 10 - _cameraProperties.Width - ((_winNextVision.Width - _cameraProperties.Width) / 2);
                        _locationLabel.Left = (_locationLabel.Parent.Width - _locationLabel.Width - _flightToolbar.Width - _messagesButton.Width - _winNextVision.Width - 80);
                        _winNextVision.Visible = true;
                    }

                    else
                    {
                        _messagesButton.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - _messagesButton.Width - _winCameraProperties.Width - 58;
                        _message.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - _message.Width - _winCameraProperties.Width - 10;
                        _cameraProperties.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - 10 - _cameraProperties.Width - ((_winCameraProperties.Width - _cameraProperties.Width) / 2);
                        _locationLabel.Left = (_locationLabel.Parent.Width - _locationLabel.Width - _flightToolbar.Width - _messagesButton.Width - _winCameraProperties.Width - 80);
                        _winCameraProperties.Visible = true;
                    }

                }
                else
                {
                    _messagesButton.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - _messagesButton.Width - 58;
                    _message.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - _message.Width - 10;
                    _cameraProperties.ButtonAction = "SHOW";
                    _winCameraProperties.takeParams = false;
                    if (_winNextVision.Enabled)
                    {
                        _cameraProperties.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - 40 - _cameraProperties.Width - ((_winCameraProperties.Width - _cameraProperties.Width) / 2);
                        _locationLabel.Left = (_locationLabel.Parent.Width - _locationLabel.Width - _flightToolbar.Width - _messagesButton.Width - _winNextVision.Width - 20);
                        _winNextVision.Visible = false;
                    }

                    else
                    {
                        _cameraProperties.Left = CpxHost.Instance.FlightData.Width - _flightToolbar.Width - 40 - _cameraProperties.Width - ((_winCameraProperties.Width - _cameraProperties.Width) / 2);
                        _locationLabel.Left = (_locationLabel.Parent.Width - _locationLabel.Width - _flightToolbar.Width - _messagesButton.Width - _winCameraProperties.Width - 20);
                        _winCameraProperties.Visible = false;
                    }

                }
            }

        }
        private void SettingsButton_Click(object sender, EventArgs e)
        {
            UIUtils.ShowSettings();
        }
        private void LoadPathButton_Click(object sender, EventArgs e)
        {
            var frm = new TestVideo();

            frm.Show();
        }
        private void SavePathButton_Click(object sender, EventArgs e)
        {

        }
        private void Map_MouseDown(object sender, MouseEventArgs e)
        {
            var map = UIUtils.GetMapControl();
            //MessageBox.Show("MOUSE DOWN!");
            _mapMouseDownPosition = map.FromLocalToLatLng(e.X, e.Y);
        }
        public void setVelocity(float vx_, float vy_, float vz_)
        {
            MAVLink.mavlink_set_position_target_local_ned_t go = new MAVLink.mavlink_set_position_target_local_ned_t
            {
                time_boot_ms = 0,
                target_system = 0,
                target_component = 0,
                coordinate_frame = 8,
                type_mask = 0b0000111111000111,
                x = 0,
                y = 0,
                z = 0,
                vx = vx_,
                vy = vy_,
                vz = vz_,
                afx = 0,
                afy = 0,
                afz = 0,
                yaw = 0,
                yaw_rate = 0
            };
            CpxHost.Instance.Plugin.Host.comPort.sendPacket(go, CpxHost.Instance.Plugin.Host.comPort.MAV.sysid, CpxHost.Instance.Plugin.Host.comPort.MAV.compid);
        }
        private void toUp()
        {
            if (!MainV2.comPort.BaseStream.IsOpen)
            {
                CustomMessageBox.Show(Resources.MessagePleaseConnect, Resources.ErrorMessage);
                return;
            }
            CustomMessageBox.Show("up");
            setVelocity(5, 0, 0);
        }
        private void toLeft()
        {
            try
            {
                if (!MainV2.comPort.BaseStream.IsOpen)
                {
                    CustomMessageBox.Show(Resources.MessagePleaseConnect, Resources.ErrorMessage);
                    return;
                }
                CpxHost.Instance.Plugin.Host.comPort.doCommand(MAVLink.MAV_CMD.CONDITION_YAW, 270, 0, 1, 1, 0, 0, 0);
                CustomMessageBox.Show("left");
            }
            catch (Exception ex)
            {
                CustomMessageBox.Show("problem with yaw com");
            }
        }
        private void toRight()
        {
            try
            {
                if (!MainV2.comPort.BaseStream.IsOpen)
                {
                    CustomMessageBox.Show(Resources.MessagePleaseConnect, Resources.ErrorMessage);
                    return;
                }
                CustomMessageBox.Show("right");
                CpxHost.Instance.Plugin.Host.comPort.doCommand(MAVLink.MAV_CMD.CONDITION_YAW, 90, 0, 1, 1, 0, 0, 0);
            }
            catch (Exception ex)
            {
                CustomMessageBox.Show("problem with yaw com");
            }
        }

        private void toDown()
        {
            if (!MainV2.comPort.BaseStream.IsOpen)
            {
                CustomMessageBox.Show(Resources.MessagePleaseConnect, Resources.ErrorMessage);
                return;
            }
            CustomMessageBox.Show("down");
            setVelocity(-5, 0, 0);
        }

        private void _flightToolbar_CommandInvoked(object sender, CpxToolbarCommandEventArgs e)
        {
            switch (e.Command)
            {
                case CpxToolbarCommandEnum.NotSet:
                    break;
                case CpxToolbarCommandEnum.ScheduledFlight:
                    StartSchedule();
                    break;
                case CpxToolbarCommandEnum.LoiterMode:
                    DoLoiter();
                    break;
                case CpxToolbarCommandEnum.ResumeFlight:
                    DoResumeFlight();
                    break;
                case CpxToolbarCommandEnum.Rtl:
                    DoRTL();
                    break;
                case CpxToolbarCommandEnum.FlyByMap:
                    DoFlyByMap();
                    break;
                case CpxToolbarCommandEnum.Takeoff:
                    DoTakeOff();
                    break;
                case CpxToolbarCommandEnum.Connect:
                    DoConnect();
                    break;
                case CpxToolbarCommandEnum.ToggleDayCamera:
                    DoToggleDayCamera();
                    break;
                case CpxToolbarCommandEnum.ToggleNightCamera:
                    DoToggleNightCamera();
                    break;
                case CpxToolbarCommandEnum.MoreActions:
                    DoMoreActions();
                    break;
                case CpxToolbarCommandEnum.Settings:
                    DoShowSettings();
                    break;
                case CpxToolbarCommandEnum.ReturnHome:
                    DoReturnHome();
                    break;
                case CpxToolbarCommandEnum.SavePlan:
                    DoSavePlan();
                    break;
                case CpxToolbarCommandEnum.LoadPlan:
                    DoLoadPlan();
                    break;
                case CpxToolbarCommandEnum.EditPlan:
                    DoEditPlan();
                    break;
                case CpxToolbarCommandEnum.Arm:
                    DoArmDisarm(true);
                    break;
                case CpxToolbarCommandEnum.Disarm:
                    DoArmDisarm(false);
                    break;
                case CpxToolbarCommandEnum.Land:
                    DoLand();
                    break;
                case CpxToolbarCommandEnum.NextVisionConnect:
                    //DoConnectNextVision();
                    break;

            }
        }

        private void DoConnectNextVision(string MavID)
        {
            MainV2._connectionControl.CMB_serialport.Text = "UDP";
            MainV2._connectionControl.CMB_baudrate.Text = "115200";
            //UIUtils.InvokePrivateMethod(CpxHost.Instance.MainForm, "MenuConnect_Click", null, EventArgs.Empty);
            if (MainV2.comPort.MAV.sysid != 0)
            {
                var connection = new VehicleConnection(MainV2.comPort, MavID);
                _connections.Add(connection);
                _cameraProperties.Visible = true;
                _winCameraProperties.Enabled = false;
                _winNextVision.Enabled = true;
                ToggleCamera("nextvision", "NextVision");
                //_videoOut.cpxVideo.Controls.Add(UIUtils.FindHud());
                _flightToolbar.nextVision = true;
                _flightToolbar.btnShowDayCamera.Visible = false;
                _flightToolbar.btnShowNightCamera.Visible = false;

            }

        }
        #endregion

        #region Public
        public VehicleConnection GetVehicleConnection()
        {
            return _connections.FirstOrDefault(x => x.ComPort == MainV2.comPort);
        }
        public void EnableJoystik()
        {
            ShowMessage(UIUtils.EnableJoystick());
        }
        public void setLandingStation(CpxLandingStation ls)
        {
            LandingStationCtrl.landingStation = ls;
        }
        public void ShowMessage(int messageCode = -1, string customMessage = "")
        {
            if (messageCode >= 0)
            {
                if (!string.IsNullOrEmpty(customMessage))
                {

                    string[] tempMessageArray = new string[] { customMessage, LastThreeMessages[0], LastThreeMessages[1] };
                    LastThreeMessages = tempMessageArray;
                    _message.MessageText = LastThreeMessages[0] + "\n" + LastThreeMessages[1] + "\n" + LastThreeMessages[2];
                }
                else if (messageCode == 13)
                {
                    _message.MessageText = LastThreeMessages[0] + "\n" + LastThreeMessages[1] + "\n" + LastThreeMessages[2];
                }
                else
                {
                    string[] tempMessageArray = new string[] { CpxConstants.Messages[messageCode].Message, LastThreeMessages[0], LastThreeMessages[1] };
                    LastThreeMessages = tempMessageArray;
                    _message.MessageText = LastThreeMessages[0] + "\n" + LastThreeMessages[1] + "\n" + LastThreeMessages[2];
                }
                if (CpxConstants.Messages[messageCode].Timeout > 0)
                {
                    Task.Run(() =>
                    {
                        Thread.Sleep(CpxConstants.Messages[messageCode].Timeout);
                        ShowMessage(-1);
                    });
                }
                _message.SafeInvoke(() => { _message.Visible = true; }, true);
            }
            else
            {
                _message.SafeInvoke(() => { _message.Visible = false; }, true);
            }
        }
        #endregion

        #region Scheduler
        private CpxTaskList _tasksList;
        private ScheduleRunner _scheduleRunner;
        public void StartSchedule()
        {
            if (_scheduleRunner != null)
            {
                _scheduleRunner.StopAllJobs();
                _scheduleRunner = null;
                // _toolbar.btnOpenSchedule.Image = Resources.icons8_timer_26;
                RemoveTaskList();
                return;
            }
            var frm = new frmOpenSchedule();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                var schedule = frm.SelectedFile;
                _scheduleRunner = new ScheduleRunner(schedule);
                ShowTaskList();
                _scheduleRunner.JobStatusChange += _scheduleRunner_JobStatusChange;
                var comport = CpxHost.Instance.Plugin.Host.comPort;
                var connection = _connections.FirstOrDefault(x => x.ComPort == comport);
                _scheduleRunner.Start();

                //  _monitor.ShowScheduleStatus(_scheduleRunner.GetNextRun().ToString("hh:mm"));

                //_toolbar.btnOpenSchedule.Image = Resources.icons8_delete_timer_26;
            }
        }
        public void ShowTaskList()
        {
            var flightData = MainV2.instance.FlightData;
            _tasksList = new CpxTaskList();
            //_flightDataMapContainer.Controls.Add(_tasksList);
            UIUtils.AddControlToFlightData(_tasksList);
            _tasksList.Location = new System.Drawing.Point(flightData.Width - _flightToolbar.Width - _tasksList.Width - 10, _telemetryPanel.Height + 10);
            _tasksList.BringToFront();

        }
        public void RemoveTaskList()
        {
            var flightData = MainV2.instance.FlightData;

            if (_tasksList != null)
            {
                flightData.Controls.Remove(_tasksList);
                _tasksList = null;
            }
        }
        private void _scheduleRunner_JobStatusChange(object sender, JobStatusChangeEventArgs e)
        {
            if (_tasksList != null)
                _tasksList.UpdateTaskStatus(e);
        }
        #endregion
        //TODO: change interface for location of project1
    }
}