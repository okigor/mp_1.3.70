﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Core
{
    public enum CpxToolbarCommandEnum
    {
        NotSet,
        ScheduledFlight,
        LoiterMode,
        ResumeFlight,
        Rtl,
        Land,
        FlyByMap,
        Takeoff,
        Connect,
        ToggleNightCamera,
        ToggleDayCamera,
        MoreActions,
        Settings,
        Arm,
        Disarm,
        NextVisionConnect,
        SavePlan,
        LoadPlan,
        ReturnHome,
        EditPlan,

        flyForward,
        flyBackwards,
        flyRight,
        flyLeft

    }
}
