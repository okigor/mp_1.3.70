﻿using CpxLib.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Core
{
    public enum CpxFontsEnum
    {
        Rubik_Bold = 0,
        Rubik_Light = 1,
        Rubik_Regular = 2,
        Rubik_Medium = 3
    }
    public static class CpxFonts
    {

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);

        private static PrivateFontCollection[] _fonts = new PrivateFontCollection[4];


        static CpxFonts()
        {

            _fonts[0] = LoadFont(Resources.Rubik_Bold);
            _fonts[1] = LoadFont(Resources.Rubik_Light);
            _fonts[2] = LoadFont(Resources.Rubik_Regular);
            _fonts[3] = LoadFont(Resources.Rubik_Medium);
            //_fonts[4] = LoadFont(Resources.Open_24_Display_St);


        }

        public static Font GetFont(CpxFontsEnum font, float size, FontStyle fontStyle)
        {
            return new Font(_fonts[(int)font].Families[0], size, fontStyle);
        }
        public static Font GetFont(CpxFontsEnum font, float size)

        {
            return new Font(_fonts[(int)font].Families[0], size);
        }


        public static FontFamily GetFontFamily(CpxFontsEnum font)
        {
            return _fonts[(int)font].Families[0];
        }

        private static PrivateFontCollection LoadFont(byte[] fontData)
        {
            var fonts = new PrivateFontCollection();
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, fontData.Length);
            AddFontMemResourceEx(fontPtr, (uint)fontData.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);

            return fonts;
        }
    }
}
