﻿using NativeWifi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Core
{
    public class WifiDef
    {

        public string Name { get; set; }
        public string ProfileName { get; set; }

        public bool Available { get; set; }
        public uint Strength { get; set; }

        public bool Connected { get; set; }
        public int InterfaceIndex { get; internal set; }
    }
    public static class WifiUtilities
    {

        public static WifiDef[] GetAvailbleNetworks()
        {
            var result = new List<WifiDef>();

            var client = new WlanClient();
            var index = 0;
            foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
            {

                var networks = wlanIface.GetAvailableNetworkList(Wlan.WlanGetAvailableNetworkFlags.IncludeAllAdhocProfiles);

                foreach (Wlan.WlanAvailableNetwork network in networks)
                {

                    var ssid = network.dot11Ssid;
                    var networkname = Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);

                    if (string.IsNullOrEmpty(networkname)) continue;
                    var def = new WifiDef
                    {

                        InterfaceIndex = index,
                        ProfileName = network.profileName,
                        Name = networkname,
                        Strength = network.wlanSignalQuality,
                        Available = network.networkConnectable
                    };


                    var entry = result.FirstOrDefault(x => x.Name == def.Name);

                    if (entry != null)
                    {
                        if (string.IsNullOrEmpty(entry.ProfileName))
                        {
                            entry.ProfileName = def.ProfileName;
                        }
                    }
                    else
                    {
                        result.Add(def);
                    }

                }

                index++;
            }



            return result.ToArray();
        }

        public static bool IsNetworkAvailable(string profileName)
        {
            var networks = GetAvailbleNetworks();

            if (networks == null) return false;

            return networks.Any(x => x.ProfileName == profileName);
        }


        public static WifiDef GetNetworkState(string profileName)
        {
            try
            {
                var client = new WlanClient();
                //var currentConnection = "";

                //if (client.Interfaces.Length > 0)
                //{
                //    currentConnection = client.Interfaces[0].CurrentConnection.profileName;
                //}

                var networks = GetAvailbleNetworks();

                foreach (var network in networks)
                {

                    if (network.ProfileName == profileName)
                    {
                        return network;
                    }
                }

                return new WifiDef
                {
                    Available = false,
                    Strength = 0,
                    Connected = false,
                    ProfileName = profileName
                };
            }
            catch (Exception)
            {
                return new WifiDef
                {
                    Available = false,
                    Strength = 0,
                    Connected = false,
                    ProfileName = profileName
                };
            }

        }
        public static bool ConnectToWifi(string profileName)
        {
            var client = new WlanClient();

            foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
            {
                if (wlanIface.InterfaceState == Wlan.WlanInterfaceState.Connected)
                {
                    if (wlanIface.CurrentConnection.profileName == profileName)
                    {
                        return true;
                    }
                }
            }


            foreach (WlanClient.WlanInterface wlanIface in client.Interfaces
                                                                 .Where(x=>x.InterfaceState != Wlan.WlanInterfaceState.NotReady)
                                                                 .OrderBy(x=>x.InterfaceState == Wlan.WlanInterfaceState.Disconnected ? 0 : 1))
            {



                foreach (Wlan.WlanProfileInfo profileInfo in wlanIface.GetProfiles())
                {
                    var name = profileInfo.profileName; // this is typically the network's SSID
                    if (name == profileName)
                    {

                        string xml = wlanIface.GetProfileXml(profileInfo.profileName);

                        return wlanIface.ConnectSynchronously(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, name, 5000);
                    }
                }
            }

            return false;

        }
    }
}
