﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Core
{
    public enum LandingStationStatusEnum
    {
        NotSet,
        Open,
        Closed,
        Opening,
        Closing,
        Unknown,
    }
}
