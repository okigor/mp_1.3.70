﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Core
{
    public class FlightStatusEventArgs : EventArgs
    {
        public FlightStatusEventArgs(DisplayTelemetryData data)
        {
            Data = data;
        }
        public FlightStatusEventArgs(LandingStationStatusEnum data)
        {
            LandingStationStatus = data;
        }
        public DisplayTelemetryData Data { get; }
        public LandingStationStatusEnum LandingStationStatus { get; }
    }
}
