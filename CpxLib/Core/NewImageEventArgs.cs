﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Core
{
    public class NewImageEventArgs: EventArgs
    {
        public NewImageEventArgs(Image image, string name)
        {
            Image = image;
            Name = name;
        }

        public Image Image { get; private set; }
        public string Name { get; private set; }
    }
}
