﻿
using CpxLib.Controls.AppControls;
using CpxLib.Model;
using CpxLib.Properties;
using DirectShowLib;
using MissionPlanner;
using MissionPlanner.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CpxLib.Core
{
    public partial class VehicleConnection
    {
        bool continueMission = false;
        private CpxLandingStation _landingStation;
        private Vehicle _metadata;
        //public CancellationTokenSource _cancelSource = new CancellationTokenSource();
        public event EventHandler<VehicleConnectionEventArgs> CommandInvoked;
        public event EventHandler<VehicleConnectionEventArgs> Error;
        public event EventHandler<VehicleConnectionEventArgs> Message;
        private const int ANGLE_THRESHOLD = 24;
        private const string CMD_CLOSE = "d";
        private const string CMD_MIDDLE_CLOSE = "m";
        private const int NUM_OF_CLOSING_ATTEMPTS = 2;
        private const int NUM_OF_ANGLE_SAMPLES = 5;
        private const int WAIT_TIME_CHARGE_INDICATION = 8000;
        public TcpClient tcpClient;
        public NetworkStream stream;
        bool isStationOpen = false;
        private const int NUM_OF_CHARGING_CHECK = 3;
        public bool inFlight = false;
        public static bool isInMission = false;

		
		public VehicleConnection(MAVLinkInterface comPort)
        {
            ComPort = comPort;
        }
        public VehicleConnection(MAVLinkInterface comPort, string fileId)
        {
            ComPort = comPort;
            FileId = fileId;
            ReloadFile();
        }

        public Vehicle Vehicle
        {
            get
            {
                return _metadata;
            }
        }

        public MAVLinkInterface ComPort { get; }
        public string FileId { get; }

        public void ReloadFile()
        {
            _metadata = ModelExtensions.GetVehicleFiles().FirstOrDefault(x => x.Id == FileId);
           
        }
        public void ConnectLandingStation()
        {
            if (!string.IsNullOrEmpty(_metadata.LandingStationIp))
            {
                if (!string.IsNullOrEmpty(_metadata.LandingStationPort))
                {
                    //MessageBox.Show(_metadata.LandingStationIp + " " + _metadata.LandingStationPort);
                    _landingStation = new CpxLandingStation
                    {
                        Host = _metadata.LandingStationIp?.Replace(" ", ""),
                        Port = _metadata.LandingStationPort
                    };
                    try
                    {
                        _landingStation.Open();
                    }
                    catch (Exception ex)
                    {
                        OnError(ex);
                    }
                }
            }
            else
            {
                var config = AppSettings.LoadConfiguration();
                _landingStation = new CpxLandingStation
                {
                    Host = config.LandingStationIP?.Replace(" ", ""),
                    Port = config.LandingStationPort
                };
                try
                {
                    _landingStation.Open();
                }
                catch (Exception ex)
                {
                    OnError(ex);
                }
            }
        }

        public bool OpenLandingStation()
        {
            isStationOpen = true;
            CpxInfo.isInCharging = false;
            bool result = _landingStation.OpenLandingStation(10000);
            if (!result)
            {
                Thread.Sleep(200);
                result = _landingStation.OpenLandingStation(10000);
            }
            if (!result)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_LANDING_NOT_OPEN);
            }
            _landingStation.Disconnect();
            return result;
        }

        internal void setContinue(bool v)
        {
            continueMission = v;
        }

        public bool CloseLandingStation(bool charging)
        {
            bool result = _landingStation.LandingStationPos(1000, CMD_CLOSE);
            if (!result)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_LANDING_NOT_CLOSE);
            }
            Thread.Sleep(1000);
            isStationOpen = false;
            if (charging)
                ChargeDrone();
            
            _landingStation.Disconnect();
            return result;
        }
        public bool MiddleCloseLandingStation()
        {
            var result = _landingStation.LandingStationPos(1000, CMD_MIDDLE_CLOSE);
            _landingStation.Disconnect();
            return result;
        }

        public bool SafeCloseLandingStation()
        {
            bool result = false;
            bool resMid;
            bool resClose;
            bool resOpen;
            for (int i = 0; i < NUM_OF_CLOSING_ATTEMPTS; i++)
            {
                resMid=MiddleCloseLandingStation();
                if (!resMid) {
                    resMid = MiddleCloseLandingStation();
                    if (!resMid)
                    {
                        CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_LANDING_NOT_CLOSE);
                        return false;
                    }
                }
                if (CheckDroneAngleFunc <= ANGLE_THRESHOLD)
                {
                    resClose=CloseLandingStation(true);
                    if (!resClose)
                    {
                        resClose = CloseLandingStation(true);
                        if (!resClose)
                        {
                            CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_LANDING_NOT_CLOSE);
                            return false;
                        }
                    }
                    result = true;
                    break;
                }
                resOpen=OpenLandingStation();
                if (!resOpen)
                {
                    resOpen = OpenLandingStation();
                    if (!resOpen)
                    {
                        CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_LANDING_NOT_CLOSE);
                        return false;

                    }
                }
            }
            if (!result)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_INCORRECT_POS);
            }
            _landingStation.Disconnect();
            return result;
        }
        private double CheckDroneAngleFunc
        {
            get
            {
                try
                {
                    double sumOfAngles = 0;
                    for (int i = 0; i < NUM_OF_ANGLE_SAMPLES; i++)
                    {
                        Thread.Sleep(200);
                        double angle = Math.Sqrt(Math.Pow(ComPort.MAV.cs.roll, 2) + Math.Pow(ComPort.MAV.cs.pitch, 2));
                        sumOfAngles += angle;
                    }
                    return sumOfAngles / NUM_OF_ANGLE_SAMPLES;
                }
                catch
                {
                    return 0;
                }
            }
        }

        public bool StopLandingStation()
        {
            var result = _landingStation.stopLandingStation(10000);
            if (!result)
            {
                CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_LANDING_NOT_STOP);
            }
            _landingStation.Disconnect();
            return result;
        }

        public bool IsInCharging()
        {
            ConnectLandingStation();
            bool result = _landingStation.IsInCharging();
            _landingStation.Disconnect();
            return result;
        }
        public void isInChargingThread()
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                Thread.Sleep(WAIT_TIME_CHARGE_INDICATION);
                if (IsInCharging() && !inFlight && !isStationOpen)
                    CpxInfo.isInCharging = true;
                else
                    CpxInfo.isInCharging = false;
            }).Start();
            return;
        }
        public bool ChargeDrone()
        {
            Thread.Sleep(500);
            bool result = _landingStation.ChargeDrone(1000);
            isInChargingThread();
            _landingStation.Disconnect();
            return result;
        }
        private void OnError(Exception ex)
        {
            var args = new VehicleConnectionEventArgs(ex);
            Error?.Invoke(this, args);
        }

        private void OnMessage(string message)
        {
            var args = new VehicleConnectionEventArgs(message);

            Message?.Invoke(this, args);
        }

        public int PingIp(string ip, int retries = 1, int numOfSuccesfulPingsToStop = 1)
        {
            var ping = new System.Net.NetworkInformation.Ping();
            var attemtToTry = retries;
            var succesfulPings = 0;

            while (attemtToTry > 0)
            {
                var reply = ping.Send(ip);

                if (reply.Status == System.Net.NetworkInformation.IPStatus.Success)
                {
                    succesfulPings++;
                }

                if (succesfulPings == numOfSuccesfulPingsToStop) return succesfulPings;

                attemtToTry--;
            }

            return succesfulPings;
        }
         

        public string GetStatus()
        {
            return "";
        }
        public string getLandingIPPort()
        {
            return _metadata.LandingStationIp + " " + _metadata.LandingStationPort;
        }
        public bool IsLandingStationDefined()
        {
            if (string.IsNullOrEmpty(_metadata.LandingStationIp)) return false;
            if (string.IsNullOrEmpty(_metadata.LandingStationPort)) return false;

            return true;
        }

        public void DoFlyByMap(FlightRoute waypointFile, CancellationToken token)
        {
            UploadWaypoints(waypointFile);
            FlyByMap(waypointFile, token);

        }

        public void UploadWaypoints(FlightRoute waypointFile)
        {
            if (!ComPort.BaseStream.IsOpen)
            {
                OnError(new Exception("Please connect to the vehicle first!"));
                return;
            }

            ComPort.giveComport = true;



            var home = new MissionPlanner.Utilities.Locationwp();
            try
            {
                home.id = (ushort)MAVLink.MAV_CMD.WAYPOINT;
                home.lat = waypointFile.HomeLocation.Lat;
                home.lng = waypointFile.HomeLocation.Lng;
                home.alt = ((float)waypointFile.HomeLocation.Alt / CurrentState.multiplieralt); // use saved home
            }
            catch
            {

                OnError(new Exception("Please connect to the vehicle first!"));
                return;
            }


            try
            {

                bool use_int = (ComPort.MAV.cs.capabilities & (uint)MAVLink.MAV_PROTOCOL_CAPABILITY.MISSION_INT) > 0;

                // set wp total
                ushort totalwpcountforupload = (ushort)(waypointFile.Commands.Count + 1);

                if (ComPort.MAV.apname == MAVLink.MAV_AUTOPILOT.PX4)
                {
                    totalwpcountforupload--;
                }

                ComPort.setWPTotal(totalwpcountforupload); // + home

                // upload from wp0
                var a = 0;

                if (ComPort.MAV.apname != MAVLink.MAV_AUTOPILOT.PX4)
                {
                    try
                    {
                        var homeans = ComPort.setWP(home, (ushort)a, MAVLink.MAV_FRAME.GLOBAL, 0, 1, use_int);
                        if (homeans != MAVLink.MAV_MISSION_RESULT.MAV_MISSION_ACCEPTED)
                        {
                            if (homeans != MAVLink.MAV_MISSION_RESULT.MAV_MISSION_INVALID_SEQUENCE)
                            {
                                //  CustomMessageBox.Show(Strings.ErrorRejectedByMAV, Strings.ERROR);
                                return;
                            }
                        }
                        a++;
                    }
                    catch (TimeoutException)
                    {
                        use_int = false;
                        // added here to prevent timeout errors
                        ComPort.setWPTotal(totalwpcountforupload);
                        var homeans = ComPort.setWP(home, (ushort)a, MAVLink.MAV_FRAME.GLOBAL, 0, 1, use_int);
                        if (homeans != MAVLink.MAV_MISSION_RESULT.MAV_MISSION_ACCEPTED)
                        {
                            if (homeans != MAVLink.MAV_MISSION_RESULT.MAV_MISSION_INVALID_SEQUENCE)
                            {
                                //   CustomMessageBox.Show(Strings.ErrorRejectedByMAV, Strings.ERROR);
                                return;
                            }
                        }
                        a++;
                    }
                }
                else
                {
                    use_int = false;
                }

                // define the default frame.
                MAVLink.MAV_FRAME frame = MAVLink.MAV_FRAME.GLOBAL_RELATIVE_ALT;

                // get the command list from the datagrid
                //     var commandlist = GetCommandList();

                // process commandlist to the mav
                for (a = 1; a <= waypointFile.Commands.Count; a++)
                {
                    //   var temp = waypointFile.Commands[a - 1];
                    var wp = new Locationwp();

                    wp.id = waypointFile.Commands[a - 1].Command;
                    wp.p1 = (float)waypointFile.Commands[a - 1].Param1;
                    wp.alt = (float)waypointFile.Commands[a - 1].Alt;
                    wp.lat = (float)waypointFile.Commands[a - 1].Lat;
                    wp.lng = (float)waypointFile.Commands[a - 1].Lng;
                    wp.p2 = (float)waypointFile.Commands[a - 1].Param2;
                    wp.p3 = (float)waypointFile.Commands[a - 1].Param3;
                    wp.p4 = (float)waypointFile.Commands[a - 1].Param4;
                    //((ProgressReporterDialogue)sender).UpdateProgressAndStatus(a * 100 / Commands.Rows.Count,
                    //    "Setting WP " + a);

                    // make sure we are using the correct frame for these commands
                    if (wp.id < (ushort)MAVLink.MAV_CMD.LAST || wp.id == (ushort)MAVLink.MAV_CMD.DO_SET_HOME)
                    {
                        frame = waypointFile.AltMode;
                    }

                    // handle current wp upload number
                    int uploadwpno = a;
                    if (ComPort.MAV.apname == MAVLink.MAV_AUTOPILOT.PX4)
                        uploadwpno--;

                    // try send the wp
                    MAVLink.MAV_MISSION_RESULT ans = ComPort.setWP(wp, (ushort)(uploadwpno), frame, 0, 1, use_int);

                    // we timed out while uploading wps/ command wasnt replaced/ command wasnt added
                    if (ans == MAVLink.MAV_MISSION_RESULT.MAV_MISSION_ERROR)
                    {
                        // resend for partial upload
                        ComPort.setWPPartialUpdate((ushort)(uploadwpno), totalwpcountforupload);
                        // reupload this point.
                        ans = ComPort.setWP(wp, (ushort)(uploadwpno), frame, 0, 1, use_int);
                    }

                    if (ans == MAVLink.MAV_MISSION_RESULT.MAV_MISSION_NO_SPACE)
                    {
                        //     sender.doWorkArgs.ErrorMessage = "Upload failed, please reduce the number of wp's";
                        return;
                    }
                    if (ans == MAVLink.MAV_MISSION_RESULT.MAV_MISSION_INVALID)
                    {
                        //      sender.doWorkArgs.ErrorMessage =
                        //          "Upload failed, mission was rejected byt the Mav,\n item had a bad option wp# " + a + " " +
                        //          ans;
                        return;
                    }
                    if (ans == MAVLink.MAV_MISSION_RESULT.MAV_MISSION_INVALID_SEQUENCE)
                    {
                        // invalid sequence can only occur if we failed to see a response from the apm when we sent the request.
                        // or there is io lag and we send 2 mission_items and get 2 responces, one valid, one a ack of the second send

                        // the ans is received via mission_ack, so we dont know for certain what our current request is for. as we may have lost the mission_request

                        // get requested wp no - 1;
                        a = ComPort.getRequestedWPNo() - 1;

                        continue;
                    }
                    if (ans != MAVLink.MAV_MISSION_RESULT.MAV_MISSION_ACCEPTED)
                    {
                        //sender.doWorkArgs.ErrorMessage = "Upload wps failed " + Enum.Parse(typeof(MAVLink.MAV_CMD), temp.id.ToString()) +
                        //                 " " + Enum.Parse(typeof(MAVLink.MAV_MISSION_RESULT), ans.ToString());
                        return;
                    }
                }

                ComPort.setWPACK();

            }
            catch (Exception ex)
            {
                // log.Error(ex);
                MessageBox.Show(ex.Message);
                MainV2.comPort.giveComport = false;
                throw;
            }

            MainV2.comPort.giveComport = false;
        }
        public async Task DoFlyByMapAsync(FlightRoute waypointFile, CancellationToken token)
        {
            await Task.Factory.StartNew(() =>
            {
                try
                {
                    DoFlyByMap(waypointFile, token);
                }
                catch (OperationCanceledException)
                {
                }
            }, token);
        }
        public void setStop(bool stop)
        {
            _landingStation.stop = stop;
        }
        private bool isDroneReady()
        {
            int batteryPercentage = (int)((ComPort.MAV.cs.battery_voltage - Double.Parse(_metadata.MinimumBattery)) / (Double.Parse(_metadata.MaximumBattery) - Double.Parse(_metadata.MinimumBattery)) * 100);
            bool battery = batteryPercentage >= 45;
            if (!battery)
                Core.CpxHost.Instance.UI.ShowMessage(CpxConstants.MSG_LAW_BATTERY);
            return battery;
        }
        private bool BatteryCheck()
        {
            //check if the battery percentage is higher then 45%
            int batteryPercentage = (int)((ComPort.MAV.cs.battery_voltage - Double.Parse(_metadata.MinimumBattery)) / (Double.Parse(_metadata.MaximumBattery) - Double.Parse(_metadata.MinimumBattery)) * 100);
            return (batteryPercentage >= 45);
            
        }
        private void waitForStop()
        {
            while (!continueMission) { Thread.Sleep(5); }
            continueMission = false;
            _landingStation.stopped = false;
        }
        private void FlyByMap(FlightRoute waypointFile, CancellationToken token)
        {
            isInMission = true;
            bool isRTL = false;
            if (token.IsCancellationRequested)
            {
                return;
            }
            while (!isDroneReady()) { Task.Delay(1000).Wait(); }

            var result = false;
            if (IsLandingStationDefined())
            {
                ConnectLandingStation();
                result = OpenLandingStation();
                inFlight = true;
                if (!result && _landingStation.stopped)
                {
                    waitForStop();
                }
                else if (!result)
                {
                    return;
                }

            }
            // set mode GUIDED in order to enable doArm
            while (ComPort.MAV.cs.gpshdop >= 1.4 || ComPort.MAV.cs.satcount < 9) { Thread.Sleep(5); }
            Thread.Sleep(3 * 1000);

            //TAKEOFF
            UIUtils.SetGuidedMode();
            UIUtils.Arm(false);
            //UIUtils.ConnectToVideoStream(ref tcpClient, ref stream, Regex.Replace(_metadata.MavIP, " ", ""));
            
            string p = "";
            float alt = 15;
            UIUtils.TakeOff(15);
            //if (_metadata.DayCameraPipline != "")
            //{
            //    int index = _metadata.DayCameraPipline.IndexOf("port");
            //    string port = _metadata.DayCameraPipline.Substring(index + 5, 4);
            //    CpxHost.Instance.UI.restartVideo(Int32.Parse(port), false);
                
            //}
            //else if (_metadata.NightCameraPipline != "")
            //{
            //    int index = _metadata.NightCameraPipline.IndexOf("port");
            //    string port = _metadata.NightCameraPipline.Substring(index + 5, 4);
            //    CpxHost.Instance.UI.restartVideo(Int32.Parse(port), false);
            //}
            // calculate wait delay for takeoff (supposing the speed is 2 meter/second)
            var waitForAltitude = (int)Math.Ceiling(1000 * (decimal)alt / 2);

            // wait until the altitude is reached
            Thread.Sleep(3000);

            while (ComPort.MAV.cs.alt < 5 && ComPort.MAV.cs.armed) { Thread.Sleep(5); }
            if (IsLandingStationDefined() && ComPort.MAV.cs.alt >= 5 && ComPort.MAV.cs.armed)
            {
                result = CloseLandingStation(false);

                if (!result && _landingStation.stopped)
                {
                    waitForStop();
                }
            }
            //CHECK
            else if (ComPort.MAV.cs.alt < 5 && ComPort.MAV.cs.armed)
            {
                ComPort.doARM(false);
            }
            if (!ComPort.MAV.cs.armed)
            {
                return;
            }

            //MISSION
            UIUtils.SetAutoMode();
            int param1 = 0;
            int param3 = 1;
            try
            {
                ComPort.doCommand(MAVLink.MAV_CMD.MISSION_START, param1, 0, param3, 0, 0, 0, 0);
            }
            catch (Exception e)
            {
                Core.CpxHost.Instance.UI.ShowMessage(CpxConstants.EXC_MISSION_START);
                AppSettings.WriteToLog(Resources.ExceptionMissionStart, e.Message);
            }

            //Task.Delay(1000).Wait();
            //bool landing = false;
            //// monitor the flight until the vehicle is disarmed
            //while (ComPort.MAV.cs.armed)
            //{
            //    Task.Delay(1000).Wait();
            //    if (ComPort.MAV.cs.wpno == waypointFile.Commands.Count && !landing)
            //    {
            //        UIUtils.SetGimbalDown();
            //        if (IsLandingStationDefined())
            //        {
            //            result = OpenLandingStation();
            //            if (!result && _landingStation.stopped)
            //            {
            //                waitForStop();
            //            }
            //            else if (!result)
            //            {
            //                return;
            //            }
            //        }

            //        //}
            //        landing = true;
            //        UIUtils.SetRtlMode();
            //        isRTL = true;
            //    }
            //}
            //if (isRTL)
            //{
            //    UIUtils.SetLoiterMode();
            //    if (IsLandingStationDefined())
            //    {
            //        inFlight = false;
            //        result = SafeCloseLandingStation();
            //        if (!result && _landingStation.stopped)
            //        {
            //            waitForStop();
            //        }
            //    }
            //}
            //isInMission = false;
            ////downloadLog();
        }

        public void downloadLog()
        {
            try
            {
                Process logEx = new Process();
                
                logEx.StartInfo.CreateNoWindow = true;
                logEx.StartInfo.FileName = @"C:\Program Files (x86)\Mission Planner\plugins\LogExtractor\LogExtractor.exe";
                logEx.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                logEx.Start();
            }
            catch
            {
                CpxHost.Instance.UI.ShowMessage(10, "download failed");
            }
            CpxHost.Instance.UI.ShowMessage(10,"logs download done");
        }

        public void logDownload()
        {
            try
            {
                Process logProcess = new Process();
                logProcess.StartInfo.FileName = @"C:\Program Files (x86)\Mission Planner\plugins\LogExtractor\LogExtractor.exe";
                //logProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                logProcess.StartInfo.WorkingDirectory = @"C:\Program Files (x86)\Mission Planner\plugins\LogExtractor";
                logProcess.Start();
            }
            catch
            {
                Core.CpxHost.Instance.UI.ShowMessage(15, "not success");
            }
            Core.CpxHost.Instance.UI.ShowMessage(15, "logs downloaded");
        }

    }
}
