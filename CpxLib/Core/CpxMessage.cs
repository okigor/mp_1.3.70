﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpxLib.Core
{
    public class CpxMessage
    {
        public CpxMessage(int code, string message)
        {
            Code = code;
            Message = message;
            Timeout = 0;
        }

        public CpxMessage(int code, string message, int timeout)
        {
            Code = code;
            Message = message;
            Timeout = timeout;
        }
        public int Code { get; private set; }
        public string Message { get; private set; }
        public bool Active { get; set; }
        public DateTime LastOccurence { get; set; } = DateTime.MinValue;
        public int Timeout { get; set; }
    }
}
