﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace CpxLib
{
    public class CpxPlugin :  MissionPlanner.Plugin.Plugin
    {
        public override string Name => "";

        public override string Version => "1.3.702";

        public override string Author => "Copterpix";

        public override bool Exit()
        {
            try
            {
                Process[] proc=Process.GetProcessesByName("Project1");
                for(int i=0;i<proc.Length;i++)
                    proc[i].Kill();

                proc = Process.GetProcessesByName("ArduCopter");
                for (int i = 0; i < proc.Length; i++)
                    proc[i].Kill();
                proc = Process.GetProcessesByName("LandingStation");
                for (int i = 0; i < proc.Length; i++)
                    proc[i].Kill();
            }
            catch (Exception e) { }
            Core.CpxHost.Instance.UI.Stop();
            return true;
        }

        public override bool Init()
        {
            return true;
        }

        public override bool Loaded()
        {
            // set reference to the current instance
            Core.CpxHost.Create(this);
            Core.CpxHost.Instance.UI.Run();
            return true;
        }
    }
}
